﻿/// <reference path="../../dist/npm/@salaxy/core/salaxy-core-all.d.ts" />
function deleteCalculation() {
    console.info("Running sample 3");
    salaxyApi.calculations.deleteCalc(
            $('#sample3-id').val(),
            function (data) {
                console.info(data);
                $('#sample3-result').text(data);
            });
}
