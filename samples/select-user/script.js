﻿/// <reference path="../../dist/npm/@salaxy/core/salaxy-core-all.d.ts" />
function sample8CalculateGuid() {
    console.info("Running sample 8");
    salaxyApi.calculator.workerSet(
        null, // This would be the calculation if one already exists - null generates a new one with 100€ salay
        $('#sample8-profile-id').val(),
        postSuccess);
}

function sample8CalculateExample() {
    console.info("Running sample 8");
    salaxyApi.calculator.workerSet(
        null, // This would be the calculation if one already exists - null generates a new one with 100€ salay
        $('#sample8-example-id').val(),
        postSuccess);
}

function formatAvatarForSearchResults(avatar) {
    if (avatar.loading) return avatar.text;
    var markup = '<div class="clearfix">';
    if (avatar.url != null) {
        markup += '<i class="fa-4x pull-left palkkaus-avatar fa"><img src="' + avatar.url + '" /></i>';
    } else {
        var icon = (avatar.entityType == "company" ? "fa-square" : "fa-user");
        markup += '<i class="fa-4x pull-left palkkaus-avatar fa ' + icon + '" style="color: ' + avatar.color + '"><strong>' + avatar.initials + '</strong></i>';
    }
    markup += ' <strong>' + avatar.displayName + '</strong><br /> ' + avatar.description;
    markup += "</div>";
    return markup;
}

function postSuccess(data) {
    console.info(data);
    $('#sample8-result-amount').text(data.salary.amount.toFixed(2));
    $('#sample8-result-price').text(data.salary.price.toFixed(2));
    $('#sample8-result-totalsalary').text(data.result.totals.totalGrossSalary.toFixed(2));
    $('#sample8-result-sidecosts').text(data.result.employerCalc.allSideCosts.toFixed(2));
    $('#sample8-result-totalpayment').text(data.result.employerCalc.totalPayment.toFixed(2));
    $('#sample8-result-avatar').html(formatAvatarForSearchResults(data.worker.avatar));
}