/// <reference path="../../dist/npm/@salaxy/core/salaxy-core-all.d.ts" />
var ajax = new salaxy.api.AjaxJQuery();
var calcApi = new salaxy.api.Calculator(ajax);
var reportsApi = new salaxy.api.Reports(ajax);

function sample1Calculate(callback) {
    calcApi.calculateSalary("hourly",
        $('#sample1-amount').val() || 1,
        $('#sample1-price').val() || 10,
        function(data) {
            console.info("Calculation results from server:\n", data);
            // NOTE: as-of-writing you would need to implement rounding here for this to be correct.
            // However, we are planning to round to 2 decimals inside API in the near-time future.
            $('#sample1-result-amount').text(data.salary.amount.toFixed(2));
            $('#sample1-result-price').text(data.salary.price.toFixed(2));
            $('#sample1-result-totalsalary').text(data.result.totals.totalGrossSalary.toFixed(2));
            $('#sample1-result-sidecosts').text(data.result.employerCalc.allSideCosts.toFixed(2));
            $('#sample1-result-totalpayment').text(data.result.employerCalc.totalPayment.toFixed(2));
            if (callback) {
                callback(data);
            }
        });
}

function getReport(reportPartialName) {
    sample1Calculate(function(data) {
        reportsApi.getReportHtmlWithPost(reportPartialName, data, function(reportHtml) {
            $('#report').html(reportHtml);
        });
    });
}