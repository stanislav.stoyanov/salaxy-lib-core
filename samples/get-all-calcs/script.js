﻿/// <reference path="../../dist/npm/@salaxy/core/salaxy-core-all.d.ts" />

var ajax = new salaxy.api.AjaxJQuery();
var calcCrudApi = new salaxy.api.Calculations(ajax);

function sampleDoLogin() {
    var uid = $('#loginUid').val();
    var pwd = $('#loginPwd').val();
    ajax.resourceOwnerLogin(uid, pwd, function(data) {
        alert("Login done");
    })
}

function getCalculations() {
    calcCrudApi.getAll(
        function(data) {
            $("#getAllCalcsResult > tbody > tr").remove();
            var index;
            for (index = 0; index < data.length; ++index) {
                var dataRow = data[index];
                $('#getAllCalcsResult > tbody:last')
                    .append('<tr><td>' + dataRow.id + '</td><td>' + dataRow.result.totals.totalBaseSalary + '</td><td>' + dataRow.result.totals.totalExpenses + '</td><td>' + dataRow.workflow.status + '</td></tr>');
            }
        });
}