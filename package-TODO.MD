TODO: Nämä asiat pitää tehdä, jotta päästään eroon Warningeista:
================================================================

- core-js@2.6.11
  - babel-polyfill@6.26.0 käyttää
  - Tarvitaanko tätä uudella TypeScript-versiolla?
- tslint => EsLInt
  - Myös grunt-tslint pois
- request (grunt-http)
  - grunt-http käyttää => dictionary, occupations, reportsDictionary => Voidaan poistaa kieliversioinnin yhteydessä?
  - grunt-http käyttää myös näitä depreciated työkaluja:
    - har-validator
	- hoek
	- teamwork
- fsevents
  - grunt-ts ja mocha => Tarvitaan => tsekkaa upgrade ja miksei ole päivitetty näissä
- flat
  - mocha => Tarvitaan => tsekkaa upgrade ja miksei ole päivitetty näissä
- coffee-script
  - grunt-replace: Käytössä kieliversiointiprosessissa => Poistetaan
- grunt-csv-json
  - Käytössä kieliversioinnissa => Poistetaan
  

Alkuperäisen ajon Warning-lista:
--------------------------------

PS C:\src\salaxy-lib-core> npm install
npm WARN deprecated core-js@2.6.11: core-js@<3 is no longer maintained and not recommended for usage due to the number of issues. Please, upgrade your dependencies 
to the actual version of core-js@3.
npm WARN deprecated tslint@6.1.3: TSLint has been deprecated in favor of ESLint. Please see https://github.com/palantir/tslint/issues/4534 for more information.    
npm WARN deprecated request@2.88.2: request has been deprecated, see https://github.com/request/request/issues/3142
npm WARN deprecated har-validator@5.1.5: this library is no longer supported
npm WARN deprecated hoek@6.1.3: This module has moved and is now available at @hapi/hoek. Please update your dependencies as this version is no longer maintained an may contain bugs and security issues.
npm WARN deprecated fsevents@1.2.13: fsevents 1 will break on node v14+ and could be using insecure binaries. Upgrade to fsevents 2.
npm WARN deprecated flat@4.1.0: Fixed a prototype pollution security issue in 4.1.0, please upgrade to ^4.1.1 or ^5.0.1.
npm WARN deprecated coffee-script@1.12.7: CoffeeScript on NPM has moved to "coffeescript" (no hyphen)
npm WARN deprecated teamwork@3.2.0: This module has moved and is now available at @hapi/teamwork. Please update your dependencies as this version is no longer maintained an may contain bugs and security issues.

> core-js@2.6.11 postinstall C:\src\salaxy-lib-core\node_modules\core-js
> node -e "try{require('./postinstall')}catch(e){}"

Thank you for using core-js ( https://github.com/zloirock/core-js ) for polyfilling JavaScript standard library!

The project needs your help! Please consider supporting of core-js on Open Collective or Patreon: 
> https://opencollective.com/core-js
> https://www.patreon.com/zloirock

Also, the author of core-js ( https://github.com/zloirock ) is looking for a good job -)

npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@^1.2.7 (node_modules\chokidar\node_modules\fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.2.13: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})  
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@~2.1.2 (node_modules\mocha\node_modules\chokidar\node_modules\fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@2.1.3: wanted {"os":"darwin","arch":"any"} (current: {"os":"win32","arch":"x64"})   
npm WARN grunt-csv-json@0.1.0 requires a peer of grunt@~0.4.0 but none is installed. You must install peer dependencies yourself.
npm WARN grunt-tslint@5.0.2 requires a peer of tslint@^5.0.0 || ^5.0.0-dev || ^5.1.0-dev || ^5.2.0-dev || ^5.3.0-dev but none is installed. You must install peer dependencies yourself.

added 699 packages from 568 contributors in 92.256s

35 packages are looking for funding
  run `npm fund` for details