General plain JavaScript / TypeScript libraries with Node.js -ajax component
----------------------------------------------------------------------------

Plain JavaScript / TypeScript stack logic for Salaxy with Node.js -ajax component.

Usage:

- Run `npm install --save @salaxy/node@latest`
- Reference or copy TypeScript files from `node_modules/@salaxy/node`

| File                   | Description                                       |
| ---------------------- | ------------------------------------------------- |
| index.js               | ES6 module compilation                            |

For more information and documentation about Salaxy platform, please go to https://developers.salaxy.com
