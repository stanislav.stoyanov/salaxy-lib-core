General plain JavaScript / TypeScript libraries with JQuery -ajax component
---------------------------------------------------------------------------

Plain JavaScript / TypeScript stack logic for Salaxy with JQuery -ajax component. 

Usage:

- Run `npm install --save @salaxy/jquery@latest`
- Reference or copy TypeScript files from `node_modules/@salaxy/jquery`

| File                                   | Description                                                                |
| ---------------------------------------| -------------------------------------------------------------------------- |
| index.js                               | ES6 module compilation                                                     |
| salaxy-lib-jquery.js                   | Browser friendly (browserify) compilation                                  | 
| salaxy-lib-jquery.min.js               | Browser friendly (browserify) compilation  (min)                           | 
| salaxy-lib-jquery-all.js               | Browser friendly (browserify) compilation with dependencies bundle         | 
| salaxy-lib-jquery-all.min.js           | Browser friendly (browserify) compilation  with dependencies bundle (min)  | 

For more information and documentation about Salaxy platform, please go to https://developers.salaxy.com

Please note that the salaxy-lib-jquery-all.js includes the following libraries:
- @salaxy/core (https://www.npmjs.com/package/@salaxy/core)
- moment.js (https://www.npmjs.com/package/moment)
- babel-polyfill (https://www.npmjs.com/package/babel-polyfill)