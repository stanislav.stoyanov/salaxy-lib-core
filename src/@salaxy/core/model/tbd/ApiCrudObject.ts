/** Base interface for CRUD objects in the Salaxy API. */
export interface ApiCrudObject {
  /** Identifier of the object. */
  id?: string | null;

  /** The date when the object was created. */
  createdAt?: string | null;
  /** The time when the object was last updated. Typically this should be a logical update by user (UserUpdatedAt in DTO), not technical updates. */
  updatedAt?: string | null;
  /** Owner ID for this data */
  owner?: string | null;
  /** Indication that for the currently logged-in account, the data is generally read-only. */
  isReadOnly?: boolean | null;
  /** Partner service that has be used in modifying the object. */
  partner?: string | null;
}
