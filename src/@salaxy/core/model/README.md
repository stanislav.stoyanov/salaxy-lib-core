Instructions on how to update entities using NSwag
--------------------------------------------------

Interfaces in module salaxy.model are generated from Salaxy API.
To update interfaces follow the steps:

- Install and start NSwagStudio ( <https://github.com/NSwag/NSwag/wiki/NSwagStudio> )
- Open either v02.nswag, nir.nswag or oauth2.nswag to get the Swagger docs either from http://localhost:82 or https://test-api.salaxy.com
- Click **Create local Copy** to re-import the Swagger JSON
- Check the path in **Output file path** in TypeScript Client tab (bottom).
  - Note especially `G:\` vs `C:\`.
- Click **Generate Files** to update `v01.ts`, `nir.ts` or `oauth2.ts`
- Add new enums in GetApiEnumTypes()
  - They are at the end of ts-file with very long enum names
- Do full rebuild for TypeScript: `grunt default` - observe any errors

NSwagStudio settings
====================

Use these settings in NSwagStudio in case you cannot open the nswag-file or you need to do the thing manually for some other reason.

- Download Salaxy API json from <https://test-api.salaxy.com/swagger/docs/v01> or <https://test-api.salaxy.com/swagger/docs/oauth2>
- Paste the downloaded API json to *Swagger Specification JSON* field
- Mark *TypeScript Client* **checked**
- Select *TypeScript Client* tab and select *Settings*
- Leave *Module name* and *Namespace* field as empty
- Select `2.7` in *TypeScript Version* field
- Mark *Inline named dictionaries* **unchecked**
- Mark *Inline named any shcmeas* **unchecked**
- Mark *Generate Client Classes* **unchecked**
- Mark *Generate DTO interfaces* **checked**
- Select `Interface` in *Type Style* field
- Select `Null` in *Null value used in object initializers* field
- Select `String` in *Date Time Type* field
- Mark *Generate default values for properties* **checked**
- Mark *Mark optional properties with ?* **checked**
- Mark *Generate clone() method* **unchecked**
- Mark *Import required types* **unchecked**


- Leave all other fields blank
- Click *Generate Outputs* button and copy the code
- Paste the generated code into *v01.ts* or *oauth2.ts* file
