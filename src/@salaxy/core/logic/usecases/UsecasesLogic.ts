import * as momentImport from "moment";
/** workaround */
const moment = momentImport;
import { householdUsecaseTree } from "../../codegen";
import {
  Calculation,
  CalculationRowType,
  FrameworkAgreement,
  TaxDeductionWorkCategories,
  TesSubtype,
} from "../../model";
import { Dates } from "../../util";
import { CalculatorLogic } from "../CalculatorLogic";
import { Translations } from "../Translations";
import { Usecase } from "./Usecase";
import { UsecaseChildCare } from "./UsecaseChildCare";
import { UsecaseCleaning } from "./UsecaseCleaning";
import { UsecaseConstruction } from "./UsecaseConstruction";
import { UsecaseGroup } from "./UsecaseGroup";

/**
 * Provides business logic for Calculation usecases:
 * Salary framework contracts ("työehtosopimus") and other salary recommendations
 * that are used as bases for salary calculation.
 */
export class UsecasesLogic {

  /**
   * Known use cases in the first implementation.
   * These implementations replace the olf Calc.Framework.Type -based solution that is not very extensible.
   * The naming is still based on the old system for easier compatibility.
   */
  public static readonly knownUseCases = {

    /** Usecase has not been defined. Note that value is null, not "https://secure.salaxy.com/v02/api/usecases/notDefined" */
    notDefined: null,

    /** Generic child care usecase */
    childCare: "https://secure.salaxy.com/v02/api/usecases/childCare",

    /** Cleaning */
    cleaning: "https://secure.salaxy.com/v02/api/usecases/cleaning",

    /** Construction (Raksa) */
    construction: "https://secure.salaxy.com/v02/api/usecases/construction",

    /** Child care according to MLL recommendations. */
    mll: "https://secure.salaxy.com/v02/api/usecases/mll",

    /** NOT IMPLEMENTED: Santa Claus for Christmas campaigns - not implemented for this release. */
    santaClaus: "https://secure.salaxy.com/v02/api/usecases/santaClaus",

    /** Generic household salary payment usecase user interface. */
    other: "https://secure.salaxy.com/v02/api/usecases/other",
  };

  /**
   * Finds a parent group of a given usecase node
   * @param role - Role for which the use cases are filtered.
   * Currently, only "household" is supported, but other roles will follow.
   * @param usecase - The usecase whose parent is fetched.
   * If you pass a group here, the function will return null which will typically mean the tree root in the user interface.
   * @example
   * // Creates a simple calculation
   * const usecase = UsecasesLogic.findUsecaseById("household", "childCare/mll");
   * UsecasesLogic.setUsecase(this.context.calculations.current, usecase);
   * this.context.calculations.recalculateCurrent((calc) => {
   *     calc.worker.paymentData.telephone = "+358401234567";
   * });
   */
  public static findUsecaseById(role: "household", usecaseId): Usecase {
    for (const group of UsecasesLogic.getUsecaseTree("household")) {
      for (const usecase of group.children || []) {
        if (usecase.id === usecaseId) {
          return usecase;
        }
      }
    }
    return null;
  }

  /**
   * Finds a group based on group id or usecase id of a child.
   * @param role - Role for which the use cases are filtered.
   * Currently, only "household" is supported, but other roles will follow.
   * @param useCaseOrGroupId - Identifier of the group or usecase.
   * If id is a usecase id, its parent is returned.
   * @example
   * // Returns a parent of a usecase or null if the selectedItem is a group.
   * const group = UsecasesLogic.findGroupById("household", this.selectedItem.id);
   * if (group.id === this.selectedItem.id) {
   *     return null;
   * }
   * return group;
   */
  public static findGroupById(role: "household", useCaseOrGroupId: string) {
    for (const group of UsecasesLogic.getUsecaseTree("household")) {
      if (group.id === useCaseOrGroupId) {
        return group;
      }
      for (const usecase of group.children || []) {
        if (usecase.id === useCaseOrGroupId) {
          return group;
        }
      }
    }
    return null;
  }

  /**
   * Gets a tree of usecases (user interfaces for defining a framework) for a given role.
   * @param role - Role for which the use cases are filtered.
   * Currently, only "household" is supported, but other roles will follow.
   */
  public static getUsecaseTree(role: "household"): UsecaseGroup[] {
    const tree: UsecaseGroup[] = householdUsecaseTree;
    for (const group of tree) {
      this.translate("HouseHoldUseCaseTree", group.id, group);
      for (const usecase of group.children) {
        usecase.badge = usecase.badge || group.badge;
        usecase.icon = usecase.icon || group.icon;
        usecase.uri = usecase.uri || group.uri;
        this.translate("HouseHoldUseCaseTree", usecase.id, usecase);

      }
    }
    return tree;
  }

  /**
   * Abstract the getting of the use case for the calculation.
   * We are expecting changes to the usecase implementation.
   * @param calc Calculation into which the usecase data is stored.
   */
  public static getUsecaseData(calc: Calculation): Usecase {
    if (!calc.usecase || !calc.usecase.uri) {
      const usecase = UsecasesLogic.getUsecaseBackwardCompatibility(calc);
      calc.usecase = {
        uri: usecase.uri,
        label: usecase.label,
        description: usecase.descr,
        data: usecase,
      };
    }
    return calc.usecase.data as Usecase;
  }

  /**
   * Sets calculation properties based on selected usecase.
   * @param calc Calculation to set
   * @param usecase Usecase to apply.
   * @example
   * // Creates a simple calculation
   * const usecase = UsecasesLogic.findUsecaseById("household", "childCare/mll");
   * UsecasesLogic.setUsecase(this.calc, usecase);
   * this.calcApi.recalculate(this.calc).then((data) => {
   *   angular.copy(data, this.calc);
   * });
   */
  public static setUsecase(calc: Calculation, usecase: Usecase): Usecase {
    calc.usecase = {
      uri: usecase.uri,
      label: usecase.label,
      description: usecase.descr,
      data: usecase,
    };
    this.applyUseCase(calc);
    return this.getUsecaseData(calc);
  }

  /**
   * Applies the usecase to a calculation, especially to the old Framework object.
   * Should be called before sending the calculation to the server (recalculate/save).
   * @param calc Calculation where usecase is set to calc.data.usecase.
   */
  public static applyUseCase(calc: Calculation) {
    const usecase = this.getUsecaseData(calc);
    switch (usecase.uri) {
      case UsecasesLogic.knownUseCases.childCare:
        this.applyUsecaseChildCare(calc);
        break;
      case UsecasesLogic.knownUseCases.mll:
        this.applyUsecaseMll(calc);
        break;
      case UsecasesLogic.knownUseCases.cleaning:
        this.applyUsecaseCleaning(calc);
        break;
      case UsecasesLogic.knownUseCases.construction:
        this.applyUsecaseConstruction(calc);
        break;
      case UsecasesLogic.knownUseCases.santaClaus:
        this.applyUsecaseSantaClaus(calc);
        break;
      case UsecasesLogic.knownUseCases.other:
        this.applyCommonUsecaseProperties(calc);
        break;
      case UsecasesLogic.knownUseCases.notDefined:
        // Nothing applied.
        break;
    }
  }

  /** Applies the constructions salary calculation usecase */
  public static applyUsecaseConstruction(calc: Calculation) {
    const usecase = this.getUsecaseData(calc) as UsecaseConstruction;
    const mainRow = CalculatorLogic.getSalaryRow(calc);
    if (mainRow.rowType === CalculationRowType.HourlySalary) {
      // Potentially add custom row message here
    } else if (mainRow.rowType === CalculationRowType.MonthlySalary) {
      // Potentially add custom row message here
    } else {
      mainRow.rowType = CalculationRowType.Salary;
    }
    usecase.taxDeductionCategories = TaxDeductionWorkCategories.HomeImprovement;
    usecase.isTesIncludedInSalary = usecase.isTesIncludedInSalary || false;
    usecase.isExpensesCustom = usecase.isExpensesCustom || false;
    usecase.subType = usecase.subType as any || TesSubtype.ConstructionCarpenter;

    // HACK: Additional rows are handled on the server-side at the moment.
    //       This should be changed to the new model before going to production.
    const fx = calc.framework;
    fx.type = FrameworkAgreement.Construction;
    fx.subType = usecase.subType as any;
    if (usecase.isExpensesCustom) {
      fx.subType = TesSubtype.ConstructionFreeContract;
    }
    fx.isTesIncludedInSalary = usecase.isTesIncludedInSalary;
    fx.dailyExpenses = usecase.dailyExpenses;
    fx.dailyTravelExpenses = usecase.dailyTravelExpenses;
    fx.dailyTravelExpensesKm = usecase.dailyTravelExpensesKm;
    usecase.taxDeductionCategories = TaxDeductionWorkCategories.HomeImprovement;

    this.applyCommonUsecaseProperties(calc);
  }

  /** Applies the childCare salary calculation usecase */
  public static applyUsecaseChildCare(calc: Calculation) {
    const usecase = this.getUsecaseData(calc) as UsecaseChildCare;
    const mainRow = CalculatorLogic.getSalaryRow(calc);
    // Salary settings
    if (mainRow.rowType === CalculationRowType.HourlySalary) {
      mainRow.message = "Lastenhoitajan tuntipalkka";
    } else {
      mainRow.rowType = CalculationRowType.MonthlySalary;
      mainRow.message = "Lastenhoitajan kuukausipalkka";
    }
    // Child care subsidy and tax deduction
    calc.rows = calc.rows
      .filter((x) => x.rowType !== CalculationRowType.ChildCareSubsidy);
    if (usecase.isChildcareSubsidy) {
      calc.rows.push({
        count: 1,
        price: usecase.subsidyAmount,
        rowType: CalculationRowType.ChildCareSubsidy,
      });
    } else {
      usecase.subsidyAmount = null;
    }
    if (usecase.isChildcareSubsidy) {
      usecase.isHouseholdDeductible = false;
      usecase.taxDeductionCategories = null;
    }
    if (usecase.isHouseholdDeductible == null) {
      usecase.isHouseholdDeductible = true;
    }
    usecase.taxDeductionCategories = usecase.isHouseholdDeductible ? TaxDeductionWorkCategories.Carework : null;
    this.applyCommonUsecaseProperties(calc);
  }

  /** Applies the cleaning salary calculation usecase */
  public static applyUsecaseCleaning(calc: Calculation) {
    const usecase = this.getUsecaseData(calc) as UsecaseCleaning;
    const mainRow = CalculatorLogic.getSalaryRow(calc);
    if (mainRow.rowType === CalculationRowType.HourlySalary) {
      // Potentially add custom row message here
    } else if (mainRow.rowType === CalculationRowType.MonthlySalary) {
      // Potentially add custom row message here
    } else {
      mainRow.rowType = CalculationRowType.Salary;
    }
    calc.rows = calc.rows
      .filter((x) => x.rowType !== CalculationRowType.HolidayCompensation);
    if (!usecase.isFullTime) {
      const salary = mainRow.count * mainRow.price;
      const percent = usecase.isContractLessThanYear ? 0.09 : 0.115;
      calc.rows.push({
        count: percent,
        price: salary,
        rowType: CalculationRowType.HolidayCompensation,
      });
    }
    if (usecase.isHouseholdDeductible == null) {
      usecase.isHouseholdDeductible = true;
    }
    usecase.taxDeductionCategories = TaxDeductionWorkCategories.Householdwork;
    this.applyCommonUsecaseProperties(calc);
  }

  /** Applies the MLL salary calculation usecase */
  public static applyUsecaseMll(calc: Calculation) {
    const usecase = this.getUsecaseData(calc) as UsecaseChildCare;
    const mainRow = CalculatorLogic.getSalaryRow(calc, CalculationRowType.HourlySalary);
    mainRow.rowType = CalculationRowType.HourlySalary;
    mainRow.message = "Lastenhoitajan tuntipalkka";
    if (!mainRow.count || mainRow.count < 2) {
      mainRow.count = 2;
    }
    if (!usecase.useCustomPrice) {
      mainRow.price = 9.50;
    }
    calc.rows = calc.rows
      .filter((x) => x.rowType !== CalculationRowType.SundayWork);
    if (usecase.isSunday) {
      calc.rows.push({
        count: 1,
        price: mainRow.count * mainRow.price,
        rowType: CalculationRowType.SundayWork,
      });
    }

    // Deductions
    calc.framework.isYksityisenHoidonTuki = false;
    usecase.isHouseholdDeductible = true;
    usecase.taxDeductionCategories = TaxDeductionWorkCategories.Carework;

    // Defaults to Period
    if (!calc.info.workStartDate) {
      calc.info.workStartDate = Dates.getToday();
      calc.info.workEndDate = null; // workEndDate is no longer valid
      calc.framework.numberOfDays = null;
    }
    if (!calc.info.workEndDate) {
      calc.info.workEndDate = calc.info.workStartDate;
      calc.framework.numberOfDays = null;
    }
    if (!calc.framework.numberOfDays) {
      calc.framework.numberOfDays = Dates.getWorkdays(
        calc.info.workStartDate,
        calc.info.workEndDate,
      ).length;
    }
    this.applyCommonUsecaseProperties(calc);
  }

  private static applyUsecaseSantaClaus(calc: Calculation) {
    const usecase = this.getUsecaseData(calc);
    const mainRow = CalculatorLogic.getSalaryRow(calc, CalculationRowType.Salary);
    mainRow.rowType = CalculationRowType.Salary;
    mainRow.message = "Joulupukin palkka";
    usecase.isHouseholdDeductible = false;
    usecase.taxDeductionCategories = null;
    const year = moment().subtract("month", 6).year();
    const nearestXmsEve = Dates.asDate(moment([year, 11, 24]));
    calc.info.workStartDate = nearestXmsEve;
    calc.info.workEndDate = nearestXmsEve;
    calc.framework.numberOfDays = 1;
    this.applyCommonUsecaseProperties(calc);
  }

  private static applyCommonUsecaseProperties(calc: Calculation) {
    const usecase = this.getUsecaseData(calc);
    const mainRow = CalculatorLogic.getSalaryRow(calc);
    calc.info.occupationCode = usecase.occupation;
    calc.salary.isHouseholdDeductible = usecase.isHouseholdDeductible || false;
    calc.salary.taxDeductionCategories = usecase.taxDeductionCategories;
    if (mainRow.rowType === CalculationRowType.HourlySalary) {
      // For hourly salary, default to null so that users really sets the value.
      if (mainRow.count === 0) {
        mainRow.count = null;
      }
    } else {
      // We currently do not support multiple months etc. in these user interfaces.
      // API does support them, but they would add unnecessary complexity to UX.
      mainRow.count = 1;
    }
    mainRow.rowType = mainRow.rowType || CalculationRowType.Salary;
  }

  /**
   * Gets a usecase for calculations that were created before the
   * new user interface and usecase model was stored on the server.
   * @param calc Original calculation.
   */
  private static getUsecaseBackwardCompatibility(calc: Calculation) {
    const mainRow = CalculatorLogic.getSalaryRow(calc);
    switch (calc.framework.type) {
      case FrameworkAgreement.ChildCare:
        if (calc.framework.isYksityisenHoidonTuki) {
          return UsecasesLogic.findUsecaseById("household", "childCare/kela");
        }
        return UsecasesLogic.findUsecaseById("household", "childCare/other");
      case FrameworkAgreement.Mll: {
        const usecase = UsecasesLogic.findUsecaseById("household", "childCare/mll") as UsecaseChildCare;
        if (mainRow.price === 19.00) {
          usecase.isSunday = true;
          usecase.useCustomPrice = false;
        } else if (mainRow.price === 9.50) {
          usecase.isSunday = false;
          usecase.useCustomPrice = false;
        } else {
          usecase.isSunday = false;
          usecase.useCustomPrice = true;
        }
        return usecase;
      }
      case FrameworkAgreement.Cleaning:
        return UsecasesLogic.findUsecaseById("household", "cleaning/91110");
      case FrameworkAgreement.Construction: {
        let usecase: UsecaseConstruction;
        switch (calc.framework.subType) {
          case TesSubtype.ConstructionCarpenter:
            usecase = UsecasesLogic.findUsecaseById("household", "construction/carpenter") as UsecaseConstruction;
            break;
          case TesSubtype.ConstructionFloor:
            usecase = UsecasesLogic.findUsecaseById("household", "construction/floor") as UsecaseConstruction;
            break;
          case TesSubtype.ConstructionFreeContract:
            usecase = UsecasesLogic.findUsecaseById("household", "construction/carpenter") as UsecaseConstruction;
            usecase.isExpensesCustom = true;
            break;
          case TesSubtype.ConstructionOther:
          case TesSubtype.NotSelected:
            usecase = UsecasesLogic.findUsecaseById("household", "construction/other") as UsecaseConstruction;
            break;
        }
        usecase.dailyExpenses = calc.framework.dailyExpenses;
        usecase.dailyTravelExpenses = calc.framework.dailyTravelExpenses;
        usecase.dailyTravelExpensesKm = calc.framework.dailyTravelExpensesKm;
        usecase.isHouseholdDeductible = calc.salary.isHouseholdDeductible;
        usecase.taxDeductionCategories = calc.salary.taxDeductionCategories;
        usecase.isTesIncludedInSalary = calc.framework.isTesIncludedInSalary;
        return usecase;
      }
      case FrameworkAgreement.Entrepreneur:
        throw new Error("Yrittäjän palkkaa ei voi maksaa kotitalouspalvelussa.");
      // return UsecasesLogic.findUsecaseById("company", "enterpreneur/other");
      case FrameworkAgreement.SantaClaus:
        throw new Error("Joulupukin palkkiota ei voi maksaa tähän aikaan vuodesta.");
      // return UsecasesLogic.findUsecaseById("household", "campaign/santaClaus");
      case FrameworkAgreement.NotDefined:
      case FrameworkAgreement.Other:
      default:
        return UsecasesLogic.findUsecaseById("household", "other/undefined");
    }
  }

  private static translate(name: string, id: string, useCaseOrGroup: Usecase | UsecaseGroup) {
    useCaseOrGroup.label = Translations.get("SALAXY.ENUM." + name + "." + id.replace(/\//g, ".") + ".label") || useCaseOrGroup.label;
    useCaseOrGroup.descr = Translations.get("SALAXY.ENUM." + name + "." + id.replace(/\//g, ".") + ".description") || useCaseOrGroup.descr;
  }
}
