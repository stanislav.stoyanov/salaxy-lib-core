import { Usecase } from "./Usecase";
/** Defines the usecase data for childCare including the MLL childCare */
export interface UsecaseChildCare extends Usecase {

    /**
     * If true, the work is done on Sunday or public holiday.
     * This means that the salary is increased by 100% according to Finnish law
     */
    isSunday: boolean;

    /**
     * If true, a custom price is used instead of the recommended price.
     * Currently, this only affects the MLL case as the normal childCare price is always set by the user.
     */
    useCustomPrice: boolean;

    /** If true, the salary calculation includes child care subsidy paid by the government / community. */
    isChildcareSubsidy?: boolean;

    /** Amount for the child care subsidy paid by the government / community. */
    subsidyAmount: number;
}
