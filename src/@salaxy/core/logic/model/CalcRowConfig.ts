import { CalculationRowUnit } from "../../model";
import { CalculationRowCategory } from "./CalculationRowCategory";
import { EnumMetadata } from "./EnumMetadata";

/**
 * Configuration for user interface of a Calculation row.
 */
export interface CalcRowConfig {

  /** Name / unique identifier of the row type. */
  name: string;

  /** Order number in the lists */
  order: number;

  /** Label for the row type */
  label?: string;

  /** Description for the row type */
  descr?: string;

  /** More info HTML */
  moreInfo?: string;

  /** Categorization of the row types. */
  category: CalculationRowCategory;

  /**
   * Favorite row types and their sort order.
   * This is currently fixed, but may later be based on user profile (e.g. most used types).
   */
  favorite?: number | null;

  /** Color is based on the category */
  color: string;

  /** Icon short text is 1-5 characters suitable for icon.
   * Has 5 characters only if very narrow, e.g. "1" and comma - typically 2-3 chars.
   */
  iconText?: string;

  /** If the row has a kind selection (the main subtype), definition of the subtypes. */
  kind?: EnumMetadata;

  /** Price input */
  price: {
    /** Label for the input */
    label?: string;
    /** Default value */
    default?: number;
    /** Type of the input control to use */
    input?: "number" | "hidden" | "readOnly";
  };
  /** Amount input */
  amount: {
    /** Label for the input */
    label?: string;
    /** Default value */
    default?: number;

    /** Type of the input control to use */
    input?: "number" | "hidden" | "readOnly";

    /** Code for the unit. */
    unit?: CalculationRowUnit;
  };
  /** The total input or read-only field. */
  total: {
    /** Type of the input control to use */
    input?: "number" | "hidden" | "readOnly";
  };
}
