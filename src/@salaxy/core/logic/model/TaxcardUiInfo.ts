import { TaxcardKind } from "../../model";

/**
 * Display information and language versioned texts that make sense to
 * end-users about the status of taxcard.
 */
export interface TaxcardUiInfo {

  /**
   * Status as in
   * "ok": Valid taxcard,
   * "missing": No taxcard, employer should add a new one.,
   * "waiting": A shared taxcard is waiting for approval. Existing may be valid or not.
   * or null: No calculation or employment relation specified.
   */
  status: "ok" | "waiting" | "missing" | null;

  /**
   * Kind that is quaranteed to be non-null (undefined). This is about the existing taxcard,
   * there may still be a new card "waiting" and that may have another kind specified.
   */
  kind: TaxcardKind;

  /** Descriptive text to the end user containing the percent if relevant. */
  text: string;

  /** Shorter version of the descriptive text to the end user. */
  shortText: string;

  /**
   * The tax percent if it is relevant for the taxcard.
   * Number is 0-60, not decimal e.g. 0.60. Null if not relevant.
   */
  percent: number;
}
