/**
 * Base inteface for Products.
 * The common properties for all Products: BaseSalaryProduct, TaxProduct, UnemploymentProduct etc.
 */
export interface Product {
    /** Identifier for the product */
    readonly id?: string | null;

    /** The main short title for the product / service */
    readonly title?: string | null;

    /** The status text that should give the user. Restrict the length to 56 characters. */
    readonly status?: string | null;

    /** One paragraph description text */
    readonly description?: string | null;

    /** The logo image for the product. */
    readonly img?: string | null;

    /** Identifier for the main product desription article in the Palkkaus.fi CMS */
    readonly articleId?: string | null;

    /** If true, the product is enabled for the current user */
    enabled?: boolean | null;

    /**
     * If false, it is not bossible to enable this product for the current account / environment.
     * Typically, this means that the UI should not show this product.
     * E.g. Raksaloki is available for households only and this is a Company account =&gt; Should not show the product.
     */
    visible?: boolean | null;
}
