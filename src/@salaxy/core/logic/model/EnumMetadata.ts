import { EnumValueMetadata } from "./EnumValueMetadata";

/** Describes an enumeration for user interface purposes. */
export interface EnumMetadata {
  /** Type name of the enumeration (UpperCamelCase) */
  name: string;
  /** Description of the enumeration if the translations have been applied. */
  descr?: string;
  /** Values of the enumeration. */
  values: EnumValueMetadata[];
}
