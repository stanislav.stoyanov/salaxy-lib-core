﻿import { CalculationRowType } from "../../model/v01";

/** Categorization / grouping for CalculationRowType */
export class CalculationRowCategories {

  /** Base salary types: the ones that you define directly */
  public static baseSalary = [
    CalculationRowType.Salary,
    CalculationRowType.HourlySalary,
    CalculationRowType.MonthlySalary,
    CalculationRowType.TotalWorkerPayment,
    CalculationRowType.TotalEmployerPayment,
    CalculationRowType.Compensation,
    // CalculationRowType.IrIncomeType,
    CalculationRowType.Board,
  ];

  /** Additions to base salary - typically a percentage of base salary */
  public static salaryAdditions = [
    CalculationRowType.Overtime,
    CalculationRowType.TesWorktimeShortening,
    CalculationRowType.EveningAddition,
    CalculationRowType.NightimeAddition,
    CalculationRowType.SaturdayAddition,
    CalculationRowType.SundayWork,
    CalculationRowType.HolidayCompensation,
    CalculationRowType.HolidayBonus,
    CalculationRowType.HolidaySalary,
    CalculationRowType.OtherAdditions,
    // CalculationRowType.Remuneration,
    // CalculationRowType.OtherCompensation,
    // CalculationRowType.WorkingTimeCompensation,
    // CalculationRowType.EmploymentTermination,
  ];

  /** All salary types: baseSalary and salaryAdditions together */
  public static salary = CalculationRowCategories.baseSalary.concat(CalculationRowCategories.salaryAdditions);

  /** Benefits: Not paid as money but taxable income */
  public static benefits = [
    CalculationRowType.AccomodationBenefit,
    CalculationRowType.MealBenefit,
    CalculationRowType.PhoneBenefit,
    CalculationRowType.OtherBenefit,
  ];

  /** Expenses and other non-taxed compensations */
  public static expenses = [
    CalculationRowType.Expenses,
    CalculationRowType.MilageOwnCar,
    CalculationRowType.DailyAllowance,
    CalculationRowType.DailyAllowanceHalf,
    CalculationRowType.MealCompensation,
    CalculationRowType.ToolCompensation,
    CalculationRowType.MilageDaily,
    CalculationRowType.MilageOther,
    CalculationRowType.ChildCareSubsidy,
  ];

  /** Deductions */
  public static deductions = [
    CalculationRowType.Advance,
    CalculationRowType.Foreclosure,
    CalculationRowType.OtherDeductions,
    CalculationRowType.PrepaidExpenses,
    CalculationRowType.UnionPayment,
  ];

  /** Holidays */
  public static holidays = [
    CalculationRowType.HolidayCompensation,
    CalculationRowType.HolidayBonus,
    CalculationRowType.HolidaySalary,
    // CalculationRowType.HolidayDays
  ];

  /**
   * Items in the section called "expenses". Besides expenses, contains also benefits.
   */
  public static expensesSection = CalculationRowCategories.expenses.concat(CalculationRowCategories.benefits, CalculationRowCategories.deductions);
}
