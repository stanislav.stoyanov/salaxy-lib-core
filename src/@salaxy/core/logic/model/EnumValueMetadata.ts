/** Describes a single item (value) within an enum type */
export interface EnumValueMetadata {
  /** Text value of the enumeration (lowerCamelCase) */
  name: string;
  /** Sorting order number of the value within the enum. */
  order: number;

  /** Label of the enumeration if the translations have been applied. */
  label?: string;

  /**
   * Description of the enumeration if the translations have been applied.
   * NOTE: Not all enumerations have description in the globalization files:
   * They are added only if necessary to the UI.
   */
  descr?: string | null;

}
