import {
  AbsenceCauseCode,
  AnnualLeavePayment,
  AnnualLeavePaymentKind,
  Avatar,
  CalculationRowType,
  EmploymentRelationType,
  HolidayBonusPaymentMethod,
  HolidayCode,
  HolidaySpecification,
  HolidayYear,
  LegalEntityType,
  WorkerAccountEmployment,
} from "../model";
import { Arrays, Dates } from "../util";

/** Logic for Holiday years and other holiday functionality. */
export class HolidaysLogic {

  /**
   * Gets the multiplier that is used in Average Daily Salary calculation (keskipäiväpalkka).
   * @param holidays Number of holidays for the year.
   */
  public static getDailySalaryMultiplier(holidays: number) {
    const multipliers = [0, 0, 1.8, 2.7, 3.6, 4.5, 5.4, 6.3, 7.2, 8.1, 9.0, 9.9, 10.8, 11.8, 12.7, 13.6, 14.5, 15.5, 16.4, 17.4, 18.3, 19.3, 20.3, 21.3, 22.2, 23.2, 24.1, 25.0, 25.9, 26.9, 27.8];
    if (holidays > 30) {
      return multipliers[30] + (holidays - 30) * 0.9;
    }
    return multipliers[holidays];
  }

  /**
   * Gets a new blank holiday year.
   * @param year Holiday accrual year (e.g. 2019) or 0 for current year and -1 for previous year (from -10 to 10).
   * Current year is always from 1.4. until 31.3. e.g. year 2019 is 1.4.2019 until 31.3.2020.
   * The main part of the holidays from holiday accrual year 2019 are kept on summer 2020.
   */
  public static getBlank(year: number = 0): HolidayYear {
    if (year < 11) {
      const currentYear = Dates.getMonth(Dates.getToday()) < 4 ? (Dates.getTodayMoment().year() - 1) : Dates.getTodayMoment().year();
      year = currentYear + year;
    }
    const result: HolidayYear = {
      employmentId: null,
      year,
      workerId: null,
      period: {
        start: `{year}-04-01`,
        end: `{year + 1}-03-31`,
      },
      accrual: {
        months: [],
      },
      leaves: {
        paid: [],
        planned: [],
      },
      spec: {
        code: HolidayCode.Undefined,
      },
    };
    result.period.daysCount = Dates.getDuration(result.period.start, result.period.end).asDays();
    return result;
  }

  /** Gets the salary payable type or default type if both are possible. */
  public static getSalaryPayableType(absenseCauseCode: AbsenceCauseCode): "defaultPaid" | "defaultUnpaid" | "both" | "paid" | "unpaid" {
    const defaults = {
      unpaidLeave: "unpaid",
      personalReason: "defaultUnpaid",
      illness: "defaultPaid",
      partTimeSickLeave: "defaultUnpaid",
      parentalLeave: "defaultUnpaid",
      specialMaternityLeave: "defaultUnpaid",
      rehabilitation: "defaultUnpaid",
      childIllness: "defaultPaid",
      partTimeChildCareLeave: "defaultUnpaid",
      training: "defaultPaid",
      jobAlternationLeave: "unpaid",
      studyLeave: "unpaid",
      industrialAction: "unpaid",
      interruptionInWorkProvision: "unpaid",
      leaveOfAbsence: "defaultUnpaid",
      militaryRefresherTraining: "defaultUnpaid",
      militaryService: "unpaid",
      layOff: "unpaid",
      childCareLeave: "unpaid",
      midWeekHoliday: "paid",
      accruedHoliday: "paid",
      occupationalAccident: "paid",
      annualLeave: "paid",
      partTimeAbsenceDueToRehabilitation: "paid",
      other: "both",
    };
    return defaults[absenseCauseCode as string];
  }

  /** Gets different total days calculation of planned leaves. */
  public static getPlannedLeavesCount(holidayYear: HolidayYear, type: "all" | "summer" | "winter" | "holidaysSaldoEnd" | "holidaysSaldoStart" = "all"): number {
    // TODO: Split period if it starts summer and ands winter.
    const year = holidayYear.year;
    const list = holidayYear.leaves.planned;
    switch (type) {
      case "all":
        return Arrays.sum(list, (x) => x.period.daysCount);
      case "summer":
        return Arrays.sum(list.filter((x) => x.period.start < `${year}-10-1`), (x) => x.period.daysCount);
      case "winter":
        return Arrays.sum(list.filter((x) => x.period.start >= `${year}-10-1`), (x) => x.period.daysCount);
      case "holidaysSaldoStart":
        return Arrays.sum(holidayYear.accrual.months, (x) => x.daysAccrued) + (holidayYear.accrual as any).startSaldo;
      case "holidaysSaldoEnd":
        return HolidaysLogic.getPlannedLeavesCount(holidayYear, "holidaysSaldoStart") - HolidaysLogic.getPlannedLeavesCount(holidayYear, "all");
    }
    return null;
  }

  /** Gets a calculated total or saldo row for paid calculations */
  public static getPaidHolidaysCalculation(
    holidayYear: HolidayYear,
    type: "startSaldo" | "total" | "endSaldo" = "total",
    dailyHolidaySalary = null, // TODO: Consider moving to HolidayYear
  ): AnnualLeavePayment {
    switch (type) {
      case "total":
        return {
          avatar: HolidaysLogic.getPaidCalculationAvatar("total", null),
          holidayBonus: Arrays.sum(holidayYear.leaves.paid, (x) => x.holidayBonus),
          holidayCompensation: Arrays.sum(holidayYear.leaves.paid, (x) => x.holidayCompensation),
          holidayDays: Arrays.sum(holidayYear.leaves.paid, (x) => x.holidayDays),
          holidaySalary: Arrays.sum(holidayYear.leaves.paid, (x) => x.holidaySalary),
        } as any;
      case "startSaldo":
        const holidaySalary = dailyHolidaySalary ? dailyHolidaySalary * holidayYear.accrual.endSaldo : null;
        return {
          avatar: HolidaysLogic.getPaidCalculationAvatar("startSaldo", null),
          holidayBonus: (holidaySalary && holidayYear.spec.bonus) ? (holidaySalary * holidayYear.spec.bonus) : null,
          holidayDays: holidayYear.accrual.endSaldo,
          holidayCompensation: null,
          holidaySalary,
        } as any;
      case "endSaldo":
        const total = HolidaysLogic.getPaidHolidaysCalculation(holidayYear, "total", dailyHolidaySalary);
        const startSaldo = HolidaysLogic.getPaidHolidaysCalculation(holidayYear, "startSaldo", dailyHolidaySalary);
        return {
          avatar: HolidaysLogic.getPaidCalculationAvatar("endSaldo", null),
          holidayBonus: startSaldo.holidayBonus ? startSaldo.holidayBonus - total.holidayBonus : null,
          holidayCompensation: startSaldo.holidaySalary ? startSaldo.holidaySalary - (total.holidaySalary + total.holidayCompensation) : null,
          holidayDays: startSaldo.holidayDays - total.holidayDays,
          holidaySalary: null,
        } as any;
    }
    return null;
  }

  /** Gets a display avatar for a paid calculation */
  public static getPaidCalculationAvatar(
    type: "row" | "startSaldo" | "total" | "endSaldo" = "row",
    row: AnnualLeavePayment,
  ): Avatar {
    switch (type) {
      case "row":
        return {
          displayName: row.kind === AnnualLeavePaymentKind.DraftCalc ? `Luonnos ${Dates.getFormattedDate(row.date)}` : `Maksettu ${Dates.getFormattedDate(row.date)}`,
          description: `Palkkakausi ${Dates.getFormattedRange(row.period.start, row.period.end)}`,
          color: "rgb(74, 146, 233)",
          initials: row.kind === "draftCalc" ? "luonnos" : "maksettu",
          entityType: LegalEntityType.Company,
        };
      case "startSaldo":
        return {
          displayName: "Käytettävissä olevat",
          description: "Edellisenä vuonna kertyneet lomat",
          color: "gray",
          initials: "saldo",
          entityType: LegalEntityType.Company,
        };
      case "total":
        return {
          displayName: "Yhteensä",
          description: "Käytetyt/maksetut lomapäivät, -rahat ja korvaukset",
          color: "rgb(74, 146, 233)",
          initials: "=",
          entityType: LegalEntityType.Company,
        };
      case "endSaldo":
        return {
          displayName: "Maksettavaa tai siirrettävää",
          description: "Seuraavalle lomavuodelle tai maksettavaa",
          color: "gray",
          initials: "saldo",
          entityType: LegalEntityType.Company,
        };
    }
  }

  /**
   * Gets the visualisation data for the a holiday year.
   * @param holidayYear The holiday year to visualize
   */
  public static getYearVisualisation(holidayYear: HolidayYear) {
    const year = holidayYear.year;
    const todayYear = Dates.getYear("today");
    const todayMonthIndex = Dates.getMonth("today") - 4;
    const currentMonthIx = todayYear < year ? todayMonthIndex : (todayYear > year ? todayMonthIndex + 24 : todayMonthIndex + 12);
    return {
      year,
      employmentId: holidayYear.employmentId,
      currentMonthIx,
      years: [
        { text: year - 1, months: 9, style: "default" },
        { text: year, months: 12, style: "primary" },
        { text: year + 1, months: 4, style: "default" },
      ],
      months: [4, 5, 6, 7, 8, 9, 10, 11, 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 1, 2, 3, 4],
      items: [
        { text: `Lomapäivien kertymä: ${(holidayYear.accrual as any).endSaldo} pv`, months: 12, start: 0, style: "default", view: "accrual" },
        { text: `Kesälomat: ${HolidaysLogic.getPlannedLeavesCount(holidayYear, "summer")} pv`, months: 5, start: 13, style: "default", view: "holidays-summer" },
        { text: `Talvilomat: ${HolidaysLogic.getPlannedLeavesCount(holidayYear, "winter")} pv`, months: 7, start: 18, style: "default", view: "holidays-winter" },
        { text: "Lomapalkat ja -rahat", months: 12, start: 13, style: "default", view: "paidHolidays" },
      ],
    };
  }
}
