import { Avatar, CompanyAccount, ContractParty, ContractPartyType, EmploymentContract, EmploymentSalaryType, EmploymentType, EmploymentWorkHoursType, LegalEntityType, PersonAccount } from "../model";
import { Dates } from "../util";
import { Translations } from "./Translations";

import * as momentImport from "moment";
/** moment variable */
const moment = momentImport;

/**
 * Provides business logic related to displaying / editing an employment contract.
 */
export class ContractLogic {

  /**
   * Creates a new contract for the current account.
   * The current account is set as Employer or Worker in this contract.
   * @param isWorker If true, current account is in role Worker.
   * If false, the current account is Employer.
   * @param account The current account object (Person or Company).
   */
  public static getBlankForCurrentAccount(isWorker: boolean, account: PersonAccount | CompanyAccount) {
    const result = this.getBlank();
    if (isWorker) {
      result.worker = ContractLogic.getContractParty(account);
    } else {
      result.employer = ContractLogic.getContractParty(account);
    }
    return result;
  }

  /**
   * Gets a new blank object with default values, suitable for UI binding.
   */
  public static getBlank(): EmploymentContract {
    const blank: EmploymentContract = {
      employer: {
        avatar: {},
        contact: {},
      },
      worker: {
        avatar: {},
        contact: {},
      },
      isWorkAtHome: true,
      salary: {},
      createdAt: Dates.getToday(),
      updatedAt: Dates.getToday(),
    };
    return blank;
  }

  /**
   * Gets the Avatar of other party in a contract:
   * Employer or Worker
   */
  public static getOtherParty(contract: EmploymentContract, currentAvatar: Avatar): Avatar {
    if (currentAvatar) {
      const id = currentAvatar.id;
      if (contract.worker && contract.worker.id !== id) {
        return contract.worker.avatar;
      } else if (contract.employer && contract.employer.id !== id) {
        return contract.employer.avatar;
      }
    }
    return {
      id: null,
      displayName: "?",
      sortableName: "?",
      initials: "?",
      firstName: "n",
      lastName: "n",
      entityType: LegalEntityType.Undefined,
    };
  }

  /** Gets a contract party from an Account object: Person or Company */
  public static getContractParty(person: PersonAccount | CompanyAccount): ContractParty {
    return {
      avatar: person.avatar,
      isSelf: true,
      contact: person.contact,
      contractPartyType: ContractPartyType.Person,
      iban: person.identity.ibanNumber,
      officialId: person.identity.officialId,
    };
  }

  /** Constructs text for price per (such as 15€/h or 2000€/kk) */
  public static constructPricePerText(formattedPrice, salaryType) {
    if (salaryType === EmploymentSalaryType.HourlyPay) {
      return formattedPrice + Translations.get("SALAXY.ENUM.EmploymentSalaryType.hourlyPay.metricAbbrev");
    } else if (EmploymentSalaryType.MontlyPay) {
      return formattedPrice + Translations.get("SALAXY.ENUM.EmploymentSalaryType.montlyPay.metricAbbrev");
    } else {
      return formattedPrice;
    }
  }

  /** Constructs text for term date */
  public static constructTermDateText(workType, startDate, endDate) {
    if (startDate && workType === EmploymentType.FullTime) {
      return moment(startDate).format("DD.MM.YYYY");
    } else if (startDate && workType !== EmploymentType.FullTime) {
      return moment(startDate).format("DD.MM.YYYY") + " - " + moment(endDate).format("DD.MM.YYYY");
    }
    return Translations.get("SALAXY.UI_TERMS.dateNotChosen");
  }

  /** Constructs text salary work hours */
  public static constructSalaryWorkHoursText(worksHoursType, workHours) {
    if (workHours) {
      switch (worksHoursType) {
        case EmploymentWorkHoursType.HoursPerDay: return workHours + " " + Translations.get("SALAXY.ENUM.EmploymentWorkHoursType.hoursPerWeek.label");
        case EmploymentWorkHoursType.HoursPerWeek: return workHours + " " + Translations.get("SALAXY.ENUM.EmploymentWorkHoursType.hoursPerDay.label");
        case EmploymentWorkHoursType.HourPerTwoWeeks: return workHours + " " + Translations.get("SALAXY.ENUM.EmploymentWorkHoursType.hourPerTwoWeeks.label");
        case EmploymentWorkHoursType.HourPerThreeWeeks: return workHours + " " +  Translations.get("SALAXY.ENUM.EmploymentWorkHoursType.hourPerThreeWeeks.label");
      }
    }
    return workHours;
  }

  /** get full address as two lines or display a human-readable default value
   * @param target Any object that has an contact property
   */
  public static constructAddressText(party: ContractParty) {
    if (party.contact && party.contact.street && party.contact.postalCode && party.contact.city) {
      return `${party.contact.street}<br />${party.contact.postalCode} ${party.contact.city.toUpperCase()}`;
    }
    return null;
  }
}
