import { vismaSignOriginalMethods } from "../codegen/index";

/**
 * Business logic relating to Onboarding process and Digital signature.
 */
export class OnboardingLogic {

  /** Gets the authentication methods for digital signature. */
  public static getTupasMethods() {

    const cdn = "https://cdn.salaxy.com/img";

    const imageOverrides = [
      "op",
      "nordea",
      "danske",
      "spankki",
      "handelsbanken",
      "mobiilivarmenne",
      "aktia",
      "pop",
      "sp",
      "omasp",
      "alandsbanken",
    ];

    const result = vismaSignOriginalMethods.map((v, i) => {
      return {
        id: v.identifier,
        title: v.name,
        img: imageOverrides[i] ? `${cdn}/salaxy/sign/${imageOverrides[i]}.png` : v.image,
        isPopular: i < 3,
      };
    });
    return result;
  }

}
