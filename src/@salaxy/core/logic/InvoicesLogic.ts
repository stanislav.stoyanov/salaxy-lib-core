import { Avatar, AvatarPictureType, Calculation, LegalEntityType, PaymentChannel, PayrollDetails } from "../model";

/** Common logic for invoices */
export class InvoicesLogic {
  /**
   * Custom type guard that determines whether the payment object is a payroll (based on whether it calcs collection).
   * @param paymentObject Payment object that is either PayrollDetails or Calculation OR does not have calcs / employer properites that is used to determine the type.
   */
  public static isPayroll(paymentObject: Calculation | PayrollDetails | any): paymentObject is PayrollDetails {
    return paymentObject && (paymentObject as PayrollDetails).calcs !== undefined;
  }

  /**
   * Custom type guard that determines whether the payment object is a calculation (based on whether it employer property).
   * @param paymentObject Payment object that is either PayrollDetails or Calculation OR does not have calcs / employer properites that is used to determine the type.
   */
  public static isCalculation(paymentObject: Calculation | PayrollDetails | any): paymentObject is Calculation {
    return paymentObject && (paymentObject as Calculation).employer !== undefined;
  }

  /** Gets and avatar image for a payment channel */
  public static getChannelAvatar(channel: PaymentChannel) {
    return this.avatars[channel] || this.avatars[PaymentChannel.Undefined];
  }

  private static avatars: { [key: string]: Avatar } = {
    [PaymentChannel.AccountorGo]: { url: "https://cdn.salaxy.com/ng1/img/paymentmethod/accountor-go.png", pictureType: AvatarPictureType.Uploaded, entityType: LegalEntityType.Company },
    [PaymentChannel.FinagoSolo]: { url: "https://cdn.salaxy.com/ng1/img/paymentmethod/finago-solo.png", pictureType: AvatarPictureType.Uploaded, entityType: LegalEntityType.Company },
    [PaymentChannel.ZeroPayment]: { color: "#60DF96", initials: "0€", entityType: LegalEntityType.Company },
    [PaymentChannel.HolviCfa]: { color: "#064545", initials: "Holvi", entityType: LegalEntityType.Company },
    [PaymentChannel.PalkkausCfaFinvoice]: { color: "#60DF96", initials: "FIN", entityType: LegalEntityType.Company },
    [PaymentChannel.PalkkausCfaPaytrail]: { color: "#e60094", initials: "P", entityType: LegalEntityType.Company },
    [PaymentChannel.PalkkausCfaReference]: { color: "#60DF96", initials: "Ref", entityType: LegalEntityType.Company },
    [PaymentChannel.PalkkausManual]: { color: "#005f81", initials: "Man", entityType: LegalEntityType.Company },
    [PaymentChannel.PalkkausWS]: { color: "#005f81", initials: "WS", entityType: LegalEntityType.Company },
    [PaymentChannel.TalenomOnline]: { color: "#553379", initials: "T", entityType: LegalEntityType.Company },
    [PaymentChannel.Test]: { color: "orange", initials: "Test", entityType: LegalEntityType.Company },
    [PaymentChannel.Undefined]: { color: "gray", initials: "?", entityType: LegalEntityType.Company },
  };
}
