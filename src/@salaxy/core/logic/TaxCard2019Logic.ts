import { Calculation, Taxcard, TaxcardKind } from "../model";
import { Brand, Dates, Numeric } from "../util";
import { CalculatorLogic } from "./CalculatorLogic";
import { EnumerationsLogic } from "./EnumerationsLogic";
import { TaxcardUiInfo } from "./model";

/**
 * Provides business logic related to TaxCards:
 * The Finnish way of determining the widtholding tax that Employer should deduct from Worker salary.
 * Taxcard handling was changed by tax authorities significantly for year 2019, hence the naming.
 */
export class TaxCard2019Logic {

  /**
   * Gets a new blank taxcard with default values, suitable for UI binding.
   *
   * @param personalId The personal ID that is used to connect the Taxcard to a person. This is required in save.
   * @param kind If specified, sets the type and defaults according to the type
   * @param year Specifies the year. This is used in December-January when there may be cards from both years.
   * Current year is the default.
   * @param today Allows changing the today date in unit testing etc. Default is actual today.
   */
  public static getBlank(personalId: string, kind: TaxcardKind = null, year?: number, today?: string): Taxcard {
    today = today || Dates.getToday();
    year = year || Dates.getTodayMoment().year();
    const newCard: Taxcard = {
      worker: null,
      card: {
        personalId,
        forYear: year,
        taxPercent: null,
        incomeLimit: null,
        taxPercent2: null,
        kind: TaxcardKind.Undefined as any,
        fileUri: null,
        previewUri: null,
        validity: {},
      },
      incomeLog: [],
      id: null,
      createdAt: today,
      updatedAt: today,
      isReadOnly: null,
    };
    if (kind) {
      newCard.card.kind = kind;
      switch (kind) {
        case TaxcardKind.Undefined:
          newCard.card.taxPercent = 23;
          break;
        case TaxcardKind.NoWithholdingHousehold:
          newCard.card.taxPercent = 0;
          newCard.card.validity.start = today;
          break;
        case TaxcardKind.NoTaxCard:
          newCard.card.taxPercent = 60;
          newCard.card.validity.start = today;
          break;
        case TaxcardKind.DefaultYearly:
          newCard.card.validity.start = year + "-02-01";
          newCard.card.validity.end = year + "-12-31";
          break;
        case TaxcardKind.Replacement:
        case TaxcardKind.Others:
        case TaxcardKind.Historical:
          newCard.card.validity.start = Dates.getYear(today) < year ? year + "-01-01"
            : (Dates.getYear(today) > year ? year + "-12-31" : today);
          newCard.card.validity.end = year + "-12-31";
          break;
      }
    }
    return newCard;
  }

  /**
   * Gets display information and texts for taxcard within a calculation.
   * Helper for showing taxcards UI's for calculations.
   * NOTE: The data is cached to the calculation ($ui) until round-trip to server.
   * @param calc Calculation for which the data is resolved.
   */
  public static getCalcTaxcardInfo(calc: Calculation): TaxcardUiInfo {
    const result: TaxcardUiInfo = {
      status: null,
      kind: TaxcardKind.Undefined,
      text: null as string,
      shortText: null as string,
      percent: null,
    };
    if (!calc) {
      return result;
    }
    const $ui = CalculatorLogic.getUiCache(calc);
    if ($ui.tax) {
      return $ui.tax;
    }
    if (!calc.worker || !calc.worker.tax) {
      $ui.tax = result;
      return result;
    }
    const tax = calc.worker.tax;
    result.kind = (tax.snapshot ? tax.snapshot.kind : null) || TaxcardKind.Undefined;
    result.status = tax.hasNewSharedCard ? "waiting" :
      (result.kind === TaxcardKind.Undefined ? "missing" : "ok");

    // TODO: Translation
    if (result.status === "waiting") {
      result.text = "Tarkista uusi verokortti";
      result.shortText = "Uusi verokortti";
    } else {
      switch (result.kind) {
        case TaxcardKind.Undefined:
          result.text = EnumerationsLogic.getEnumLabel("TaxcardKind", result.kind);
          result.shortText = "Ei verokorttia";
          break;
        case TaxcardKind.NoTaxCard:
          result.text = EnumerationsLogic.getEnumLabel("TaxcardKind", result.kind);
          result.shortText = result.text;
          result.percent = 60;
          break;
        case TaxcardKind.NoWithholdingHousehold:
          result.text = EnumerationsLogic.getEnumLabel("TaxcardKind", result.kind);
          result.shortText = result.text;
          result.percent = 0;
          break;
        default:
          result.percent = calc.worker.tax.snapshot.taxPercent;
          if (calc.result.totals.totalTaxable > 0) {
            result.percent = (calc.worker.tax.estimatedTax / calc.result.totals.totalTaxable) * 100;
          }
          result.text = EnumerationsLogic.getEnumLabel("TaxcardKind", result.kind) + ": " + Numeric.formatPercent(result.percent);
          result.shortText = "Ennakonpidätys" + ": " + Numeric.formatPercent(result.percent);
          break;
      }
    }
    $ui.tax = result;
    return result;
  }

  /**
   * Checks if the taxcard is valid for the salary date.
   * This is used when in test environment the salaryDate may be changed in UX.
   * Typically, you should get this information from the API methods.
   * @param card Taxcard to check.
   * @param salaryDate Salary date. Default is today.
   */
  public static isTaxcardValid(card: Taxcard, salaryDate: string = "today"): boolean {
    if (!card || !card.card || !card.card.validity) {
      return false;
    }
    salaryDate = Dates.asDate(salaryDate) || Dates.getToday();
    if (!(card.card.validity.start > salaryDate) && !(card.card.validity.end < salaryDate)) {
      return true;
    }
    if (Dates.getMonth(salaryDate) === 1 && card.card.forYear === Dates.getYear(salaryDate) - 1) {
      return true;
    }
    return false;
  }

  /**
   * TODO: This is currently used for charting only. As such it could probably be simplified
   * and / or combined with getMainChart() / getPieChart().
   * Gets information about the the income limit and real percent.
   * Note that this method calculates the real percent i.e.
   * for examples and no tax cards 60%. If you wish to see the example percent here (23%),
   * catch it separately.
   * @param taxcard Card to analyze
   */
  public static getIncomeLimitInfo(taxcard: Taxcard) {
    const result = {
      type: TaxcardKind.Undefined,
      percent: 60,
      hasLimit: false,
      fractionOfLimit: 0,
    };
    if (!taxcard || !taxcard.card || !taxcard.card.kind) {
      return result;
    }

    const card = taxcard.card;
    result.percent = card.taxPercent === 0 ? 0 : card.taxPercent || 60;
    result.type = card.kind;

    switch (result.type) {
      // May or may not have limits
      case TaxcardKind.DefaultYearly:
      case TaxcardKind.Replacement:
      case TaxcardKind.Others:
        result.hasLimit = !!(card.incomeLimit && card.taxPercent2);
        if (result.hasLimit) {
          result.fractionOfLimit = taxcard.result.totalIncome / card.incomeLimit;
          if (taxcard.result.totalIncome > card.incomeLimit && card.taxPercent2) {
            result.percent = card.taxPercent2;
          }
        }
        return result;
      // No limit
      case TaxcardKind.NoTaxCard:
      case TaxcardKind.NoWithholdingHousehold:
      case TaxcardKind.Undefined:
      default:
        return result;
    }
  }

  /** Gets the main chart for taxcards */
  public static getMainChart(taxcard: Taxcard) {

    const limits = TaxCard2019Logic.getIncomeLimitInfo(taxcard);
    const taxCardIncomeLimit = taxcard.card.incomeLimit;
    const totalIncome = taxcard.result.totalIncome;
    const taxPercent = taxcard.card.taxPercent;
    const taxPercent2 = taxcard.card.taxPercent2;

    let incomeSum = 0;
    let incomes = taxcard.incomeLog.sort((x, y) => x.paidAt < y.paidAt ? -1 : (x.paidAt > y.paidAt ? 1 : 0)).map((value: any) => {
      incomeSum += value.income;
      return { x: value.paidAt, y: incomeSum };
    });
    if (incomes.length === 0) {
      incomes = [
        { x: taxcard.card.validity.start, y: 0 },
      ];
    }
    const validity = [
      { x: taxcard.card.validity.start, y: 0 },
      { x: taxcard.card.validity.end, y: taxcard.card.incomeLimit },
    ];

    const colorPrimary = Brand.getBrandColor("primary", "rgba");
    const colorDanger = Brand.getBrandColor("danger", "rgba");
    const colorWarning = Brand.getBrandColor("warning", "rgba");
    const result = {
      limits,
      data: [
        incomes,
        validity,
      ],
      labels: null,
      datasets: [
        {
          label: "Palkanmaksu",
          fill: false,
          index: 2,
          lineTension: 0,
          backgroundColor: colorPrimary,
          pointBackgroundColor: colorPrimary,
          pointHoverBackgroundColor: colorPrimary,
          borderColor: colorPrimary,
          pointBorderColor: colorPrimary,
          pointHoverBorderColor: colorPrimary,
        },
        {
          label: "Tuloraja",
          backgroundColor: colorWarning,
          pointBackgroundColor: colorWarning,
          pointHoverBackgroundColor: colorWarning,
          borderColor: colorWarning,
          pointBorderColor: colorWarning,
          pointHoverBorderColor: colorWarning,
          fill: false,
          borderDash: [5, 5],
          lineTension: 0,
        },
      ],
      options: {
        maintainAspectRatio: false,
        legend: {
          display: true,
          position: "bottom",
        },
        title: {
          display: true,
          text: "Maksetut palkat ja vuosikohtainen tuloraja",
        },
        scales: {
          yAxes: [{
            display: true,
            ticks: {
              suggestedMax: taxCardIncomeLimit * 1.5,    // minimum will be 0, unless there is a lower value.
              // OR //
              beginAtZero: true,   // minimum value will be 0.
            },
          }],
          xAxes: [{
            type: "time",
            time: {
              displayFormats: {
                millisecond: "D.M.YYYY",
                second: "D.M.YYYY",
                minute: "D.M.YYYY",
                hour: "D.M.YYYY",
                day: "D.M.YYYY",
                week: "D.M.YYYY",
                month: "D.M.YYYY",
                year: "D.M.YYYY",
              },
              minUnit: "day",
              tooltipFormat: "D.M.YYYY",
            },

          }],
        },
        annotation: {
          annotations: [{
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: taxCardIncomeLimit,
            borderColor: colorDanger,
            borderWidth: 2,
            label: {
              position: "left",
              enabled: true,
              content: taxCardIncomeLimit + " €",
              fontColor: "#fff",
              fontSize: 11,
              backgroundColor: colorDanger,
              yAdjust: 0,
              xAdjust: 60,
            },
          },
          {
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: taxCardIncomeLimit,
            borderColor: "transparent",
            borderWidth: 0,
            label: {
              position: "left",
              enabled: true,
              content: taxPercent2 + " %",
              fontSize: 16,
              fontColor: colorDanger,
              backgroundColor: "transparent",
              yAdjust: -11,
              xAdjust: 5,
            },
          },
          {
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: taxCardIncomeLimit,
            borderColor: "transparent",
            borderWidth: 0,
            label: {
              position: "left",
              enabled: true,
              content: taxPercent + " %",
              fontSize: 16,
              fontColor: colorWarning,
              backgroundColor: "transparent",
              yAdjust: 15,
              xAdjust: 5,
            },
          },
          {
            type: "line",
            mode: "horizontal",
            scaleID: "y-axis-0",
            value: 0,
            borderColor: "transparent",
            borderWidth: 0,
            label: {
              position: "right",
              enabled: true,
              content: `31.12.${taxcard.card.forYear}: ${Numeric.round(totalIncome, 0)} €`,
              fontSize: 12,
              fontColor: colorPrimary,
              backgroundColor: "#fff",
              yAdjust: -30,
              xAdjust: 10,
            },
          }],
        },
        tooltips: {
          backgroundColor: "rgba(255,255,255,1)",
          titleFontColor: "#000",
          bodyFontColor: "#000",
          borderColor: "#000",
          displayColors: false,
          intersect: false,
          enabled: true,
          mode: "single",
          callbacks: {
            label: (tooltipItem, data) => {
              let label = data.datasets[tooltipItem.datasetIndex].label || "";

              if (label) {
                label += " " + tooltipItem.xLabel + ": ";
              }
              label += Math.round(tooltipItem.yLabel * 100) / 100;
              return label;
            },
          },
        },
        events: ["mousemove", "click"],
      },
    };
    return result;
  }

  /** Gets the pie chart for the taxcards. This is used for simplistic cases that do not have an income limit. */
  public static getPieChartData(taxcard: Taxcard) {
    const limits = TaxCard2019Logic.getIncomeLimitInfo(taxcard);
    const netSalary = 100 - limits.percent;
    const label1Suffix = limits.fractionOfLimit < 1 ? "perusprosentti" : "lisäprosentti";
    const getColorPalette = (color: string) => {
      return {
        backgroundColor: color,
        pointBackgroundColor: color,
        pointHoverBackgroundColor: color,
        borderColor: color,
        pointBorderColor: color,
        pointHoverBorderColor: color,
      };
    };
    const result = {
      limits,
      data: [
        limits.percent,
        100 - limits.percent,
      ],
      datasets: [
        {
          label: ["Test", "test 2"],
        },
      ],
      labels: [
        `Ennakonpidätys (${label1Suffix}) ${Numeric.formatPercent(limits.percent)}`,
        `Käteen jäävä osuus ${Numeric.formatPercent(netSalary)}`,
      ],
      colors: [
        getColorPalette(limits.type === TaxcardKind.NoTaxCard || limits.type === TaxcardKind.NoWithholdingHousehold
          ? Brand.getBrandColor("danger", "rgba")
          : Brand.getBrandColor("warning", "rgba")),
        getColorPalette(Brand.getBrandColor("primary", "rgba")),
      ],
      options: {
        legend: {
          display: true,
          position: "bottom",
          fullWidth: true,
          reverse: true,
        },
        tooltips: {
          enabled: true,
          mode: "single",
          bodySpacing: 6,
          callbacks: {
            label: (tooltipItem, data) => {
              return data.labels[tooltipItem.index];
            },
          },
        },
        events: ["mousemove", "click"],
      },
    };
    return result;
  }

}
