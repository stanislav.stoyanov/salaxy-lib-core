import { accountProductDefaults } from "../codegen";
import {
    AccountingProduct,
    AccountProducts,
    BaseSalaryProduct,
    HealthCareHelttiProduct,
    InsuranceProduct,
    PensionEntrepreneurProduct,
    PensionProduct,
    TaxProduct,
    UnemploymentProduct,
} from "../model";
import { Product } from "./model/index";
import { Translations } from "./Translations";

/**
 * Framework independendent business logic methods and helpers for Products.
 */
export class ProductsLogic {

    /** Gets the default products for a given role */
    public static getDefault(role: "household" | "company" | "worker") {
        const result: AccountProducts = this.getAccountProductDefaults();

        if (role === "household" || role === "company") {
            if (role === "household") {
                result.pensionEntrepreneur = null;
            }
        } else if (role === "worker") {
            result.accounting = null;
            result.baseService = null;
            result.healthCareHeltti = null;
            result.insurance = null;
            result.pension = null;
            result.tax = null;
            result.unemployment = null;
        }
        return result;
    }

    /**
     * Gets a product based on id.
     * @param accountProducts The account products object containing all the products.
     * @param id Product id to fetch for - this is the name of the property key in accountProducts and id property of the value.
     * @param defaultValue Default value to return if accountProducts or id is falsy or id is not found or object returned by id is falsy.
     */
    public static getProductById(accountProducts: AccountProducts, id: string, defaultValue: any = null): Product {
        if (!accountProducts || !id) {
            return defaultValue;
        }
        if (!accountProducts[id]) {
            accountProducts[id] = {};
        }
        const prod = accountProducts[id];
        return prod || defaultValue;
    }

    /**
     * Gets a product of a given type.
     * @param typeName Name of the type (the asme as generic typ T).
     */
    public static getProductByType<T extends Product>(
        accountProducts: AccountProducts, typeName: string, defaultValue: any = null): T {
        switch (typeName) {
            case "HealthCareHelttiProduct":
                return ProductsLogic.getProductById(accountProducts, "healthCareHeltti", defaultValue) as HealthCareHelttiProduct as T;
            case "AccountingProduct":
                return ProductsLogic.getProductById(accountProducts, "accounting", defaultValue) as AccountingProduct as T;
            case "BaseSalaryProduct":
                return ProductsLogic.getProductById(accountProducts, "baseService", defaultValue) as BaseSalaryProduct as T;
            case "InsuranceProduct":
                return ProductsLogic.getProductById(accountProducts, "insurance", defaultValue) as InsuranceProduct as T;
            case "PensionProduct":
                return ProductsLogic.getProductById(accountProducts, "pension", defaultValue) as PensionProduct as T;
            case "TaxProduct":
                return ProductsLogic.getProductById(accountProducts, "tax", defaultValue) as TaxProduct as T;
            case "UnemploymentProduct":
                return ProductsLogic.getProductById(accountProducts, "unemployment", defaultValue) as UnemploymentProduct as T;
            case "PensionEntrepreneurProduct":
                return ProductsLogic.getProductById(accountProducts, "pensionEntrepreneur", defaultValue) as PensionEntrepreneurProduct as T;
            default:
                throw new Error("Non-supported product type: " + typeName);
        }
    }

    /** Gets all the products for anonymous user (everything visible) */
    public static getAllProducts(): AccountProducts {
        return this.getAccountProductDefaults();
    }

    private static getAccountProductDefaults(): AccountProducts {
        const result: AccountProducts = accountProductDefaults;

        for (const key in result) {
            if (result.hasOwnProperty(key)) {
                const product = result[key];
                product.title = Translations.get("SALAXY.ENUM.Product." + key + ".label");
                product.description = Translations.get("SALAXY.ENUM.Product." + key + ".description");
                product.status = Translations.get("SALAXY.ENUM.Product." + key + ".status");
            }
        }
        return result;
    }
}
