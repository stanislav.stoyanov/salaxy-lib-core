import { DataRow } from "./DataRow";
import { Map } from "./Map";
import { Mapper } from "./Mapper";

/** Utility class for creating mappers */
export class Mappers {
  /** Returns a new path only mapper. Field names are built from the paths (separated by dots or slashes) using underscores between the fields.  */
  public static pathMapper<TObject, TRow extends DataRow>(paths: string[], defaultObject: TObject = null): Mapper<TObject, TRow> {
    const map = {} as Map<TObject, TRow>;
    paths.forEach((path) => {
      const dotPath = this.getDotPath(path);
      map[this.getFriendlyFieldName(dotPath)] = dotPath;
    });

    return new Mapper<TObject, TRow>(map, defaultObject);
  }

  private static getDotPath(path: string): string {
    return path.replace(/\//g, ".");
  }
  private static getFriendlyFieldName(path: string): string {
    const fields = path.split(".");
    const field = fields[fields.length - 1];
    return field;
  }
}
