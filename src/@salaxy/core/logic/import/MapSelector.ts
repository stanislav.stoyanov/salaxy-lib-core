import {DataRow} from "./DataRow";

/**
 * Selector that selects an object from hierarchical data source to linear export row
 * and typically also defines the mapping to other direction.
 */
export interface MapSelector<TObject, TRow extends DataRow> {

  /**
   * Defines source property as a simple object path if possible.
   * If a more complex logic is needed, you can define get and set functions below.
   * The property is also used in generating documentation.
   */
  path: string;

  /**
   * Function that defines how the row value(s) is derived from the object, typically in export.
   * If null, this is defined by path.
   */
  fromObject?: (source: TObject, row: TRow) => void;

  /**
   * Function that sets the value from row back to object, typically in import.
   * If null, this is defined by path.
   */
  fromRow?: (row: TRow, object: TObject) => void;

  /** Title of the property. If null, path is used with reflection. */
  title?: string;

  /** Description of the property. If null, path is used with reflection. */
  descr?: string;

  /** Type of the property. If null, path is used with reflection. */
  type?: string;

}
