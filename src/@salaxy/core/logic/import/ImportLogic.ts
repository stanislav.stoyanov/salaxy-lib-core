import { ApiValidationErrorType } from "../../model";
import { DataRow } from "./DataRow";
import { ImportResult } from "./ImportResult";

/** Methods for importing data into Salaxy system. */
export class ImportLogic {

  /**
   * Converts a tab-separated values text to raw JSON.
   * Used to converting data copy-pasted from Excel to JSON.
   * @param inputText The TSV string that should be imported.
   * @returns ImportResult object with the data and other realted information.
   */
  public static tsvToJson(inputText: string): ImportResult<DataRow> {
    const result: ImportResult<DataRow> = {
      ignoredLines: 0,
      data: [],
      headers: [],
      errors: [],
    };
    if (!inputText) {
      return result;
    }
    const inputLines = inputText.split(/\r\n|\n/);
    if (inputLines.length === 0) {
      return result;
    }
    result.headers = inputLines[0].split(/\t/);
    for (let i = 1; i < inputLines.length; i++) {
      const data = inputLines[i].split(/\t/);
      const row: DataRow = {};
      let isEmptyLine = true;
      for (let j = 0; j < result.headers.length; j++) {
        if (j < data.length) {
          row[result.headers[j]] = data[j];
          if (data[j]) {
            isEmptyLine = false;
          }
        }
      }
      if (isEmptyLine) {
        result.ignoredLines++;
      } else {
        if (data.length !== result.headers.length) {
          row.validation = {
            isValid: false,
            errors: [ {
            key: null,
            type: ApiValidationErrorType.General,
            code : "InvalidColAmount",
            msg : `PARSE ERROR: Row ${i} has ${data.length} columns whereas the header row has ${result.headers.length}.`,
            }],
          };
        }
        result.data.push(row);
      }
    }
    result.errors = result.data.filter((x) => x.validation && x.validation.errors).map( (x) => x.validation.errors).reduce( (all, x) => all.concat(x), []);
    return result;
  }

}
