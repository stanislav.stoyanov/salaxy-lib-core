import { TaxcardKind } from "../../../model";
import {DataRow} from "../DataRow";

/**
 * Data row for taxcards data. This is typically used for imports and exports.
 */
export interface TaxcardsDataRow extends DataRow  {

  /** Id for the taxcard */
  id?: string | null;

  /** The personal ID as written in the tax card. */
  personalId: string;

  /** Year that the tax card is valid for. Typically this means from February of this year to January of the following. */
  forYear: number;

  /** Start date of the period */
  start?: string | null;

  /** End date of the period. */
  end?: string | null;

  /** Tax percent as percent. I.e. for 50% set 50, not 0.5. */
  taxPercent?: number | null;

  /** Income up to which the TaxPercent can be used. Any income above the limit is taxed with ExcessTaxPercent. */
  incomeLimit?: number | null;

  /** Tax percentage that is used in calculating the widthholding tax for the part of income above the IncomeLimit. */
  taxPercent2?: number | null;

  /** Type of the taxcard as of 2019, when the taxcards logic was simplified by the Finnish tax authorities. */
  kind?: TaxcardKind | null;
}
