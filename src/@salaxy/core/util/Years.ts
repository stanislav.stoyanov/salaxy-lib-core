import { Dates } from "./Dates";

import { yearlyChangingNumbersYears } from "../codegen";
import { yearlyHolidaysYears } from "../codegen";
import { YearlyChangingNumbers } from "../model";
import { Holidays } from "../model";

/**
 * Helper for yearly changing sidecost percentages and holidays.
 */
export class Years {

  /**
   * Gets salary calculation parameters that are changing yearly and holidays of the year.
   * The method is designed for the end-of-year and as such it only supports 2 years:
   * the current / previous year (from Jan to Nov) OR current / next year (in approx. December).
   *
   * @param forDate - Dately object (JavaScript date, ISO string formatted date, or moment) for which the numbers are fetched.
   *
   * @return The yearly changing numbers if year supported. Error if year not supported.
   */
  public static getYearlyChangingNumbers(forYear: any): YearlyChangingNumbers {
    const year = Dates.asMoment(forYear).year();
    const numbers = yearlyChangingNumbersYears.find((x) => x.year === year);
    if (!numbers) {
      throw new Error(`Year ${year} not supported.`);
    }
    numbers.holidays = this.getYearlyHolidays(forYear);
    return numbers;
  }

  /**
   * Gets holidays for the given year.
   *
   * @param forDate - Dately object (JavaScript date, ISO string formatted date, or moment) for which the holidays are fetched.
   *
   * @return The holidays for the given year.
   */
  public static getYearlyHolidays(forYear: any): Holidays {
    const year = Dates.asMoment(forYear).year();
    const yearlyHolidays = yearlyHolidaysYears.find((x) => x.year === year);
    if (!yearlyHolidays) {
      throw new Error(`Year ${year} not supported.`);
    }
    return yearlyHolidays.holidays;
  }
}
