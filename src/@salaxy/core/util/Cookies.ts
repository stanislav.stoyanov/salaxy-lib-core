/**
 * Namespace salaxy.util contains utility classes that are not related to Salaxy business logic,
 * but to general functionality: JavaScript programming, browsers etc.
 *
 * All implementation should be independent from the framework (Angular 1, Angular, jQuery etc.).
 * Only the following dependencies are accepted:
 *
 * - Moment.js (not currently added)
 * - (New dependencies must be approved by Matti & Olli)
 */

/**
 * Very simple cookie implementation - at the moment for token storage only.
 * Expand if needed for other use cases (see todo section in the docs).
 *
 * @todo Expand this class as necessary e.g. something like this: https://developer.mozilla.org/en-US/docs/Web/API/Document/cookie/Simple_document.cookie_framework
 * or this https://github.com/Booyanach/cookie-wrapper ... which unfortunately cannot be used at the moment, because they are GPL license.
 */
export class Cookies {

    /**
     * Gets a cookie value by a key
     * @param cname - CName / key to look for.
     */
    public get(key: string) {
        const cookie = document.cookie;
        const cookieParts = cookie.split(";");
        for (const item of cookieParts) {
            const cookiePart = decodeURIComponent(item).trim();
            if (cookiePart.indexOf(key + "=") === 0) {
                return cookiePart.substring(key.length + 1);
            }
        }
        return null;
    }

    /**
     * Sets a cookie value for the specified CName
     * @param cname - CName for the cookie value. Name is uri encoded as a fallback, but generally, you should make sure this is a valid cname.
     * @param value - Value to set for the cookie. Value is uri encoded.
     * @param expirationDays - Days until the cookie expires.
     * Set this to null or 0 to not set the expiration date at all.
     * This defaults the behavior where cookie is deleted when browser is closed.
     */
    public setCookie(cname: string, value: string, expirationDays?: number) {
        cname = encodeURIComponent(cname);
        value = encodeURIComponent(value);

        if (expirationDays && expirationDays > 0) {
            const d = new Date();
            d.setTime(d.getTime() + (expirationDays * 24 * 60 * 60 * 1000));
            const expires = "expires=" + d.toUTCString();
            document.cookie = cname + "=" + value + ";" + expires + ";path=/";
        } else {
            document.cookie = cname + "=" + value + ";path=/";
        }
    }
}
