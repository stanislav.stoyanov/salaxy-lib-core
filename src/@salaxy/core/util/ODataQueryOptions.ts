
/** Defines the query options of OData */
export interface ODataQueryOptions {

  /** Tells the server to include the total count of matching entities in the response. (Useful for server-side paging.)  */
  $count?: boolean;

  /** Expands related entities inline. At the moment, this typically not implemented. */
  $expand?: string;

  /** Filters the results, based on a Boolean condition. */
  $filter?: string;

  /** Sorts the results. */
  $orderby?: string;

  /** Selects which properties to include in the response. */
  $select?: string;

  /** Skips the first n results. */
  $skip?: number;

  /** Returns only the first n the results. */
  $top?: number;

  /**
   * Performs free-text search using the in-built logic in the API.
   */
  $search?: string;

  /**
   * If true, perfoms the query also in anonymous mode
   * (no token, token expired or invalid token).
   * Default is that the query is done only if user has a valid token.
   */
  anon?: boolean;
}
