/**
 * Helpers for performing operations with objects
 */
export class Objects {

  /**
   * Uses JSON Serialization / Deserialization to deep clone an object.
   * @param value Object to clone
   * @returns A cloned object.
   */
  public static copy<T>(value: T): T {
    return JSON.parse(JSON.stringify(value, (k, v) => v === undefined ? null : v)) as T;
  }

  /**
   * Extends one object with another using Intersection types.
   * https://www.typescriptlang.org/docs/handbook/advanced-types.html
   * @param first First object.
   * @param second Second object.
   */
  public static extend<T, U>(first: T, second: U): T & U {
    const result = {} as T & U;
    for (const id in first) {
      if (first.hasOwnProperty(id)) {
        (result as any)[id] = (first as any)[id];
      }
    }
    for (const id in second) {
      if (second.hasOwnProperty(id) && !result.hasOwnProperty(id)) {
        (result as any)[id] = (second as any)[id];
      }
    }
    return result;
  }

   /**
    * Compares objects a and b.
    * Returns true if the objects are equal using value-to-value comparison.
    *
    * @param a - Any object.
    * @param b - Any object to compare with.
    */
  public static equal(a: any, b: any): boolean {
    if (a === b) {
      return true;
    }

    if (a && b && typeof a === "object" && typeof b === "object") {
      const arrA = Array.isArray(a);
      const arrB = Array.isArray(b);
      let i: number;
      let length: number;
      let key: string;

      if (arrA && arrB) {
        // both are arrays
        length = a.length;
        if (length !== b.length) {
          // different length
          return false;
        }
        for (i = length; i-- !== 0;) {
          if (!this.equal(a[i], b[i])) {
            // members are not equal
            return false;
          }
        }
        // all members are equal
        return true;
      }

      if (arrA !== arrB) {
        // either a or be is an array, not both
        return false;
      }

      const dateA = a instanceof Date;
      const dateB = b instanceof Date;
      if (dateA !== dateB) {
        // either a or b is a date, not both
        return false;
      }
      if (dateA && dateB) {
        // both are dates, test times equality
        return a.getTime() === b.getTime();
      }

      const regexpA = a instanceof RegExp;
      const regexpB = b instanceof RegExp;
      if (regexpA !== regexpB) {
        // either a or b is RegExp, not both
        return false;
      }
      if (regexpA && regexpB) {
        // both are RegExp, test string equality
        return a.toString() === b.toString();
      }

      const keys = Object.keys(a);
      length = keys.length;

      if (length !== Object.keys(b).length) {
        // a and b have different number of properties
        return false;
      }

      for (i = length; i-- !== 0;) {
        if (!Object.prototype.hasOwnProperty.call(b, keys[i])) {
          // b does not have the same properties as a
          return false;
        }
      }

      for (i = length; i-- !== 0;) {
        key = keys[i];
        if (!this.equal(a[key], b[key])) {
          // property has not the same value
          return false;
        }
      }

      // both objects are equal
      return true;
    }

    // return false, except both are NaN
    return a !== a && b !== b;
  }
}
