import * as momentImport from "moment";
import { SalaryDateLogic } from "../logic";
import { DateRange, HolidayDate, HolidayGroup } from "../model";
import { Years } from "./Years";

/**
 * Import the moment for puposes of this library.
 * @hidden
 */
const moment = momentImport;

/**
 * Object that can be understood as date:
 *
 * - String is an ISO string "yyyy-mm-dd" without any time / timezone information.
 *   This is the default Date representation in Salaxy library and should typically be used.
 * - Date is a JavaScript date object
 * - Number is (as in Date constructor) a Unix time stamp - number of milliseconds since January 1, 1970, 00:00:00 UTC (the Unix epoch)
 * - Array of numbers is (as in Date constructor) [year, monthIndex, day].
 * - MomentJS object
 * - Special string "today" will set the value as today. Same thing for "yesterday" | "tomorrow" and "salaryDate", and these strings may be extended.
 *
 * If the object cannot be understood as date it will typically be converted to null. Sometimes this may also be an error.
 */
export type DatelyObject = string | Date | number | number[] | momentImport.Moment | "today" | "yesterday" | "tomorrow" | "salaryDate";

/**
 * Date parsing, formatting and other operations within the Salaxy framework.
 * We use ISO 8601 as our Date format even inside JavaScript objects.
 * This is because of the following reasons:
 *
 * 1. Generally, we do not want to use JavaScript Date because it presents so many problems.
 * 2. However, we do not want to force our library consumers using Moment (or some other Date/Time library).
 * 3. We store dates in JSON as ISO 8601 date strings - this is universally the best practice
 * 4. ISO 8601 provides a way to present a date without time and most importantly timezone information
 * 5. ISO 8601 date also works best for most UI components (because of similar reasons as above).
 *
 * Implementation uses MomentJS, but we should avoid using MomentJS outside this library as much as possible.
 * This library is only for Dates. Time / DateTime functionality should be in separate library if necessary.
 */
export class Dates {

  /**
   * Gets an object and returns it as the date format that is stored in the data structure with ISO 8601 format.
   *
   * @param datelyObject - Object that should be converted to Salaxy date: A string, JS Date object, Moment etc.
   *
   * @return The date in simplest possible ISO 8601 compliant format "YYYY-MM-DD", null if empty, not valid date etc.
   */
  public static asDate(datelyObject: DatelyObject): string {
    const asMoment = this.asMoment(datelyObject);
    return asMoment ? asMoment.format("YYYY-MM-DD") : null;
  }

  /**
   * Gets a date based on year, month and day (1-based).
   * NOTE: Unlike Date() constructor or Moment() constructor, all numbers are 1-based:
   * (2017, 2, 2) results to "2017-02-02" and not "2017-01-01T22:00:00.000Z" like in those constructors.
   *
   * @param year Year component of the date or "today" for today's year.
   * @param month Month component of the date (1-12) or "today" for today's month.
   * @param day Day of month component of the date (1-31) or "today" for today's day of month.
   */
  public static getDate(year: number | "today", month: number | "today", day: number | "today"): string {
    year = (year === "today") ? Dates.getTodayMoment().year() : year;
    month = (month === "today") ? Dates.getTodayMoment().month() : (month - 1);
    day = (day === "today") ? Dates.getTodayMoment().date() : day;
    if (!day || month == null || month < 0 || month > 11 || !year) {
      return null;
    }
    return this.asDate([year, month, day]);
  }

  /**
   * Converts the date to a JavaScript Date. Please note, that also time part is returned.
   * @param  datelyObject - Object that should be converted: A string, JS Date object, Moment etc.
   *
   * @return JavaScript date corresponding to input or null.
   */
  public static asJSDate(datelyObject: DatelyObject): Date {
    const asMoment = this.asMoment(datelyObject);
    return asMoment ? asMoment.toDate() : null;
  }

  /**
   * Checks whether a string is in ISO format.
   *
   * @param isoDate Value that is evaluated.
   * @param required If set to true, the value is required. If required is null the false will be returned.
   *
   * @return True if the given date is in the correct format.
   */
  public static isValidDateTime(isoDate: string, required = false): boolean {
    if (!isoDate) {
      return !required;
    }
    return moment(isoDate, moment.ISO_8601).isValid();
  }

  /** Get the date of today as ISO 8601 string format (YYYY-MM-DD)  */
  public static getToday(): string {
    return this.asDate(this.getTodayMoment());
  }

  /** Get the date of today as moment object. */
  public static getTodayMoment(): momentImport.Moment {
    return moment(0, "HH");
  }

  /**
   * Gets the dately object as a Moment. If empty, not valid etc., returns null.
   * If the object is already a returns the given objectg (no clone).
   *
   * @param datelyObject Object that should be converted to Moment: A string, JS Date object, Moment, array of numbers etc.
   * @param allowInvalid If true, returns also invalid moment object. By default returns null, if the dately object is parsed as invalid moment.
   *
   *  @return A Moment object or null.
   */
  public static asMoment(datelyObject: DatelyObject, allowInvalid: boolean = false): momentImport.Moment {
    if (!datelyObject) {
      return null;
    }
    if (moment.isMoment(datelyObject)) {
      if (datelyObject.isValid() || allowInvalid) {
        return datelyObject;
      }
      return null;
    }
    return this.getMoment(datelyObject, allowInvalid);
  }

  /**
   * Gets the dately object as a Moment. If empty, not valid etc., returns null.
   * If the object is already a Moment, clones it.
   *
   * @param datelyObject Object that should be converted to Moment: A string, JS Date object, Moment, array of numbers etc.
   * @param allowInvalid If true, returns also invalid moment object. By default returns null, if the dately object is parsed as invalid moment.
   *
   *  @return A Moment object or null.
   */
  public static getMoment(datelyObject: DatelyObject, allowInvalid: boolean = false): momentImport.Moment {
    if (!datelyObject) {
      return null;
    }

    let asMoment: momentImport.Moment;
    if (moment.isMoment(datelyObject)) {
      asMoment = datelyObject.clone();
    } else {
        if (moment.isDate(datelyObject) || Number.isInteger(datelyObject as number) || Array.isArray(datelyObject)) {
          asMoment = moment(datelyObject);
        } else if (datelyObject === "today") {
          asMoment = this.getTodayMoment();
        } else if (datelyObject === "yesterday") {
          asMoment = this.getTodayMoment().add(-1, "day");
        } else if (datelyObject === "tomorrow") {
          asMoment = this.getTodayMoment().add(1, "day");
        } else if (datelyObject === "salaryDate") {
          asMoment = moment(SalaryDateLogic.CalculateSalaryDate(null, null, true, null));
        } else {
          asMoment = moment(datelyObject, moment.ISO_8601);
        }
    }
    if (asMoment.isValid() || allowInvalid) {
      return asMoment;
    }
    return null;
  }

  /**
   * Returns the duration  object. If parameter is empty or null, defaults to Today (not Now).
   *
   * @param startDatelyObject Duration start that is parsed using getMoment().
   * Null before conversion defaults to today. Invalid objects result to invalid duration.
   * @param endDatelyObject Duration start that is parsed using getMoment().
   * Null before conversion defaults to today. Invalid objects result to invalid duration.
   *
   * @return A Duration object, which may also be invalid
   */
  public static getDuration(startDatelyObject: DatelyObject, endDatelyObject: DatelyObject): momentImport.Duration {
    startDatelyObject = this.asMoment(startDatelyObject, true) || this.getTodayMoment();
    endDatelyObject = this.asMoment(endDatelyObject, true) || this.getTodayMoment();
    return moment.duration(endDatelyObject.diff(startDatelyObject));
  }

  /**
   * Gets a date range based on start and end dates.
   * Days count is calculated using getWorkdays() (Add parameter if you need other day count methods).
   *
   * @param start DateRange start that should be converted to Salaxy date: A string, JS Date object, Moment etc.
   * @param end DateRange end that should be converted to Salaxy date: A string, JS Date object, Moment etc.
   *
   * @return DateRange based on start and end dates.
   */
  public static getDateRange(start: DatelyObject, end: DatelyObject): DateRange {
    const result: DateRange = {
      start: this.asDate(start),
      end: this.asDate(end),
    };
    result.daysCount = this.getWorkdays(result.start, result.end).length;
    return result;
  }

  /**
   * Gets the workdays between two dates including both days.
   * Returns empty array if there is no start or if end is before start.
   * Returns 1 day if there is no end: we assume that the user selected just one day.
   *
   * @param startDate A dately value as the start date (a string, JS Date object, Moment etc.).
   * @param endDate Corresponding end date.
   * @param holidayGroup The holiday group for defining which days not to count as working day. The default group is Holiday.
   *
   * @return The work days between the two dates (inclusive).
   */
  public static getWorkdays(startDate: DatelyObject, endDate: DatelyObject, holidayGroup: HolidayGroup = HolidayGroup.Holiday): string[] {
    startDate = this.asDate(startDate);
    endDate = this.asDate(endDate) || startDate;
    if (!startDate) {
      return [];
    }
    const result: string[] = [];
    while (startDate <= endDate) {
      if (this.isWorkday(startDate, holidayGroup)) {
        result.push(startDate);
      }
      startDate = this.asDate(this.getMoment(startDate).add(1, "day"));
    }
    return result;
  }

  /**
   * Gets the vacation days (6 day week Mon-Sat) between two dates including both days.
   * Returns empty array if there is no start or if end is before start.
   * Returns 1 day if there is no end: we assume that the user selected just one day.
   *
   * @param startDate A dately value as the start date (a string, JS Date object, Moment etc.).
   * @param endDate Corresponding end date.
   * @param holidayGroup The holiday group for defining which days not to count as working day. The default group is Holiday.
   *
   * @return The work days between the two dates (inclusive).
   */
  public static getVacationDays(startDate: DatelyObject, endDate: DatelyObject, holidayGroup: HolidayGroup = HolidayGroup.Holiday): string[] {
    startDate = this.asDate(startDate);
    endDate = this.asDate(endDate) || startDate;
    if (!startDate) {
      return [];
    }
    const result: string[] = [];
    while (startDate <= endDate) {
      if (this.isVacationday(startDate, holidayGroup)) {
        result.push(startDate);
      }
      startDate = this.asDate(this.getMoment(startDate).add(1, "day"));
    }
    return result;
  }

  /**
   * Gets a formatted range of two dates.
   *
   * @param startDate A dately value as the start date (a string, JS Date object, Moment etc.).
   * @param endDate Corresponding end date.
   *
   * @return A date range string with possible formats:
   *         "12. - 25.11.2019", "12.11. - 02.12.2019", "12.11.2019 - 12.01.2020", "12.01.2020 -" or "- 12.01.2020".
   */
  public static getFormattedRange(startDate: DatelyObject, endDate: DatelyObject): string {
    const formatFull = "DD.MM.YYYY";
    const start = this.asMoment(startDate);
    const end = this.asMoment(endDate);
    if (!start && !end) {
      return "-";
    }
    if (!start && end) {
      return " - " + end.format(formatFull);
    }
    if (start && !end) {
      return start.format(formatFull) + " - ";
    }
    if (start.isSame(end)) {
      return start.format(formatFull);
    }
    const formatMonthDay = "DD.MM.";
    const formatDay = "DD.";
    const sameYear = start.year() === end.year();
    const sameMonth = start.month() === end.month();
    return start.format(sameYear ? (sameMonth ? formatDay : formatMonthDay) : formatFull) + " - " + end.format(formatFull);
  }

  /**
   * Output a formatted date with default date format (may later be localized).
   * @param date A dately value (a string, JS Date object, Moment etc.) that is formatted.
   * @return A Simple date string e.g. "12.11.2019", if the value is empty or null returns "-".
   */
  public static getFormattedDate(date: DatelyObject): string {
    return this.format(date, "DD.MM.YYYY", "-");
  }

  /**
   * Formats a dately object using MomentJs formatting:
   * Default is "DD.MM.YYYY"
   * For all format string options, see https://momentjs.com/docs/#/displaying/format/
   */
  public static format(date: DatelyObject, format: string = "DD.MM.YYYY", nullValue: string = null) {
    const asMoment = this.asMoment(date);
    return asMoment ? asMoment.format(format) : nullValue;
  }

  /**
   * Gets the month as 1-based number: 1-12.
   *
   * @param datelyObject - Object that represents month: A string, JS Date object, Moment etc.
   * Special string "today" can be used for fetching today's date.
   *
   * @return Month between 1-12 or null if the object is not a dately object.
   */
  public static getMonth(datelyObject: DatelyObject): number {
    const asMoment = this.asMoment(datelyObject);
    return asMoment ? asMoment.month() + 1 : null;
  }

  /**
   * Gets the year of a date or today.
   *
   * @param datelyObject - Object that represents month: A string, JS Date object, Moment etc.
   * Special string "today" can be used for fetching today's date.
   *
   * @return Year, e.g. 2019 or null if the object is not a dately object.
   */
  public static getYear(datelyObject: DatelyObject): number {
    const asMoment = this.asMoment(datelyObject);
    return asMoment ? asMoment.year() : null;
  }

  /**
   * Returns true if the given dately object is a holiday.
   * Please note that the weekend days are not by default holidays
   *
   * @param datelyObject - Object that should be converted to date: A string, JS Date object, Moment etc.
   * @param holidayGroup - The holiday group for defining which days to count as holiday day. The default group is Holiday.
   *
   * @return True, if the given dately object is a holiday.
   */
  public static isHoliday(datelyObject: DatelyObject,  holidayGroup: HolidayGroup = HolidayGroup.Holiday ): boolean {
      const moment = this.asMoment(datelyObject);
      if (!moment) {
        return false;
      }
      if (!holidayGroup) {
        return false;
      }
      const holidays = Years.getYearlyHolidays(moment);
      for (const holiday in holidays) {
        if (holidays.hasOwnProperty(holiday)) {
          const holidayDate = holidays[holiday] as HolidayDate;
          if (this.asDate(holidayDate.date) === this.asDate(moment) &&
            holidayDate.holidayGroups.find((x) => x === holidayGroup)) {
            return true;
          }
        }
      }
      return false;
  }

  /**
   * Returns true if the given dately object is a working day (5 day week).
   * The day must not be Saturday or Sunday or not any day in the given holiday categoria.
   *
   * @param datelyObject - Object that should be converted to date: A string, JS Date object, Moment etc.
   * @param holidayGroup - The holiday group for defining which days to count as holiday day. The default group is Holiday.
   *
   * @return True, if the given dately object is a working day.
   */
  public static isWorkday(datelyObject: DatelyObject, holidayGroup: HolidayGroup = HolidayGroup.Holiday): boolean {

    const moment = this.asMoment(datelyObject);
    if (!moment) {
      return false;
    }

    if (moment.isoWeekday() === 6 || // Saturday
        moment.isoWeekday() === 7) { // Sunday
      return false;
    }

    if (!holidayGroup) {
      return true;
    }

    return !this.isHoliday(moment, holidayGroup);
  }

  /**
   * Returns true if the given dately object is a vacation day (6 day week).
   * The day must not be Sunday or not any day in the given holiday categoria.
   *
   * @param datelyObject - Object that should be converted to date: A string, JS Date object, Moment etc.
   * @param holidayGroup - The holiday group for defining which days to count as holiday day. The default group is Holiday.
   *
   * @return True, if the given dately object is a working day.
   */
  public static isVacationday(datelyObject: DatelyObject, holidayGroup: HolidayGroup = HolidayGroup.Holiday): boolean {

    const moment = this.asMoment(datelyObject);
    if (!moment) {
      return false;
    }

    if ( moment.isoWeekday() === 7) { // Sunday
      return false;
    }

    if (!holidayGroup) {
      return true;
    }

    return !this.isHoliday(moment, holidayGroup);
  }

/**
 * Adds given amount of working days to given day.
 *
 * @param datelyObject - Object that should be converted to date: A string, JS Date object, Moment etc.
 * @param days - Working days to add (positive or negative number).
 * If zero or not set, returns datelyObject or next workday if datelyObject is not a workday.
 * @param holidayGroup - The holiday group for defining which days to count as holiday day. The default group is Holiday.
 *
 * @return New working day with amount of given working days ahead.
 */
  public static addWorkdays(datelyObject: DatelyObject, days: number, holidayGroup: HolidayGroup = HolidayGroup.Holiday): string {
    const newMoment = this.getMoment(datelyObject);
    if (!newMoment) {
      return null;
    }
    if (!days) {
      while (true) {
        if (this.isWorkday(newMoment, holidayGroup)) {
          return this.asDate(newMoment);
        }
        newMoment.add(1, "days");
      }
    }
    let count = 0;
    const step = days > 0 ? 1 : -1;
    while (true) {
      newMoment.add(step, "days");
      if (this.isWorkday(newMoment, holidayGroup)) {
        count += step;
        if (count === days) {
          return this.asDate(newMoment);
        }
      }
    }
  }
}
