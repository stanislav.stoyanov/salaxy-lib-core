/**
 * Utility for accessing brand specific style definitions.
 */
export class Brand {

    /**
     * Returns the brand color defined as a global variable.
     *
     * @param color - Named brand color to get.
     * @param format - Color format: hex like '#289548', rgb like 'rgb(40, 149, 72)' or rgba like 'rgba(40, 149, 72, 1.0'. Default value is hex.
     */
    public static getBrandColor(color: "primary" | "success" | "info" | "warning" | "danger", format: "hex" | "rgb" | "rgba" = "hex"): string {
        let value = Brand.getStylePropertyValue("--salaxy-brand-" + color);
        if (!value) {
            switch (color) {
                case "primary":
                    value = "#009fd8";
                    break;
                case "success":
                    value = "#289548";
                    break;
                case "info":
                    value = "#91d214";
                    break;
                case "warning":
                    value = "#ff911e";
                    break;
                case "danger":
                    value = "#d55552";
                    break;
            }
        }
        switch (format) {
            case "rgb":
                return Brand.hex2rgb(value);
            case "rgba":
                return Brand.hex2rgba(value);
            case "hex":
            default:
                return value;
        }

    }

    /**
     * Returns the css global style variable.
     * E.g. --salaxy-brand-primary
     * @param variableName - Variable name.
     */
    public static getStylePropertyValue(variableName: string): string {
        const style = getComputedStyle(document.body);
        if (!style) {
            return null;
        }
        let value = style.getPropertyValue(variableName);
        if (!value) {
            return null;
        }
        value = value.trim();
        return value;
    }

   /**
    * Returns the hex color value as rgb css function.
    * For example:  value '#289548' as 'rgba(40, 149, 72)'
    *
    * @param hexValue - Hex value.
    */
    public static hex2rgb(hexValue: string): string {
        const values = this.hex2rgbaValues(hexValue);
        if (!values) {
            return null;
        }
        return `rgb(${values[0]}, ${values[1]}, ${values[2]})`;
    }

   /**
    * Returns the hex color value as rgba css function.
    * For example:  value '#289548ff' as 'rgba(40, 149, 72, 1)'
    *
    * @param hexValue - Hex value.
    */
    public static hex2rgba(hexValue: string): string {
        const values = this.hex2rgbaValues(hexValue);
        if (!values) {
            return null;
        }
        return `rgba(${values[0]}, ${values[1]}, ${values[2]}, ${values[3].toFixed(2)})`;
    }

    /**
     * Returns the rgb value as hex string.
     * For example: 40, 149, 72 as #289548.
     *
     * @param r - red parameter.
     * @param g - green parameter.
     * @param b - blue parameter.
     */
    public static rgb2hex(r: number, g: number, b: number): string {
        // tslint:disable-next-line
        return "#" + ((b | g << 8 | r << 16) | 1 << 24).toString(16).substring(1)
    }

    /**
     * Returns the rgba value as hex string.
     * For example: 40, 149, 72, 1 as #289548ff.
     *
     * @param r - red parameter.
     * @param g - green parameter.
     * @param b - blue parameter.
     * @param a - alpha parameter.
     */
    public static rgba2hex(r: number, g: number, b: number, a: number): string {
        // tslint:disable-next-line
        const hexA = (Math.round(255 * a) | 1 << 8).toString(16).substring(1);
        return Brand.rgb2hex(r, g, b) + hexA;
    }

    private static hex2rgbaValues(hexValue: string): number[] {
        if (!hexValue) {
            return null;
        }
        hexValue = hexValue.trim().toLowerCase();

        if (hexValue.length === 0) {
            return;
        }

        if (hexValue.charAt(0) === "#") {
            // remove #
            hexValue = hexValue.substring(1);
        }

        if (hexValue.length === 3) {
            // add alpha
            hexValue += "f";
        }
        if (hexValue.length === 4) {
            // convert to 8 digits
            hexValue =
            hexValue.charAt(0) +
            hexValue.charAt(0) +
            hexValue.charAt(1) +
            hexValue.charAt(1) +
            hexValue.charAt(2) +
            hexValue.charAt(2) +
            hexValue.charAt(3) +
            hexValue.charAt(3);
        }
        if (hexValue.length === 6) {
            // add alpha
            hexValue += "ff";
        }
        if (hexValue.length !== 8) {
            // invalid length
            return null;
        }
        const r = parseInt(hexValue.substring(0, 2), 16);
        const g = parseInt(hexValue.substring(2, 4), 16);
        const b = parseInt(hexValue.substring(4, 6), 16);
        const a =  ((0.0 + parseInt(hexValue.substring(6, 8), 16)) / 255.0);

        return [r, g, b, a];
    }
}
