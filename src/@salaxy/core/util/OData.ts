import { Ajax } from "../api/Ajax";
import { ODataQueryOptions } from "./ODataQueryOptions";
import { ODataResult } from "./ODataResult";
import { Token } from "./Token";

/** Provides helpers for handling OData queries */
export class OData {

  /**
   * Makes an OData query to the the given service URL.
   * @param baseUrl The service URL that is used, e.g. "/v03-rc/api/taxcards/current".
   * @param query The options for the query: Filter, search sort etc.
   * This is either a strongly typed object or query string that is added directly.
   * @param ajax The Ajax implementation that is used for HTTP get.
   */
  public static getOData<T>(baseUrl: string, query: ODataQueryOptions | string, ajax: Ajax): Promise<ODataResult<T>> {
    let fullPath;
    if (typeof query === "string") {
      query = query.trim();
      if (query.length > 0 && query.substr(0, 1) === "?") {
        query = query.substr(1);
      }
      fullPath = (query.length === 0) ? baseUrl : (baseUrl + "?" + query);
    } else {
      query = query || {};
      if (Token.validate(ajax.getCurrentToken()) !== "ok" && !query.anon) {
        throw new Error("OData call without token and anon option not set to true.");
      }
      fullPath = OData.getUrl(baseUrl, query);
    }
    return ajax.getJSON(fullPath).then((data) => {
      return OData.getODataResult<T>(data);
    });
  }

  /**
   * Gets the result from Ajax result (json) to strongly typed result.
   * @param data Result data coming from the ajax call.
   * This may be either an OData object with count and next page link or just an array depending on the call.
   */
  public static getODataResult<T>(data: any): ODataResult<T> {
    const result: ODataResult<T> = {
      items: [],
    };
    if (data.items) {
      result.items = data.items;
      result.count = data.count || null;
      result.nextPageLink = data.nextPageLink || null;
    } else if (Array.isArray(data)) {
      result.items = data;
      result.count = null;
      result.nextPageLink = null;
    } else {
      throw Error("Unexpected result: " + typeof(data));
    }
    return result;
  }

  /**
   * Gets the URL including the query string using the provided OData query options.
   * @param baseUrl Base URL of the service to which the query is added.
   * If null, the returned URL is the query string part starting with the question mark
   * ...or if none of the options is set, returns just an empty string.
   * @param queryOptions options based on which the query is constructed.
   */
  public static getUrl(baseUrl: string, queryOptions: ODataQueryOptions) {
    queryOptions = queryOptions || {};
    baseUrl = (baseUrl || "").trim();
    const queries: string[] = [];
    if (queryOptions.$count) {
      queries.push("$count=true");
    }
    if (queryOptions.$expand) {
      queries.push("$expand=" + queryOptions.$expand);
    }
    if (queryOptions.$filter) {
      queries.push("$filter=" + queryOptions.$filter);
    }
    if (queryOptions.$orderby) {
      queries.push("$orderby=" + queryOptions.$orderby);
    }
    if (queryOptions.$select) {
      queries.push("$select=" + queryOptions.$select);
    }
    if (queryOptions.$skip) {
      queries.push("$skip=" + queryOptions.$skip);
    }
    if (queryOptions.$top) {
      queries.push("$top=" + queryOptions.$top);
    }
    if (queryOptions.$search) {
      let search = queryOptions.$search;
      if (search.indexOf("\"") >= 0) {
        // phrase search
        queries.push("$search=" + encodeURIComponent(`${search}`));
      } else {
        search = queryOptions.$search
        .replace(/-/g, "\-")
        .replace(/\+/g, "\+")
        .replace(/\|/g, "\|")
        .replace(/\(/g, "\(")
        .replace(/\)/g, "\)");
        search =  search.endsWith("*") ? search.substring(0, search.length - 1) : search;
        // search by phrase, term and wildcard
        queries.push("$search=" + encodeURIComponent(`"${search}" | ${search} | ${search}*`));
      }
    }
    return baseUrl + (queries.length === 0 ? "" : (baseUrl.indexOf("?") >= 0 ? "&" : "?") + queries.join("&"));
  }

}
