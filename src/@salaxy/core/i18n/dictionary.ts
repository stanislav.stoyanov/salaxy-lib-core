/* tslint:disable */
var _dictionary = _dictionary || {};
_dictionary["en"] = {
  "SALAXY": {
    "ENUM": {
      "AbcSection": {
        "association": {
          "description": "",
          "label": "Associations"
        },
        "blog": {
          "description": "",
          "label": "Blog"
        },
        "businessOwner": {
          "description": "",
          "label": "Entrepreneur"
        },
        "documentTemplates": {
          "description": "",
          "label": "Documents and forms"
        },
        "doNotShow": {
          "description": "",
          "label": "Not shown to user (Admin only)"
        },
        "employee": {
          "description": "",
          "label": "Employee"
        },
        "employment": {
          "description": "",
          "label": "Employment relationship and contract"
        },
        "entrepreneur": {
          "description": "",
          "label": "Company or community"
        },
        "examples": {
          "description": "",
          "label": "Examples of hiring situations"
        },
        "householdEmployer": {
          "description": "",
          "label": "Household"
        },
        "instructionsAndExamples": {
          "description": "",
          "label": "User manual"
        },
        "insurance": {
          "description": "",
          "label": "Insurances and pensions"
        },
        "palkkausGeneral": {
          "description": "",
          "label": "Information about Palkkaus.fi service"
        },
        "palkkausInstructions": {
          "description": "",
          "label": "User manuals"
        },
        "personEmployer": {
          "description": "",
          "label": "Household employee"
        },
        "press": {
          "description": "",
          "label": "Releases and material"
        },
        "productLongDescription": {
          "description": "",
          "label": "Product description"
        },
        "salary": {
          "description": "",
          "label": "Payroll calculation and salary payment"
        },
        "undefined": {
          "description": "",
          "label": "New article / undefined"
        },
        "worker": {
          "description": "",
          "label": "Employer"
        }
      },
      "AbsenceCauseCode": {
        "accruedHoliday": {
          "label": "Accrued holiday"
        },
        "annualLeave": {
          "label": "Annual leave"
        },
        "childCareLeave": {
          "label": "Child care leave"
        },
        "childIllness": {
          "label": "Child's illness or a compelling family reason"
        },
        "illness": {
          "label": "Illness"
        },
        "industrialAction": {
          "label": "Industrial action or lock-out"
        },
        "interruptionInWorkProvision": {
          "label": "Interruption in work provision"
        },
        "jobAlternationLeave": {
          "label": "Job alternation leave"
        },
        "layOff": {
          "label": "Lay-off"
        },
        "leaveOfAbsence": {
          "label": "Leave of absence"
        },
        "midWeekHoliday": {
          "label": "Mid-week holiday"
        },
        "militaryRefresherTraining": {
          "label": "Military refresher training"
        },
        "militaryService": {
          "label": "Military or non-military service"
        },
        "occupationalAccident": {
          "label": "Occupational accident"
        },
        "other": {
          "label": "Other"
        },
        "parentalLeave": {
          "label": "Maternity, paternity and parental leave"
        },
        "partTimeAbsenceDueToRehabilitation": {
          "label": "Part-time absence due to rehabilitation"
        },
        "partTimeChildCareLeave": {
          "label": "Part-time child care leave"
        },
        "partTimeSickLeave": {
          "label": "Part-time sick leave"
        },
        "personalReason": {
          "label": ""
        },
        "rehabilitation": {
          "label": "Rehabilitation"
        },
        "specialMaternityLeave": {
          "label": "Special maternity leave"
        },
        "studyLeave": {
          "label": "Study leave"
        },
        "training": {
          "label": "Training, education"
        },
        "unpaidLeave": {
          "label": ""
        }
      },
      "AgeGroupCode": {
        "a": {
          "label": ""
        },
        "b": {
          "label": ""
        },
        "c": {
          "label": ""
        },
        "u": {
          "label": ""
        }
      },
      "AgeRange": {
        "age_15": {
          "description": "",
          "label": "15 yrs"
        },
        "age16": {
          "description": "",
          "label": "16 yrs"
        },
        "age17": {
          "description": "",
          "label": "17 yrs"
        },
        "age18_52": {
          "description": "",
          "label": "18-52 yrs"
        },
        "age53_62": {
          "description": "",
          "label": "53-62 yrs"
        },
        "age63_64": {
          "description": "",
          "label": "63-64 yrs"
        },
        "age65_67": {
          "description": "",
          "label": "65-67 yrs"
        },
        "age68AndOVer": {
          "description": "",
          "label": "68 yrs and over"
        },
        "unknown": {
          "description": "",
          "label": "Unknown"
        }
      },
      "AllowanceCode": {
        "expensesWorkingAbroad": {
          "label": ""
        },
        "fullDailyAllowance": {
          "label": ""
        },
        "internationalDailyAllowance": {
          "label": ""
        },
        "mealAllowance": {
          "label": ""
        },
        "partialDailyAllowance": {
          "label": ""
        }
      },
      "AnnualLeavePaymentKind": {
        "draftCalc": {
          "description": "",
          "label": ""
        },
        "endSaldo": {
          "description": "",
          "label": ""
        },
        "info": {
          "description": "",
          "label": ""
        },
        "manualBonus": {
          "description": "",
          "label": ""
        },
        "manualCompensation": {
          "description": "",
          "label": ""
        },
        "manualSalary": {
          "description": "",
          "label": ""
        },
        "paidCalc": {
          "description": "",
          "label": ""
        },
        "startSaldo": {
          "description": "",
          "label": ""
        },
        "total": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "ApiTestErrorType": {
        "default": {
          "description": "",
          "label": "Default"
        },
        "userFriendly": {
          "description": "",
          "label": "User friendly"
        }
      },
      "ApiValidationErrorType": {
        "general": {
          "label": "General"
        },
        "invalid": {
          "label": "Invalid"
        },
        "required": {
          "label": "Required"
        },
        "warning": {
          "label": ""
        }
      },
      "AuthenticationMethod": {
        "auth0Database": {
          "description": "",
          "label": "Email-Password authentication"
        },
        "emailPwdLocal": {
          "description": "",
          "label": "Database"
        },
        "facebook": {
          "description": "",
          "label": "Facebook account"
        },
        "google": {
          "description": "",
          "label": "Google or Gmail account"
        },
        "internalLocalhost": {
          "description": "",
          "label": "Internal"
        },
        "linkedIn": {
          "description": "",
          "label": "LinkedIn account"
        },
        "microsoft": {
          "description": "",
          "label": "Microsoft account"
        },
        "salaxy": {
          "description": "",
          "label": "Proxy to another account"
        },
        "test": {
          "description": "",
          "label": "Temporary test credential"
        },
        "undefined": {
          "description": "",
          "label": "Unknown"
        },
        "x509": {
          "description": "",
          "label": "X.509 certicate"
        }
      },
      "AuthorizationStatus": {
        "accessDenied": {
          "description": "",
          "label": "Access Denied"
        },
        "expiredOauthTokdescription": "",
        "expiredOauthToklabel": "Session expired",
        "invalidOAuthTokdescription": "",
        "invalidOAuthToklabel": "Error in authentication",
        "noOauthTokdescription": "",
        "noOauthToklabel": "No Oauth Token",
        "oK": {
          "description": "",
          "label": "OK"
        },
        "undefined": {
          "description": "",
          "label": "Undefined"
        }
      },
      "AuthorizationType": {
        "companyContract": {
          "description": "",
          "label": "Employer contract"
        },
        "employerAuthorization": {
          "description": "",
          "label": "Power of attorney"
        },
        "manual": {
          "description": "",
          "label": "Power of attorney submitted separately"
        },
        "none": {
          "description": "",
          "label": "Authentication failed"
        },
        "temporary": {
          "description": "",
          "label": "Temporary identification"
        },
        "workerContract": {
          "description": "",
          "label": "Employee contract"
        }
      },
      "AvatarPictureType": {
        "gravatar": {
          "description": "",
          "label": "Gravatar"
        },
        "icon": {
          "description": "",
          "label": "Icon"
        },
        "uploaded": {
          "description": "",
          "label": "Picture from own computer"
        }
      },
      "BlobFileType": {
        "authorizationPdf": {
          "description": "",
          "label": "Power of attorney"
        },
        "avatar": {
          "description": "",
          "label": "Icon"
        },
        "calcReport": {
          "description": "",
          "label": ""
        },
        "expensesReceipt": {
          "description": "",
          "label": "Expense receipt"
        },
        "monthlyReport": {
          "description": "",
          "label": "Monthly Report"
        },
        "raksaloki": {
          "description": "",
          "label": "Expense receipt"
        },
        "taxCard": {
          "description": "",
          "label": "Tax Card"
        },
        "template": {
          "description": "",
          "label": "Template"
        },
        "unknown": {
          "description": "",
          "label": "Unknown"
        },
        "yearlyReport": {
          "description": "",
          "label": "Yearly Report"
        }
      },
      "BlobRepository": {
        "cdnImages": {
          "description": "",
          "label": ""
        },
        "fileSystemContent": {
          "description": "",
          "label": ""
        },
        "gitContent": {
          "description": "",
          "label": ""
        },
        "payload": {
          "description": "",
          "label": ""
        },
        "reports": {
          "description": "",
          "label": ""
        },
        "systemFiles": {
          "description": "",
          "label": ""
        },
        "userFiles": {
          "description": "",
          "label": ""
        },
        "versionHistory": {
          "description": "",
          "label": ""
        }
      },
      "BoardKind": {
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "CalculationFlag": {
        "accidentInsurance": {
          "label": ""
        },
        "cfDeduction": {
          "label": ""
        },
        "cfDeductionAtSalaxy": {
          "label": ""
        },
        "cfNoPayment": {
          "label": ""
        },
        "cfPayment": {
          "label": ""
        },
        "exclude": {
          "label": ""
        },
        "healthInsurance": {
          "label": ""
        },
        "noTax": {
          "label": ""
        },
        "pensionInsurance": {
          "label": ""
        },
        "tax": {
          "label": ""
        },
        "taxDeduction": {
          "label": ""
        },
        "unemploymentInsurance": {
          "label": ""
        }
      },
      "CalculationRowSource": {
        "manualRow": {
          "description": "",
          "label": "Additional row"
        },
        "salaryPage": {
          "description": "",
          "label": "Salary Page"
        },
        "tesSelection": {
          "description": "",
          "label": "Use case"
        }
      },
      "CalculationRowType": {
        "accomodationBenefit": {
          "amountLabel": "",
          "description": "Taxable value of employee accomodation benefit.",
          "iconText": "",
          "label": "Accomodation benefit",
          "moreInfo": "",
          "priceLabel": ""
        },
        "advance": {
          "amountLabel": "",
          "description": "Salary advance deducation. Salary paid in advance and deducted from net salary.",
          "iconText": "",
          "label": "Salary advance",
          "moreInfo": "",
          "priceLabel": "Paid in advance"
        },
        "board": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "carBenefit": {
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "chainsawReduction": {
          "amountLabel": "",
          "description": "Deduction for use of chainsaw, special cases (pre payment law 15 §, pre payment regulation 15 §).",
          "iconText": "",
          "label": "Choose…",
          "moreInfo": "",
          "priceLabel": ""
        },
        "childCareSubsidy": {
          "amountLabel": "",
          "description": "KELA support to communal workers for childcare. Affects incidental expenses.",
          "iconText": "KELA",
          "label": "Childcare subsidy",
          "moreInfo": "",
          "priceLabel": "Paid for by KELA"
        },
        "compensation": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "Work compensation",
          "moreInfo": "",
          "priceLabel": "Work compensation"
        },
        "dailyAllowance": {
          "amountLabel": "Days",
          "description": "Daily allowance, 40 euros per day, if commute is more than 10 hours or over 15 km.",
          "iconText": 40,
          "label": "Daily allowance",
          "moreInfo": "",
          "priceLabel": "Euro / day"
        },
        "dailyAllowanceHalf": {
          "amountLabel": "Days",
          "description": "Half day allowance, 19 euros per day, if commute is more than 6 hours or over 15 km.",
          "iconText": 19,
          "label": "Half day allowance",
          "moreInfo": "",
          "priceLabel": "Euro / day"
        },
        "employmentTermination": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "eveningAddition": {
          "amountLabel": "Hours",
          "description": "Addition for evening work according to collective bargaining agreement.",
          "iconText": "EVNG",
          "label": "Evening shift addition",
          "moreInfo": "",
          "priceLabel": ""
        },
        "expenses": {
          "amountLabel": "",
          "description": "Employee expense claims (use of taxi, office supplies etc.).",
          "iconText": "expense",
          "label": "Expense claim",
          "moreInfo": "",
          "priceLabel": "Expense claim"
        },
        "foreclosure": {
          "amountLabel": "",
          "description": "Employee salary foreclosure, paid directly to bailiff.",
          "iconText": "",
          "label": "Salary foreclosure",
          "moreInfo": "",
          "priceLabel": "Sum"
        },
        "foreclosureByPalkkaus": {
          "amountLabel": "",
          "description": "Employee salary foreclosure made by Palkkaus.fi, paid directly to bailiff.",
          "iconText": "",
          "label": "Salary foreclosure made by Palkkaus.fi",
          "moreInfo": "",
          "priceLabel": ""
        },
        "holidayBonus": {
          "amountLabel": "Percent",
          "description": "Holiday bonus according to collective bargaining agreement.",
          "iconText": "CBA",
          "label": "Holiday bonus",
          "moreInfo": "",
          "priceLabel": "Salary for holiday period"
        },
        "holidayCompensation": {
          "amountLabel": "Percent",
          "description": "Taxable value of employee holiday compensation if holiday is not held.",
          "iconText": "11,5%",
          "label": "Holiday compensation",
          "moreInfo": "",
          "priceLabel": "Salary amount"
        },
        "holidaySalary": {
          "amountLabel": "",
          "description": "Salary for annual holiday.",
          "iconText": "vacation",
          "label": "Holiday salary",
          "moreInfo": "",
          "priceLabel": "Salary for holiday period"
        },
        "hourlySalary": {
          "amountLabel": "",
          "description": "Salary payment based on hours put in.",
          "iconText": "h",
          "label": "Hourly salary",
          "moreInfo": "",
          "priceLabel": "Hourly salary"
        },
        "irIncomeType": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "mealBenefit": {
          "amountLabel": "",
          "description": "Taxable value of employee meal benefit.",
          "iconText": "food",
          "label": "Meal benefit",
          "moreInfo": "",
          "priceLabel": ""
        },
        "mealCompensation": {
          "amountLabel": "Days",
          "description": "Meal compensation when daily allowence is not paid.",
          "iconText": "meal",
          "label": "Meal compensation",
          "moreInfo": "",
          "priceLabel": "Compensation / day"
        },
        "milageDaily": {
          "amountLabel": "Days",
          "description": "Mileage compensation accoring to collective bargaining agreement.",
          "iconText": "km",
          "label": "Daily mileage compensation",
          "moreInfo": "",
          "priceLabel": "Compensation / day"
        },
        "milageOther": {
          "amountLabel": "Days",
          "description": "Mileage compensation due to other justifications.",
          "iconText": "km",
          "label": "Other mileage compensation",
          "moreInfo": "",
          "priceLabel": "Compensation / day"
        },
        "milageOwnCar": {
          "amountLabel": "",
          "description": "Mileage compensation due to use of own car.",
          "iconText": "0,43€",
          "label": "Mileage compensation, own car",
          "moreInfo": "",
          "priceLabel": "Compensation / km"
        },
        "monthlySalary": {
          "amountLabel": "",
          "description": "Fixed monthly income.",
          "iconText": "month",
          "label": "Monthly salary",
          "moreInfo": "",
          "priceLabel": "Monthly salary"
        },
        "nightimeAddition": {
          "amountLabel": "Hours",
          "description": "Addition for nighttime work according to collective bargaining agreement.",
          "iconText": "NIGHT",
          "label": "Night shift addition",
          "moreInfo": "",
          "priceLabel": ""
        },
        "nonProfitOrg": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "otherAdditions": {
          "amountLabel": "",
          "description": "Other additions according to collective bargaining agreement.",
          "iconText": "addition",
          "label": "Other additions",
          "moreInfo": "",
          "priceLabel": ""
        },
        "otherBenefit": {
          "amountLabel": "",
          "description": "Taxable value of other employee benefits.",
          "iconText": "Benefit",
          "label": "Other benefits",
          "moreInfo": "",
          "priceLabel": ""
        },
        "otherCompensation": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "otherDeductions": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "overtime": {
          "amountLabel": "Percent",
          "description": "Overtime payments.",
          "iconText": "over",
          "label": "Overtime work compensation",
          "moreInfo": "",
          "priceLabel": "Salary amount"
        },
        "phoneBenefit": {
          "amountLabel": "Months",
          "description": "Taxable value of employee mobile phone benefit.",
          "iconText": 20,
          "label": "Mobile phone benefit",
          "moreInfo": "",
          "priceLabel": "Mobile phone benefit / mon"
        },
        "prepaidExpenses": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "remuneration": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "salary": {
          "amountLabel": "",
          "description": "Lump sum for work effort.",
          "iconText": "D",
          "label": "Salary or commission",
          "moreInfo": "",
          "priceLabel": ""
        },
        "saturdayAddition": {
          "amountLabel": "Percent",
          "description": "Addition for Saturday work according to collective bargaining agreement.",
          "iconText": "SAT",
          "label": "Saturday addition",
          "moreInfo": "",
          "priceLabel": "Salary amount"
        },
        "subsidisedCommute": {
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "sundayWork": {
          "amountLabel": "",
          "description": "Addition for Sunday work according to collective bargaining agreement.",
          "iconText": "SUN",
          "label": "Sunday or public holiday addition",
          "moreInfo": "",
          "priceLabel": "Salary amount"
        },
        "tesWorktimeShortening": {
          "amountLabel": "Percent",
          "description": "Separate salary portion that is defined by the sector/industry collective bargaining agreement. Aka. \"pekkaspäivät\" in Finnish.",
          "iconText": "CBA",
          "label": "Compensation for accrued time off",
          "moreInfo": "",
          "priceLabel": "Salary amount"
        },
        "toolCompensation": {
          "amountLabel": "Days",
          "description": "Compensation for use of own tools for work. Compensation defined according to collective bargaining agreement.",
          "iconText": "",
          "label": "Work tool compensation",
          "moreInfo": "",
          "priceLabel": "Compensation / day"
        },
        "totalEmployerPayment": {
          "amountLabel": "",
          "description": "Total cost calculation: what a salary payment will cost for the employer.",
          "iconText": "sum",
          "label": "Total cost",
          "moreInfo": "",
          "priceLabel": ""
        },
        "totals": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "Total",
          "moreInfo": "",
          "priceLabel": ""
        },
        "totalWorkerPayment": {
          "amountLabel": "",
          "description": "Net salary calculation: what is left when tax deductions etc. are made from gross salary.",
          "iconText": "net",
          "label": "Net salary",
          "moreInfo": "",
          "priceLabel": ""
        },
        "unionPayment": {
          "amountLabel": "",
          "description": "Employee union payment made directly to trade union, if employer chooses to do so.",
          "iconText": "Trade union",
          "label": "Union payment",
          "moreInfo": "",
          "priceLabel": ""
        },
        "unknown": {
          "amountLabel": "",
          "description": "Row type undefined.",
          "iconText": "",
          "label": "Choose…",
          "moreInfo": "",
          "priceLabel": ""
        },
        "workingTimeCompensation": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        }
      },
      "CalculationRowUnit": {
        "count": {
          "description": "",
          "label": "amount"
        },
        "days": {
          "description": "",
          "label": "days"
        },
        "kilometers": {
          "description": "",
          "label": "km"
        },
        "percent": {
          "description": "",
          "label": "%"
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "CalculationStatus": {
        "draft": {
          "description": "",
          "label": "Draft"
        },
        "externalPaymentCanceled": {
          "description": "",
          "label": "Payment canceled outside of Salaxy-service"
        },
        "externalPaymentError": {
          "description": "",
          "label": "Errors (outside of Salaxy-service)"
        },
        "externalPaymentStarted": {
          "description": "",
          "label": "Payment initiated outside of Salaxy-service"
        },
        "externalPaymentSucceeded": {
          "description": "",
          "label": "Payment completed outside of Salaxy-service"
        },
        "paymentCanceled": {
          "description": "",
          "label": "Payment canceled"
        },
        "paymentError": {
          "description": "",
          "label": "Error in payment"
        },
        "paymentRefunded": {
          "description": "",
          "label": "Payment canceled and refunded"
        },
        "paymentStarted": {
          "description": "",
          "label": "Payment started"
        },
        "paymentSucceeded": {
          "description": "",
          "label": "Payment succeeded"
        },
        "paymentWorkerCopy": {
          "description": "",
          "label": "Copy for employee"
        },
        "payrollDraft": {
          "description": "",
          "label": ""
        },
        "proDraft": {
          "label": ""
        },
        "sharedApproved": {
          "description": "",
          "label": ""
        },
        "sharedRejected": {
          "description": "",
          "label": ""
        },
        "sharedWaiting": {
          "description": "",
          "label": ""
        },
        "template": {
          "description": "",
          "label": "Template"
        },
        "workerRequestAccepted": {
          "description": "",
          "label": "Payslip made by employee accepted"
        },
        "workerRequestDeclined": {
          "description": "",
          "label": "Payslip made by employee rejected"
        },
        "workerRequested": {
          "description": "",
          "label": "Payslip made by employee"
        }
      },
      "CarBenefitCode": {
        "fullCarBenefit": {
          "label": ""
        },
        "limitedCarBenefit": {
          "label": ""
        }
      },
      "CarBenefitKind": {
        "fullCarBenefit": {
          "description": "",
          "label": ""
        },
        "limitedCarBenefit": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "CompanyType": {
        "fiOy": {
          "description": "",
          "label": "Limited companies, also and public limited companies"
        },
        "fiRy": {
          "description": "",
          "label": "Associations"
        },
        "fiTm": {
          "description": "",
          "label": "Private trader or farmer"
        },
        "fiYy": {
          "description": "",
          "label": "Other, e.g. limited partnerships, open companies etc."
        },
        "unknown": {
          "description": "",
          "label": ""
        }
      },
      "ContractPartyType": {
        "customContact": {
          "description": "",
          "label": ""
        },
        "person": {
          "description": "",
          "label": ""
        }
      },
      "DailyAllowanceKind": {
        "fullDailyAllowance": {
          "description": "",
          "label": ""
        },
        "internationalDailyAllowance": {
          "description": "",
          "label": ""
        },
        "mealAllowance": {
          "description": "",
          "label": ""
        },
        "partialDailyAllowance": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "DateOfBirthAccuracy": {
        "ageBased": {
          "description": "",
          "label": "Age Based"
        },
        "ageGroupBased": {
          "description": "",
          "label": "Age Group Based"
        },
        "assumption": {
          "description": "",
          "label": "Assumption"
        },
        "exact": {
          "description": "",
          "label": "Exact date"
        },
        "monthCorrect": {
          "description": "",
          "label": "Month Correct"
        },
        "verified": {
          "description": "",
          "label": "Verified"
        }
      },
      "EmployerType": {
        "fullTimeEmployer": {
          "description": "",
          "label": "Ongoing contract or over 8178 € / 6 months"
        },
        "miniEmployer": {
          "description": "",
          "label": "Under 1300 € per year"
        },
        "smallEmployer": {
          "description": "",
          "label": "1300 € or more per year"
        },
        "tyelOwnContractEmployer": {
          "description": "",
          "label": "TyEL-annual contract or taken care of by customer"
        },
        "unknown": {
          "description": "",
          "label": "Unknown"
        }
      },
      "EmploymentContract": {
        "annualLeave": {
          "label": ""
        },
        "benefitsException": {
          "label": ""
        },
        "noticeException": {
          "label": ""
        },
        "otherTerms": {
          "label": ""
        }
      },
      "EmploymentRelationType": {
        "athlete": {
          "description": "",
          "label": ""
        },
        "boardMember": {
          "description": "",
          "label": ""
        },
        "compensation": {
          "description": "",
          "label": ""
        },
        "employedByStateEmploymentFund": {
          "description": "",
          "label": ""
        },
        "entrepreneur": {
          "description": "",
          "label": ""
        },
        "entrepreneurNew": {
          "description": "",
          "label": ""
        },
        "farmer": {
          "description": "",
          "label": ""
        },
        "hourlySalary": {
          "description": "",
          "label": ""
        },
        "keyEmployee": {
          "description": "",
          "label": ""
        },
        "monthlySalary": {
          "description": "",
          "label": ""
        },
        "performingArtist": {
          "description": "",
          "label": ""
        },
        "salary": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "EmploymentSalaryType": {
        "hourlyPay": {
          "description": "",
          "label": "Hourly salary",
          "metricAbbrev": "/h"
        },
        "montlyPay": {
          "description": "",
          "label": "Monthly salary",
          "metricAbbrev": "/mo"
        },
        "otherPay": {
          "description": "",
          "label": "Other"
        },
        "unknown": {
          "description": "",
          "label": "Choose…"
        }
      },
      "EmploymentTerminationKind": {
        "monetaryWorkingTimeBankCompensation": {
          "description": "",
          "label": ""
        },
        "noticePeriodCompensation": {
          "description": "",
          "label": ""
        },
        "pensionPaidByEmployer": {
          "description": "",
          "label": ""
        },
        "terminationAndLayOffDamages": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        },
        "voluntaryTerminationCompensation": {
          "description": "",
          "label": ""
        }
      },
      "EmploymentType": {
        "fullTime": {
          "description": "",
          "label": "Permanent employment"
        },
        "job": {
          "description": "",
          "label": "One-off work"
        },
        "partTime": {
          "description": "",
          "label": "Part-time employment"
        },
        "unknown": {
          "description": "",
          "label": "Choose…"
        }
      },
      "EmploymentWorkHoursType": {
        "hourPerThreeWeeks": {
          "description": "",
          "label": "Hours/three weeks"
        },
        "hourPerTwoWeeks": {
          "description": "",
          "label": "Hours/two weeks"
        },
        "hoursPerDay": {
          "description": "",
          "label": "Hours/day"
        },
        "hoursPerWeek": {
          "description": "",
          "label": "Hours/week"
        }
      },
      "FrameworkAgreement": {
        "childCare": {
          "description": "Permanent nanny and subsidies.",
          "label": "Other childcare"
        },
        "cleaning": {
          "description": "Cleaning and real estate work.",
          "label": "Cleaning- and real estate sector"
        },
        "construction": {
          "description": "Work according to construction sector colloctive bargaining agreement.",
          "label": "Construction and renovation work"
        },
        "entrepreneur": {
          "description": "Entrepreneurs salary.",
          "label": "Entrepreneur"
        },
        "mll": {
          "description": "Salary for MLL nanny.",
          "label": "MLL recommondations"
        },
        "notDefined": {
          "description": "Collective bargaining agreement or recommendation undefined.",
          "label": "Undefined"
        },
        "other": {
          "description": "Other work och no collective bargaining agreement.",
          "label": "Other work"
        },
        "santaClaus": {
          "description": "Santa Claus home visit.",
          "label": "Santa Claus"
        }
      },
      "Gender": {
        "female": {
          "description": "",
          "label": "Female"
        },
        "male": {
          "description": "",
          "label": "Male"
        },
        "unknown": {
          "description": "",
          "label": ""
        }
      },
      "HelttiIndustry": {
        "i": {
          "description": "",
          "label": "Housing and hopsitality operations"
        },
        "j": {
          "description": "",
          "label": "Information systems and communication"
        },
        "k": {
          "description": "",
          "label": "Finance and insurance operations"
        },
        "m": {
          "description": "",
          "label": "Professionell, vetenskaplig och teknisk verksamhet"
        },
        "n": {
          "description": "",
          "label": "Administrative and support operations"
        },
        "notDefined": {
          "description": "",
          "label": "Choose…"
        },
        "o": {
          "description": "",
          "label": "Public administration and national defense"
        },
        "other": {
          "description": "",
          "label": "Other industry"
        },
        "q": {
          "description": "",
          "label": "Health and social service operations"
        },
        "r": {
          "description": "",
          "label": "Arts, entertainment and recreational operations"
        },
        "s": {
          "description": "",
          "label": "Other service operations"
        }
      },
      "HelttiProductPackage": {
        "large": {
          "description": "",
          "label": "Hifi"
        },
        "medium": {
          "description": "",
          "label": "Growth"
        },
        "notDefined": {
          "description": "",
          "label": "No contract"
        },
        "small": {
          "description": "",
          "label": "Start"
        }
      },
      "HolidayAccrualKind": {
        "calculation": {
          "label": ""
        },
        "holiday": {
          "label": ""
        },
        "holidayCompensated": {
          "label": ""
        },
        "manual": {
          "label": ""
        },
        "saldo": {
          "label": ""
        }
      },
      "HolidayBonusPaymentMethod": {
        "none": {
          "label": ""
        },
        "pay24Days": {
          "label": ""
        },
        "payAllBonus": {
          "label": ""
        },
        "payForHolidaySalary": {
          "label": ""
        },
        "paySummerBonus": {
          "label": ""
        }
      },
      "HolidayCode": {
        "holidayCompensation": {
          "description": "",
          "label": ""
        },
        "holidayCompensationIncluded": {
          "description": "",
          "label": ""
        },
        "noHolidays": {
          "description": "",
          "label": ""
        },
        "permanent14Days": {
          "description": "",
          "label": ""
        },
        "permanent35Hours": {
          "description": "",
          "label": ""
        },
        "temporaryTimeOff": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "HolidayYearInitType": {
        "createEmpty": {
          "label": ""
        },
        "createFromCalcs": {
          "label": ""
        },
        "createMonths": {
          "label": ""
        },
        "ignore": {
          "label": ""
        }
      },
      "HouseHoldUseCaseTree": {
        "childCare": {
          "description": "",
          "kela": {
            "description": "",
            "label": ""
          },
          "label": "",
          "mll": {
            "description": "",
            "label": ""
          },
          "other": {
            "description": "",
            "label": ""
          }
        },
        "cleaning": {
          "51530": {
            "description": "",
            "label": ""
          },
          "53221": {
            "description": "",
            "label": ""
          },
          "61132": {
            "description": "",
            "label": ""
          },
          "91110": {
            "description": "",
            "label": ""
          },
          "description": "",
          "label": "",
          "other": {
            "description": "",
            "label": ""
          }
        },
        "construction": {
          "carpenter": {
            "description": "",
            "label": ""
          },
          "description": "",
          "floor": {
            "description": "",
            "label": ""
          },
          "label": "",
          "new": {
            "description": "",
            "label": ""
          },
          "painter": {
            "description": "",
            "label": ""
          },
          "renovation": {
            "description": "",
            "label": ""
          },
          "roof": {
            "description": "",
            "label": ""
          }
        },
        "other": {
          "51530": {
            "description": "",
            "label": "Building caretakers"
          },
          "53112": {
            "description": "",
            "label": "Private childminders"
          },
          "53212": {
            "description": "",
            "label": "Mental handicap nurses"
          },
          "53213": {
            "description": "",
            "label": "Social work assistants"
          },
          "53221": {
            "description": "",
            "label": "Household service workers"
          },
          "53222": {
            "description": "",
            "label": "Personal care workers, family carers, etc."
          },
          "61112": {
            "description": "",
            "label": "Field crop supervisors and workers"
          },
          "61132": {
            "description": "",
            "label": "Gardeners, horticultural and nursery growers and workers"
          },
          "62100": {
            "description": "",
            "label": "Forestry and related workers"
          },
          "71110": {
            "description": "",
            "label": "House builders"
          },
          "71150": {
            "description": "",
            "label": "Carpenters and joiners"
          },
          "71210": {
            "description": "",
            "label": "Roofers"
          },
          "71310": {
            "description": "",
            "label": "Painters and related workers"
          },
          "91110": {
            "description": "",
            "label": "Domestic cleaners and helpers"
          },
          "description": "",
          "label": "Other",
          "undefined": {
            "description": "",
            "label": "Other"
          }
        }
      },
      "InsuranceCompany": {
        "aktia": {
          "description": "",
          "label": "Aktia"
        },
        "alandia": {
          "description": "",
          "label": "Alandia"
        },
        "aVakuutus": {
          "description": "",
          "label": "A-Vakuutus"
        },
        "fennia": {
          "description": "",
          "label": "Fennia"
        },
        "folksam": {
          "description": "",
          "label": "Folksam"
        },
        "if": {
          "description": "",
          "label": "If"
        },
        "lähiTapiola": {
          "description": "",
          "label": "LähiTapiola"
        },
        "none": {
          "description": "",
          "label": "(Choose insurance company)"
        },
        "other": {
          "description": "",
          "label": "Other company"
        },
        "pohjantähti": {
          "description": "",
          "label": "Pohjantähti"
        },
        "pohjola": {
          "description": "",
          "label": "OP Vakuutus"
        },
        "redarnas": {
          "description": "",
          "label": "Redarnas"
        },
        "tryg": {
          "description": "",
          "label": "Tryg"
        },
        "turva": {
          "description": "",
          "label": "Turva"
        },
        "ålands": {
          "description": "",
          "label": "Ålands"
        }
      },
      "InvoiceStatus": {
        "canceled": {
          "description": "",
          "label": ""
        },
        "error": {
          "description": "",
          "label": ""
        },
        "forecast": {
          "description": "",
          "label": ""
        },
        "paid": {
          "description": "",
          "label": ""
        },
        "paymentStarted": {
          "description": "",
          "label": ""
        },
        "preview": {
          "description": "",
          "label": ""
        },
        "read": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        },
        "unread": {
          "description": "",
          "label": ""
        },
        "waitingConfirmation": {
          "description": "",
          "label": ""
        },
        "waitingPalkkaus": {
          "description": "",
          "label": ""
        }
      },
      "IrFlags": {
        "noMoney": {
          "label": ""
        },
        "oneOff": {
          "label": ""
        },
        "unjustEnrichment": {
          "label": ""
        }
      },
      "IrIncomeTypeKind": {
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "IrInsuranceExceptions": {
        "excludeAccidentInsurance": {
          "label": ""
        },
        "excludeAll": {
          "label": ""
        },
        "excludeHealthInsurance": {
          "label": ""
        },
        "excludePension": {
          "label": ""
        },
        "excludeUnemployment": {
          "label": ""
        },
        "includeAccidentInsurance": {
          "label": ""
        },
        "includeAll": {
          "label": ""
        },
        "includeHealthInsurance": {
          "label": ""
        },
        "includePension": {
          "label": ""
        },
        "includeUnemployment": {
          "label": ""
        }
      },
      "LegalEntityType": {
        "company": {
          "description": "",
          "label": "Business ID"
        },
        "partner": {
          "description": "",
          "label": "Partner"
        },
        "person": {
          "description": "",
          "label": "Person"
        },
        "personCreatedByEmployer": {
          "description": "",
          "label": "Employee"
        },
        "undefined": {
          "description": "",
          "label": "Unknown"
        }
      },
      "MealBenefitKind": {
        "cateringContract": {
          "description": "",
          "label": ""
        },
        "institute": {
          "description": "",
          "label": ""
        },
        "mealAllowance": {
          "description": "",
          "label": ""
        },
        "mealTicket": {
          "description": "",
          "label": ""
        },
        "restaurantWorker": {
          "description": "",
          "label": ""
        },
        "taxableAmount": {
          "description": "",
          "label": ""
        },
        "teacher": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "MessageType": {
        "email": {
          "description": "",
          "label": "E-mail"
        },
        "sms": {
          "description": "",
          "label": "SMS"
        }
      },
      "NonProfitOrgKind": {
        "accomodationAllowance": {
          "description": "",
          "label": ""
        },
        "dailyAllowance": {
          "description": "",
          "label": ""
        },
        "kilometreAllowance": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "OnboardingStatus": {
        "created": {
          "description": "",
          "label": "Created"
        },
        "done": {
          "description": "",
          "label": "Done"
        },
        "opdescription": "",
        "oplabel": "In progress"
      },
      "OtherCompensationKind": {
        "accruedTimeOffCompensation": {
          "description": "",
          "label": ""
        },
        "capitalIncomePayment": {
          "description": "",
          "label": ""
        },
        "employeeInventionCompensation": {
          "description": "",
          "label": ""
        },
        "employeeStockOption": {
          "description": "",
          "label": ""
        },
        "employeeStockOptionWithLowerPrice": {
          "description": "",
          "label": ""
        },
        "lectureFee": {
          "description": "",
          "label": ""
        },
        "meetingFee": {
          "description": "",
          "label": ""
        },
        "membershipOfGoverningBodyCompensation": {
          "description": "",
          "label": ""
        },
        "monetaryGiftForEmployees": {
          "description": "",
          "label": ""
        },
        "otherTaxableIncomeAsEarnedIncome": {
          "description": "",
          "label": ""
        },
        "positionOfTrustCompensation": {
          "description": "",
          "label": ""
        },
        "stockOptionsAndGrants": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        },
        "useCompensationAsCapitalIncome": {
          "description": "",
          "label": ""
        },
        "useCompensationAsEarnedIncome": {
          "description": "",
          "label": ""
        },
        "workEffortBasedDividendsAsNonWage": {
          "description": "",
          "label": ""
        },
        "workEffortBasedDividendsAsWage": {
          "description": "",
          "label": ""
        }
      },
      "PalkkausCampaignPricing": {
        "campaignAfterFirstUse": {
          "description": "",
          "label": ""
        },
        "campaignFirstUse": {
          "description": "",
          "label": ""
        },
        "companyFreeTrial": {
          "description": "",
          "label": ""
        },
        "none": {
          "description": "",
          "label": ""
        }
      },
      "PartnerSite": {
        "careDotCom": {
          "description": "",
          "label": ""
        },
        "dev": {
          "description": "",
          "label": ""
        },
        "duunitori": {
          "description": "",
          "label": ""
        },
        "example": {
          "description": "",
          "label": ""
        },
        "mol": {
          "description": "",
          "label": ""
        },
        "palkkaapakolaindescription": "",
        "palkkaapakolainlabel": "",
        "palkkaus": {
          "description": "",
          "label": ""
        },
        "palkkausEmployerFillIn": {
          "description": "",
          "label": ""
        },
        "palkkausIdentified": {
          "description": "",
          "label": ""
        },
        "raksa": {
          "description": "",
          "label": ""
        },
        "refugeeJobs": {
          "description": "",
          "label": ""
        },
        "talkkarit": {
          "description": "",
          "label": ""
        },
        "unknown": {
          "description": "",
          "label": ""
        }
      },
      "PaymentChannel": {
        "accountorGo": {
          "description": "",
          "label": ""
        },
        "finagoSolo": {
          "description": "",
          "label": ""
        },
        "holviCfa": {
          "description": "",
          "label": ""
        },
        "palkkausCfaFinvoice": {
          "description": "",
          "label": ""
        },
        "palkkausCfaPaytrail": {
          "description": "",
          "label": ""
        },
        "palkkausCfaReference": {
          "description": "",
          "label": ""
        },
        "palkkausManual": {
          "description": "",
          "label": ""
        },
        "palkkausWS": {
          "description": "",
          "label": ""
        },
        "talenomOnline": {
          "description": "",
          "label": ""
        },
        "test": {
          "description": "",
          "label": ""
        },
        "zeroPayment": {
          "description": "",
          "label": ""
        }
      },
      "PaymentStatus": {
        "bankDelivered": {
          "description": "",
          "label": "Approved by bank"
        },
        "bankError": {
          "description": "",
          "label": "Error in bank"
        },
        "bankPartialError": {
          "description": "",
          "label": "Error in bank"
        },
        "bankTechApproval": {
          "description": "",
          "label": "Received by bank"
        },
        "cancelled": {
          "description": "",
          "label": "Payment canceled"
        },
        "new": {
          "description": "",
          "label": "New"
        },
        "paid": {
          "description": "",
          "label": "Payment made"
        },
        "sentToBank": {
          "description": "",
          "label": "Sent to bank"
        },
        "unknown": {
          "description": "",
          "label": "Unknown"
        }
      },
      "PayrollStatus": {
        "draft": {
          "label": "Draft"
        },
        "paymentCanceled": {
          "label": "Payment canceled"
        },
        "paymentError": {
          "label": "Error in payment"
        },
        "paymentStarted": {
          "label": "Payment started"
        },
        "paymentSucceeded": {
          "label": "Payment succeeded"
        },
        "template": {
          "label": "Template"
        }
      },
      "PensionCalculation": {
        "athlete": {
          "description": "",
          "label": ""
        },
        "boardRemuneration": {
          "description": "",
          "label": ""
        },
        "compensation": {
          "description": "",
          "label": ""
        },
        "employee": {
          "description": "",
          "label": ""
        },
        "entrepreneur": {
          "description": "",
          "label": ""
        },
        "farmer": {
          "description": "",
          "label": ""
        },
        "partialOwner": {
          "description": "",
          "label": ""
        },
        "smallEntrepreneur": {
          "description": "",
          "label": ""
        },
        "smallFarmer": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "PensionCompany": {
        "apteekkien": {
          "description": "",
          "label": "Apteekkien Eläkekassa"
        },
        "elo": {
          "description": "",
          "label": "Elo"
        },
        "etera": {
          "description": "",
          "label": "Etera"
        },
        "ilmarindescription": "",
        "ilmarinen": {
          "label": "Ilmarinen"
        },
        "none": {
          "description": "",
          "label": "Choose…"
        },
        "other": {
          "description": "",
          "label": "Other company"
        },
        "pensionsAlandia": {
          "description": "",
          "label": "Alandia"
        },
        "varma": {
          "description": "",
          "label": "Varma"
        },
        "veritas": {
          "description": "",
          "label": "Veritas"
        },
        "verso": {
          "description": "",
          "label": "Verso"
        }
      },
      "PeriodType": {
        "custom": {
          "label": ""
        },
        "month": {
          "label": ""
        },
        "quarter": {
          "label": ""
        },
        "year": {
          "label": ""
        }
      },
      "PricingModel": {
        "fixedFee": {
          "label": ""
        },
        "noFee": {
          "label": ""
        },
        "palkkausFee": {
          "label": ""
        }
      },
      "Product": {
        "accounting": {
          "description": "",
          "label": "",
          "status": ""
        },
        "baseService": {
          "description": "",
          "label": "",
          "status": ""
        },
        "healthCareHeltti": {
          "description": "",
          "label": "",
          "status": ""
        },
        "insurance": {
          "description": "",
          "label": "",
          "status": ""
        },
        "pension": {
          "description": "",
          "label": "",
          "status": ""
        },
        "pensionEntrepreneur": {
          "description": "",
          "label": "",
          "status": ""
        },
        "tax": {
          "description": "",
          "label": "",
          "status": ""
        },
        "taxCard": {
          "description": "",
          "label": "",
          "status": ""
        },
        "unemployment": {
          "description": "",
          "label": "",
          "status": "",
          "statusTVR": ""
        }
      },
      "ProductListFilter": {
        "all": {
          "description": "",
          "label": ""
        },
        "available": {
          "description": "",
          "label": ""
        }
      },
      "Profession": {
        "0": {
          "label": "Armed forces"
        },
        "1": {
          "label": "Managers"
        },
        "2": {
          "label": "Professionals"
        },
        "3": {
          "label": "Technicians and associate professionals"
        },
        "4": {
          "label": "Clerical support workers"
        },
        "5": {
          "label": "Service and sales workers"
        },
        "6": {
          "label": "Skilled agricultural, forestry and fishery workers"
        },
        "7": {
          "label": "Craft and related trades workers"
        },
        "8": {
          "label": "Plant and machine operators, and assemblers"
        },
        "9": {
          "label": "Elementary occupations"
        },
        "11": {
          "label": "Chief executives, senior officials and legislators"
        },
        "12": {
          "label": "Administrative and commercial managers"
        },
        "13": {
          "label": "Production and specialised services managers"
        },
        "14": {
          "label": "Hospitality, retail and other services managers"
        },
        "21": {
          "label": "Science and engineering professionals"
        },
        "22": {
          "label": "Health professionals"
        },
        "23": {
          "label": "Teaching professionals"
        },
        "24": {
          "label": "Business and administration professionals"
        },
        "25": {
          "label": "Information and communications technology professionals"
        },
        "26": {
          "label": "Legal, social and cultural professionals"
        },
        "31": {
          "label": "Science and engineering associate professionals"
        },
        "32": {
          "label": "Health associate professionals"
        },
        "33": {
          "label": "Business and administration associate professionals"
        },
        "34": {
          "label": "Legal, social, cultural and related associate professionals"
        },
        "35": {
          "label": "Information and communications technicians"
        },
        "41": {
          "label": "General and keyboard clerks"
        },
        "42": {
          "label": "Customer services clerks"
        },
        "43": {
          "label": "Numerical and material recording clerks"
        },
        "44": {
          "label": "Other clerical support workers"
        },
        "51": {
          "label": "Personal service workers"
        },
        "52": {
          "label": "Sales workers"
        },
        "53": {
          "label": "Personal care workers"
        },
        "54": {
          "label": "Protective services workers"
        },
        "61": {
          "label": "Market-oriented skilled agricultural workers"
        },
        "62": {
          "label": "Market-oriented skilled forestry, fishery and hunting workers"
        },
        "63": {
          "label": "Subsistence farmers, fishers, hunters and gatherers"
        },
        "71": {
          "label": "Building and related trades workers, excluding electricians"
        },
        "72": {
          "label": "Metal, machinery and related trades workers"
        },
        "73": {
          "label": "Handicraft and printing workers"
        },
        "74": {
          "label": "Electrical and electronic trades workers"
        },
        "75": {
          "label": "Food processing, wood working, garment and other craft and related trades workers"
        },
        "81": {
          "label": "Stationary plant and machine operators"
        },
        "82": {
          "label": "Assemblers"
        },
        "83": {
          "label": "Drivers and mobile plant operators"
        },
        "91": {
          "label": "Cleaners and helpers"
        },
        "92": {
          "label": "Agricultural, forestry and fishery labourers"
        },
        "93": {
          "label": "Labourers in mining, construction, manufacturing and transport"
        },
        "94": {
          "label": "Food preparation assistants"
        },
        "95": {
          "label": "Street and related sales and service workers"
        },
        "96": {
          "label": "Refuse workers and other elementary workers"
        },
        "111": {
          "label": "Legislators and senior officials"
        },
        "112": {
          "label": "Managing directors and chief executives"
        },
        "121": {
          "label": "Business services and administration managers"
        },
        "122": {
          "label": "Sales, marketing and development managers"
        },
        "131": {
          "label": "Production managers in agriculture, forestry and fisheries"
        },
        "132": {
          "label": "Manufacturing, mining, construction, and distribution managers"
        },
        "133": {
          "label": "Information and communications technology service managers"
        },
        "134": {
          "label": "Professional services managers"
        },
        "141": {
          "label": "Hotel and restaurant managers"
        },
        "142": {
          "label": "Retail and wholesale trade managers"
        },
        "143": {
          "label": "Other services managers"
        },
        "211": {
          "label": "Physical and earth science professionals"
        },
        "212": {
          "label": "Mathematicians, actuaries and statisticians"
        },
        "213": {
          "label": "Life science professionals"
        },
        "214": {
          "label": "Engineering professionals (excluding electrotechnology)"
        },
        "215": {
          "label": "Electrotechnology engineers"
        },
        "216": {
          "label": "Architects, planners, surveyors and designers"
        },
        "221": {
          "label": "Medical doctors"
        },
        "222": {
          "label": "Nursing and midwifery professionals"
        },
        "223": {
          "label": "Traditional and complementary medicine professionals"
        },
        "224": {
          "label": "Paramedical practitioners"
        },
        "225": {
          "label": "Veterinarians"
        },
        "226": {
          "label": "Other health professionals"
        },
        "231": {
          "label": "University and higher education teachers"
        },
        "232": {
          "label": "Vocational education teachers"
        },
        "233": {
          "label": "Secondary education teachers"
        },
        "234": {
          "label": "Primary school and early childhood teachers"
        },
        "235": {
          "label": "Other teaching professionals"
        },
        "241": {
          "label": "Finance professionals"
        },
        "242": {
          "label": "Administration professionals"
        },
        "243": {
          "label": "Sales, marketing and public relations professionals"
        },
        "251": {
          "label": "Software and applications developers and analysts"
        },
        "252": {
          "label": "Database and network professionals"
        },
        "261": {
          "label": "Legal professionals"
        },
        "262": {
          "label": "Librarians, archivists and curators"
        },
        "263": {
          "label": "Social and religious professionals"
        },
        "264": {
          "label": "Authors, journalists and linguists"
        },
        "265": {
          "label": "Creative and performing artists"
        },
        "311": {
          "label": "Physical and engineering science technicians"
        },
        "312": {
          "label": "Mining, manufacturing and construction supervisors"
        },
        "313": {
          "label": "Process control technicians"
        },
        "314": {
          "label": "Life science technicians and related associate professionals"
        },
        "315": {
          "label": "Ship and aircraft controllers and technicians"
        },
        "321": {
          "label": "Medical and pharmaceutical technicians"
        },
        "322": {
          "label": "Nursing and midwifery associate professionals"
        },
        "323": {
          "label": "Traditional and complementary medicine associate professionals"
        },
        "324": {
          "label": "Veterinary technicians and assistants"
        },
        "325": {
          "label": "Other health associate professionals"
        },
        "331": {
          "label": "Financial and mathematical associate professionals"
        },
        "332": {
          "label": "Sales and purchasing agents and brokers"
        },
        "333": {
          "label": "Business services agents"
        },
        "334": {
          "label": "Administrative and specialised secretaries"
        },
        "335": {
          "label": "Regulatory government associate professionals"
        },
        "341": {
          "label": "Legal, social and religious associate professionals"
        },
        "342": {
          "label": "Sports and fitness workers"
        },
        "343": {
          "label": "Artistic, cultural and culinary associate professionals"
        },
        "351": {
          "label": "Information and communications technology operations and user support technicians"
        },
        "352": {
          "label": "Telecommunications and broadcasting technicians"
        },
        "411": {
          "label": "General office clerks"
        },
        "412": {
          "label": "Secretaries (general)"
        },
        "413": {
          "label": "Keyboard operators"
        },
        "421": {
          "label": "Tellers, money collectors and related clerks"
        },
        "422": {
          "label": "Client information workers"
        },
        "431": {
          "label": "Numerical clerks"
        },
        "432": {
          "label": "Material-recording and transport clerks"
        },
        "441": {
          "label": "Other clerical support workers"
        },
        "511": {
          "label": "Travel attendants, conductors and guides"
        },
        "512": {
          "label": "Cooks"
        },
        "513": {
          "label": "Waiters and bartenders"
        },
        "514": {
          "label": "Hairdressers, beauticians and related workers"
        },
        "515": {
          "label": "Building and housekeeping supervisors"
        },
        "516": {
          "label": "Other personal services workers"
        },
        "521": {
          "label": "Street and market salespersons"
        },
        "522": {
          "label": "Shop salespersons"
        },
        "523": {
          "label": "Cashiers and ticket clerks"
        },
        "524": {
          "label": "Other sales workers"
        },
        "531": {
          "label": "Child care workers and teachers' aides"
        },
        "532": {
          "label": "Personal care workers in health services"
        },
        "541": {
          "label": "Protective services workers"
        },
        "611": {
          "label": "Market gardeners and crop growers"
        },
        "612": {
          "label": "Animal producers"
        },
        "613": {
          "label": "Mixed crop and animal producers"
        },
        "621": {
          "label": "Forestry and related workers"
        },
        "622": {
          "label": "Fishery workers, hunters and trappers"
        },
        "631": {
          "label": "Subsistence crop farmers"
        },
        "632": {
          "label": "Subsistence livestock farmers"
        },
        "633": {
          "label": "Subsistence mixed crop and livestock farmers"
        },
        "634": {
          "label": "Subsistence fishers, hunters, trappers and gatherers"
        },
        "711": {
          "label": "Building frame and related trades workers"
        },
        "712": {
          "label": "Building finishers and related trades workers"
        },
        "713": {
          "label": "Painters, building structure cleaners and related trades workers"
        },
        "721": {
          "label": "Sheet and structural metal workers, moulders and welders, and related workers"
        },
        "722": {
          "label": "Blacksmiths, toolmakers and related trades workers"
        },
        "723": {
          "label": "Machinery mechanics and repairers"
        },
        "731": {
          "label": "Handicraft workers"
        },
        "732": {
          "label": "Printing trades workers"
        },
        "741": {
          "label": "Electrical equipment installers and repairers"
        },
        "742": {
          "label": "Electronics and telecommunications installers and repairers"
        },
        "751": {
          "label": "Food processing and related trades workers"
        },
        "752": {
          "label": "Wood treaters, cabinet-makers and related trades workers"
        },
        "753": {
          "label": "Garment and related trades workers"
        },
        "754": {
          "label": "Other craft and related workers"
        },
        "811": {
          "label": "Mining and mineral processing plant operators"
        },
        "812": {
          "label": "Metal processing and finishing plant operators"
        },
        "813": {
          "label": "Chemical and photographic products plant and machine operators"
        },
        "814": {
          "label": "Rubber, plastic and paper products machine operators"
        },
        "815": {
          "label": "Textile, fur and leather products machine operators"
        },
        "816": {
          "label": "Food and related products machine operators"
        },
        "817": {
          "label": "Wood processing and papermaking plant operators"
        },
        "818": {
          "label": "Other stationary plant and machine operators"
        },
        "821": {
          "label": "Assemblers"
        },
        "831": {
          "label": "Locomotive engine drivers and related workers"
        },
        "832": {
          "label": "Car, van and motorcycle drivers"
        },
        "833": {
          "label": "Heavy truck and bus drivers"
        },
        "834": {
          "label": "Mobile plant operators"
        },
        "835": {
          "label": "Ships' deck crews and related workers"
        },
        "911": {
          "label": "Domestic, hotel and office cleaners and helpers"
        },
        "912": {
          "label": "Vehicle, window, laundry and other hand cleaning workers"
        },
        "921": {
          "label": "Agricultural, forestry and fishery labourers"
        },
        "931": {
          "label": "Mining and construction labourers"
        },
        "932": {
          "label": "Manufacturing labourers"
        },
        "933": {
          "label": "Transport and storage labourers"
        },
        "941": {
          "label": "Food preparation assistants"
        },
        "951": {
          "label": "Street and related service workers"
        },
        "952": {
          "label": "Street vendors (excluding food)"
        },
        "961": {
          "label": "Refuse workers"
        },
        "962": {
          "label": "Other elementary workers"
        },
        "1111": {
          "label": "Legislators"
        },
        "1112": {
          "label": "Senior government officials"
        },
        "1113": {
          "label": "Traditional chiefs and heads of village"
        },
        "1114": {
          "label": "Senior officials of special-interest organizations"
        },
        "1120": {
          "label": "Managing directors and chief executives"
        },
        "1211": {
          "label": "Finance managers"
        },
        "1212": {
          "label": "Human resource managers"
        },
        "1213": {
          "label": "Policy and planning managers"
        },
        "1219": {
          "label": "Business services and administration managers not elsewhere classified"
        },
        "1221": {
          "label": "Sales and marketing managers"
        },
        "1222": {
          "label": "Advertising and public relations managers"
        },
        "1223": {
          "label": "Research and development managers"
        },
        "1311": {
          "label": "Agricultural and forestry production managers"
        },
        "1312": {
          "label": "Aquaculture and fisheries production managers"
        },
        "1321": {
          "label": "Manufacturing managers"
        },
        "1322": {
          "label": "Mining managers"
        },
        "1323": {
          "label": "Construction managers"
        },
        "1324": {
          "label": "Supply, distribution and related managers"
        },
        "1330": {
          "label": "Information and communications technology service managers"
        },
        "1341": {
          "label": "Child care services managers"
        },
        "1342": {
          "label": "Health services managers"
        },
        "1343": {
          "label": "Aged care services managers"
        },
        "1344": {
          "label": "Social welfare managers"
        },
        "1345": {
          "label": "Education managers"
        },
        "1346": {
          "label": "Financial and insurance services branch managers"
        },
        "1349": {
          "label": "Professional services managers not elsewhere classified"
        },
        "1411": {
          "label": "Hotel managers"
        },
        "1412": {
          "label": "Restaurant managers"
        },
        "1420": {
          "label": "Retail and wholesale trade managers"
        },
        "1431": {
          "label": "Sports, recreation and cultural centre managers"
        },
        "1439": {
          "label": "Services managers not elsewhere classified"
        },
        "2111": {
          "label": "Physicists and astronomers"
        },
        "2112": {
          "label": "Meteorologists"
        },
        "2113": {
          "label": "Chemists"
        },
        "2114": {
          "label": "Geologists and geophysicists"
        },
        "2120": {
          "label": "Mathematicians, actuaries and statisticians"
        },
        "2131": {
          "label": "Biologists, botanists, zoologists and related professionals"
        },
        "2132": {
          "label": "Farming, forestry and fisheries advisers"
        },
        "2133": {
          "label": "Environmental protection professionals"
        },
        "2141": {
          "label": "Industrial and production engineers"
        },
        "2142": {
          "label": "Civil engineers"
        },
        "2143": {
          "label": "Environmental engineers"
        },
        "2144": {
          "label": "Mechanical engineers"
        },
        "2145": {
          "label": "Chemical engineers"
        },
        "2146": {
          "label": "Mining engineers, metallurgists and related professionals"
        },
        "2149": {
          "label": "Engineering professionals not elsewhere classified"
        },
        "2151": {
          "label": "Electrical engineers"
        },
        "2152": {
          "label": "Electronics engineers"
        },
        "2153": {
          "label": "Telecommunications engineers"
        },
        "2161": {
          "label": "Building architects"
        },
        "2162": {
          "label": "Landscape architects"
        },
        "2163": {
          "label": "Product and garment designers"
        },
        "2164": {
          "label": "Town and traffic planners"
        },
        "2165": {
          "label": "Cartographers and surveyors"
        },
        "2166": {
          "label": "Graphic and multimedia designers"
        },
        "2211": {
          "label": "Generalist medical practitioners"
        },
        "2212": {
          "label": "Specialist medical practitioners"
        },
        "2221": {
          "label": "Nursing professionals"
        },
        "2222": {
          "label": "Midwifery professionals"
        },
        "2230": {
          "label": "Traditional and complementary medicine professionals"
        },
        "2240": {
          "label": "Paramedical practitioners"
        },
        "2250": {
          "label": "Veterinarians"
        },
        "2261": {
          "label": "Dentists"
        },
        "2262": {
          "label": "Pharmacists"
        },
        "2263": {
          "label": "Environmental and occupational health and hygiene professionals"
        },
        "2264": {
          "label": "Physiotherapists"
        },
        "2265": {
          "label": "Dieticians and nutritionists"
        },
        "2266": {
          "label": "Audiologists and speech therapists"
        },
        "2267": {
          "label": "Optometrists and ophthalmic opticians"
        },
        "2269": {
          "label": "Health professionals not elsewhere classified"
        },
        "2310": {
          "label": "University and higher education teachers"
        },
        "2320": {
          "label": "Vocational education teachers"
        },
        "2330": {
          "label": "Secondary education teachers"
        },
        "2341": {
          "label": "Primary school teachers"
        },
        "2342": {
          "label": "Early childhood educators"
        },
        "2351": {
          "label": "Education methods specialists"
        },
        "2352": {
          "label": "Special needs teachers"
        },
        "2353": {
          "label": "Other language teachers"
        },
        "2354": {
          "label": "Other music teachers"
        },
        "2355": {
          "label": "Other arts teachers"
        },
        "2356": {
          "label": "Information technology trainers"
        },
        "2359": {
          "label": "Teaching professionals not elsewhere classified"
        },
        "2411": {
          "label": "Accountants"
        },
        "2412": {
          "label": "Financial and investment advisers"
        },
        "2413": {
          "label": "Financial analysts"
        },
        "2421": {
          "label": "Management and organization analysts"
        },
        "2422": {
          "label": "Policy administration professionals"
        },
        "2423": {
          "label": "Personnel and careers professionals"
        },
        "2424": {
          "label": "Training and staff development professionals"
        },
        "2431": {
          "label": "Advertising and marketing professionals"
        },
        "2432": {
          "label": "Public relations professionals"
        },
        "2433": {
          "label": "Technical and medical sales professionals (excluding ICT)"
        },
        "2434": {
          "label": "Information and communications technology sales professionals"
        },
        "2511": {
          "label": "Systems analysts"
        },
        "2512": {
          "label": "Software developers"
        },
        "2513": {
          "label": "Web and multimedia developers"
        },
        "2514": {
          "label": "Applications programmers"
        },
        "2519": {
          "label": "Software and applications developers and analysts not elsewhere classified"
        },
        "2521": {
          "label": "Database designers and administrators"
        },
        "2522": {
          "label": "Systems administrators"
        },
        "2523": {
          "label": "Computer network professionals"
        },
        "2529": {
          "label": "Database and network professionals not elsewhere classified"
        },
        "2611": {
          "label": "Lawyers"
        },
        "2612": {
          "label": "Judges"
        },
        "2619": {
          "label": "Legal professionals not elsewhere classified"
        },
        "2621": {
          "label": "Archivists and curators"
        },
        "2622": {
          "label": "Librarians and related information professionals"
        },
        "2631": {
          "label": "Economists"
        },
        "2632": {
          "label": "Sociologists, anthropologists and related professionals"
        },
        "2633": {
          "label": "Philosophers, historians and political scientists"
        },
        "2634": {
          "label": "Psychologists"
        },
        "2635": {
          "label": "Social work and counselling professionals"
        },
        "2636": {
          "label": "Religious professionals"
        },
        "2641": {
          "label": "Authors and related writers"
        },
        "2642": {
          "label": "Journalists"
        },
        "2643": {
          "label": "Translators, interpreters and other linguists"
        },
        "2651": {
          "label": "Visual artists"
        },
        "2652": {
          "label": "Musicians, singers and composers"
        },
        "2653": {
          "label": "Dancers and choreographers"
        },
        "2654": {
          "label": "Film, stage and related directors and producers"
        },
        "2655": {
          "label": "Actors"
        },
        "2656": {
          "label": "Announcers on radio, television and other media"
        },
        "2659": {
          "label": "Creative and performing artists not elsewhere classified"
        },
        "3111": {
          "label": "Chemical and physical science technicians"
        },
        "3112": {
          "label": "Civil engineering technicians"
        },
        "3113": {
          "label": "Electrical engineering technicians"
        },
        "3114": {
          "label": "Electronics engineering technicians"
        },
        "3115": {
          "label": "Mechanical engineering technicians"
        },
        "3116": {
          "label": "Chemical engineering technicians"
        },
        "3117": {
          "label": "Mining and metallurgical technicians"
        },
        "3118": {
          "label": "Draughtspersons"
        },
        "3119": {
          "label": "Physical and engineering science technicians not elsewhere classified"
        },
        "3121": {
          "label": "Mining supervisors"
        },
        "3122": {
          "label": "Manufacturing supervisors"
        },
        "3123": {
          "label": "Construction supervisors"
        },
        "3131": {
          "label": "Power production plant operators"
        },
        "3132": {
          "label": "Incinerator and water treatment plant operators"
        },
        "3133": {
          "label": "Chemical processing plant controllers"
        },
        "3134": {
          "label": "Petroleum and natural gas refining plant operators"
        },
        "3135": {
          "label": "Metal production process controllers"
        },
        "3139": {
          "label": "Process control technicians not elsewhere classified"
        },
        "3141": {
          "label": "Life science technicians (excluding medical)"
        },
        "3142": {
          "label": "Agricultural technicians"
        },
        "3143": {
          "label": "Forestry technicians"
        },
        "3151": {
          "label": "Ships' engineers"
        },
        "3152": {
          "label": "Ships' deck officers and pilots"
        },
        "3153": {
          "label": "Aircraft pilots and related associate professionals"
        },
        "3154": {
          "label": "Air traffic controllers"
        },
        "3155": {
          "label": "Air traffic safety electronics technicians"
        },
        "3211": {
          "label": "Medical imaging and therapeutic equipment technicians"
        },
        "3212": {
          "label": "Medical and pathology laboratory technicians"
        },
        "3213": {
          "label": "Pharmaceutical technicians and assistants"
        },
        "3214": {
          "label": "Medical and dental prosthetic technicians"
        },
        "3221": {
          "label": "Nursing associate professionals"
        },
        "3222": {
          "label": "Midwifery associate professionals"
        },
        "3230": {
          "label": "Traditional and complementary medicine associate professionals"
        },
        "3240": {
          "label": "Veterinary technicians and assistants"
        },
        "3251": {
          "label": "Dental assistants and therapists"
        },
        "3252": {
          "label": "Medical records and health information technicians"
        },
        "3253": {
          "label": "Community health workers"
        },
        "3254": {
          "label": "Dispensing opticians"
        },
        "3255": {
          "label": "Physiotherapy technicians and assistants"
        },
        "3256": {
          "label": "Medical assistants"
        },
        "3257": {
          "label": "Environmental and occupational health inspectors and associates"
        },
        "3258": {
          "label": "Ambulance workers"
        },
        "3259": {
          "label": "Health associate professionals not elsewhere classified"
        },
        "3311": {
          "label": "Securities and finance dealers and brokers"
        },
        "3312": {
          "label": "Credit and loans officers"
        },
        "3313": {
          "label": "Accounting associate professionals"
        },
        "3314": {
          "label": "Statistical, mathematical and related associate professionals"
        },
        "3315": {
          "label": "Valuers and loss assessors"
        },
        "3321": {
          "label": "Insurance representatives"
        },
        "3322": {
          "label": "Commercial sales representatives"
        },
        "3323": {
          "label": "Buyers"
        },
        "3324": {
          "label": "Trade brokers"
        },
        "3331": {
          "label": "Clearing and forwarding agents"
        },
        "3332": {
          "label": "Conference and event planners"
        },
        "3333": {
          "label": "Employment agents and contractors"
        },
        "3334": {
          "label": "Real estate agents and property managers"
        },
        "3339": {
          "label": "Business services agents not elsewhere classified"
        },
        "3341": {
          "label": "Office supervisors"
        },
        "3342": {
          "label": "Legal secretaries"
        },
        "3343": {
          "label": "Administrative and executive secretaries"
        },
        "3344": {
          "label": "Medical secretaries"
        },
        "3351": {
          "label": "Customs and border inspectors"
        },
        "3352": {
          "label": "Government tax and excise officials"
        },
        "3353": {
          "label": "Government social benefits officials"
        },
        "3354": {
          "label": "Government licensing officials"
        },
        "3355": {
          "label": "Police inspectors and detectives"
        },
        "3359": {
          "label": "Regulatory government associate professionals not elsewhere classified"
        },
        "3411": {
          "label": "Legal and Related Associate Professionals"
        },
        "3412": {
          "label": "Social work associate professionals"
        },
        "3413": {
          "label": "Religious associate professionals"
        },
        "3421": {
          "label": "Athletes and sports players"
        },
        "3422": {
          "label": "Sports coaches, instructors and officials"
        },
        "3423": {
          "label": "Fitness and recreation instructors and program leaders"
        },
        "3431": {
          "label": "Photographers"
        },
        "3432": {
          "label": "Interior designers and decorators"
        },
        "3433": {
          "label": "Gallery, museum and library technicians"
        },
        "3434": {
          "label": "Chefs"
        },
        "3435": {
          "label": "Other artistic and cultural associate professionals"
        },
        "3511": {
          "label": "Information and communications technology operations technicians"
        },
        "3512": {
          "label": "Information and communications technology user support technicians"
        },
        "3513": {
          "label": "Computer network and systems technicians"
        },
        "3514": {
          "label": "Web technicians"
        },
        "3521": {
          "label": "Broadcasting and audio-visual technicians"
        },
        "3522": {
          "label": "Telecommunications engineering technicians"
        },
        "4110": {
          "label": "General office clerks"
        },
        "4120": {
          "label": "Secretaries (general)"
        },
        "4131": {
          "label": "Typists and word processing operators"
        },
        "4132": {
          "label": "Data entry clerks"
        },
        "4211": {
          "label": "Bank tellers and related clerks"
        },
        "4212": {
          "label": "Bookmakers, croupiers and related gaming workers"
        },
        "4213": {
          "label": "Pawnbrokers and money-lenders"
        },
        "4214": {
          "label": "Debt-collectors and related workers"
        },
        "4221": {
          "label": "Travel consultants and clerks"
        },
        "4222": {
          "label": "Contact centre information clerks"
        },
        "4223": {
          "label": "Telephone switchboard operators"
        },
        "4224": {
          "label": "Hotel receptionists"
        },
        "4225": {
          "label": "Enquiry clerks"
        },
        "4226": {
          "label": "Receptionists (general)"
        },
        "4227": {
          "label": "Survey and market research interviewers"
        },
        "4229": {
          "label": "Client information workers not elsewhere classified"
        },
        "4311": {
          "label": "Accounting and bookkeeping clerks"
        },
        "4312": {
          "label": "Statistical, finance and insurance clerks"
        },
        "4313": {
          "label": "Payroll clerks"
        },
        "4321": {
          "label": "Stock clerks"
        },
        "4322": {
          "label": "Production clerks"
        },
        "4323": {
          "label": "Transport clerks"
        },
        "4411": {
          "label": "Library clerks"
        },
        "4412": {
          "label": "Mail carriers and sorting clerks"
        },
        "4413": {
          "label": "Coding, proof-reading and related clerks"
        },
        "4414": {
          "label": "Scribes and related workers"
        },
        "4415": {
          "label": "Filing and copying clerks"
        },
        "4416": {
          "label": "Personnel clerks"
        },
        "4419": {
          "label": "Clerical support workers not elsewhere classified"
        },
        "5111": {
          "label": "Travel attendants and travel stewards"
        },
        "5112": {
          "label": "Transport conductors"
        },
        "5113": {
          "label": "Travel guides"
        },
        "5120": {
          "label": "Cooks"
        },
        "5131": {
          "label": "Waiters"
        },
        "5132": {
          "label": "Bartenders"
        },
        "5141": {
          "label": "Hairdressers"
        },
        "5142": {
          "label": "Beauticians and related workers"
        },
        "5151": {
          "label": "Cleaning and housekeeping supervisors in offices, hotels and other establishments"
        },
        "5152": {
          "label": "Domestic housekeepers"
        },
        "5153": {
          "label": "Building caretakers"
        },
        "5161": {
          "label": "Astrologers, fortune-tellers and related workers"
        },
        "5162": {
          "label": "Companions and valets"
        },
        "5163": {
          "label": "Undertakers and embalmers"
        },
        "5164": {
          "label": "Pet groomers and animal care workers"
        },
        "5165": {
          "label": "Driving instructors"
        },
        "5169": {
          "label": "Personal services workers not elsewhere classified"
        },
        "5211": {
          "label": "Stall and market salespersons"
        },
        "5212": {
          "label": "Street food salespersons"
        },
        "5221": {
          "label": "Shop keepers"
        },
        "5222": {
          "label": "Shop supervisors"
        },
        "5223": {
          "label": "Shop sales assistants"
        },
        "5230": {
          "label": "Cashiers and ticket clerks"
        },
        "5241": {
          "label": "Fashion and other models"
        },
        "5242": {
          "label": "Sales demonstrators"
        },
        "5243": {
          "label": "Door to door salespersons"
        },
        "5244": {
          "label": "Contact centre salespersons"
        },
        "5245": {
          "label": "Service station attendants"
        },
        "5246": {
          "label": "Food service counter attendants"
        },
        "5249": {
          "label": "Sales workers not elsewhere classified"
        },
        "5311": {
          "label": "Child care workers"
        },
        "5312": {
          "label": "Teachers' aides"
        },
        "5321": {
          "label": "Health care assistants"
        },
        "5322": {
          "label": "Home-based personal care workers"
        },
        "5329": {
          "label": "Personal care workers in health services not elsewhere classified"
        },
        "5411": {
          "label": "Fire-fighters"
        },
        "5412": {
          "label": "Police officers"
        },
        "5413": {
          "label": "Prison guards"
        },
        "5414": {
          "label": "Security guards"
        },
        "5419": {
          "label": "Protective services workers not elsewhere classified"
        },
        "6111": {
          "label": "Field crop and vegetable growers"
        },
        "6112": {
          "label": "Tree and shrub crop growers"
        },
        "6113": {
          "label": "Gardeners, horticultural and nursery growers"
        },
        "6114": {
          "label": "Mixed crop growers"
        },
        "6121": {
          "label": "Livestock and dairy producers"
        },
        "6122": {
          "label": "Poultry producers"
        },
        "6123": {
          "label": "Apiarists and sericulturists"
        },
        "6129": {
          "label": "Animal producers not elsewhere classified"
        },
        "6130": {
          "label": "Mixed crop and animal producers"
        },
        "6210": {
          "label": "Forestry and related workers"
        },
        "6221": {
          "label": "Aquaculture workers"
        },
        "6222": {
          "label": "Inland and coastal waters fishery workers"
        },
        "6223": {
          "label": "Deep-sea fishery workers"
        },
        "6224": {
          "label": "Hunters and trappers"
        },
        "6310": {
          "label": "Subsistence crop farmers"
        },
        "6320": {
          "label": "Subsistence livestock farmers"
        },
        "6330": {
          "label": "Subsistence mixed crop and livestock farmers"
        },
        "6340": {
          "label": "Subsistence fishers, hunters, trappers and gatherers"
        },
        "7111": {
          "label": "House builders"
        },
        "7112": {
          "label": "Bricklayers and related workers"
        },
        "7113": {
          "label": "Stonemasons, stone cutters, splitters and carvers"
        },
        "7114": {
          "label": "Concrete placers, concrete finishers and related workers"
        },
        "7115": {
          "label": "Carpenters and joiners"
        },
        "7119": {
          "label": "Building frame and related trades workers not elsewhere classified"
        },
        "7121": {
          "label": "Roofers"
        },
        "7122": {
          "label": "Floor layers and tile setters"
        },
        "7123": {
          "label": "Plasterers"
        },
        "7124": {
          "label": "Insulation workers"
        },
        "7125": {
          "label": "Glaziers"
        },
        "7126": {
          "label": "Plumbers and pipe fitters"
        },
        "7127": {
          "label": "Air conditioning and refrigeration mechanics"
        },
        "7131": {
          "label": "Painters and related workers"
        },
        "7132": {
          "label": "Spray painters and varnishers"
        },
        "7133": {
          "label": "Building structure cleaners"
        },
        "7211": {
          "label": "Metal moulders and coremakers"
        },
        "7212": {
          "label": "Welders and flamecutters"
        },
        "7213": {
          "label": "Sheet-metal workers"
        },
        "7214": {
          "label": "Structural-metal preparers and erectors"
        },
        "7215": {
          "label": "Riggers and cable splicers"
        },
        "7221": {
          "label": "Blacksmiths, hammersmiths and forging press workers"
        },
        "7222": {
          "label": "Toolmakers and related workers"
        },
        "7223": {
          "label": "Metal working machine tool setters and operators"
        },
        "7224": {
          "label": "Metal polishers, wheel grinders and tool sharpeners"
        },
        "7231": {
          "label": "Motor vehicle mechanics and repairers"
        },
        "7232": {
          "label": "Aircraft engine mechanics and repairers"
        },
        "7233": {
          "label": "Agricultural and industrial machinery mechanics and repairers"
        },
        "7234": {
          "label": "Bicycle and related repairers"
        },
        "7311": {
          "label": "Precision-instrument makers and repairers"
        },
        "7312": {
          "label": "Musical instrument makers and tuners"
        },
        "7313": {
          "label": "Jewellery and precious-metal workers"
        },
        "7314": {
          "label": "Potters and related workers"
        },
        "7315": {
          "label": "Glass makers, cutters, grinders and finishers"
        },
        "7316": {
          "label": "Sign writers, decorative painters, engravers and etchers"
        },
        "7317": {
          "label": "Handicraft workers in wood, basketry and related materials"
        },
        "7318": {
          "label": "Handicraft workers in textile, leather and related materials"
        },
        "7319": {
          "label": "Handicraft workers not elsewhere classified"
        },
        "7321": {
          "label": "Pre-press technicians"
        },
        "7322": {
          "label": "Printers"
        },
        "7323": {
          "label": "Print finishing and binding workers"
        },
        "7411": {
          "label": "Building and related electricians"
        },
        "7412": {
          "label": "Electrical mechanics and fitters"
        },
        "7413": {
          "label": "Electrical line installers and repairers"
        },
        "7421": {
          "label": "Electronics mechanics and servicers"
        },
        "7422": {
          "label": "Information and communications technology installers and servicers"
        },
        "7511": {
          "label": "Butchers, fishmongers and related food preparers"
        },
        "7512": {
          "label": "Bakers, pastry-cooks and confectionery makers"
        },
        "7513": {
          "label": "Dairy-products makers"
        },
        "7514": {
          "label": "Fruit, vegetable and related preservers"
        },
        "7515": {
          "label": "Food and beverage tasters and graders"
        },
        "7516": {
          "label": "Tobacco preparers and tobacco products makers"
        },
        "7521": {
          "label": "Wood treaters"
        },
        "7522": {
          "label": "Cabinet-makers and related workers"
        },
        "7523": {
          "label": "Woodworking-machine tool setters and operators"
        },
        "7531": {
          "label": "Tailors, dressmakers, furriers and hatters"
        },
        "7532": {
          "label": "Garment and related pattern-makers and cutters"
        },
        "7533": {
          "label": "Sewing, embroidery and related workers"
        },
        "7534": {
          "label": "Upholsterers and related workers"
        },
        "7535": {
          "label": "Pelt dressers, tanners and fellmongers"
        },
        "7536": {
          "label": "Shoemakers and related workers"
        },
        "7541": {
          "label": "Underwater divers"
        },
        "7542": {
          "label": "Shotfirers and blasters"
        },
        "7543": {
          "label": "Product graders and testers (excluding foods and beverages)"
        },
        "7544": {
          "label": "Fumigators and other pest and weed controllers"
        },
        "7549": {
          "label": "Craft and related workers not elsewhere classified"
        },
        "8111": {
          "label": "Miners and quarriers"
        },
        "8112": {
          "label": "Mineral and stone processing plant operators"
        },
        "8113": {
          "label": "Well drillers and borers and related workers"
        },
        "8114": {
          "label": "Cement, stone and other mineral products machine operators"
        },
        "8121": {
          "label": "Metal processing plant operators"
        },
        "8122": {
          "label": "Metal finishing, plating and coating machine operators"
        },
        "8131": {
          "label": "Chemical products plant and machine operators"
        },
        "8132": {
          "label": "Photographic products machine operators"
        },
        "8141": {
          "label": "Rubber products machine operators"
        },
        "8142": {
          "label": "Plastic products machine operators"
        },
        "8143": {
          "label": "Paper products machine operators"
        },
        "8151": {
          "label": "Fibre preparing, spinning and winding machine operators"
        },
        "8152": {
          "label": "Weaving and knitting machine operators"
        },
        "8153": {
          "label": "Sewing machine operators"
        },
        "8154": {
          "label": "Bleaching, dyeing and fabric cleaning machine operators"
        },
        "8155": {
          "label": "Fur and leather preparing machine operators"
        },
        "8156": {
          "label": "Shoemaking and related machine operators"
        },
        "8157": {
          "label": "Laundry machine operators"
        },
        "8159": {
          "label": "Textile, fur and leather products machine operators not elsewhere classified"
        },
        "8160": {
          "label": "Food and related products machine operators"
        },
        "8171": {
          "label": "Pulp and papermaking plant operators"
        },
        "8172": {
          "label": "Wood processing plant operators"
        },
        "8181": {
          "label": "Glass and ceramics plant operators"
        },
        "8182": {
          "label": "Steam engine and boiler operators"
        },
        "8183": {
          "label": "Packing, bottling and labelling machine operators"
        },
        "8189": {
          "label": "Stationary plant and machine operators not elsewhere classified"
        },
        "8211": {
          "label": "Mechanical machinery assemblers"
        },
        "8212": {
          "label": "Electrical and electronic equipment assemblers"
        },
        "8219": {
          "label": "Assemblers not elsewhere classified"
        },
        "8311": {
          "label": "Locomotive engine drivers"
        },
        "8312": {
          "label": "Railway brake, signal and switch operators"
        },
        "8321": {
          "label": "Motorcycle drivers"
        },
        "8322": {
          "label": "Car, taxi and van drivers"
        },
        "8331": {
          "label": "Bus and tram drivers"
        },
        "8332": {
          "label": "Heavy truck and lorry drivers"
        },
        "8341": {
          "label": "Mobile farm and forestry plant operators"
        },
        "8342": {
          "label": "Earthmoving and related plant operators"
        },
        "8343": {
          "label": "Crane, hoist and related plant operators"
        },
        "8344": {
          "label": "Lifting truck operators"
        },
        "8350": {
          "label": "Ships' deck crews and related workers"
        },
        "9111": {
          "label": "Domestic cleaners and helpers"
        },
        "9112": {
          "label": "Cleaners and helpers in offices, hotels and other establishments"
        },
        "9121": {
          "label": "Hand launderers and pressers"
        },
        "9122": {
          "label": "Vehicle cleaners"
        },
        "9123": {
          "label": "Window cleaners"
        },
        "9129": {
          "label": "Other cleaning workers"
        },
        "9211": {
          "label": "Crop farm labourers"
        },
        "9212": {
          "label": "Livestock farm labourers"
        },
        "9213": {
          "label": "Mixed crop and livestock farm labourers"
        },
        "9214": {
          "label": "Garden and horticultural labourers"
        },
        "9215": {
          "label": "Forestry labourers"
        },
        "9216": {
          "label": "Fishery and aquaculture labourers"
        },
        "9311": {
          "label": "Mining and quarrying labourers"
        },
        "9312": {
          "label": "Civil engineering labourers"
        },
        "9313": {
          "label": "Building construction labourers"
        },
        "9321": {
          "label": "Hand packers"
        },
        "9329": {
          "label": "Manufacturing labourers not elsewhere classified"
        },
        "9331": {
          "label": "Hand and pedal vehicle drivers"
        },
        "9332": {
          "label": "Drivers of animal-drawn vehicles and machinery"
        },
        "9333": {
          "label": "Freight handlers"
        },
        "9334": {
          "label": "Shelf fillers"
        },
        "9411": {
          "label": "Fast food preparers"
        },
        "9412": {
          "label": "Kitchen helpers"
        },
        "9510": {
          "label": "Street and related service workers"
        },
        "9520": {
          "label": "Street vendors (excluding food)"
        },
        "9611": {
          "label": "Garbage and recycling collectors"
        },
        "9612": {
          "label": "Refuse sorters"
        },
        "9613": {
          "label": "Sweepers and related labourers"
        },
        "9621": {
          "label": "Messengers, package deliverers and luggage porters"
        },
        "9622": {
          "label": "Odd job persons"
        },
        "9623": {
          "label": "Meter readers and vending-machine collectors"
        },
        "9624": {
          "label": "Water and firewood collectors"
        },
        "9629": {
          "label": "Elementary workers not elsewhere classified"
        },
        "11121": {
          "label": "Senior central government officials"
        },
        "11122": {
          "label": "Senior district and local government officials and senior officials"
        },
        "11141": {
          "label": "Senior officials of employers', workers' and other economic-interest organisations"
        },
        "11142": {
          "label": "Senior officials of humanitarian and other special-interest organisations"
        },
        "21321": {
          "label": "Agronomists and fishery professionals"
        },
        "21322": {
          "label": "Forestry professionals"
        },
        "21531": {
          "label": "Telecommunications researchers"
        },
        "21532": {
          "label": "Information technology researchers"
        },
        "22121": {
          "label": "Senior physicians"
        },
        "22122": {
          "label": "Specialists"
        },
        "22211": {
          "label": "Matrons"
        },
        "22212": {
          "label": "Ward sisters"
        },
        "23101": {
          "label": "Professors"
        },
        "23102": {
          "label": "Lecturers and senior assistants (university)"
        },
        "23103": {
          "label": "Assistants and part-time lecturers (university)"
        },
        "23104": {
          "label": "Head teachers (polytechnics)"
        },
        "23105": {
          "label": "Lecturers (polytechnics)"
        },
        "23106": {
          "label": "Part-time lecturers, etc. (polytechnics)"
        },
        "23301": {
          "label": "Teachers in mathematical subjects"
        },
        "23302": {
          "label": "Native language and literature teachers"
        },
        "23303": {
          "label": "Crafts and art teachers"
        },
        "23304": {
          "label": "Other secondary education teaching professionals"
        },
        "23411": {
          "label": "Class teachers in primary education"
        },
        "23412": {
          "label": "Subject teachers in primary education"
        },
        "23591": {
          "label": "Career counsellors"
        },
        "23592": {
          "label": "Other teaching professionals"
        },
        "26211": {
          "label": "Archivists"
        },
        "26212": {
          "label": "Curators"
        },
        "26351": {
          "label": "Social workers, etc."
        },
        "26352": {
          "label": "Social planners, etc."
        },
        "26421": {
          "label": "Managing editors and subeditors"
        },
        "26422": {
          "label": "Journalists"
        },
        "26423": {
          "label": "Radio and television journalists"
        },
        "31121": {
          "label": "Building construction technicians"
        },
        "31122": {
          "label": "Civil engineering technicians"
        },
        "31123": {
          "label": "Cartographic and surveying technicians"
        },
        "31521": {
          "label": "Large ships' officers and mates"
        },
        "31522": {
          "label": "Small ships' officers"
        },
        "31523": {
          "label": "Harbour traffic controllers and harbour masters"
        },
        "32141": {
          "label": "Dental technicians"
        },
        "32142": {
          "label": "Prosthetist-orthotists"
        },
        "32211": {
          "label": "Nurses"
        },
        "32212": {
          "label": "Public health nurses"
        },
        "32591": {
          "label": "Occupational therapists"
        },
        "32592": {
          "label": "Other therapists"
        },
        "33341": {
          "label": "Estate agents"
        },
        "33342": {
          "label": "Property managers"
        },
        "34111": {
          "label": "Legal and related business associate professionals"
        },
        "34112": {
          "label": "Agents, officials and other organisation associate professionals"
        },
        "34113": {
          "label": "Consumer advisers, etc."
        },
        "34121": {
          "label": "Social instructors"
        },
        "34122": {
          "label": "Youth leaders (not parishes)"
        },
        "34123": {
          "label": "Work and craft leaders"
        },
        "34131": {
          "label": "Deacons and deaconesses"
        },
        "34139": {
          "label": "Other religious associate professionals"
        },
        "34351": {
          "label": "Producer's assistants and other stagecraft associate professionals"
        },
        "34359": {
          "label": "Other artistic and cultural associate professionals"
        },
        "42291": {
          "label": "Emergency officers"
        },
        "42299": {
          "label": "Other client information clerks not elsewhere classified"
        },
        "43231": {
          "label": "Rail traffic controllers, traffic co-ordinators, etc."
        },
        "43239": {
          "label": "Other transport clerks"
        },
        "44121": {
          "label": "Mail carriers"
        },
        "44122": {
          "label": "Porters"
        },
        "51201": {
          "label": "Cooks"
        },
        "51202": {
          "label": "Restaurant services supervisors and shift managers"
        },
        "51631": {
          "label": "Undertakers and related funeral workers"
        },
        "51632": {
          "label": "Other funeral workers"
        },
        "53111": {
          "label": "Childminders in kindergartens and other institutions"
        },
        "53112": {
          "label": "Private childminders"
        },
        "53113": {
          "label": "Children's club leaders"
        },
        "53211": {
          "label": "Practical mental nurses"
        },
        "53212": {
          "label": "Mental handicap nurses"
        },
        "53213": {
          "label": "Social work assistants"
        },
        "53219": {
          "label": "Other practical nurses"
        },
        "53221": {
          "label": "Household service workers"
        },
        "53222": {
          "label": "Personal care workers, family carers, etc."
        },
        "53291": {
          "label": "Dental assistants"
        },
        "53292": {
          "label": "Equipment maintenance assistants"
        },
        "53293": {
          "label": "Pharmaceutical assistants"
        },
        "53294": {
          "label": "Massage therapists and practical rehabilitation nurses"
        },
        "61111": {
          "label": "Crop growers"
        },
        "61112": {
          "label": "Field crop supervisors and workers"
        },
        "61131": {
          "label": "Gardeners, horticultural and nursery growers"
        },
        "61132": {
          "label": "Gardeners, horticultural and nursery growers and workers"
        },
        "61211": {
          "label": "Dairy and livestock producers"
        },
        "61212": {
          "label": "Dairy and livestock workers"
        },
        "61213": {
          "label": "Pet producers"
        },
        "61214": {
          "label": "Farm relief workers"
        },
        "61291": {
          "label": "Fur and reindeer producers"
        },
        "61299": {
          "label": "Animal workers not elsewhere classified"
        },
        "62211": {
          "label": "Fish farmers"
        },
        "62212": {
          "label": "Fishery supervisors and workers"
        },
        "74211": {
          "label": "Electronics installers and repairers"
        },
        "74212": {
          "label": "Automation installers and repairers"
        },
        "81821": {
          "label": "Power-production and waste treatment plant workers"
        },
        "81822": {
          "label": "Other plant workers, etc."
        },
        "83441": {
          "label": "Stevedores"
        },
        "83442": {
          "label": "Lifting-truck operators, etc."
        },
        "91121": {
          "label": "Office cleaners, etc."
        },
        "91122": {
          "label": "Hotel cleaners"
        },
        "91123": {
          "label": "Hospital and institutional helpers"
        },
        "91124": {
          "label": "Kindergarten assistants"
        },
        "91129": {
          "label": "Other cleaners not elsewhere classified"
        },
        "01": {
          "label": "Commissioned armed forces officers"
        },
        "011": {
          "label": "Commissioned armed forces officers"
        },
        "0110": {
          "label": "Commissioned armed forces officers"
        },
        "02": {
          "label": "Non-commissioned armed forces officers"
        },
        "021": {
          "label": "Non-commissioned armed forces officers"
        },
        "0210": {
          "label": "Non-commissioned armed forces officers"
        },
        "03": {
          "label": "Armed forces occupations, other ranks"
        },
        "031": {
          "label": "Armded forces occupations, other ranks"
        },
        "0310": {
          "label": "Armed forces occupations, other ranks"
        },
        "#X": {
          "label": "Unknown"
        },
        "#XX": {
          "label": "Unknown"
        },
        "#XXX": {
          "label": "Unknown"
        },
        "#XXXX": {
          "label": "Unknown"
        },
        "#XXXXX": {
          "label": "Unknown"
        }
      },
      "RemunerationKind": {
        "bonusPay": {
          "description": "",
          "label": ""
        },
        "commission": {
          "description": "",
          "label": ""
        },
        "initiativeFee": {
          "description": "",
          "label": ""
        },
        "performanceBonus": {
          "description": "",
          "label": ""
        },
        "profitSharingBonus": {
          "description": "",
          "label": ""
        },
        "shareIssueForEmployees": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "ReportCategory": {
        "all": {
          "description": "",
          "label": ""
        },
        "monthly": {
          "description": "",
          "label": ""
        },
        "yearly": {
          "description": "",
          "label": ""
        }
      },
      "ReportType": {
        "employerReport": {
          "description": "",
          "label": "Employer Report"
        },
        "employmentContract": {
          "description": "",
          "label": "Employment Contract"
        },
        "example": {
          "description": "",
          "label": "Example"
        },
        "householdDeduction": {
          "description": "",
          "label": "Household Deduction"
        },
        "insurance": {
          "description": "",
          "label": "Insurance"
        },
        "monthlyDetails": {
          "description": "",
          "label": "Monthly Details"
        },
        "monthlyLiikekirjuri": {
          "description": "",
          "label": "Monthly Liikekirjuri"
        },
        "monthlyPension": {
          "description": "",
          "label": "Monthly Pension"
        },
        "monthlyRapko": {
          "description": "",
          "label": "Monthly Rapko"
        },
        "paymentReport": {
          "description": "",
          "label": "Payment Report"
        },
        "salarySlip": {
          "description": "",
          "label": "Salary Slip"
        },
        "salarySlipPaid": {
          "description": "",
          "label": "Salary Slip Paid"
        },
        "taxHouseholdDeduction14B": {
          "description": "",
          "label": "Tax Household Deduction14B"
        },
        "taxHouseholdDeduction14BSpouseA": {
          "description": "",
          "label": "Tax Household Deduction14BSpouse A"
        },
        "taxHouseholdDeduction14BSpouseB": {
          "description": "",
          "label": "Tax Household Deduction14BSpouse B"
        },
        "taxMonthly4001": {
          "description": "",
          "label": "Tax Monthly4001"
        },
        "taxYearly7801": {
          "description": "",
          "label": "Tax Yearly7801"
        },
        "undefined": {
          "description": "",
          "label": "Undefined"
        },
        "unemployment": {
          "description": "",
          "label": "Unemployment"
        },
        "yearEndReport": {
          "description": "",
          "label": "Year End Report"
        },
        "yearlyDetails": {
          "description": "",
          "label": "Yearly Details"
        },
        "yearlyWorkerSummary": {
          "description": "",
          "label": "Yearly Worker Summary"
        }
      },
      "SalaryKind": {
        "compensation": {
          "description": "",
          "label": "Compensation",
          "labels": {
            "household": ""
          }
        },
        "fixedSalary": {
          "description": "",
          "label": "Fixed Salary"
        },
        "hourlySalary": {
          "description": "",
          "label": "Hourly Salary"
        },
        "monthlySalary": {
          "description": "",
          "label": "Monthly Salary"
        },
        "salary": {
          "description": "",
          "label": ""
        },
        "totalEmployerPayment": {
          "description": "",
          "label": "Total Employer Payment"
        },
        "totalWorkerPayment": {
          "description": "",
          "label": "Total Worker Payment"
        },
        "undefined": {
          "description": "",
          "label": "Undefined"
        }
      },
      "SalaryPeriod": {
        "fixed": {
          "description": "",
          "label": ""
        },
        "hourly": {
          "description": "",
          "label": ""
        },
        "monthly": {
          "description": "",
          "label": ""
        },
        "notDefined": {
          "description": "",
          "label": ""
        }
      },
      "SalaryPerMonthRange": {
        "noPensionRequired": {
          "description": "",
          "label": "Under 57,10 €"
        },
        "normal": {
          "description": "",
          "label": "57,10 € or over"
        },
        "unknown": {
          "description": "",
          "label": "Unknown"
        }
      },
      "SalaryPerYearRange": {
        "noMonthlyReporting": {
          "description": "",
          "label": "202 - 1500 €"
        },
        "noReportingNecessary": {
          "description": "",
          "label": "2 - 200 €"
        },
        "normal": {
          "description": "",
          "label": "Over 1500 €"
        },
        "zeroSalary": {
          "description": "",
          "label": 1
        }
      },
      "ServiceModel": {
        "partnerOnly": {
          "label": ""
        },
        "shared": {
          "label": ""
        }
      },
      "SubsidisedCommuteKind": {
        "noDeduction": {
          "description": "",
          "label": ""
        },
        "periodicalDeduction": {
          "description": "",
          "label": ""
        },
        "singleDeduction": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "TaxCardIncomeType": {
        "externalSalaries": {
          "description": "",
          "label": "External Salaries"
        },
        "previousEmployerSalaries": {
          "description": "Salaries earned earlier under this year. Mark them here as one total sum in euros.",
          "label": "Salary paid by former employers"
        },
        "salaxyCalculation": {
          "description": "",
          "label": "Salaxy Calculation"
        },
        "unknown": {
          "description": "",
          "label": "Unknown"
        }
      },
      "TaxcardKind": {
        "defaultYearly": {
          "label": ""
        },
        "historical": {
          "label": ""
        },
        "noTaxCard": {
          "label": ""
        },
        "noWithholdingHousehold": {
          "label": ""
        },
        "others": {
          "label": ""
        },
        "replacement": {
          "label": ""
        },
        "undefined": {
          "label": ""
        }
      },
      "TaxcardState": {
        "employerAdded": {
          "label": "Added by employer"
        },
        "expired": {
          "label": "Expired"
        },
        "new": {
          "label": "New"
        },
        "replaced": {
          "label": "Replaced by new"
        },
        "shared": {
          "label": "Shared by Worker"
        },
        "sharedApproved": {
          "label": "Approved by employer"
        },
        "sharedRejected": {
          "label": "Rejected"
        },
        "sharedRejectedWithoutOpen": {
          "label": "Rejected without open"
        },
        "sharedWaiting": {
          "label": "Pending"
        },
        "verifiedVero": {
          "label": "Fetched from Tax authorities"
        }
      },
      "TaxcardValidity": {
        "expired": {
          "label": ""
        },
        "future": {
          "label": ""
        },
        "undefined": {
          "label": ""
        },
        "valid": {
          "label": ""
        },
        "validJanuary": {
          "label": ""
        }
      },
      "TaxDeductionWorkCategories": {
        "carework": {
          "description": "",
          "label": "Nursing and care work"
        },
        "homeImprovement": {
          "description": "",
          "label": "Home improvement work"
        },
        "householdwork": {
          "description": "",
          "label": "Household work"
        },
        "none": {
          "description": "",
          "label": "Choose…"
        },
        "ownPropety": {
          "description": "",
          "label": "Leisure home in own use"
        },
        "relativesProperty": {
          "description": "",
          "label": "Leisure home of parents or relatives"
        }
      },
      "TesSubtype": {
        "constructionCarpenter": {
          "description": "",
          "label": ""
        },
        "constructionFloor": {
          "description": "",
          "label": ""
        },
        "constructionFreeContract": {
          "description": "",
          "label": ""
        },
        "constructionOther": {
          "description": "",
          "label": ""
        },
        "notSelected": {
          "description": "",
          "label": ""
        }
      },
      "TestEnum": {
        "valueEight": {
          "description": "",
          "label": ""
        },
        "valueFive": {
          "description": "",
          "label": ""
        },
        "valueFour": {
          "description": "",
          "label": ""
        },
        "valueNine": {
          "description": "",
          "label": ""
        },
        "valueOne": {
          "description": "",
          "label": ""
        },
        "valueSevdescription": "",
        "valueSevlabel": "",
        "valueSix": {
          "description": "",
          "label": ""
        },
        "valueTdescription": "",
        "valueThree": {
          "description": "",
          "label": ""
        },
        "valueTlabel": "",
        "valueTwo": {
          "description": "",
          "label": ""
        },
        "valueZero": {
          "description": "",
          "label": ""
        }
      },
      "UnionPaymentKind": {
        "other": {
          "description": "",
          "label": ""
        },
        "raksaNormal": {
          "description": "",
          "label": ""
        },
        "raksaUnemploymentOnly": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "UnionPaymentType": {
        "notSelected": {
          "description": "",
          "label": "No membership"
        },
        "other": {
          "description": "",
          "label": "Other union"
        },
        "raksaNormal": {
          "description": "",
          "label": ""
        },
        "raksaUnemploymentOnly": {
          "description": "",
          "label": ""
        }
      },
      "WageBasis": {
        "hourly": {
          "label": ""
        },
        "monthly": {
          "label": ""
        },
        "other": {
          "label": ""
        },
        "performanceBased": {
          "label": ""
        },
        "undefined": {
          "label": ""
        }
      },
      "WebSiteUserRole": {
        "company": {
          "description": "",
          "label": "Entrepreneur"
        },
        "household": {
          "description": "",
          "label": "Employer"
        },
        "none": {
          "description": "",
          "label": "Choose…"
        },
        "worker": {
          "description": "",
          "label": "Employee"
        }
      },
      "WorkingTimeCompensationKind": {
        "emergencyWorkCompensation": {
          "description": "",
          "label": ""
        },
        "eveningShiftAllowance": {
          "description": "",
          "label": ""
        },
        "eveningWorkCompensation": {
          "description": "",
          "label": ""
        },
        "extraWorkPremium": {
          "description": "",
          "label": ""
        },
        "nightShiftCompensation": {
          "description": "",
          "label": ""
        },
        "nightWorkAllowance": {
          "description": "",
          "label": ""
        },
        "otherCompensation": {
          "description": "",
          "label": ""
        },
        "overtimeCompensation": {
          "description": "",
          "label": ""
        },
        "saturdayPay": {
          "description": "",
          "label": ""
        },
        "standByCompensation": {
          "description": "",
          "label": ""
        },
        "sundayWorkCompensation": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        },
        "waitingTimeCompensation": {
          "description": "",
          "label": ""
        },
        "weeklyRestCompensation": {
          "description": "",
          "label": ""
        }
      },
      "YtjCompanyType": {
        "alandFederation": {
          "description": "",
          "label": "Ålandian enterprise"
        },
        "asoAssociation": {
          "description": "",
          "label": "Housing right association"
        },
        "association": {
          "description": "",
          "label": "Non-profit association"
        },
        "condominium": {
          "description": "",
          "label": "Housing company"
        },
        "cooperativeBank": {
          "description": "",
          "label": "Bank cooperative"
        },
        "europeanCooperative": {
          "description": "",
          "label": "European cooperative"
        },
        "europeanCooperativeBank": {
          "description": "",
          "label": "European bank cooperative"
        },
        "federationOfMunicipalitiesEstablishment": {
          "description": "",
          "label": "Federal enterprise"
        },
        "financialAssociation": {
          "description": "",
          "label": "Financial association"
        },
        "forestCareAssociation": {
          "description": "",
          "label": "Forestry association"
        },
        "foundation": {
          "description": "",
          "label": "Foundation"
        },
        "housingCooperative": {
          "description": "",
          "label": "Housing cooperative"
        },
        "hypoAssociation": {
          "description": "",
          "label": "Mortgage society"
        },
        "insuranceAssociation": {
          "description": "",
          "label": "Insurance association"
        },
        "ky": {
          "description": "",
          "label": "Limited partnership"
        },
        "municipalEstablishment": {
          "description": "",
          "label": "Municipial enterprise"
        },
        "mutualInsuranceAssociation": {
          "description": "",
          "label": "Mutual insurance association"
        },
        "noCompanyType": {
          "description": "",
          "label": "No company form"
        },
        "openCompany": {
          "description": "",
          "label": "Open company"
        },
        "osuuskunta": {
          "description": "",
          "label": "Cooperative"
        },
        "otherAssociation": {
          "description": "",
          "label": "Other association"
        },
        "otherFinancialAssociation": {
          "description": "",
          "label": "Other financial association"
        },
        "oy": {
          "description": "",
          "label": "Limited company"
        },
        "privateEntrepreneur": {
          "description": "",
          "label": "Private trader, trade name"
        },
        "reindeerHerdingCooperative": {
          "description": "",
          "label": "Reindeer grazing association"
        },
        "savingsBank": {
          "description": "",
          "label": "Savings bank"
        },
        "specialPurposeAssociation": {
          "description": "",
          "label": "Association based on special legislation"
        },
        "stateEstablishment": {
          "description": "",
          "label": "State-owned enterprise"
        },
        "unknown": {
          "description": "",
          "label": "Unknown company form"
        }
      }
    },
    "UI_TERMS": {
      "add": "Add",
      "addNew": "Add new",
      "addRow": "Add row",
      "areYouSure": "Are you sure?",
      "back": "Back",
      "calculate": "Calculate",
      "calculations": "Calculations",
      "cancel": "Cancel",
      "cancelEditing": "Cancel editing",
      "choose": "Choose…",
      "chooseWorkDateRange": "",
      "chooseWorkDuration": "",
      "clear": "Clear",
      "close": "Close",
      "confirm": "Confirm",
      "copy": "Copy",
      "copyAsNew": "Copy as new",
      "copyAsRequest": "Copy as Request",
      "copyToClipboard": "",
      "dateFormats": {
        "dm": "d.m."
      },
      "dateNotChosen": "date has not been chosen",
      "delete": "Delete",
      "demo": "DEMO",
      "dismiss": "Dismiss",
      "done": "Done",
      "edit": "Edit",
      "gotIt": "Got it",
      "isDeleting": "Deleting...",
      "isSaving": "Saving...",
      "learnMore": "Learn more",
      "loading": "Loading...",
      "login": "Log in",
      "logout": "Log out",
      "logoutAlt": "End current session",
      "makeOrder": "",
      "minCharacters": "Start with at least 3 letters...",
      "monthHalf": "Half a month",
      "monthShort0": "Dec",
      "monthShort1": "Jan",
      "monthShort10": "Oct",
      "monthShort11": "Nov",
      "monthShort12": "Dec",
      "monthShort2": "Feb",
      "monthShort3": "Mar",
      "monthShort4": "Apr",
      "monthShort5": "May",
      "monthShort6": "Jun",
      "monthShort7": "Jul",
      "monthShort8": "Aug",
      "monthShort9": "Sep",
      "moreInfo": "More info",
      "navigation": "Navigation",
      "newCalculation": "New calculation",
      "next": "Next",
      "no": "No",
      "noteRequiredField": "* Required Field",
      "noThanks": "No thanks",
      "ok": "OK",
      "oldVersion": "old version",
      "orChoose": "",
      "otherPeriod": "Other period",
      "pay": "Pay",
      "pleaseWait": "Please wait...",
      "postpone": "Not now",
      "previous": "Previous",
      "printable": "Printable",
      "recalculate": "Recalculate",
      "register": "Register",
      "save": "Save",
      "saveEdited": "Save changes",
      "search": "Search...",
      "searchByName": "Search by name...",
      "select": "Select",
      "selectAlt": "Select...",
      "sendEmail": "Send an email",
      "show": "Show",
      "skip": "Skip",
      "sureToDeleteRecord": "Are you sure to delete this record.",
      "today": "Today",
      "updateCalculation": "Update calculation",
      "updatingCalculation": "Updating calculation...",
      "use": "",
      "week2": "2 weeks",
      "yes": "Yes"
    },
    "VALIDATION": {
      "Files": {
        "ContentTypeError": "",
        "Empty": "",
        "MaxFileSize": "",
        "NoFiles": "",
        "SaveError": ""
      },
      "IllegalWorkerForCompanyType": {
        "description": "",
        "label": ""
      },
      "IncompleteAdvancePayment": {
        "description": "",
        "label": ""
      },
      "IncompleteForeclosure": {
        "description": "",
        "label": ""
      },
      "IncompleteOtherDeductions": {
        "description": "",
        "label": ""
      },
      "IncompletePensionPayment": {
        "description": "",
        "label": ""
      },
      "IncompletePrepaidExpenses": {
        "description": "",
        "label": ""
      },
      "IncompleteUnemploymentInsurancePayment": {
        "description": "",
        "label": ""
      },
      "IncompleteUnionPayment": {
        "description": "",
        "label": ""
      },
      "MissingOccupationCode": {
        "description": "",
        "label": ""
      },
      "NegativeRowTotalNotSupported": {
        "description": "",
        "label": ""
      },
      "Payroll": {
        "Calc": {
          "EmailAddress": {
            "label": ""
          },
          "FirstNameNotEmpty": {
            "label": ""
          },
          "IbanInvalid": {
            "label": ""
          },
          "IbanNotEmpty": {
            "label": ""
          },
          "InvalidStatusForPayroll": {
            "label": ""
          },
          "LastNameNotEmpty": {
            "label": ""
          },
          "PayrollIdConflict": {
            "label": ""
          },
          "SocialSecurityNumber_InTest": {
            "label": ""
          },
          "SocialSecurityNumber": {
            "label": ""
          },
          "TaxCardType": {
            "label": ""
          },
          "TaxPercent": {
            "label": ""
          },
          "TelephoneOrEmailNotEmpty": {
            "label": ""
          },
          "TotalGreaterThan0": {
            "label": ""
          }
        },
        "NoCalculations": {
          "label": ""
        }
      },
      "SharedTaxcard": {
        "description": "There is a new shared taxcard from employee waiting for approval.",
        "label": "Taxcard waiting approval!"
      },
      "ValidationErrors": {
        "maxlength": "",
        "minlength": "",
        "required": "",
        "sxyCompanyIdFi": "",
        "sxyCurrency": "",
        "sxyEmail": "",
        "sxyIban": "",
        "sxyInteger": "",
        "sxyMobilePhone": "",
        "sxyNumber": "",
        "sxyPensionContractNumber": "",
        "sxyPersonalIdFi": "",
        "sxyPostalCodeFi": "",
        "sxySmsVerificationCode": "",
        "sxyTaxPercent": "",
        "sxyTemporaryPensionContractNumber": ""
      }
    }
  }
};
_dictionary["fi"] = {
  "SALAXY": {
    "ENUM": {
      "AbcSection": {
        "association": {
          "description": "",
          "label": "Yhdistyksille"
        },
        "blog": {
          "description": "",
          "label": "Blogi"
        },
        "businessOwner": {
          "description": "",
          "label": "Yrittäjä"
        },
        "documentTemplates": {
          "description": "",
          "label": "Dokumenttipohjat ja lomakkeet"
        },
        "doNotShow": {
          "description": "",
          "label": "Ei näytetä käyttäjälle (vain Admin)"
        },
        "employee": {
          "description": "",
          "label": "Työntekijä"
        },
        "employment": {
          "description": "",
          "label": "Työsuhde ja työsopimus"
        },
        "entrepreneur": {
          "description": "",
          "label": "Yritys tai yhteisö"
        },
        "examples": {
          "description": "",
          "label": "Esimerkkejä palkkaustilanteista"
        },
        "householdEmployer": {
          "description": "",
          "label": "Kotitalous"
        },
        "instructionsAndExamples": {
          "description": "",
          "label": "Käyttöohjeet"
        },
        "insurance": {
          "description": "",
          "label": "Vakuutukset ja eläke"
        },
        "palkkausGeneral": {
          "description": "",
          "label": "Tietoa Palkkaus.fi -palvelusta"
        },
        "palkkausInstructions": {
          "description": "",
          "label": "Käyttöohjeita"
        },
        "personEmployer": {
          "description": "",
          "label": "Kotitalous työnantajana"
        },
        "press": {
          "description": "",
          "label": "Tiedotteet ja materiaali"
        },
        "productLongDescription": {
          "description": "",
          "label": "Tuotekuvaus"
        },
        "salary": {
          "description": "",
          "label": "Palkanlaskenta ja palkanmaksu"
        },
        "undefined": {
          "description": "",
          "label": "Uudet artikkelit / ei määritetty"
        },
        "worker": {
          "description": "",
          "label": "Työntekijälle"
        }
      },
      "AbsenceCauseCode": {
        "accruedHoliday": {
          "label": "Pekkasvapaa"
        },
        "annualLeave": {
          "label": "Vuosiloma"
        },
        "childCareLeave": {
          "label": "Hoitovapaa"
        },
        "childIllness": {
          "label": "Lapsen sairaus tai pakottava perhesyy"
        },
        "illness": {
          "label": "Sairaus"
        },
        "industrialAction": {
          "label": "Lakko tai työsulku"
        },
        "interruptionInWorkProvision": {
          "label": "Työtarjonnan keskeytyminen"
        },
        "jobAlternationLeave": {
          "label": "Vuorotteluvapaa"
        },
        "layOff": {
          "label": "Lomautus"
        },
        "leaveOfAbsence": {
          "label": "Virkavapaa"
        },
        "midWeekHoliday": {
          "label": "Arkipyhä"
        },
        "militaryRefresherTraining": {
          "label": "Kertausharjoitus"
        },
        "militaryService": {
          "label": "Varusmiespalvelus tai siviilipalvelus"
        },
        "occupationalAccident": {
          "label": "Työtapaturma"
        },
        "other": {
          "label": "Muu syy"
        },
        "parentalLeave": {
          "label": "Äitiys-, isyys- ja vanhempainvapaa"
        },
        "partTimeAbsenceDueToRehabilitation": {
          "label": "Osittainen poissaolo kuntoutuksen vuoksi"
        },
        "partTimeChildCareLeave": {
          "label": "Osittainen hoitovapaa"
        },
        "partTimeSickLeave": {
          "label": "Osa-aikainen sairauspoissaolo"
        },
        "personalReason": {
          "label": "Henkilökohtainen muu syy"
        },
        "rehabilitation": {
          "label": "Kuntoutus"
        },
        "specialMaternityLeave": {
          "label": "Erityisäitiysvapaa"
        },
        "studyLeave": {
          "label": "Opintovapaa"
        },
        "training": {
          "label": "Koulutus"
        },
        "unpaidLeave": {
          "label": "Palkaton loma / vapaa"
        }
      },
      "AgeGroupCode": {
        "a": {
          "label": "Ikäryhmä A"
        },
        "b": {
          "label": "Ikäryhmä B"
        },
        "c": {
          "label": "Ikäryhmä C"
        },
        "u": {
          "label": "Ikäryhmä U"
        }
      },
      "AgeRange": {
        "age_15": {
          "description": "",
          "label": "-15v"
        },
        "age16": {
          "description": "",
          "label": "16v"
        },
        "age17": {
          "description": "",
          "label": "17v"
        },
        "age18_52": {
          "description": "",
          "label": "18-52v"
        },
        "age53_62": {
          "description": "",
          "label": "53-62v"
        },
        "age63_64": {
          "description": "",
          "label": "63-64v"
        },
        "age65_67": {
          "description": "",
          "label": "65-67v"
        },
        "age68AndOVer": {
          "description": "",
          "label": "68v-"
        },
        "unknown": {
          "description": "",
          "label": "Ei tiedossa"
        }
      },
      "AllowanceCode": {
        "expensesWorkingAbroad": {
          "label": "Ulkomaan työskentelyyn liittyvät verovapaat korvaukset"
        },
        "fullDailyAllowance": {
          "label": "Kokopäiväraha"
        },
        "internationalDailyAllowance": {
          "label": "Ulkomaan päiväraha"
        },
        "mealAllowance": {
          "label": "Ateriakorvaus"
        },
        "partialDailyAllowance": {
          "label": "Osapäiväraha"
        }
      },
      "AnnualLeavePaymentKind": {
        "draftCalc": {
          "description": "Palkkalaskelman luonnos, jossa lomaan liittyviä maksuja",
          "label": "Palkkalaskelman luonnos"
        },
        "endSaldo": {
          "description": "Seuraavalle lomavuodelle tai maksettavaa",
          "label": "Maksettavaa tai siirrettävää"
        },
        "info": {
          "description": "Lomavuodella ei ole maksuja",
          "label": "Ei rivejä"
        },
        "manualBonus": {
          "description": "Lomaraha maksettuna erikseen maksettuna erillisenä korjauksena",
          "label": "Lomaraha"
        },
        "manualCompensation": {
          "description": "Korvattavat lomapäivät ja niiden korvaus sekä lomaraha manuaalisena korjauksena",
          "label": "Lomakorvaus"
        },
        "manualSalary": {
          "description": "Pidetyt lomapäivät ja niiden palkka sekä lomaraha manuaalisena korjauksena",
          "label": "Loma-ajan palkka"
        },
        "paidCalc": {
          "description": "Palkkaus.fi-palvelussa maksettu palkka, jossa lomaan liittyviä maksuja",
          "label": "Maksettu palkkalaskelma"
        },
        "startSaldo": {
          "description": "Edellisenä vuonna kertyneet lomat",
          "label": "Käytettävissä olevat"
        },
        "total": {
          "description": "Käytetyt/maksetut lomapäivät, -rahat ja korvaukset",
          "label": "Yhteensä"
        },
        "undefined": {
          "description": "Rivin tyyppiä ei ole valittu",
          "label": "Valitse..."
        }
      },
      "ApiTestErrorType": {
        "default": {
          "description": "",
          "label": "Oletus"
        },
        "userFriendly": {
          "description": "",
          "label": "User Friendly"
        }
      },
      "ApiValidationErrorType": {
        "general": {
          "label": "Yleisvirhe"
        },
        "invalid": {
          "label": "Virhe"
        },
        "required": {
          "label": "Arvo puuttuu"
        },
        "warning": {
          "label": "Varoitus"
        }
      },
      "AuthenticationMethod": {
        "auth0Database": {
          "description": "",
          "label": "Sähköpostitunnistus"
        },
        "emailPwdLocal": {
          "description": "",
          "label": "Tietokanta"
        },
        "facebook": {
          "description": "",
          "label": "Facebook -tili"
        },
        "google": {
          "description": "",
          "label": "Google / Gmail authentication"
        },
        "internalLocalhost": {
          "description": "",
          "label": ""
        },
        "linkedIn": {
          "description": "",
          "label": "LinkedIn authentication"
        },
        "microsoft": {
          "description": "",
          "label": "Microsoft -tili"
        },
        "salaxy": {
          "description": "",
          "label": ""
        },
        "test": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": "Tuntematon"
        },
        "x509": {
          "description": "",
          "label": ""
        }
      },
      "AuthorizationStatus": {
        "accessDenied": {
          "description": "",
          "label": "Pääsy kielletty"
        },
        "expiredOauthTokdescription": "",
        "expiredOauthToklabel": "Istunto vanheutunut",
        "invalidOAuthTokdescription": "",
        "invalidOAuthToklabel": "Virhe tunnistuksessa",
        "noOauthTokdescription": "",
        "noOauthToklabel": "Virhe tunnistuksessa (ei tokenia)",
        "oK": {
          "description": "",
          "label": "OK"
        },
        "undefined": {
          "description": "",
          "label": "Ei määritetty"
        }
      },
      "AuthorizationType": {
        "companyContract": {
          "description": "",
          "label": "Työnantajan sopimus"
        },
        "employerAuthorization": {
          "description": "",
          "label": "Valtakirja"
        },
        "manual": {
          "description": "",
          "label": "Valtakirja toimitettu erikseen"
        },
        "none": {
          "description": "",
          "label": "Ei tunnistettu"
        },
        "temporary": {
          "description": "",
          "label": "Temp tunnistus"
        },
        "workerContract": {
          "description": "",
          "label": "Työntekijän sopimus"
        }
      },
      "AvatarPictureType": {
        "gravatar": {
          "description": "",
          "label": "Gravatar"
        },
        "icon": {
          "description": "",
          "label": "Kuvake"
        },
        "uploaded": {
          "description": "",
          "label": "Kuva omalta koneelta"
        }
      },
      "BlobFileType": {
        "authorizationPdf": {
          "description": "",
          "label": "Valtakirja"
        },
        "avatar": {
          "description": "",
          "label": "Ikoni"
        },
        "calcReport": {
          "description": "",
          "label": ""
        },
        "expensesReceipt": {
          "description": "",
          "label": "Kulukuitti"
        },
        "monthlyReport": {
          "description": "",
          "label": "Kuukausiraportti"
        },
        "raksaloki": {
          "description": "",
          "label": "Kulukuitti"
        },
        "taxCard": {
          "description": "",
          "label": "Verokortti"
        },
        "template": {
          "description": "",
          "label": "Kaavain"
        },
        "unknown": {
          "description": "",
          "label": ""
        },
        "yearlyReport": {
          "description": "",
          "label": "Vuosiraportti"
        }
      },
      "BlobRepository": {
        "cdnImages": {
          "description": "",
          "label": "Content Delivery Network (CDN)"
        },
        "fileSystemContent": {
          "description": "",
          "label": "Content in local file system (localhost/dev/test only)"
        },
        "gitContent": {
          "description": "",
          "label": "Content in GIT"
        },
        "payload": {
          "description": "",
          "label": "Payload of large objects"
        },
        "reports": {
          "description": "",
          "label": "Backoffice reports"
        },
        "systemFiles": {
          "description": "",
          "label": "System Files"
        },
        "userFiles": {
          "description": "",
          "label": "User Files"
        },
        "versionHistory": {
          "description": "",
          "label": "Version History"
        }
      },
      "BoardKind": {
        "undefined": {
          "description": "Oletusarvo",
          "label": "Oletusarvo"
        }
      },
      "CalculationFlag": {
        "accidentInsurance": {
          "label": "Työtapaturma- ja ammattitautivakuutusmaksun alainen"
        },
        "cfDeduction": {
          "label": "Vähennetään maksusta asiakasvaratililla"
        },
        "cfDeductionAtSalaxy": {
          "label": "Vähennetään nettomaksusta asiakasvaratililtä työntekijälle"
        },
        "cfNoPayment": {
          "label": "Ei makseta asiakasvaratilin kautta"
        },
        "cfPayment": {
          "label": "Maksetaan asiakasvaratilin kautta"
        },
        "exclude": {
          "label": "Ei käytetä laskelmalla"
        },
        "healthInsurance": {
          "label": "Sairausvakuutusmaksun alainen"
        },
        "noTax": {
          "label": "Ei vaikuta ennakonpidätykseen"
        },
        "pensionInsurance": {
          "label": "Työeläkevakuutusmaksun alainen"
        },
        "tax": {
          "label": "Lasketaan ennakonpidätykseen"
        },
        "taxDeduction": {
          "label": "Vähentää ennakonpidätrystä"
        },
        "unemploymentInsurance": {
          "label": "Työttömyysvakuutusmaksun alainen"
        }
      },
      "CalculationRowSource": {
        "manualRow": {
          "description": "",
          "label": "Lisärivi"
        },
        "salaryPage": {
          "description": "",
          "label": "Pääpalkka"
        },
        "tesSelection": {
          "description": "",
          "label": "Käyttötapaus"
        }
      },
      "CalculationRowType": {
        "accomodationBenefit": {
          "amountLabel": "Kuukausia",
          "description": "Työnantajan maksaman asunnon verotusarvo.",
          "iconText": "as",
          "label": "Asuntoetu",
          "moreInfo": "",
          "priceLabel": "Asuntoetu / kk"
        },
        "advance": {
          "amountLabel": "Määrä",
          "description": "Palkasta on saatettu jo maksaa ennakkoa. Tällöin tämä osuus vähennetään maksettavasta nettomaksusta.",
          "iconText": "adv",
          "label": "Palkkaennakko",
          "moreInfo": "",
          "priceLabel": "Maksettu ennakko"
        },
        "board": {
          "amountLabel": "Määrä",
          "description": "Hallintoelimen jäsenyydestä maksettu palkkio",
          "iconText": "hal",
          "label": "Hallituspalkkio",
          "moreInfo": "Luottamustoimipalkkio, joka maksetaan yhteisön hallituksen tai vastaavan hallintoelimen jäsenyyden perusteella. ",
          "priceLabel": "Hinta"
        },
        "carBenefit": {
          "description": "Autoetu lisätietoineen ja vähennyksineen",
          "iconText": "auto",
          "label": "Autoetu",
          "moreInfo": "Autoedulla tarkoitetaan verotettavaa etua, jonka työntekijä saa, jos saa käyttää työnantajan omistamaa tai hallinnoimaa (esim. leasing-auto) autoa työajojen lisäksi yksityisajoihin.\r\n\r\nAutoetu on palkanlisänä tiukasti säännelty. Sen arvo lasketaan verottajan laskentakaavalla ja sekä arvo että laskennan perusteet tulee ilmoittaa tulorekisteriin. Ilmoitus pitää tehdä myös silloin, jos työntekijä maksaa verotusarvosta korvauksen, jolloin verotettavaa tuloa ei synny. Ilmoitus tehdään lähtökohtaisesti palkkakausittain tai vähintään kuukausittain, jos muuta palkkaa ei makseta. Kilometrikohtaisessa laskennassa ilmoituksen saa vaihtoehtoisesti tehdä vain vuoden lopussa.\r\n\r\nLaskentakaava perustuu kahteen luokkaan: vapaa autoetu ja käyttöetu:\r\n\r\n- Vapaassa autoedussa työnantaja huolehtii kaikista auton käyttöön liittyvistä kustannuksista, kuten polttonesteistä, vakuutuksista, veroista, huolloista ja korjauksista.\r\n- Käyttöedussa auton käyttäjä maksaa yksityisajojensa polttoainekustannukset, sopimuksen mukaan mahd. myös muita kustannuksia.\r\n\r\nLisäksi laskentakaavaan tarvitaan auton hinta, lisävarusteiden hinta sekä ikäryhmä A, B tai C. Tyypillisesti arvo lasketaan kiinteällä 18 000 kilometrin vuosittaisella yksityisajolla. Jos yksityisajot ovat ilmiselvästi suuremmat, tulisi käyttää kilometreihin perustuvaa menetelmää. \r\n\r\nAutoedun verotusarvon laskuri löytyy osoitteesta: https://www.vero.fi/autoetulaskuri\r\n\r\nVaihtoehtoisesti verotusarvon voi laskea kilometripohjaisesti. Tämä on edullisempaa, jos yksityisajon kilometrit ovat vähäiset, mutta erittäin työlästä, koska silloin yksityisajoista tulee pitää ajopäiväkirjaa ja nämä kilometrit pitää raportoida verottajalle. Jos käytetään kilometrikohtaista laskentaa, autoedun voi ilmoittaa vain kerran vuodessa: tyypillisesti vuoden viimeisellä palkkalaskelmalla.",
          "priceLabel": "Autoedun verotusarvo"
        },
        "chainsawReduction": {
          "amountLabel": "",
          "description": "Moottorisahavähennys erikoistapauksena (Ennakkoperintälaki 15 §, Ennakkoperintäasetus 18 §)",
          "iconText": "",
          "label": "Valitse...",
          "moreInfo": "",
          "priceLabel": "Määrä"
        },
        "childCareSubsidy": {
          "amountLabel": "Määrä",
          "description": "KELA:n ja kuntien työntekijälle suoraan maksama tuki lastenhoidosta.",
          "iconText": "KELA",
          "label": "Yksityisen hoidon tuki",
          "moreInfo": "KELA:n maksama yksityisen hoidon tuki vaikuttaa sivukuihin, sillä työnantaja maksaa esim. työeläkkeen myös siltä osin. Tämä summa tulee riveinä työnantajan maksaman palkan päälle.",
          "priceLabel": "KELA maksaa"
        },
        "compensation": {
          "amountLabel": "Määrä",
          "description": "Työstä, tehtävästä tai palveluksesta maksettu korvaus, joka ei ole palkkaa.",
          "iconText": "T",
          "label": "Työkorvaus",
          "moreInfo": "",
          "priceLabel": "Työkorvaus"
        },
        "dailyAllowance": {
          "amountLabel": "Päiviä",
          "description": "Päiväraha: kokopäiväraha (oletus) sekä muut päivärahat",
          "iconText": "pv",
          "label": "Päiväraha",
          "moreInfo": "Päiväraha on korvaus kohtuullisesta ruokailu- ja muiden elinkustannusten lisäyksestä, jotka palkansaajalle aiheutuu työmatkasta. Kuitenkaan näihin kustannuksiin ei kuulu matkustamisesta (liput, kilometrit) eikä majoittumisesta aiheutuvia kustannuksia: Ne maksetaan tyypillisesti erikseen kuittien perusteella tai kilometrikorvauksina.\r\n\r\nVerohallinto määrittää päivärahan enimmäismäärät vuosittain. Usein käytetään näitä enimmäismääriä, mutta tämä ei ole mikään yleispätevä sääntö. Verottoman enimmäismäärän yrittävät summat tulee maksaa verotettavana palkkana tai työkorvauksena.\r\n\r\nPäivärahan saaminen edellyttää työ- tai virkamatkaa, joka suuntautuu yli 15 kilometriä työpaikalta tai kotoa riippuen siitä, kummasta alkaen matka tehdään. Osapäivärahaa maksetaan kotimaassa yli kuusi tuntia ja kokopäivärahaa yli 10 tuntia kestävältä työmatkalta. Päivärahan sijaan on myös mahdollista maksaa ateriakorvausta. Ateriakorvauksen saamisen edellytyksenä on, ettei matka oikeuta päivärahaan tai sitä ei muutoin makseta.",
          "priceLabel": "Euroa / päivä"
        },
        "dailyAllowanceHalf": {
          "amountLabel": "Päiviä",
          "description": "Päiväraha, kun työmatka on yli 6-10 tuntia.",
          "iconText": 19,
          "label": "Osapäiväraha",
          "moreInfo": "Päivärahaa maksetaan 19 euroa, jos työmatka on yli 6 tuntia. Matkakohde pitää olla 15 km päässä joko asunnosta tai varsinaisesta työpaikasta.",
          "priceLabel": "Euroa / päivä"
        },
        "employmentTermination": {
          "amountLabel": "Määrä",
          "description": "Tulorekisterin tulolajeissa oleva työsuhteen päättämiseen liittyvä korvaus",
          "iconText": "off",
          "label": "Työsuhteen päättäminen",
          "moreInfo": "",
          "priceLabel": "Hinta"
        },
        "eveningAddition": {
          "amountLabel": "Tuntia",
          "description": "Korvaus iltatyöstä työsopimuksen tai työehtosopimuksen mukaan.",
          "iconText": "ILTA",
          "label": "Iltavuorolisä",
          "moreInfo": "",
          "priceLabel": "Lisä (per tunti)"
        },
        "expenses": {
          "amountLabel": "Määrä",
          "description": "Kulukorvaukset (esim. taksi, tarvikkeet) maksetaan yleensä kuittien perusteella.",
          "iconText": "kulu",
          "label": "Kulukorvaus",
          "moreInfo": "",
          "priceLabel": "Kulukorvaus"
        },
        "foreclosure": {
          "amountLabel": "Määrä",
          "description": "Työnantajan suoraan ulosottomiehelle maksama ulosottopidätys. Tehdään palkasta maksukiellon perusteella.",
          "iconText": "ulos",
          "label": "Ulosmittaus (työnantaja)",
          "moreInfo": "",
          "priceLabel": "Summa"
        },
        "foreclosureByPalkkaus": {
          "amountLabel": "",
          "description": "Palkkaus.fi-palvelun maksama ulosottopidätys. Tehdään palkasta ulosottomiehen lähettämän maksukiellon perusteella.",
          "iconText": "",
          "label": "Ulosmittaus (Palkkaus.fi)",
          "moreInfo": "",
          "priceLabel": ""
        },
        "holidayBonus": {
          "amountLabel": "Prosentti",
          "description": "Työehtosopimuksissa (TES) määritetty lomaraha (ent. lomaltapaluuraha).",
          "iconText": "TES",
          "label": "Lomaraha",
          "moreInfo": "",
          "priceLabel": "Loma-ajan palkka"
        },
        "holidayCompensation": {
          "amountLabel": "Prosentti",
          "description": "Korvaus loma-ajan palkasta, jos lomaa ei pidetä.",
          "iconText": "11,5%",
          "label": "Lomakorvaus",
          "moreInfo": "",
          "priceLabel": "Palkkasumma"
        },
        "holidaySalary": {
          "amountLabel": "",
          "description": "Palkka loma-ajalta.",
          "iconText": "loma",
          "label": "Loma-ajan palkka",
          "moreInfo": "",
          "priceLabel": "Loma-ajan palkka"
        },
        "hourlySalary": {
          "amountLabel": "Tunteja",
          "description": "Palkanmaksu tehtyjen tuntien perusteella.",
          "iconText": "h",
          "label": "Tuntipalkka",
          "moreInfo": "",
          "priceLabel": "Tuntipalkka"
        },
        "irIncomeType": {
          "amountLabel": "Määrä",
          "description": "Erikseen syötetty tulorekisterin tulolaji, mikä ei sisälly muihin laskelman riveihin.",
          "iconText": "katre",
          "label": "Muu tulorekisterin tulolaji",
          "moreInfo": "Laskelma luo automaattisesti tulorekisterin tulolajit kullekin riville rivin tyypin perusteella. Tällä rivityyppillä voi erikseen syöttää tulorekisterin tulolajin, mikä ei sisälly muihin laskelman riveihin.",
          "priceLabel": "Hinta"
        },
        "mealBenefit": {
          "amountLabel": "Aterioita",
          "description": "Työnantaja työntekijälleen tarjoama ateria. Myös lounassetelit.",
          "iconText": "rav",
          "label": "Ravintoetu",
          "moreInfo": "Ravintoetu on luontoisetu, joka syntyy, kun työnantaja tarjoaa työntekijälleen aterian. Työnantaja voi tukea ruokailukustannuksia ruokailulipukkeella (Lounasseteli) tai muulla kohdennetulla maksuvälineellä taikka järjestämällä työpaikkaruokailun (sopimusruokailu).\r\n\r\nRavintoedun verotusarvo määräytyy Verohallinnon päätöksen mukaisena vuosittain vahvistettavana arvona.\r\n\r\nVaihtoehtona ravintoedulle on ateriakorvaus, joka maksetaan työmatkalla. Sen maksaminen edellyttää, että työmatkasta ei suoriteta päivärahaa ja että palkansaajalla ei työn vuoksi ole mahdollisuutta ruokailutauon aikana aterioida tavanomaisella ruokailupaikallaan. Ateriakorvaus on veroton etu (maksetaan työntekijälle), kun erilaiset ravintoedut ovat luontoisetuja (yritys maksaa kulun ja siitä osa on työntekijältä verotettavaa tuloa).\r\n\r\nJos työntekijän palkanmaksupäivä on esimerkiksi kuukauden puolivälissä, tieto ravintoedun määrästä annetaan tulorekisteriin ennen kuin on varmuutta ravintoedun kokonaismäärästä kyseisenä kuukautena (esimerkiksi mahdolliset poissaolot eivät ole työnantajan tiedossa). Tämän vuoksi on katsottu riittäväksi, että ravintoetu täsmäytetään oikean suuruiseksi viimeistään seuraavalla palkkatietoilmoituksella. \r\n\r\nJoulukuun ravintoedun määrä on korjattava antamalla joulukuulle korvaava ilmoitus, jotta ravintoedun määrä korjaantuu oikealle verovuodelle. Tästä täsmäyksestä Palkkaus.fi antaa erillinen ohjeistuksen ennen vuodenvaihdetta.\r\n\r\nJos ravintoetu toteutetaan saldoon perustuvaa maksuvälinettä käyttäen (esimerkiksi maksukortti), täsmäytys on tehtävä vähintään vuoden viimeisellä palkkatietoilmoituksella.",
          "priceLabel": "Nimellisarvo / kustannus"
        },
        "mealCompensation": {
          "amountLabel": "Päiviä",
          "description": "Ateriakorvauksen voi maksaa työmatkoilta, joilta ei makseta päivärahaa.",
          "iconText": "ater",
          "label": "Ateriakorvaus",
          "moreInfo": "",
          "priceLabel": "Korvaus / päivä"
        },
        "milageDaily": {
          "amountLabel": "Päiviä",
          "description": "Kilometrikorvaus määritetään monissa TES:ssa päiväkohtaisina summina.",
          "iconText": "km",
          "label": "Kilometrikorvaus, päiväkohtainen",
          "moreInfo": "Esim. Rakennusalan työehtosopimuksissa on päiväkohtaiset taulukot, joiden perusteella maksetaan kilometrikorvauksia.",
          "priceLabel": "Korvaus / päivä"
        },
        "milageOther": {
          "amountLabel": "Päiviä",
          "description": "Kilometrikorvaus jollain muulla perusteella.",
          "iconText": "km",
          "label": "Kilometrikorvaus, muu peruste",
          "moreInfo": "",
          "priceLabel": "Korvaus / päivä"
        },
        "milageOwnCar": {
          "amountLabel": "Kilometriä",
          "description": "Kilometrikorvaus oman auton käytöstä.",
          "iconText": "0,43€",
          "label": "Kilometrikorvaus, oma auto",
          "moreInfo": "",
          "priceLabel": "Korvaus / km"
        },
        "monthlySalary": {
          "amountLabel": "Kuukausia",
          "description": "Kiinteä kuukausipalkka.",
          "iconText": "kk",
          "label": "Kuukausipalkka",
          "moreInfo": "",
          "priceLabel": "Kuukausipalkka"
        },
        "nightimeAddition": {
          "amountLabel": "Tuntia",
          "description": "Korvaus yötyöstä työsopimuksen tai työehtosopimuksen mukaan.",
          "iconText": "YÖ",
          "label": "Yövuorolisä",
          "moreInfo": "",
          "priceLabel": "Lisä (per tunti)"
        },
        "nonProfitOrg": {
          "amountLabel": "Määrä",
          "description": "Yleishyödylliset yhteisön vapaaehtoistyöhön osallistuvalle maksama päiväraha tai kilometrikorvaus.",
          "iconText": "NPO",
          "label": "Yleishyödyllisen yhteisön päiväraha ja km-korvaus",
          "moreInfo": "Yleishyödylliset yhteisöt voivat maksaa korvauksettomaan vapaaehtoistyöhön osallistuvalle päivärahaa tai kilometrikorvausta.\r\n\r\nHUOM: Tällä käyttöliittymällä ilmoitetaan vain vapaaehtoistyöhön liittyvät korvaukset. Jos korvaukset liittyvät työsuhteeseen / palkkaan, korvaukset tulee ilmoittaa erikseen kohdissa Kilometrikorvaus ja Päiväraha (kokopäiväraha tai osapäiväraha). Jos yhdistys ei ole verottajan määrittelemällä tavalla yleishyödyllinen, työsuhteen ulkopuoliset korvaukset, myös vapaaehtoistyöstä, tulee tilittää ennakonpidätyksen alaisena työkorvauksena. Vastaanottaja voi sitten vähentää ne omassa verotuksessaan.\r\n\r\nIlmoitettava tulo on määritetty tuloverolain 71 § 3 momentissa: \r\n\r\nPäivärahan voi maksaa enintään 20 päivältä per kalenterivuosi ja lisäksi majoittumiskorvaus (tositteen perusteella).\r\n\r\nKilometrikorvausten osalta voidaan maksaa matkustamiskustannusten korvaus. Toisin kuin tavallisessa kilometrikorvauksessa, tämä voidaan maksaa myös verovelvollisen asunnolta tehdystä matkasta. Muulla kuin julkisella kulkuneuvolla tehdystä matkasta on verovapaata enintään 3 000 euroa kalenterivuodelta.\r\n\r\nTässä kohdassa ilmoitetaan vain verovapaat määrät. Jos yleishyödyllinen yhteisö maksaan päivärahaa enemmän kuin yllä, ylimenevä osa ilmoitetaan työkorvauksena. Palkkaus.fi ei tarkkaile näitä rajoja, vaan työnantajan tulee huolehtia tästä itse.",
          "priceLabel": "Hinta"
        },
        "otherAdditions": {
          "amountLabel": "Määrä",
          "description": "Muut sovitut aikaperusteiset lisät.",
          "iconText": "lis",
          "label": "Muut lisät",
          "moreInfo": "",
          "priceLabel": "Euroa"
        },
        "otherBenefit": {
          "amountLabel": "Kuukausia",
          "description": "Muu työnantajan maksama verotettava etu.",
          "iconText": "etu",
          "label": "Muu etu",
          "moreInfo": "",
          "priceLabel": "Muu etu € / kk"
        },
        "otherCompensation": {
          "amountLabel": "Määrä",
          "description": "Tulorekisterin tulolajeissa oleva muu korvaus tai palkkio",
          "iconText": "muu",
          "label": "Muu palkkio tai korvaus",
          "moreInfo": "",
          "priceLabel": "Hinta"
        },
        "otherDeductions": {
          "amountLabel": "Määrä",
          "description": "Muu vähennys, joka pienentää työntekijälle maksettavan nettopalkan määrää.",
          "iconText": "muu",
          "label": "Muu vähennys",
          "moreInfo": "",
          "priceLabel": "Vähennys"
        },
        "overtime": {
          "amountLabel": "Prosentti",
          "description": "Ylityökorvaukset",
          "iconText": "yli",
          "label": "Ylityöt",
          "moreInfo": "",
          "priceLabel": "Palkkasumma"
        },
        "phoneBenefit": {
          "amountLabel": "Kuukausia",
          "description": "Puhelimen verotusarvo, jonka verottaja määrittää vuosittain.",
          "iconText": 20,
          "label": "Puhelinetu",
          "moreInfo": "",
          "priceLabel": "Puhelinetu / kk"
        },
        "prepaidExpenses": {
          "amountLabel": "Määrä",
          "description": "Kulukorvaus, mikä on jo maksettu ennakkoon työntekijälle. Vähentää työntekijälle maksettavien kulukorvausten määrää.",
          "iconText": "ek",
          "label": "Ennakkoon maksettu kulukorvaus",
          "moreInfo": "",
          "priceLabel": "Maksettu ennakko"
        },
        "remuneration": {
          "amountLabel": "Määrä",
          "description": "Tulorekisterin tulolajeissa oleva kertaluonteinen palkkio",
          "iconText": "ker",
          "label": "Kertaluonteinen palkkio",
          "moreInfo": "",
          "priceLabel": "Hinta"
        },
        "salary": {
          "amountLabel": "",
          "description": "Kertakorvaus työstä.",
          "iconText": "P",
          "label": "Palkka tai palkkio",
          "moreInfo": "",
          "priceLabel": "Palkka tai palkkio"
        },
        "saturdayAddition": {
          "amountLabel": "Prosentti",
          "description": "Lisä lauantaina tehdystä työstä työ- tai työehtosopimuksen mukaan.",
          "iconText": "LA",
          "label": "Lauantailisä",
          "moreInfo": "",
          "priceLabel": "Palkkasumma"
        },
        "subsidisedCommute": {
          "description": "Työnantajan työntekijälle kustantaman ja hankkima julkisen liikenteen henkilökohtainen joukkoliikennelippu, josta osa on verovapaata.",
          "iconText": "bus",
          "label": "Työsuhdematkalippu",
          "moreInfo": "Työsuhdematkalippu on työnantajan työntekijälle kustantaman ja hankkima julkisen liikenteen henkilökohtainen joukkoliikennelippu. Osa työsuhdematkalipun arvosta on verovapaata ja osa veronalaista: Verovapaata on 0 - 300€ ja 750 – 3 400€ vuonna 2019. Palkkaus.fi-palvelu laskee verovapaan ja veronalaisen palkan lipun hinnan perusteella. Lisäksi otetaan huomioon työntekijän mahdollisesti työnantajalle maksama korvaus. Kaikki nämä tiedot täytyy ilmoittaa erikseen verottajalle / tulorekisteriin. \r\n\r\nJos palkansaaja ostaa matkalipun itse ja työnantaja maksaa työntekijälle matkalipun hinnan tai osan siitä, työnantajan maksama korvaus katsotaan kokonaisuudessaan työntekijän palkaksi. Tällöin työntekijälle maksettu lipun hinta ilmoitetaan vain palkkana.\r\n\r\nMatkalippu on yleensä voimassa vuoden ja siksi työsuhdematkalipussa veronalaisen ja verovapaan edun määriä arvioidaan vuositasolla (toisin kuin muissa luontoiseduissa kuukausitasolla). \r\n\r\nJos kuitenkin korvausta peritään työntekijän palkasta säännöllisesti koko vuosi (esim. kerran kuussa) voi maksaja jo etukäteen ennakoida tilanteen ja jakaa verovapaan osuuden koko vuodelle ja ilmoittaa sen kuukausittain tulorekisteriin samassa yhteydessä perimänsä korvauksen kanssa. Tämä siitä huolimatta, että työsuhdematkalippu annettaisiin kerran vuodessa.\r\n\r\nJos haluat toteuttaa tämän jaon, sinun tulee asettaa kohta ”Jaetaanko korvaus palkkajaksoihin?” arvoksi Kyllä ja määrittää jaksojen määrä (oletus 12 kuukautta). Palkkaus.fi-palvelu ei tässä versiossa tarkkaile, että vuodessa kaikki jaksot tulee ilmoitettua oikein, joten sinun tulee vuoden lopussa tarkistaa, että kaikki työsuhdematkalipun osat on ilmoitettu. Erityisen huolellisesti pitää tarkistaa, jos vaihdat jaksojen määrää kesken vuotta (ei suositeltavaa). Ota tarvittaessa yhteyttä Palkanlaskijaasi tai Palkkaus.fi-asiakaspalveluun.",
          "priceLabel": "Lipun hinta"
        },
        "sundayWork": {
          "amountLabel": "Määrä",
          "description": "Lain määrittämä 100% korotus sunnuntailta ja pyhäpäiviltä.",
          "iconText": "SU",
          "label": "Sunnuntai- / pyhätyö",
          "moreInfo": "",
          "priceLabel": "Palkkasumma"
        },
        "tesWorktimeShortening": {
          "amountLabel": "Prosentti",
          "description": "Erillinen palkanosa on määritetty rakennusalan työehtosopimuksissa.",
          "iconText": "TES",
          "label": "Erillinen palkan osa (Raksa)",
          "moreInfo": "Ne ovat pääosin yleissitovia ja koskevat myös yksityishenkilöä. Entinen työajan lyhennysraha / \"pekkaset\".",
          "priceLabel": "Palkkasumma"
        },
        "toolCompensation": {
          "amountLabel": "Päiviä",
          "description": "Työvälinekorvaus on määritetty joissakin työehtosopimuksissa (esim. Raksa TES).",
          "iconText": "tool",
          "label": "Työvälinekorvaus",
          "moreInfo": "",
          "priceLabel": "Korvaus / päivä"
        },
        "totalEmployerPayment": {
          "amountLabel": "",
          "description": "Laskee palkan kokonaiskustannuksena: Mitä palkanmaksu maksaa sivukuluineen työnantajalle. Lopputulos on kertakorvaus.",
          "iconText": "sum",
          "label": "Kokonaiskustannus",
          "moreInfo": "",
          "priceLabel": ""
        },
        "totals": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "Yhteensä",
          "moreInfo": "",
          "priceLabel": ""
        },
        "totalWorkerPayment": {
          "amountLabel": "",
          "description": "Laskee palkan nettopalkkana: Mitä työntekijälle jää käteen sivukulujen ja verojen jälkeen. Lopputulos on kertakorvaus.",
          "iconText": "net",
          "label": "Nettopalkka",
          "moreInfo": "Laskee palkan nettopalkkana: Mitä työntekijälle jää käteen sivukulujen ja verojen jälkeen. Lopputulos on kertakorvaus.",
          "priceLabel": ""
        },
        "unionPayment": {
          "amountLabel": "Prosenttia palkasta",
          "description": "Työnantajan tulee maksaa AY-jäsenmaksu suoraan liitolle, jos työntekijä näin toivoo.",
          "iconText": "AY",
          "label": "Ay-jäsenmaksu",
          "moreInfo": "Työnantaja voi vähentää AY-jäsenmaksun suoraan työntekijän palkasta ja tilittää sen ammattiliitolle. Vähentäminen tapahtuu työntekijän pyynnöstä / suostumuksella. AY jäsenmaksu vähennetään työntekijän nettopalkasta.\n\nMenettelystä on saatettu sopia työehtosopimuksessa, jolloin AY-jäsenmaksun tilittäminen saattaa olla työnantajan velvollisuus. Siitä, koskeeko velvollisuus vain järjestäytyneitä työnantajia, ei tätä kirjoitettaessa ole täyttä varmuutta: Asiasta on eri mielipiteitä ja siitä ei ole oikeustapausta.\n\nTyypillisesti tilanne on Palkkaus.fi-asiakkailla (pienillä yrityksillä) harvinainen ja siksi AY-jäsenmaksuja ei toistaiseksi hoideta Palkkaus.fi-palvelun kautta: Työnantajan tulee itse hoitaa jäsenmaksun tilitys suoraan liittoon. Näin vähennettävä summa vähennetään Palkkaus.fi-palvelun asiakasvaratilille maksettavasta summasta. Lisätietoja ilmoittamisesta ja maksamisesta saat kyseisestä liitosta.",
          "priceLabel": "Ennakonpidätyksen alainen palkka"
        },
        "unknown": {
          "amountLabel": "Kuukausia",
          "description": "Rivin tyyppiä ei ole vielä määritetty.",
          "iconText": "",
          "label": "Valitse...",
          "moreInfo": "",
          "priceLabel": ""
        },
        "workingTimeCompensation": {
          "amountLabel": "Määrä",
          "description": "Tulorekisterin tulolajeissa oleva aikaperusteinen lisä ",
          "iconText": "lisä",
          "label": "Aikaperusteinen lisä",
          "moreInfo": "",
          "priceLabel": "Hinta"
        }
      },
      "CalculationRowUnit": {
        "count": {
          "description": "",
          "label": "kpl"
        },
        "days": {
          "description": "",
          "label": "päivää"
        },
        "kilometers": {
          "description": "",
          "label": "km"
        },
        "percent": {
          "description": "",
          "label": "%"
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "CalculationStatus": {
        "draft": {
          "description": "",
          "label": "Luonnos"
        },
        "externalPaymentCanceled": {
          "description": "",
          "label": "Maksu peruttu salaxy-palvelun ulkopuolella"
        },
        "externalPaymentError": {
          "description": "",
          "label": "Virheitä (salaxy-palvelun ulkopuolella)"
        },
        "externalPaymentStarted": {
          "description": "",
          "label": "Maksu aloitettu salaxy-palvelun ulkopuolella"
        },
        "externalPaymentSucceeded": {
          "description": "",
          "label": "Maksettu salaxy-palvelun ulkopuolella"
        },
        "paymentCanceled": {
          "description": "",
          "label": "Maksu peruttu"
        },
        "paymentError": {
          "description": "",
          "label": "Virheitä"
        },
        "paymentRefunded": {
          "description": "",
          "label": "Maksu peruttu ja rahat palautettu"
        },
        "paymentStarted": {
          "description": "",
          "label": "Maksu aloitettu"
        },
        "paymentSucceeded": {
          "description": "",
          "label": "Maksettu"
        },
        "paymentWorkerCopy": {
          "description": "",
          "label": "Työntekijän kopio"
        },
        "payrollDraft": {
          "description": "",
          "label": "Palkkalistan laskelma"
        },
        "proDraft": {
          "label": ""
        },
        "sharedApproved": {
          "description": "",
          "label": "Työntekijältä vastaanotettu palkkalaskelma hyväksytty"
        },
        "sharedRejected": {
          "description": "",
          "label": "Työntekijältä vastaanotettu palkkalaskelma hylätty"
        },
        "sharedWaiting": {
          "description": "",
          "label": "Työntekijältä vastaanotettu palkkalaskelma"
        },
        "template": {
          "description": "",
          "label": "Laskelmapohja (template)"
        },
        "workerRequestAccepted": {
          "description": "",
          "label": "Työntekijän tekemä palkkalaskelma hyväksytty"
        },
        "workerRequestDeclined": {
          "description": "",
          "label": "Työntekijän tekemä palkkalaskelma hylätty"
        },
        "workerRequested": {
          "description": "",
          "label": "Työntekijän tekemä palkkalaskelma"
        }
      },
      "CarBenefitCode": {
        "fullCarBenefit": {
          "label": "Vapaa autoetu"
        },
        "limitedCarBenefit": {
          "label": "Käyttöetu"
        }
      },
      "CarBenefitKind": {
        "fullCarBenefit": {
          "description": "Työnantaja huolehtii kaikista auton käyttöön liittyvistä kustannuksista",
          "label": "Vapaa autoetu"
        },
        "limitedCarBenefit": {
          "description": "Auton käyttäjä maksaa yksityisajojensa polttoainekustannukset",
          "label": "Käyttöetu"
        },
        "undefined": {
          "description": "Autoedun tyypin valinta on pakollinen",
          "label": "Ei valintaa"
        }
      },
      "CompanyType": {
        "fiOy": {
          "description": "",
          "label": "Osakeyhtiö, myös asunto-osakeyhtiö ja Oyj"
        },
        "fiRy": {
          "description": "",
          "label": "Yhdistys"
        },
        "fiTm": {
          "description": "",
          "label": "Yksityinen elinkeinonharjoittaja tai Maatalousyrittäjä"
        },
        "fiYy": {
          "description": "",
          "label": "Muu, esim. Kommandiittiyhtiö, Avoin yhtiö,"
        },
        "unknown": {
          "description": "",
          "label": ""
        }
      },
      "ContractPartyType": {
        "customContact": {
          "description": "",
          "label": "Custom Contact"
        },
        "person": {
          "description": "",
          "label": "Person"
        }
      },
      "DailyAllowanceKind": {
        "fullDailyAllowance": {
          "description": "Päiväraha, kun työmatka on yli 10 tuntia.",
          "label": "Kokopäiväraha"
        },
        "internationalDailyAllowance": {
          "description": "Päiväraha ulkomaanmatkoilla",
          "label": "Ulkomaan päiväraha"
        },
        "mealAllowance": {
          "description": "Ateriakorvauksen voi maksaa työmatkoilta, joilta ei makseta päivärahaa. Ateriakorvauksen enimmäismäärä on 10,75€ per ateria, 2 ateriaa per päivä (21,50 € per päivä).",
          "label": "Ateriakorvaus"
        },
        "partialDailyAllowance": {
          "description": "Päiväraha, kun työmatka on 6-10 tuntia.",
          "label": "Osapäiväraha"
        },
        "undefined": {
          "description": "Ei valintaa, oletus kokopäiväraha",
          "label": "Ei valintaa"
        }
      },
      "DateOfBirthAccuracy": {
        "ageBased": {
          "description": "",
          "label": "Iän perusteella"
        },
        "ageGroupBased": {
          "description": "",
          "label": "Ikäluokan perusteella"
        },
        "assumption": {
          "description": "",
          "label": "Oletus"
        },
        "exact": {
          "description": "",
          "label": "Täsmällinen päivämäärä"
        },
        "monthCorrect": {
          "description": "",
          "label": "Kuukausi oikea"
        },
        "verified": {
          "description": "",
          "label": "Vahvistettu"
        }
      },
      "EmployerType": {
        "fullTimeEmployer": {
          "description": "",
          "label": "Jatkuva sopimus tai yli 8178€ / 6kk"
        },
        "miniEmployer": {
          "description": "",
          "label": "Alle 1300€ / vuosi"
        },
        "smallEmployer": {
          "description": "",
          "label": "Väh. 1300€ / vuosi"
        },
        "tyelOwnContractEmployer": {
          "description": "",
          "label": "TyEL-vuosisopimus tai hoitaa itse."
        },
        "unknown": {
          "description": "",
          "label": "Ei tiedossa"
        }
      },
      "EmploymentContract": {
        "annualLeave": {
          "label": "Vuosiloma"
        },
        "benefitsException": {
          "label": "Benefits"
        },
        "noticeException": {
          "label": "Irtisanomisaika"
        },
        "otherTerms": {
          "label": "Muut sopimusehdot"
        }
      },
      "EmploymentRelationType": {
        "athlete": {
          "description": "",
          "label": "Urheilija"
        },
        "boardMember": {
          "description": "",
          "label": "Hallituksen jäsen"
        },
        "compensation": {
          "description": "",
          "label": "Suoritus työkorvauksena"
        },
        "employedByStateEmploymentFund": {
          "description": "",
          "label": "Valtion työllisyysmäärärahoilla työllistetty (palkkatuki)"
        },
        "entrepreneur": {
          "description": "",
          "label": "Yrittäjä tai yrittäjän perheenjäsen"
        },
        "entrepreneurNew": {
          "description": "",
          "label": "Yrittäjä, osaomistaja tai yrittäjän perheenjäsen"
        },
        "farmer": {
          "description": "",
          "label": "Maatalousyrittäjä tai perheenjäsen"
        },
        "hourlySalary": {
          "description": "",
          "label": "Tuntipalkkainen työntekijä"
        },
        "keyEmployee": {
          "description": "",
          "label": "Avainhenkilö. Ulkomaalainen asiantuntija, jolle maksetaan avainhenkilön lähdeveron alainen palkka."
        },
        "monthlySalary": {
          "description": "",
          "label": "Kuukausipalkkainen työntekijä"
        },
        "performingArtist": {
          "description": "",
          "label": "Esiintyvä taitelija"
        },
        "salary": {
          "description": "",
          "label": "Keikkatyöntekijä (kertaluonteinen työ)"
        },
        "undefined": {
          "description": "",
          "label": "Muu"
        }
      },
      "EmploymentSalaryType": {
        "hourlyPay": {
          "description": "",
          "label": "tuntipalkka",
          "metricAbbrev": "/h"
        },
        "montlyPay": {
          "description": "",
          "label": "kuukausipalkka",
          "metricAbbrev": "/kk"
        },
        "otherPay": {
          "description": "",
          "label": "muu"
        },
        "unknown": {
          "description": "",
          "label": "Valitse..."
        }
      },
      "EmploymentTerminationKind": {
        "monetaryWorkingTimeBankCompensation": {
          "description": "Palkka, joka maksetaan työaikapankkiin säästetyn ajan perusteella.",
          "label": "Työaikapankin rahakorvaus"
        },
        "noticePeriodCompensation": {
          "description": "Korvaus, jonka työnantaja maksaa työntekijälle palvelussuhteen päättyessä ilman irtisanomisaikaa eli tilanteessa, jossa jätetään irtisanomisaika noudattamatta ja työsuhde päättyy heti (TSL 6:4). Tulolajilla ilmoitetaan myös korvaus, jonka työnantaja maksaa, kun työntekijä irtisanoutuu yli 200 päivää kestäneen lomautuksen johdosta sekä, kun työnantaja irtisanoo lomautettuna olevan työntekijän.",
          "label": "Irtisanomisajan korvaus"
        },
        "pensionPaidByEmployer": {
          "description": "Eläke, jonka työnantaja maksaa entiselle työntekijälleen tai tämän edunsaajalle aiemman palvelussuhteen perusteella.",
          "label": "Työnantajan maksama eläke"
        },
        "terminationAndLayOffDamages": {
          "description": "Korvaus, jota työnantaja on velvollinen maksamaan, kun työnantaja on päättänyt palvelussuhteen henkilön työsopimuslain, kunnallisesta viranhaltijasta annetun lain tai valtion virkamieslain vastaisesti. Kohdassa ilmoitetaan myös korvaus työsuhteen purkamisesta (TSL 8:1, 55/2001) sekä korvaus perusteettomasta lomauttamisesta.",
          "label": "Vahingonkorvaus päättämis- ja lomautustilanteissa"
        },
        "undefined": {
          "description": "Ei valintaa",
          "label": "Ei valintaa"
        },
        "voluntaryTerminationCompensation": {
          "description": "Korvaus, jonka työnantaja maksaa vapaaehtoisesti irtisanomis- tai purkamistilanteessa tai silloin, kun palvelussuhde päätetään sopimalla.",
          "label": "Vapaaehtoinen korvaus päättämistilanteessa"
        }
      },
      "EmploymentType": {
        "fullTime": {
          "description": "",
          "label": "Toistaiseksi voimassaoleva"
        },
        "job": {
          "description": "",
          "label": "Kertaluonteinen työ"
        },
        "partTime": {
          "description": "",
          "label": "Määräaikainen"
        },
        "unknown": {
          "description": "",
          "label": "Valitse..."
        }
      },
      "EmploymentWorkHoursType": {
        "hourPerThreeWeeks": {
          "description": "",
          "label": "3 viikon jaksossa"
        },
        "hourPerTwoWeeks": {
          "description": "",
          "label": "2 viikon jaksossa"
        },
        "hoursPerDay": {
          "description": "",
          "label": "tuntia/vuorokaudessa"
        },
        "hoursPerWeek": {
          "description": "",
          "label": "tuntia/viikkossa"
        }
      },
      "FrameworkAgreement": {
        "childCare": {
          "description": "Jatkuva lastenhoitaja ja siihen liittyvät tuet",
          "label": "Muu lastenhoito"
        },
        "cleaning": {
          "description": "Siivous- ja kiinteistöalan työ",
          "label": "Siivous- ja kiinteistöala"
        },
        "construction": {
          "description": "Työtä rakennusalan työehtosopimuksen mukaan",
          "label": "Rakennus- ja remonttityö"
        },
        "entrepreneur": {
          "description": "Yrittäjän oma palkka",
          "label": "Yrittäjä"
        },
        "mll": {
          "description": "MLL:n lastenhoitajan palkka",
          "label": "MLL suositukset"
        },
        "notDefined": {
          "description": "Työehtosopimusta tai suositusta ei ole määritetty",
          "label": "Ei määritetty"
        },
        "other": {
          "description": "Muu työ - ei työehtosopimusta",
          "label": "Muu työ"
        },
        "santaClaus": {
          "description": "Joulupukin kotikäynti jouluaattona",
          "label": "Joulupukki kotiin"
        }
      },
      "Gender": {
        "female": {
          "description": "",
          "label": "Nainen"
        },
        "male": {
          "description": "",
          "label": "Mies"
        },
        "unknown": {
          "description": "",
          "label": "-"
        }
      },
      "HelttiIndustry": {
        "i": {
          "description": "",
          "label": "Majoitus- ja ravitsemistoiminta"
        },
        "j": {
          "description": "",
          "label": "Informaatio ja viestintä"
        },
        "k": {
          "description": "",
          "label": "Rahoitus- ja vakuutustoiminta"
        },
        "m": {
          "description": "",
          "label": "Ammatillinen, tieteellinen ja tekninen toiminta"
        },
        "n": {
          "description": "",
          "label": "Hallinto- ja tukipalvelutoiminta"
        },
        "notDefined": {
          "description": "",
          "label": "Ei valintaa"
        },
        "o": {
          "description": "",
          "label": "Julkinen hallinto ja maanpuolustus"
        },
        "other": {
          "description": "",
          "label": "Muu toimiala"
        },
        "q": {
          "description": "",
          "label": "Terveys- ja sosiaalipalvelut"
        },
        "r": {
          "description": "",
          "label": "Taiteet, viihde ja virkistys"
        },
        "s": {
          "description": "",
          "label": "Muu palvelutoiminta"
        }
      },
      "HelttiProductPackage": {
        "large": {
          "description": "",
          "label": "Tuloskunto"
        },
        "medium": {
          "description": "",
          "label": "Kasvuyritys"
        },
        "notDefined": {
          "description": "",
          "label": "Ei sopimusta"
        },
        "small": {
          "description": "",
          "label": "Start-up"
        }
      },
      "HolidayAccrualKind": {
        "calculation": {
          "label": "Palkkalaskelma"
        },
        "holiday": {
          "label": "Pidetty loma"
        },
        "holidayCompensated": {
          "label": "Lomakorvaus"
        },
        "manual": {
          "label": "Käsin syötetty kuukausikertymä"
        },
        "saldo": {
          "label": "Lomavuoden alku- tai loppusaldo"
        }
      },
      "HolidayBonusPaymentMethod": {
        "none": {
          "label": "Ei lomarahaa"
        },
        "pay24Days": {
          "label": "Kesäloma 24 päivää (ei huomioi lomasuunnitelmia)"
        },
        "payAllBonus": {
          "label": "Kaikki kerralla (kesäloman yhteydessä)"
        },
        "payForHolidaySalary": {
          "label": "Maksetun loma-ajan palkan mukaan (oletus)"
        },
        "paySummerBonus": {
          "label": "Suunnitellut kesälomat (max 24pv)"
        }
      },
      "HolidayCode": {
        "holidayCompensation": {
          "description": "Lomia ei kerrytetä, vaan lomakorvaus (+mahd. lomaraha) maksetaan heti palkan yhteydessä. Tyypillistä rakennusalalla ja muissa keikkatöissä.",
          "label": "Lomakorvaus palkanmaksun yhteydessä"
        },
        "holidayCompensationIncluded": {
          "description": "Lomia ei kerrytetä, vaan lomakorvaus on sovittu osaksi kokonaispalkkaa. Tämä on mahdollista vain poikkeustilanteissa, lähinnä kotitalouksilla.",
          "label": "Lomakorvaus osa kokonaispalkkaa"
        },
        "noHolidays": {
          "description": "Maksun saajalla ei ole lomaoikeutta. Esim. hallituksen jäsen, työkorvauksella työskentelevät, joskus toimitusjohtaja.",
          "label": "Ei lomaoikeutta"
        },
        "permanent14Days": {
          "description": "Työntekijä työskentelee työsopimuksen mukaan kaikkina kuukausina vähintään 14 päivää / kk",
          "label": "14 päivän sääntö"
        },
        "permanent35Hours": {
          "description": "Työntekijä työskentelee vähintään yhtenä kuukautena vuodessa 35 tuntia / kk, mutta ei kuulu 14 päivän säännön piiriin.",
          "label": "35 tunnin sääntö"
        },
        "temporaryTimeOff": {
          "description": "Työntekijä ei kuulu 14 päivän tai 35 tunnin säännön piiriin.",
          "label": "Vapaan ansaintasääntö"
        },
        "undefined": {
          "description": "Loma-ajan kertymistä ei ole määritetty",
          "label": "Lisää lomakirjanpito"
        }
      },
      "HolidayYearInitType": {
        "createEmpty": {
          "label": "Luo uusi tyhjä lomavuosi"
        },
        "createFromCalcs": {
          "label": "Luo lomavuosi ja lisää lomapäivät maksettujen laskelmien perusteella"
        },
        "createMonths": {
          "label": "Luo lomavuosi ja lisää kaikki kuukaudet"
        },
        "ignore": {
          "label": "Älä lisää tai poista lomavuotta"
        }
      },
      "HouseHoldUseCaseTree": {
        "childCare": {
          "description": "Tilapäinen tai jatkuva",
          "kela": {
            "description": "Lastenhoitaja kotona.",
            "label": "Yksityisen hoidon tuki"
          },
          "label": "Lastenhoito",
          "mll": {
            "description": "Palkka MLL:n suosituksen mukaan.",
            "label": "Tilapäinen lastenhoito"
          },
          "other": {
            "description": "Muu jatkuva tai tilapäinen lastenhoito",
            "label": "Muu lastenhoito"
          }
        },
        "cleaning": {
          "51530": {
            "description": "Kiinteistöhuollon työntekijät",
            "label": "Kiinteistöhuolto"
          },
          "53221": {
            "description": "Kotityöpalvelutyöntekijän työ",
            "label": "Kotityöpalvelu"
          },
          "61132": {
            "description": "Puutarha- ja kasvihuonetyöntekijät",
            "label": "Puutarhanhoito"
          },
          "91110": {
            "description": "Siivoustyö tai kotiapulainen",
            "label": "Kotisiivous tai kotiapulainen"
          },
          "description": "Kodin siivous ja kunnossapito",
          "label": "Kotisiivous",
          "other": {
            "description": "Kodin muut työt",
            "label": "Muut"
          }
        },
        "construction": {
          "carpenter": {
            "description": "Kirvesmiehet ja rakennuspuusepät",
            "label": "Kirvesmies"
          },
          "description": "Rakennusalan työehtosopimus.",
          "floor": {
            "description": "Lattianpäällystys",
            "label": "Lattiatyöt"
          },
          "label": "Rakennus- ja remonttityö",
          "new": {
            "description": "Uudisrakennukset",
            "label": "Muu uudisrakentaminen"
          },
          "painter": {
            "description": "Rakennusmaalarit ym.",
            "label": "Maalaaminen"
          },
          "renovation": {
            "description": "Asunnon tai vapaa-ajan asunnon remontointi",
            "label": "Muu remontointi"
          },
          "roof": {
            "description": "Kattoasentajat ja -korjaajat",
            "label": "Kattotyöt"
          }
        },
        "other": {
          "51530": {
            "description": "",
            "label": "Kiinteistöhuollon työntekijät"
          },
          "53112": {
            "description": "",
            "label": "Perhepäivähoitajat (sis. kotitalousten lastenhoitajat)"
          },
          "53212": {
            "description": "",
            "label": "Kehitysvammaisten hoitajat"
          },
          "53213": {
            "description": "",
            "label": "Sosiaalialan hoitajat"
          },
          "53221": {
            "description": "",
            "label": "Kotityöpalvelutyöntekijät"
          },
          "53222": {
            "description": "",
            "label": "Henkilökohtaiset avustajat, omaishoitajat ym."
          },
          "61112": {
            "description": "",
            "label": "Peltoviljelytyönjohtajat ja -työntekijät"
          },
          "61132": {
            "description": "",
            "label": "Puutarha- ja kasvihuonetyönjohtajat ja - työntekijät"
          },
          "62100": {
            "description": "",
            "label": "Metsurit ja metsätyöntekijät"
          },
          "71110": {
            "description": "",
            "label": "Talonrakentajat (sis. remonttimiehet)"
          },
          "71150": {
            "description": "",
            "label": "Kirvesmiehet ja rakennuspuusepät"
          },
          "71210": {
            "description": "",
            "label": "Kattoasentajat ja -korjaajat"
          },
          "71310": {
            "description": "",
            "label": "Rakennusmaalarit ym."
          },
          "91110": {
            "description": "",
            "label": "Kotiapulaiset ja -siivoojat"
          },
          "description": "Yleinen palkkalaskuri",
          "label": "Muu työ",
          "undefined": {
            "description": "",
            "label": "Muu työ"
          }
        }
      },
      "InsuranceCompany": {
        "aktia": {
          "description": "",
          "label": "Aktia"
        },
        "alandia": {
          "description": "",
          "label": "Alandia"
        },
        "aVakuutus": {
          "description": "",
          "label": "A-Vakuutus"
        },
        "fennia": {
          "description": "",
          "label": "Fennia"
        },
        "folksam": {
          "description": "",
          "label": "Folksam"
        },
        "if": {
          "description": "",
          "label": "If"
        },
        "lähiTapiola": {
          "description": "",
          "label": "LähiTapiola"
        },
        "none": {
          "description": "",
          "label": "(Ei valittu vielä)"
        },
        "other": {
          "description": "",
          "label": "Muu vakuutusyhtiö"
        },
        "pohjantähti": {
          "description": "",
          "label": "Pohjantähti"
        },
        "pohjola": {
          "description": "",
          "label": "OP Vakuutus"
        },
        "redarnas": {
          "description": "",
          "label": "Redarnas"
        },
        "tryg": {
          "description": "",
          "label": "Tryg"
        },
        "turva": {
          "description": "",
          "label": "Turva"
        },
        "ålands": {
          "description": "",
          "label": "Ålands"
        }
      },
      "InvoiceStatus": {
        "canceled": {
          "description": "Maksu on peruttu",
          "label": "Maksu peruttu"
        },
        "error": {
          "description": "Maksujärjestelmä on merkinnyt maksun virheelliseksi",
          "label": "Virhetila"
        },
        "forecast": {
          "description": "Tämä lasku on Palkkaus.fi arvio: Varsinainen lasku tulee muusta kanavasta.",
          "label": "Arvio"
        },
        "paid": {
          "description": "Maksukanava on antanut maksukuittauksen: Maksu maksetaan 99% todennäköisyydellä. 100% varmuuden saat maksukanavasta.",
          "label": "Maksettu"
        },
        "paymentStarted": {
          "description": "Maksuprosessi on aloitettu, esim. maksu on lähetetty pankkiin.",
          "label": "Maksu aloitettu"
        },
        "preview": {
          "description": "Esikatselulasku on tämänhetkinen tilanne. Laskuun saattaa olla tulossa vielä uusia eriä.",
          "label": "Esikatselu"
        },
        "read": {
          "description": "Lasku on lähetetty maksujärjestelmään",
          "label": "Käsittelyssä"
        },
        "undefined": {
          "description": "Luonnos, ei maksujonossa",
          "label": "Valitse..."
        },
        "unread": {
          "description": "Laskua ei vielä ole lähetetty maksujärjestelmään",
          "label": "Ei käsitelty"
        },
        "waitingConfirmation": {
          "description": "Odottaa maksun manuaalista vahvistusta yrittäjältä / tilitoimistolta. Tulorekisteri-ilmoitus lähetetään vasta tämän jälkeen.",
          "label": "Odottaa vahvistusta"
        },
        "waitingPalkkaus": {
          "description": "Palkkalaskelma on otettu tarkistukseen, koska siinä saattaa olla virheitä. Maksu lähtee tarkistuksen jälkeen, yleensä seuraavana arkipäivänä.",
          "label": "Tarkistuksessa"
        }
      },
      "IrFlags": {
        "noMoney": {
          "label": "Maksettu muuna kuin rahana (NoMoney)"
        },
        "oneOff": {
          "label": "Kertakorvaus (OneOff)"
        },
        "unjustEnrichment": {
          "label": "Perusteeton etu (UnjustEnrichment)"
        }
      },
      "IrIncomeTypeKind": {
        "undefined": {
          "description": "Oletusarvo",
          "label": "Oletusarvo"
        }
      },
      "IrInsuranceExceptions": {
        "excludeAccidentInsurance": {
          "label": "Poista työtapaturma- ja ammattitautivakuutusmaksun alaisuus"
        },
        "excludeAll": {
          "label": "Poista kaikkien sosiaalivakuutusmaksujen alaisuus"
        },
        "excludeHealthInsurance": {
          "label": "Poista sairausvakuutusmaksun alaisuus"
        },
        "excludePension": {
          "label": "Poista työeläkevakuutusmaksun alaisuus"
        },
        "excludeUnemployment": {
          "label": "Poista työttömyysvakuutusmaksun alaisuus"
        },
        "includeAccidentInsurance": {
          "label": "Lisää työtapaturma- ja ammattitautivakuutusmaksun alaisuus"
        },
        "includeAll": {
          "label": "Lisää kaikkien sosiaalivakuutusmaksujen alaisuus"
        },
        "includeHealthInsurance": {
          "label": "Lisää sairausvakuutusmaksun alaisuus"
        },
        "includePension": {
          "label": "Lisää työeläkevakuutusmaksun alaisuus"
        },
        "includeUnemployment": {
          "label": "Lisää työttömyysvakuutusmaksun alaisuus"
        }
      },
      "LegalEntityType": {
        "company": {
          "description": "",
          "label": "Y-tunnus"
        },
        "partner": {
          "description": "",
          "label": "Kumppani"
        },
        "person": {
          "description": "",
          "label": "Henkilö"
        },
        "personCreatedByEmployer": {
          "description": "",
          "label": "Työntekijä (TA:n)"
        },
        "undefined": {
          "description": "",
          "label": "Tuntematon"
        }
      },
      "MealBenefitKind": {
        "cateringContract": {
          "description": "Työnantajan järjestämä sopimusruokailu, jonka kustannus työnantajalle on verottajan vuosittain määrittämissä rajoissa (vuonna 2020 6,80€ - 10,70€). Arvostetaan verotuksessa 6,80€ per ruokailu.",
          "label": "Sopimusruokailu"
        },
        "institute": {
          "description": "Sairaalan, koulun, päiväkodin tai muun vastaavan laitoksen henkilökuntaan kuuluvan laitosruokailun yhteydessä saaman ravintoedun raha-arvona pidetään 5,10 € ateriaa kohden (vuonna 2020).",
          "label": "Sairaalan, koulun, päiväkodin tms. ruokailu"
        },
        "mealAllowance": {
          "description": "Ateriakorvauksen on veroton korvaus (muut tämän sivun valinnat ovat ravintoetuja / luontoisetuja). Sen maksaminen edellyttää, että työmatkasta ei suoriteta päivärahaa ja että palkansaajalla ei työn vuoksi ole mahdollisuutta ruokailutauon aikana aterioida tavanomaisella ruokailupaikallaan. \nLuonnollisesti näiltä päiviltä ei voi käyttää muuta ravintoetua. Ateriakorvauksen enimmäismäärä on 10,75€ per ateria, 2 ateriaa per päivä (21,50 € per päivä).",
          "label": "Ateriakorvaus"
        },
        "mealTicket": {
          "description": "Ruokailulipuke (Lounasseteli tai vast.) tai maksukortille ladattava tapahtuma, jonka kustannus työnantajalle on verottajan vuosittain määrittämissä rajoissa (vuonna 2020 6,80€ - 10,70€). Arvostetaan verotuksessa 75 % nimellisarvosta.",
          "label": "Ruokailulipuke"
        },
        "restaurantWorker": {
          "description": "Hotelli- ja ravintola-alan henkilökunnan ja lentohenkilöstön lennon aikana saaman ravintoedun raha-arvona pidetään 5,78 € ateriaa kohden (vuonna 2020).",
          "label": "Hotelli- ja ravintola-alan ravintoetu"
        },
        "taxableAmount": {
          "description": "Jos ravintoedun hankkimisesta työnantajalle aiheutuneiden kustannusten määrä on yli tai alle verottajan vuosittain määrittämien rajojen (vuonna 2020 6,80€ - 10,70€) tai jos etu muuten ylittää rajata (esim. enemmän ruokailuja kuin työpäiviä), edun arvona pidetään suoria kustannuksia / nimellisarvoa.\nKäytä tätä valintaa myös, jos et halua käyttää automaattista verotusarvon laskentaa muissa kohdissa.",
          "label": "Ravintoetu, verotusarvo"
        },
        "teacher": {
          "description": "Koulun, päiväkodin tai vastaavan laitoksen henkilökunnan oppilaiden tai hoidettavien ruokailun valvonnan yhteydessä saaman ravintoedun arvo on 4,08 € ateriaa kohden (vuonna 2020).",
          "label": "Ruokailu valvonnan yhteydessä"
        },
        "undefined": {
          "description": "Ei valintaa, oletus \"Verotusarvo\".",
          "label": "Ei valintaa"
        }
      },
      "MessageType": {
        "email": {
          "description": "",
          "label": "Sähköposti"
        },
        "sms": {
          "description": "",
          "label": "Tekstiviesti"
        }
      },
      "NonProfitOrgKind": {
        "accomodationAllowance": {
          "description": "Majoittumiskorvaus (tositteen perusteella). ",
          "label": "Majoittumiskorvaus"
        },
        "dailyAllowance": {
          "description": "Päivärahan voi maksaa enintään 20 päivältä per kalenterivuosi ja lisäksi majoittumiskorvaus (kts alla).",
          "label": "Päiväraha"
        },
        "kilometreAllowance": {
          "description": "Matkakustannusten korvaus. Muulla kuin julkisella kulkuneuvolla tehdystä matkasta on verovapaata enintään 3 000 euroa kalenterivuodelta.",
          "label": "Kilometrikorvaus"
        },
        "undefined": {
          "description": "Tyyppiä ei määritetty, oletus kilometrikorvaus",
          "label": "Tyyppiä ei määritetty"
        }
      },
      "OnboardingStatus": {
        "created": {
          "description": "",
          "label": "Luotu"
        },
        "done": {
          "description": "",
          "label": "Valmis"
        },
        "opdescription": "",
        "oplabel": "Käynnissä"
      },
      "OtherCompensationKind": {
        "accruedTimeOffCompensation": {
          "description": "Työaikakorvaus, joka maksetaan ansionvähentymisestä, joka aiheutuu työajan tasaamisesta keskimääräiseen viikkotyöaikaan.",
          "label": "Työajantasauskorvaus"
        },
        "capitalIncomePayment": {
          "description": "Suoritus, joka on saajalleen pääomatuloa. Tällainen on esimerkiksi työntekijälle tai osakkaalle maksettu korko, kuten palkkasaatavan korko tai pääomatuloa oleva takausprovisio.",
          "label": "Pääomatuloa oleva suoritus"
        },
        "employeeInventionCompensation": {
          "description": "Palkkio, joka maksetaan työsuhdekeksinnöstä.",
          "label": "Palkkio työsuhdekeksinnöstä"
        },
        "employeeStockOption": {
          "description": "Palkkio, joka annetaan työnantajayrityksen option muodossa.",
          "label": "Työsuhdeoptio"
        },
        "employeeStockOptionWithLowerPrice": {
          "description": "Palkkio, joka annetaan työnantajayrityksen option muodossa ja jossa työntekijältä peritty option merkintähinta option luovutusajankohtana on selvästi alhaisempi kuin osakkeen markkinahinta.",
          "label": "Työsuhdeoptio, jonka merkintähinta on luovutusajankohtana alhaisempi kuin markkinahinta"
        },
        "lectureFee": {
          "description": "Palkkio, joka maksetaan luennon pitämisestä.",
          "label": "Luentopalkkio"
        },
        "meetingFee": {
          "description": "Palkkio, joka maksetaan kokoukseen osallistumisesta.",
          "label": "Kokouspalkkio"
        },
        "membershipOfGoverningBodyCompensation": {
          "description": "Luottamustoimipalkkio, joka maksetaan yhteisön hallituksen tai vastaavan hallintoelimen jäsenyyden perusteella.",
          "label": "Hallintoelimen jäsenyydestä maksettu palkkio"
        },
        "monetaryGiftForEmployees": {
          "description": "Työnantajalta rahana tai siihen verrattavana suorituksena saatu lahja.",
          "label": "Henkilöstölle annettu rahalahja"
        },
        "otherTaxableIncomeAsEarnedIncome": {
          "description": "Tulolajilla ilmoitetaan muiden veronalaisten ansiotuloa olevien suoritusten yhteissumma.",
          "label": "Muu veronalainen ansiotulo"
        },
        "positionOfTrustCompensation": {
          "description": "Palkkio, joka maksetaan muusta luottamustoimen hoitamisesta kuin kokouksiin osallistumisesta.",
          "label": "Luottamustoimipalkkio"
        },
        "stockOptionsAndGrants": {
          "description": "Palkkio, joka annetaan työnantajayrityksen osakkeina tai sovitun palkkion sijaan rahasummana, joka vastaa osakkeiden arvoa.",
          "label": "Osakepalkkio"
        },
        "undefined": {
          "description": "Ei valintaa",
          "label": "Ei valintaa"
        },
        "useCompensationAsCapitalIncome": {
          "description": "Korvaus, joka suoritetaan tekijänoikeuden, teollisoikeuden tai teollisia, kaupallisia tai tieteellisiä kokemuksia koskevien tietojen luovuttamisesta tai käyttöoikeudesta.",
          "label": "Käyttökorvaus pääomatuloa"
        },
        "useCompensationAsEarnedIncome": {
          "description": "Korvaus, joka suoritetaan tekijänoikeuden, teollisoikeuden tai teollisia, kaupallisia tai tieteellisiä kokemuksia koskevien tietojen luovuttamisesta tai käyttöoikeudesta.",
          "label": "Käyttökorvaus ansiotuloa"
        },
        "workEffortBasedDividendsAsNonWage": {
          "description": "Osakkaalle työkorvauksena maksettava osinko, jonka jakoperusteena on osingonsaajan tai tämän intressipiiriin kuuluvan henkilön työpanos.",
          "label": "Työpanokseen perustuva osinko tai ylijäämä (työkorvaus)"
        },
        "workEffortBasedDividendsAsWage": {
          "description": "Osakkaalle palkkana maksettava osinko, jonka jakoperusteena on osingonsaajan tai tämän intressipiiriin kuuluvan henkilön työpanos.",
          "label": "Työpanokseen perustuva osinko tai ylijäämä (palkka)"
        }
      },
      "PalkkausCampaignPricing": {
        "campaignAfterFirstUse": {
          "description": "",
          "label": "Campaign After First Use"
        },
        "campaignFirstUse": {
          "description": "",
          "label": "Campaign First Use"
        },
        "companyFreeTrial": {
          "description": "",
          "label": "Company Free Trial"
        },
        "none": {
          "description": "",
          "label": "None"
        }
      },
      "PartnerSite": {
        "careDotCom": {
          "description": "",
          "label": "Care Dot Com"
        },
        "dev": {
          "description": "",
          "label": "Dev"
        },
        "duunitori": {
          "description": "",
          "label": "Duunitori"
        },
        "example": {
          "description": "",
          "label": "Example"
        },
        "mol": {
          "description": "",
          "label": "Mol"
        },
        "palkkaapakolaindescription": "",
        "palkkaapakolainlabel": "Palkkaapakolainen",
        "palkkaus": {
          "description": "",
          "label": "Palkkaus"
        },
        "palkkausEmployerFillIn": {
          "description": "",
          "label": "Palkkaus Employer Fill In"
        },
        "palkkausIdentified": {
          "description": "",
          "label": "Palkkaus Identified"
        },
        "raksa": {
          "description": "",
          "label": "Raksa"
        },
        "refugeeJobs": {
          "description": "",
          "label": "Refugee Jobs"
        },
        "talkkarit": {
          "description": "",
          "label": "Talkkarit"
        },
        "unknown": {
          "description": "",
          "label": "Unknown"
        }
      },
      "PaymentChannel": {
        "accountorGo": {
          "description": "Laskujen lähetys suoraan Accountor Go -palveluun",
          "label": "Accountor Go"
        },
        "finagoSolo": {
          "description": "Laskujen lähetys Finago Soloon",
          "label": "Finago Solo"
        },
        "holviCfa": {
          "description": "Palkka, ennakonpidätys ja työeläke yhdellä helpolla Holvi-maksulla Palkkaus.fi asiakasvaratilin kautta.",
          "label": "Holvi"
        },
        "palkkausCfaFinvoice": {
          "description": "Lähetä itsellesi verkkolasku, jossa palkka, ennakonpidätys ja työeläke on yhdessä helpossa laskussa. Maksa esim. kirjanpito-ohjelmassasi. Maksu tapahtuu Palkkaus.fi asiakasvaratilin kautta.",
          "label": "Verkkolasku (yksi maksu)"
        },
        "palkkausCfaPaytrail": {
          "description": "Palkka, ennakonpidätys ja työeläke yhdellä helpolla verkkomaksulla Palkkaus.fi asiakasvaratilin kautta.",
          "label": "Paytrail verkkomaksu (yksi maksu)"
        },
        "palkkausCfaReference": {
          "description": "PDF-paperilasku, jonka voit maksaa missä tahansa verkkopankissa tai ohjelmassa. Palkka, ennakonpidätys ja työeläke yhdessä laskussa Palkkaus.fi asiakasvaratilin kautta.",
          "label": "Yksi tilisiirto viitenumerolla"
        },
        "palkkausManual": {
          "description": "PDF-paperilaskut luodaan erikseen palkalle, ennakonpidätykselle jne. ja maksetaan suoraan vastaanottajalle eräpäivinä.",
          "label": "Tilisiirrot viitenumerolla"
        },
        "palkkausWS": {
          "description": "Palkasta, ennakonpidätyksestä jne. luodaan erilliset laskut ja ne lähetetään pankkien WS-kanavaan. Vaatii sopimuksen pankin kanssa.",
          "label": "Maksut suoraan pankkiin"
        },
        "talenomOnline": {
          "description": "Laskujen lähetys suoraan Talenom Online -palveluun",
          "label": "Talenom Online"
        },
        "test": {
          "description": "Laskujen lähetys testimaksupalveluun",
          "label": "Testimaksu"
        },
        "zeroPayment": {
          "description": "Maksun summa on nolla. Tätä maksua ei lähetetä maksukanavaan, vaan se merkitään suoraan maksetuksi.",
          "label": "Nollamaksu (ei maksettavaa)"
        }
      },
      "PaymentStatus": {
        "bankDelivered": {
          "description": "",
          "label": "Pankki hyväksynyt"
        },
        "bankError": {
          "description": "",
          "label": "Virhe pankissa"
        },
        "bankPartialError": {
          "description": "",
          "label": "Virhe pankissa"
        },
        "bankTechApproval": {
          "description": "",
          "label": "Pankki vastaanottanut"
        },
        "cancelled": {
          "description": "",
          "label": "Peruttu"
        },
        "new": {
          "description": "",
          "label": "Uusi"
        },
        "paid": {
          "description": "",
          "label": "Maksettu"
        },
        "sentToBank": {
          "description": "",
          "label": "Lähetetty pankkiin"
        },
        "unknown": {
          "description": "",
          "label": "Tuntematon"
        }
      },
      "PayrollStatus": {
        "draft": {
          "label": "Luonnos"
        },
        "paymentCanceled": {
          "label": "Maksu peruttu"
        },
        "paymentError": {
          "label": "Virheitä"
        },
        "paymentStarted": {
          "label": "Maksu aloitettu"
        },
        "paymentSucceeded": {
          "label": "Maksettu"
        },
        "template": {
          "label": "Palkkalistapohja (template)"
        }
      },
      "PensionCalculation": {
        "athlete": {
          "description": "",
          "label": "Urheilijan eläke"
        },
        "boardRemuneration": {
          "description": "",
          "label": "Hallituspalkkio"
        },
        "compensation": {
          "description": "",
          "label": "Työkorvaus"
        },
        "employee": {
          "description": "",
          "label": "TyEL"
        },
        "entrepreneur": {
          "description": "",
          "label": "YEL"
        },
        "farmer": {
          "description": "",
          "label": "MYEL"
        },
        "partialOwner": {
          "description": "Osaomistajan palkka\r\nYrityksen osaomistajat maksavat pienempää palkansaajan työttömyysvakuutusmaksua kuin työntekijät.\r\n•\tOsaomistajan työnantajan työttömyysvakuutusmaksu on 0,5 % palkkasummasta.\r\n•\tPalkansaajan, joka on myös osaomistaja, työttömyysvakuutusmaksuprosentti on 0,78 %. Työnantaja pidättää sen palkasta palkanmaksun yhteydessä.\r\nJos et ole ole yrityksessä johtavassa asemassa, mutta työskentelet yrityksessä tai yhteisössä, olet osaomistaja, jos jokin seuraavista ehdoista täyttyy: \r\n•\tsinulla itselläsi on vähintään 50 % kyseisen yrityksen tai yhteisön osakepääomasta, äänivallasta tai muusta määräämisvallasta \r\n•\tperheenjäsenelläsi on vähintään 50 % kyseisen yrityksen tai yhteisön osakepääomasta, äänivallasta tai muusta määräämisvallasta\r\n•\tsinulla on yhdessä perheenjäsentesi kanssa vähintään 50 % osakepääomasta, äänivallasta tai muusta määräämisvallasta. \r\nJos olet johtavassa asemassa mutta et ole yrittäjä, voit olla työttömyysturvalain mukaan osaomistaja, jos työskentelet yrityksessä ja jompikumpi seuraavista ehdoista täyttyy:\r\n•\tsinulla on vähintään 15 % osakepääomasta, äänivallasta tai muusta määräämisvallasta\r\n•\tperheenjäsenilläsi tai teillä yhdessä on vähintään 30 % osakepääomasta, äänivallasta tai muusta määräämisvallasta\r\nTyöllisyysrahaston sivulla voi testata onko osaomistaja vai palkansaaja\r\nhttps://www.tyollisyysrahasto.fi/tyottomyysvakuusmaksut/tyottomyysvakuutusmaksuvelvollisuus/#osaomistaja\r\nOsaomistajan käsite on määritelty työttömyysturvalaissa (1290/2002). \r\n",
          "label": "TyEL, osaomistaja"
        },
        "smallEntrepreneur": {
          "description": "Tulonsaaja täyttää yrittäjän eläkelain 3 §:ssä säädetyt edellytykset, mutta jolla ei ole vakuuttamisvelvollisuutta yrittäjän eläkelain 4 §:n nojalla.\nVakuuttamisvelvollisuus ei koske:\n1) yrittäjän toimintaa ennen 18 vuoden iän täyttämistä seuraavan kalenterikuukauden alkua, eikä työskentelyä, joka jatkuu sen kalenterikuukauden jälkeen, jolloin vuonna 1957 tai sitä ennen syntynyt yrittäjä täyttää 68 vuotta, vuosina 1958–1961 syntynyt yrittäjä täyttää 69 vuotta ja vuonna 1962 ja sen jälkeen syntynyt yrittäjä täyttää 70 vuotta (vakuuttamisvelvollisuuden yläikäraja); (29.1.2016/72)\n2) yrittäjän toimintaa, joka ei ole jatkunut yhdenjaksoisesti vähintään neljää kuukautta;\n3) yrittäjän toimintaa, joka on alkanut tai jatkunut sen jälkeen, kun hän on siirtynyt työeläkelakien mukaiselle vanhuuseläkkeelle;\n4) yrittäjää, jonka työtulo tässä laissa tarkoitetusta yrittäjätoiminnasta on arvioitava pienemmäksi kuin 5 504,14 euroa vuodessa;\n5) ansiotyötä, jonka perusteella yrittäjällä on oikeus eläkkeeseen muun työeläkelain mukaan; eikä\n6) yrittäjää, johon ei sovelleta Suomen lainsäädäntöä EU:n sosiaaliturvan perusasetuksen tai sosiaaliturvasopimuksen sovellettavaa lainsäädäntöä koskevien määräysten perusteella. (14.5.2010/355)",
          "label": "YEL, ei vakuuttamisvelvollisuutta"
        },
        "smallFarmer": {
          "description": "Tulonsaaja täyttää maatalousyrittäjän eläkelain 3 §:ssä säädetyt edellytykset, mutta jolla ei ole mainitun lain perusteella vakuuttamisvelvollisuutta.\n\nVakuuttamisvelvollisuus ei koske:\n1) maatalousyrittäjän toimintaa ennen 18 vuoden iän täyttämistä seuraavan kalenterikuukauden alkua eikä toimintaa, joka jatkuu sen kalenterikuukauden jälkeen, jolloin vuonna 1957 tai sitä ennen syntynyt maatalousyrittäjä täyttää 68 vuotta, vuosina 1958–1961 syntynyt maatalousyrittäjä täyttää 69 vuotta ja vuonna 1962 ja sen jälkeen syntynyt maatalousyrittäjä täyttää 70 vuotta (vakuuttamisvelvollisuuden yläikäraja); (29.1.2016/75)\n2) maatalousyrittäjän toimintaa, joka ei ole jatkunut 18 vuoden iän täyttämistä seuraavan kalenterikuukauden alun jälkeen yhdenjaksoisesti vähintään neljää kuukautta;\n3) maatalousyrittäjän toimintaa, joka on alkanut tai jatkunut sen jälkeen, kun hän on siirtynyt työeläkelakien mukaiselle vanhuuseläkkeelle;\n4) maatalousyrittäjää, jonka työtulo tässä laissa tarkoitetusta maatalousyrittäjätoiminnasta on arvioitava pienemmäksi kuin 2 752,07 euroa vuodessa;\n5) maatilatalouden, kalastuksen tai poronhoidon yhteydessä harjoitettua toimintaa, jota on pidettävä eri yrityksenä;\n6) ansiotyötä, jonka perusteella maatalousyrittäjällä on oikeus eläkkeeseen muun työeläkelain mukaan; eikä\n7) maatalousyrittäjää, johon ei sovelleta Suomen lainsäädäntöä EU:n sosiaaliturvan perusasetuksen tai sosiaaliturvasopimuksen sovellettavaa lainsäädäntöä koskevien määräysten perusteella. (14.5.2010/357)\n\nVakuuttamisvelvollisuus ei koske apurahansaajan työtä:\n1) ennen 18 vuoden iän täyttämistä seuraavan kalenterikuukauden alkua, eikä työtä, joka jatkuu sen kalenterikuukauden jälkeen, jolloin vuonna 1957 tai sitä ennen syntynyt apurahansaaja täyttää 68 vuotta, vuosina 1958–1961 syntynyt apurahansaaja täyttää 69 vuotta ja vuonna 1962 ja sen jälkeen syntynyt apurahansaaja täyttää 70 vuotta (vakuuttamisvelvollisuuden yläikäraja); (29.1.2016/75)\n2) joka ei ole jatkunut 18 vuoden iän täyttämistä seuraavan kalenterikuukauden alun jälkeen yhdenjaksoisesti vähintään neljä kuukautta;\n3) joka on alkanut tai jatkunut sen jälkeen, kun hän on siirtynyt työeläkelakien mukaiselle vanhuuseläkkeelle;\n4) ajalta, jona apurahansaajalle arvioitava vuotuinen työtulo on pienempi kuin 2 752,07 euroa;\n5) jos apurahansaajaan ei sovelleta Suomen sosiaaliturvalainsäädäntöä EU:n sosiaaliturvan perusasetuksen tai sosiaaliturvasopimuksen sovellettavaa lainsäädäntöä koskevien määräysten perusteella. (14.5.2010/357)",
          "label": "MYEL, ei vakuuttamisvelvollisuutta"
        },
        "undefined": {
          "description": "",
          "label": "(Ei valittu vielä)"
        }
      },
      "PensionCompany": {
        "apteekkien": {
          "description": "",
          "label": "Apteekkien Eläkekassa"
        },
        "elo": {
          "description": "",
          "label": "Elo"
        },
        "etera": {
          "description": "",
          "label": "Etera"
        },
        "ilmarindescription": "",
        "ilmarinen": {
          "label": "Ilmarinen"
        },
        "none": {
          "description": "",
          "label": "(Ei valittu vielä)"
        },
        "other": {
          "description": "",
          "label": "Muu yhtiö"
        },
        "pensionsAlandia": {
          "description": "",
          "label": "Alandia"
        },
        "varma": {
          "description": "",
          "label": "Varma"
        },
        "veritas": {
          "description": "",
          "label": "Veritas"
        },
        "verso": {
          "description": "",
          "label": "Verso"
        }
      },
      "PeriodType": {
        "custom": {
          "label": "Muu periodi"
        },
        "month": {
          "label": "Kuukausi"
        },
        "quarter": {
          "label": "Neljännesvuosi"
        },
        "year": {
          "label": "Kalenterivuosi"
        }
      },
      "PricingModel": {
        "fixedFee": {
          "label": "Kiinteä palkkio"
        },
        "noFee": {
          "label": "Ei palkkiota"
        },
        "palkkausFee": {
          "label": "Palkkaus.fi-palkkio (1%)"
        }
      },
      "Product": {
        "accounting": {
          "description": "Palkkalaskelmien tiedot helposti kirjanpitäjälle",
          "label": "Kirjanpitoraportit",
          "status": "Ei käytössä"
        },
        "baseService": {
          "description": "Digiajan palkanmaksupalvelu",
          "label": "Palkkaus.fi-tili",
          "status": "Palkkaamisen peruspalvelut"
        },
        "healthCareHeltti": {
          "description": "Tuottavammat ja terveemmät työntekijät",
          "label": "Työterveyshuolto",
          "status": "Ei sopimusta"
        },
        "insurance": {
          "description": "Tapaturmavakuutus vakuuttaa työntekijäsi",
          "label": "Työtapaturmavakuutus",
          "status": "Ei sopimusta. HUOM: työnantajan 1300 € :n vuosisääntö!"
        },
        "pension": {
          "description": "TyEL-vakuutus on työntekijän eläketurva",
          "label": "TyEL-eläkevakuutus",
          "status": "Ei sopimusta: TyEL maksetaan tilapäisenä työnantajana"
        },
        "pensionEntrepreneur": {
          "description": "YEL-vakuutus on yrittäjän eläketurva",
          "label": "Yrittäjän eläke",
          "status": "Ei YEL-sopimusta"
        },
        "tax": {
          "description": "Ennakonpidätykset ja sava-maksu",
          "label": "Yhteydet verottajaan",
          "status": "Ennakonpidätys ja ilmoitukset"
        },
        "taxCard": {
          "description": "Työntekijän verokortti ja sen käsittely",
          "label": "Verokortti (TT)",
          "status": "Ei verokorttia"
        },
        "unemployment": {
          "description": "Työttömyysturva rahoitetaan TVR-maksuilla",
          "label": "Työttömyysvakuutus (TVR)",
          "status": "Ilmoitukset TVR:lle",
          "statusTVR": "Ilmoitukset TVR:lle"
        }
      },
      "ProductListFilter": {
        "all": {
          "description": "",
          "label": "All products, even the currently non-available."
        },
        "available": {
          "description": "",
          "label": "Available products for this user"
        }
      },
      "Profession": {
        "0": {
          "label": "Sotilaat"
        },
        "1": {
          "label": "Johtajat"
        },
        "2": {
          "label": "Erityisasiantuntijat"
        },
        "3": {
          "label": "Asiantuntijat"
        },
        "4": {
          "label": "Toimisto- ja asiakaspalvelutyöntekijät"
        },
        "5": {
          "label": "Palvelu- ja myyntityöntekijät"
        },
        "6": {
          "label": "Maanviljelijät, metsätyöntekijät ym."
        },
        "7": {
          "label": "Rakennus-, korjaus- ja valmistustyöntekijät"
        },
        "8": {
          "label": "Prosessi- ja kuljetustyöntekijät"
        },
        "9": {
          "label": "Muut työntekijät"
        },
        "11": {
          "label": "Johtajat, ylimmät virkamiehet ja järjestöjen johtajat"
        },
        "12": {
          "label": "Hallintojohtajat ja kaupalliset johtajat"
        },
        "13": {
          "label": "Tuotantotoiminnan ja yhteiskunnan peruspalvelujen johtajat"
        },
        "14": {
          "label": "Hotelli- ja ravintola-alan, vähittäiskaupan ja muiden palvelualojen johtajat"
        },
        "21": {
          "label": "Luonnontieteiden ja tekniikan erityisasiantuntijat"
        },
        "22": {
          "label": "Terveydenhuollon erityisasiantuntijat"
        },
        "23": {
          "label": "Opettajat ja muut opetusalan erityisasiantuntijat"
        },
        "24": {
          "label": "Liike-elämän ja hallinnon erityisasiantuntijat"
        },
        "25": {
          "label": "Tieto- ja viestintäteknologian erityisasiantuntijat"
        },
        "26": {
          "label": "Lainopilliset, sosiaalialan ja kulttuurialan erityisasiantuntijat"
        },
        "31": {
          "label": "Luonnontieteiden ja tekniikan asiantuntijat"
        },
        "32": {
          "label": "Terveydenhuollon asiantuntijat"
        },
        "33": {
          "label": "Liike-elämän ja hallinnon asiantuntijat"
        },
        "34": {
          "label": "Lainopilliset avustajat sekä sosiaali- ja kulttuurialan asiantuntijat"
        },
        "35": {
          "label": "Informaatio- ja tietoliikenneteknologian asiantuntijat"
        },
        "41": {
          "label": "Toimistotyöntekijät"
        },
        "42": {
          "label": "Asiakaspalvelutyöntekijät"
        },
        "43": {
          "label": "Laskennan ja varastoinnin toimistotyöntekijät"
        },
        "44": {
          "label": "Muut toimisto- ja asiakaspalvelutyöntekijät"
        },
        "51": {
          "label": "Palvelutyöntekijät"
        },
        "52": {
          "label": "Myyjät, kauppiaat ym."
        },
        "53": {
          "label": "Hoivapalvelun ja terveydenhuollon työntekijät"
        },
        "54": {
          "label": "Suojelu- ja vartiointityöntekijät"
        },
        "61": {
          "label": "Maanviljelijät ja eläintenkasvattajat ym."
        },
        "62": {
          "label": "Metsä- ja kalatalouden työntekijät"
        },
        "63": {
          "label": "Kotitarveviljelijät, -kalastajat ja -metsästäjät"
        },
        "71": {
          "label": "Rakennustyöntekijät ym. (pl. sähköasentajat)"
        },
        "72": {
          "label": "Konepaja- ja valimotyöntekijät sekä asentajat ja korjaajat"
        },
        "73": {
          "label": "Käsityötuotteiden valmistajat, hienomekaanikot sekä painoalan työntekijät"
        },
        "74": {
          "label": "Sähkö- ja elektroniikka-alan työntekijät"
        },
        "75": {
          "label": "Elintarvike-, puutyö- ja vaatetus- ja jalkinealan valmistustyöntekijät ym."
        },
        "81": {
          "label": "Prosessityöntekijät"
        },
        "82": {
          "label": "Teollisuustuotteiden kokoonpanijat"
        },
        "83": {
          "label": "Kuljetustyöntekijät"
        },
        "91": {
          "label": "Siivoojat, kotiapulaiset ja muut puhdistustyöntekijät"
        },
        "92": {
          "label": "Maa-, metsä- ja kalatalouden avustavat työntekijät"
        },
        "93": {
          "label": "Teollisuuden ja rakentamisen avustavat työntekijät"
        },
        "94": {
          "label": "Avustavat keittiö- ja ruokatyöntekijät"
        },
        "95": {
          "label": "Katumyyjät, kengänkiillottajat ym."
        },
        "96": {
          "label": "Katujen puhtaanapidon ja jätehuollon työntekijät ym."
        },
        "111": {
          "label": "Ylimmät virkamiehet ja järjestöjen johtajat"
        },
        "112": {
          "label": "Toimitusjohtajat ja pääjohtajat"
        },
        "121": {
          "label": "Liiketoiminta- ja hallintojohtajat"
        },
        "122": {
          "label": "Myynti-, markkinointi- ja kehitysjohtajat"
        },
        "131": {
          "label": "Maa-, metsä- ja kalatalouden johtajat"
        },
        "132": {
          "label": "Teollisuuden tuotantojohtajat sekä kaivos-, rakennus- ja jakelujohtajat"
        },
        "133": {
          "label": "Tieto- ja viestintäteknologiajohtajat"
        },
        "134": {
          "label": "Yhteiskunnan peruspalvelujen sekä rahoitus- ja vakuutuspalvelujen johtajat"
        },
        "141": {
          "label": "Hotellin- ja ravintolanjohtajat"
        },
        "142": {
          "label": "Vähittäis- ja tukkukaupan johtajat"
        },
        "143": {
          "label": "Muiden palvelualojen johtajat"
        },
        "211": {
          "label": "Luonnon- ja geotieteen erityisasiantuntijat"
        },
        "212": {
          "label": "Matematiikan ja tilastotieteen erityisasiantuntijat"
        },
        "213": {
          "label": "Biotieteiden erityisasiantuntijat"
        },
        "214": {
          "label": "Tekniikan erityisasiantuntijat (pl. sähköteknologia)"
        },
        "215": {
          "label": "Sähköteknologian erityisasiantuntijat"
        },
        "216": {
          "label": "Arkkitehdit, suunnittelijat ja maanmittaajat"
        },
        "221": {
          "label": "Lääkärit"
        },
        "222": {
          "label": "Hoitotyön erityisasiantuntijat"
        },
        "223": {
          "label": "Perinteisen ja vaihtoehtohoidon erityisasiantuntijat"
        },
        "224": {
          "label": "Muiden terveyspalvelujen tuottajat"
        },
        "225": {
          "label": "Eläinlääkärit"
        },
        "226": {
          "label": "Muut terveydenhuollon erityisasiantuntijat"
        },
        "231": {
          "label": "Yliopisto- ja korkeakouluopettajat"
        },
        "232": {
          "label": "Ammatillisen koulutuksen opettajat"
        },
        "233": {
          "label": "Lukion ja peruskoulun yläluokkien opettajat"
        },
        "234": {
          "label": "Peruskoulun alaluokkien opettajat ja lastentarhanopettajat"
        },
        "235": {
          "label": "Muut opetusalan erityisasiantuntijat"
        },
        "241": {
          "label": "Rahoitusalan erityisasiantuntijat"
        },
        "242": {
          "label": "Hallinnon erityisasiantuntijat"
        },
        "243": {
          "label": "Myynnin, markkinoinnin ja tiedotuksen erityisasiantuntijat"
        },
        "251": {
          "label": "Systeemityön erityisasiantuntijat"
        },
        "252": {
          "label": "Tietokantojen,- verkkojen ja järjestelmäohjelmistojen erityisasiantuntijat"
        },
        "261": {
          "label": "Lainopilliset erityisasiantuntijat"
        },
        "262": {
          "label": "Kirjaston- ja arkistonhoitajat sekä museoalan erityisasiantuntijat"
        },
        "263": {
          "label": "Yhteiskunta- ja sosiaalialan sekä uskonnollisen elämän erityisasiantuntijat"
        },
        "264": {
          "label": "Toimittajat, kirjailijat ja kielitieteilijät"
        },
        "265": {
          "label": "Taiteilijat"
        },
        "311": {
          "label": "Fysiikan, kemian ja teknisten alojen asiantuntijat"
        },
        "312": {
          "label": "Työnjohtajat kaivos-, teollisuus- ja rakennustoiminnassa"
        },
        "313": {
          "label": "Prosessinvalvonnan asiantuntijat"
        },
        "314": {
          "label": "Biotieteiden asiantuntijat"
        },
        "315": {
          "label": "Laiva-, lento- ja satamaliikenteen päälliköt ja ohjaajat"
        },
        "321": {
          "label": "Terveydenhuollon tekniset asiantuntijat"
        },
        "322": {
          "label": "Sairaanhoitajat, kätilöt ym."
        },
        "323": {
          "label": "Luontais- ja vaihtoehtohoitajat"
        },
        "324": {
          "label": "Seminologit ym."
        },
        "325": {
          "label": "Muut terveydenhuollon asiantuntijat"
        },
        "331": {
          "label": "Rahoitus-, vakuutus- ja laskentatoimen asiantuntijat"
        },
        "332": {
          "label": "Myynti- ja ostoagentit"
        },
        "333": {
          "label": "Yrityspalveluiden välittäjät"
        },
        "334": {
          "label": "Hallinnolliset ja erikoistuneet sihteerit"
        },
        "335": {
          "label": "Julkishallinnon valmistelu- ja valvontavirkamiehet"
        },
        "341": {
          "label": "Lainopilliset asiantuntijat sekä sosiaalialan ja seurakunnan työntekijät"
        },
        "342": {
          "label": "Urheilijat, urheiluvalmentajat, liikunnanohjaajat ym."
        },
        "343": {
          "label": "Taide- ja kulttuurialan asiantuntijat sekä keittiöpäälliköt"
        },
        "351": {
          "label": "Informaatio- ja tietoliikenneteknologian teknikot sekä käyttäjätukihenkilöt"
        },
        "352": {
          "label": "Teleliikenne- sekä radio- ja tv-teknikot"
        },
        "411": {
          "label": "Toimistoavustajat"
        },
        "412": {
          "label": "Yleissihteerit"
        },
        "413": {
          "label": "Tekstinkäsittelijät ja tallentajat"
        },
        "421": {
          "label": "Rahaliikenteen asiakaspalvelutyöntekijät"
        },
        "422": {
          "label": "Muut asiakaspalvelutyöntekijät"
        },
        "431": {
          "label": "Palkanlaskijat, vakuutuskäsittelijät ym."
        },
        "432": {
          "label": "Kuljetuksen ja varastoinnin toimistotyöntekijät"
        },
        "441": {
          "label": "Muut toimisto- ja asiakaspalvelutyöntekijät"
        },
        "511": {
          "label": "Matkustuspalvelutyöntekijät, konduktöörit ja oppaat"
        },
        "512": {
          "label": "Ravintola- ja suurtaloustyöntekijät"
        },
        "513": {
          "label": "Tarjoilutyöntekijät"
        },
        "514": {
          "label": "Kampaajat, parturit, kosmetologit ym."
        },
        "515": {
          "label": "Kiinteistöhuollon ja siivoustyön esimiehet"
        },
        "516": {
          "label": "Muut henkilökohtaisen palvelun työntekijät"
        },
        "521": {
          "label": "Katu- ja torikauppiaat"
        },
        "522": {
          "label": "Myyjät ja kauppiaat"
        },
        "523": {
          "label": "Kassanhoitajat ja lipunmyyjät"
        },
        "524": {
          "label": "Muut myyntityöntekijät"
        },
        "531": {
          "label": "Lastenhoitajat ja koulunkäyntiavustajat"
        },
        "532": {
          "label": "Lähihoitajat, muut terveydenhuollon työntekijät ja kodinhoitajat"
        },
        "541": {
          "label": "Suojelu- ja vartiointityöntekijät"
        },
        "611": {
          "label": "Pelto- ja puutarhaviljelijät"
        },
        "612": {
          "label": "Eläintenkasvattajat"
        },
        "613": {
          "label": "Yhdistetyn maanviljelyn ja eläintenkasvatuksen harjoittajat"
        },
        "621": {
          "label": "Metsurit ja metsätyöntekijät"
        },
        "622": {
          "label": "Kalanviljelijät, kalastajat ja metsästäjät"
        },
        "631": {
          "label": "Kotitarveviljelijät"
        },
        "632": {
          "label": "Kotitarvekarjankasvattajat"
        },
        "633": {
          "label": "Kotitarveviljelijät ja -karjankasvattajat"
        },
        "634": {
          "label": "Kotitarvekalastajat ja -metsästäjät"
        },
        "711": {
          "label": "Rakennustyöntekijät ym."
        },
        "712": {
          "label": "Rakennusten viimeistelytyöntekijät"
        },
        "713": {
          "label": "Maalarit ja rakennuspuhdistajat"
        },
        "721": {
          "label": "Valimotyöntekijät, hitsaajat, levysepät ym."
        },
        "722": {
          "label": "Sepät, työkaluntekijät ja koneenasettajat"
        },
        "723": {
          "label": "Koneasentajat ja -korjaajat"
        },
        "731": {
          "label": "Käsityötuotteiden valmistajat ja hienomekaanikot"
        },
        "732": {
          "label": "Painoalan työntekijät"
        },
        "741": {
          "label": "Sähkölaitteiden asentajat ja korjaajat"
        },
        "742": {
          "label": "Elektroniikka- ja tietoliikenneasentajat ja -korjaajat"
        },
        "751": {
          "label": "Lihanleikkaajat, leipurit, meijeristit ym."
        },
        "752": {
          "label": "Puutavaran käsittelijät, puusepät ym."
        },
        "753": {
          "label": "Vaatetusalan työntekijät ym."
        },
        "754": {
          "label": "Muut pääluokkaan 7 luokiteltavat työntekijät"
        },
        "811": {
          "label": "Kaivos- ja louhintatyön koneenkäyttäjät"
        },
        "812": {
          "label": "Metalliteollisuuden prosessityöntekijät ja viimeistelijät"
        },
        "813": {
          "label": "Kemianteollisuuden ja valokuvatuotteiden valmistuksen prosessityöntekijät"
        },
        "814": {
          "label": "Kumi-, muovi- ja paperituotteiden valmistuksen prosessityöntekijät"
        },
        "815": {
          "label": "Tekstiili-, turkis- ja nahkatuoteteollisuuden prosessityöntekijät"
        },
        "816": {
          "label": "Elintarviketeollisuuden prosessityöntekijät"
        },
        "817": {
          "label": "Sahatavaran sekä paperin ja kartongin valmistuksen prosessityöntekijät"
        },
        "818": {
          "label": "Muut prosessityöntekijät"
        },
        "821": {
          "label": "Teollisuustuotteiden kokoonpanijat"
        },
        "831": {
          "label": "Raideliikenteen kuljettajat ja työntekijät"
        },
        "832": {
          "label": "Henkilö- ja pakettiauton- sekä moottoripyöränkuljettajat"
        },
        "833": {
          "label": "Raskaiden moottoriajoneuvojen kuljettajat"
        },
        "834": {
          "label": "Työkoneiden kuljettajat"
        },
        "835": {
          "label": "Kansimiehistö ym. vesiliikenteen työntekijät"
        },
        "911": {
          "label": "Koti-, hotelli- ja toimistosiivoojat ym."
        },
        "912": {
          "label": "Ajoneuvojen puhdistajat, ikkunanpesijät ym."
        },
        "921": {
          "label": "Maa-, metsä- ja kalatalouden avustavat työntekijät"
        },
        "931": {
          "label": "Avustavat kaivos- ja rakennustyöntekijät"
        },
        "932": {
          "label": "Valmistusalan avustavat työntekijät"
        },
        "933": {
          "label": "Rahdinkäsittelijät ja varastotyöntekijät ym."
        },
        "941": {
          "label": "Avustavat keittiö- ja ruokatyöntekijät"
        },
        "951": {
          "label": "Mainosten jakajat, kengänkiillottajat ym."
        },
        "952": {
          "label": "Katumyyjät (pl. elintarvikkeet)"
        },
        "961": {
          "label": "Jätehuoltotyöntekijät"
        },
        "962": {
          "label": "Sanomalehtien jakajat, lähetit ym."
        },
        "1111": {
          "label": "Lainsäätäjät"
        },
        "1112": {
          "label": "Julkishallinnon ylimmät virkamiehet"
        },
        "1113": {
          "label": "Heimo- ja kyläpäälliköt"
        },
        "1114": {
          "label": "Järjestöjen johtajat"
        },
        "1120": {
          "label": "Toimitusjohtajat ja pääjohtajat"
        },
        "1211": {
          "label": "Talousjohtajat"
        },
        "1212": {
          "label": "Henkilöstöjohtajat"
        },
        "1213": {
          "label": "Politiikka- ja suunnittelujohtajat"
        },
        "1219": {
          "label": "Muut hallintojohtajat ja kaupalliset johtajat"
        },
        "1221": {
          "label": "Myynti- ja markkinointijohtajat"
        },
        "1222": {
          "label": "Mainos- ja tiedotusjohtajat"
        },
        "1223": {
          "label": "Tutkimus- ja kehitysjohtajat"
        },
        "1311": {
          "label": "Maa- ja metsätalouden johtajat"
        },
        "1312": {
          "label": "Vesiviljely- ja kalatalouden johtajat"
        },
        "1321": {
          "label": "Teollisuuden tuotantojohtajat"
        },
        "1322": {
          "label": "Kaivostoiminnan tuotantojohtajat"
        },
        "1323": {
          "label": "Rakennustoiminnan tuotantojohtajat"
        },
        "1324": {
          "label": "Hankinta- ja jakelujohtajat"
        },
        "1330": {
          "label": "Tieto- ja viestintäteknologiajohtajat"
        },
        "1341": {
          "label": "Lastenhoidon johtajat"
        },
        "1342": {
          "label": "Terveydenhuollon johtajat"
        },
        "1343": {
          "label": "Vanhustenhuollon johtajat"
        },
        "1344": {
          "label": "Sosiaalihuollon johtajat"
        },
        "1345": {
          "label": "Opetusalan johtajat"
        },
        "1346": {
          "label": "Rahoitus- ja vakuutuspalvelujen johtajat"
        },
        "1349": {
          "label": "Muut yhteiskunnan palvelujen johtajat"
        },
        "1411": {
          "label": "Hotellinjohtajat"
        },
        "1412": {
          "label": "Ravintolanjohtajat"
        },
        "1420": {
          "label": "Vähittäis- ja tukkukaupan johtajat"
        },
        "1431": {
          "label": "Urheilu-, vapaa-aika- ja kulttuurikeskusten johtajat"
        },
        "1439": {
          "label": "Muut palvelualojen johtajat"
        },
        "2111": {
          "label": "Fyysikot ja astronomit"
        },
        "2112": {
          "label": "Meteorologit"
        },
        "2113": {
          "label": "Kemistit"
        },
        "2114": {
          "label": "Geologit ja geofyysikot"
        },
        "2120": {
          "label": "Matemaatikot, aktuaarit ja tilastotieteilijät"
        },
        "2131": {
          "label": "Biologit, kasvi- ja eläintieteilijät ym. erityisasiantuntijat"
        },
        "2132": {
          "label": "Maa-, metsä- ja kalatalouden erityisasiantuntijat"
        },
        "2133": {
          "label": "Ympäristön- ja luonnonsuojelun erityisasiantuntijat"
        },
        "2141": {
          "label": "Teollisen valmistuksen ja tuotantotekniikan erityisasiantuntijat"
        },
        "2142": {
          "label": "Maa- ja vesirakentamisen erityisasiantuntijat"
        },
        "2143": {
          "label": "Ympäristötekniikan erityisasiantuntijat"
        },
        "2144": {
          "label": "Konetekniikan erityisasiantuntijat"
        },
        "2145": {
          "label": "Puunjalostuksen ja kemian prosessitekniikan erityisasiantuntijat"
        },
        "2146": {
          "label": "Kaivosteollisuuden, metallurgian ym. erityisasiantuntijat"
        },
        "2149": {
          "label": "Muut tekniikan erityisasiantuntijat"
        },
        "2151": {
          "label": "Sähkötekniikan erityisasiantuntijat"
        },
        "2152": {
          "label": "Elektroniikan erityisasiantuntijat"
        },
        "2153": {
          "label": "ICT-alan erityisasiantuntijat"
        },
        "2161": {
          "label": "Talonrakennuksen arkkitehdit"
        },
        "2162": {
          "label": "Maisema-arkkitehdit"
        },
        "2163": {
          "label": "Tuote- ja vaatesuunnittelijat"
        },
        "2164": {
          "label": "Yhdyskunta- ja liikennesuunnittelijat"
        },
        "2165": {
          "label": "Kartoituksen ja maanmittauksen erityisasiantuntijat"
        },
        "2166": {
          "label": "Graafiset ja multimediasuunnittelijat"
        },
        "2211": {
          "label": "Yleislääkärit"
        },
        "2212": {
          "label": "Ylilääkärit ja erikoislääkärit"
        },
        "2221": {
          "label": "Ylihoitajat ja osastonhoitajat"
        },
        "2222": {
          "label": "Johtavat kätilöt"
        },
        "2230": {
          "label": "Perinteisen ja vaihtoehtohoidon erityisasiantuntijat"
        },
        "2240": {
          "label": "Muiden terveyspalvelujen tuottajat"
        },
        "2250": {
          "label": "Eläinlääkärit"
        },
        "2261": {
          "label": "Hammaslääkärit"
        },
        "2262": {
          "label": "Proviisorit"
        },
        "2263": {
          "label": "Ympäristöterveyden ja työsuojelun erityisasiantuntijat"
        },
        "2264": {
          "label": "Fysioterapian erityisasiantuntijat"
        },
        "2265": {
          "label": "Ravitsemusalan erityisasiantuntijat"
        },
        "2266": {
          "label": "Kuulontutkijat ja puheterapeutit"
        },
        "2267": {
          "label": "Optometrian erityisasiantuntijat"
        },
        "2269": {
          "label": "Muut muualla luokittelemattomat terveydenhuollon erityisasiantuntijat"
        },
        "2310": {
          "label": "Yliopistojen ja ammattikorkeakoulujen opettajat"
        },
        "2320": {
          "label": "Ammatillisen koulutuksen opettajat"
        },
        "2330": {
          "label": "Lukion ja peruskoulun yläluokkien opettajat"
        },
        "2341": {
          "label": "Peruskoulun alaluokkien opettajat"
        },
        "2342": {
          "label": "Lastentarhanopettajat"
        },
        "2351": {
          "label": "Opetusmenetelmien erityisasiantuntijat"
        },
        "2352": {
          "label": "Erityisopettajat"
        },
        "2353": {
          "label": "Muut kieltenopettajat"
        },
        "2354": {
          "label": "Muut musiikin opettajat"
        },
        "2355": {
          "label": "Muut taideaineiden opettajat"
        },
        "2356": {
          "label": "Muut tietotekniikan opettajat ja kouluttajat"
        },
        "2359": {
          "label": "Opinto-ohjaajat ja muut opetuksen erityisasiantuntijat"
        },
        "2411": {
          "label": "Laskentatoimen erityisasiantuntijat ja tilintarkastajat"
        },
        "2412": {
          "label": "Rahoitus- ja sijoitusneuvojat"
        },
        "2413": {
          "label": "Rahoitusanalyytikot"
        },
        "2421": {
          "label": "Johtamisen ja organisaatioiden erityisasiantuntijat"
        },
        "2422": {
          "label": "Hallinnon ja elinkeinojen kehittämisen erityisasiantuntijat"
        },
        "2423": {
          "label": "Henkilöstöhallinnon erityisasiantuntijat ja urasuunnittelijat"
        },
        "2424": {
          "label": "Henkilöstön kehittämisen erityisasiantuntijat ja kouluttajat"
        },
        "2431": {
          "label": "Mainonnan ja markkinoinnin erityisasiantuntijat"
        },
        "2432": {
          "label": "Tiedottajat"
        },
        "2433": {
          "label": "Myynti-insinöörit ja lääke-esittelijät (pl. tieto- ja viestintätekniikka)"
        },
        "2434": {
          "label": "Tieto- ja viestintätekniikan myynnin erityisasiantuntijat"
        },
        "2511": {
          "label": "Sovellusarkkitehdit"
        },
        "2512": {
          "label": "Sovellussuunnittelijat"
        },
        "2513": {
          "label": "Web- ja multimediakehittäjät"
        },
        "2514": {
          "label": "Sovellusohjelmoijat"
        },
        "2519": {
          "label": "Muut ohjelmisto- ja sovelluskehittäjät"
        },
        "2521": {
          "label": "Tietokantasuunnittelijat ja -vastaavat"
        },
        "2522": {
          "label": "Tietojärjestelmien ylläpitäjät"
        },
        "2523": {
          "label": "Tietoverkkojen erityisasiantuntijat"
        },
        "2529": {
          "label": "Muut tietokanta- ja tietoverkkojen erityisasiantuntijat"
        },
        "2611": {
          "label": "Asianajajat"
        },
        "2612": {
          "label": "Tuomioistuinlakimiehet"
        },
        "2619": {
          "label": "Muut lainopilliset erityisasiantuntijat"
        },
        "2621": {
          "label": "Arkistonhoitajat ja museoalan erityisasiantuntijat"
        },
        "2622": {
          "label": "Kirjastonhoitajat, informaatikot ym."
        },
        "2631": {
          "label": "Ekonomistit"
        },
        "2632": {
          "label": "Yhteiskunta- ja kulttuuritutkijat"
        },
        "2633": {
          "label": "Historioitsijat, politiikan tutkijat ja filosofit"
        },
        "2634": {
          "label": "Psykologit"
        },
        "2635": {
          "label": "Sosiaalityön erityisasiantuntijat"
        },
        "2636": {
          "label": "Papit ym. uskonnollisen elämän erityisasiantuntijat"
        },
        "2641": {
          "label": "Kirjailijat ym."
        },
        "2642": {
          "label": "Toimittajat"
        },
        "2643": {
          "label": "Kääntäjät, tulkit ja muut kielitieteilijät"
        },
        "2651": {
          "label": "Kuvataiteilijat"
        },
        "2652": {
          "label": "Muusikot, laulajat ja säveltäjät"
        },
        "2653": {
          "label": "Tanssitaiteilijat ja koreografit"
        },
        "2654": {
          "label": "Ohjaajat ja tuottajat"
        },
        "2655": {
          "label": "Näyttelijät"
        },
        "2656": {
          "label": "Juontajat, kuuluttajat ym."
        },
        "2659": {
          "label": "Muut taiteilijat"
        },
        "3111": {
          "label": "Luonnontieteen tekniset asiantuntijat"
        },
        "3112": {
          "label": "Rakentamisen asiantuntijat"
        },
        "3113": {
          "label": "Sähkötekniikan asiantuntijat"
        },
        "3114": {
          "label": "Elektroniikan asiantuntijat"
        },
        "3115": {
          "label": "Konetekniikan asiantuntijat"
        },
        "3116": {
          "label": "Kemian prosessitekniikan asiantuntijat"
        },
        "3117": {
          "label": "Kaivosteollisuuden ja metallurgian asiantuntijat"
        },
        "3118": {
          "label": "Tekniset piirtäjät"
        },
        "3119": {
          "label": "Muut fysiikan, kemian ja teknisten alojen asiantuntijat"
        },
        "3121": {
          "label": "Kaivostyönjohtajat"
        },
        "3122": {
          "label": "Teollisuuden työnjohtajat"
        },
        "3123": {
          "label": "Rakennusalan työnjohtajat"
        },
        "3131": {
          "label": "Voimalaitosten prosessinhoitajat"
        },
        "3132": {
          "label": "Jätteenpoltto- ja vedenpuhdistuslaitosten prosessinhoitajat"
        },
        "3133": {
          "label": "Kemianteollisuuden prosessinhoitajat"
        },
        "3134": {
          "label": "Öljy- ja maakaasujalostamon prosessinhoitajat"
        },
        "3135": {
          "label": "Metallien jalostuksen prosessinhoitajat"
        },
        "3139": {
          "label": "Muut prosessinvalvonnan asiantuntijat"
        },
        "3141": {
          "label": "Laborantit ym."
        },
        "3142": {
          "label": "Maa- ja kalatalousteknikot"
        },
        "3143": {
          "label": "Metsätalousteknikot"
        },
        "3151": {
          "label": "Laivojen konepäälliköt ja -mestarit"
        },
        "3152": {
          "label": "Vesiliikenteen perämiehet ja päälliköt"
        },
        "3153": {
          "label": "Lentokapteenit ja -perämiehet"
        },
        "3154": {
          "label": "Lennonjohtajat"
        },
        "3155": {
          "label": "Lennonvalvonnan tekniset asiantuntijat"
        },
        "3211": {
          "label": "Lääketieteellisen kuvantamis- ja laitetekniikan asiantuntijat"
        },
        "3212": {
          "label": "Bioanalyytikot (terveydenhuolto)"
        },
        "3213": {
          "label": "Farmaseutit"
        },
        "3214": {
          "label": "Hammas- ja apuvälineteknikot"
        },
        "3221": {
          "label": "Sairaanhoitajat ym."
        },
        "3222": {
          "label": "Kätilöt"
        },
        "3230": {
          "label": "Luontais- ja vaihtoehtohoitajat"
        },
        "3240": {
          "label": "Seminologit ym."
        },
        "3251": {
          "label": "Suuhygienistit"
        },
        "3252": {
          "label": "Potilas- ja terveystietojen käsittelijät"
        },
        "3253": {
          "label": "Terveysneuvojat"
        },
        "3254": {
          "label": "Optikot"
        },
        "3255": {
          "label": "Fysioterapeutit ym."
        },
        "3256": {
          "label": "Avustavat hoitotyöntekijät"
        },
        "3257": {
          "label": "Terveys- ja työsuojelutarkastajat"
        },
        "3258": {
          "label": "Sairaankuljetuksen ensihoitajat"
        },
        "3259": {
          "label": "Muut muualla luokittelemattomat terveydenhuollon asiantuntijat"
        },
        "3311": {
          "label": "Arvopaperi- ja valuuttakauppiaat"
        },
        "3312": {
          "label": "Luotto- ja laina-asiantuntijat"
        },
        "3313": {
          "label": "Kirjanpidon ja laskentatoimen asiantuntijat"
        },
        "3314": {
          "label": "Tilastointi- ja matematiikka-asiantuntijat"
        },
        "3315": {
          "label": "Arvioitsijat ja vahinkotarkastajat"
        },
        "3321": {
          "label": "Vakuutusalan palvelumyyjät"
        },
        "3322": {
          "label": "Myyntiedustajat"
        },
        "3323": {
          "label": "Sisäänostajat"
        },
        "3324": {
          "label": "Kaupanvälittäjät"
        },
        "3331": {
          "label": "Huolitsijat, tulli- ja laivanselvittäjät"
        },
        "3332": {
          "label": "Konferenssi- ja tapahtumajärjestäjät"
        },
        "3333": {
          "label": "Työnvälittäjät"
        },
        "3334": {
          "label": "Kiinteistönvälittäjät ja isännöitsijät"
        },
        "3339": {
          "label": "Muut liike-elämän asiantuntijat"
        },
        "3341": {
          "label": "Toimistotyön esimiehet"
        },
        "3342": {
          "label": "Asianajosihteerit"
        },
        "3343": {
          "label": "Johdon sihteerit ja osastosihteerit"
        },
        "3344": {
          "label": "Toimistosihteerit (terveydenhuolto)"
        },
        "3351": {
          "label": "Tulli- ja rajavirkamiehet"
        },
        "3352": {
          "label": "Verovalmistelijat ja -tarkastajat"
        },
        "3353": {
          "label": "Sosiaaliturvaetuuksien käsittelijät"
        },
        "3354": {
          "label": "Lupavirkamiehet"
        },
        "3355": {
          "label": "Komisariot ja ylikonstaapelit"
        },
        "3359": {
          "label": "Muut julkishallinnon valmistelu- ja valvontavirkamiehet"
        },
        "3411": {
          "label": "Lainopilliset avustajat ja järjestöalan asiantuntijat"
        },
        "3412": {
          "label": "Sosiaalialan ohjaajat ja neuvojat ym."
        },
        "3413": {
          "label": "Seurakuntatyöntekijät"
        },
        "3421": {
          "label": "Urheilijat"
        },
        "3422": {
          "label": "Urheiluvalmentajat ja toimitsijat"
        },
        "3423": {
          "label": "Liikunnan ja vapaa-ajan ohjaajat"
        },
        "3431": {
          "label": "Valokuvaajat"
        },
        "3432": {
          "label": "Sisustussuunnittelijat ym."
        },
        "3433": {
          "label": "Gallerioiden, museoiden ja kirjastojen tekniset työntekijät"
        },
        "3434": {
          "label": "Keittiöpäälliköt"
        },
        "3435": {
          "label": "Muut taide- ja kulttuurialan asiantuntijat"
        },
        "3511": {
          "label": "Käytön operaattorit"
        },
        "3512": {
          "label": "Käytön tukihenkilöt"
        },
        "3513": {
          "label": "Tietoverkkoteknikot"
        },
        "3514": {
          "label": "Webmasterit ja -teknikot"
        },
        "3521": {
          "label": "Lähetys- ja audiovisuaaliteknikot"
        },
        "3522": {
          "label": "Televiestinnän tekniset asiantuntijat"
        },
        "4110": {
          "label": "Toimistoavustajat"
        },
        "4120": {
          "label": "Yleissihteerit"
        },
        "4131": {
          "label": "Tekstinkäsittelijät"
        },
        "4132": {
          "label": "Tallentajat"
        },
        "4211": {
          "label": "Pankki- ym. toimihenkilöt"
        },
        "4212": {
          "label": "Vedonvälittäjät, bingo- ja kasinopelin hoitajat ym."
        },
        "4213": {
          "label": "Panttilainaajat"
        },
        "4214": {
          "label": "Maksujenperijät"
        },
        "4221": {
          "label": "Matkatoimistovirkailijat"
        },
        "4222": {
          "label": "Puhelinpalveluneuvojat"
        },
        "4223": {
          "label": "Puhelinvaihteenhoitajat"
        },
        "4224": {
          "label": "Hotellin vastaanottovirkailijat"
        },
        "4225": {
          "label": "Informaatiopisteen asiakasneuvojat"
        },
        "4226": {
          "label": "Vastaanoton ja neuvonnan hoitajat"
        },
        "4227": {
          "label": "Tutkimus- ja markkinatutkimushaastattelijat"
        },
        "4229": {
          "label": "Muualla luokittelemattomat asiakaspalvelutyöntekijät"
        },
        "4311": {
          "label": "Taloushallinnon toimistotyöntekijät"
        },
        "4312": {
          "label": "Tilasto-, rahoitus- ja vakuutusalan toimistotyöntekijät"
        },
        "4313": {
          "label": "Palkanlaskijat"
        },
        "4321": {
          "label": "Varastonhoitajat ym."
        },
        "4322": {
          "label": "Tuotannon valmistelijat"
        },
        "4323": {
          "label": "Kuljetuksen toimistotyöntekijät"
        },
        "4411": {
          "label": "Kirjastotyöntekijät"
        },
        "4412": {
          "label": "Postinkantajat ja -lajittelijat"
        },
        "4413": {
          "label": "Koodaajat, oikolukijat ym."
        },
        "4414": {
          "label": "Kirjurit ym."
        },
        "4415": {
          "label": "Arkistotyöntekijät"
        },
        "4416": {
          "label": "Henkilöstöhallinnon avustavat toimistotyöntekijät"
        },
        "4419": {
          "label": "Muut muualla luokittelemattomat toimisto- ja asiakaspalvelutyöntekijät"
        },
        "5111": {
          "label": "Lentoemännät, purserit ym."
        },
        "5112": {
          "label": "Konduktöörit, lipuntarkastajat ym."
        },
        "5113": {
          "label": "Matkaoppaat"
        },
        "5120": {
          "label": "Ravintola- ja suurtaloustyöntekijät"
        },
        "5131": {
          "label": "Tarjoilijat"
        },
        "5132": {
          "label": "Baarimestarit"
        },
        "5141": {
          "label": "Kampaajat ja parturit"
        },
        "5142": {
          "label": "Kosmetologit ym."
        },
        "5151": {
          "label": "Siivoustyön esimiehet toimistoissa, hotelleissa ja muissa laitoksissa"
        },
        "5152": {
          "label": "Yksityiskotien taloudenhoitajat"
        },
        "5153": {
          "label": "Kiinteistöhuollon työntekijät"
        },
        "5161": {
          "label": "Astrologit, ennustajat ym."
        },
        "5162": {
          "label": "Henkilökohtaiset palvelijat"
        },
        "5163": {
          "label": "Hautauspalvelutyöntekijät"
        },
        "5164": {
          "label": "Eläintenhoitajat ja lemmikkieläinten trimmaajat"
        },
        "5165": {
          "label": "Ajo-opettajat"
        },
        "5169": {
          "label": "Muualla luokittelemattomat henkilökohtaisen palvelun työntekijät"
        },
        "5211": {
          "label": "Kioski- ja torimyyjät"
        },
        "5212": {
          "label": "Katumyyjät (elintarvikkeet)"
        },
        "5221": {
          "label": "Kauppiaat (pienyrittäjät)"
        },
        "5222": {
          "label": "Myymäläesimiehet"
        },
        "5223": {
          "label": "Myyjät"
        },
        "5230": {
          "label": "Kassanhoitajat ja lipunmyyjät"
        },
        "5241": {
          "label": "Mallit"
        },
        "5242": {
          "label": "Tuote-esittelijät"
        },
        "5243": {
          "label": "Suoramyyjät"
        },
        "5244": {
          "label": "Puhelin- ja asiakaspalvelukeskusten myyjät"
        },
        "5245": {
          "label": "Huoltamotyöntekijät"
        },
        "5246": {
          "label": "Kahvila- ja baarimyyjät"
        },
        "5249": {
          "label": "Muut muualla luokittelemattomat myyntityöntekijät"
        },
        "5311": {
          "label": "Lastenhoitotyöntekijät"
        },
        "5312": {
          "label": "Koulunkäyntiavustajat"
        },
        "5321": {
          "label": "Lähihoitajat"
        },
        "5322": {
          "label": "Kodinhoitajat (kotipalvelutoiminta)"
        },
        "5329": {
          "label": "Muut terveydenhuoltoalan työntekijät"
        },
        "5411": {
          "label": "Palomiehet"
        },
        "5412": {
          "label": "Poliisit"
        },
        "5413": {
          "label": "Vanginvartijat"
        },
        "5414": {
          "label": "Vartijat"
        },
        "5419": {
          "label": "Muut suojelu- ja vartiointityöntekijät"
        },
        "6111": {
          "label": "Pelto- ja avomaaviljelijät"
        },
        "6112": {
          "label": "Hedelmäpuiden ja pensaiden yms. kasvattajat"
        },
        "6113": {
          "label": "Puutarhurit, kasvihuoneviljelijät ja -työntekijät"
        },
        "6114": {
          "label": "Yhdistetyn maan- ja vihannesviljelyn tai puutarhanhoidon ym. harjoittajat"
        },
        "6121": {
          "label": "Liha- ja lypsykarjan kasvattajat sekä muiden kotieläinten kasvattajat"
        },
        "6122": {
          "label": "Siipikarjankasvattajat"
        },
        "6123": {
          "label": "Mehiläistenhoitajat ym."
        },
        "6129": {
          "label": "Muut eläinten kasvattajat ja hoitajat"
        },
        "6130": {
          "label": "Yhdistetyn maanviljelyn ja eläintenkasvatuksen harjoittajat"
        },
        "6210": {
          "label": "Metsurit ja metsätyöntekijät"
        },
        "6221": {
          "label": "Kalanviljelijät ja -viljelytyöntekijät"
        },
        "6222": {
          "label": "Kalastajat"
        },
        "6223": {
          "label": "Syvänmerenkalastajat"
        },
        "6224": {
          "label": "Riistanhoitajat ja metsästäjät"
        },
        "6310": {
          "label": "Kotitarveviljelijät"
        },
        "6320": {
          "label": "Kotitarvekarjankasvattajat"
        },
        "6330": {
          "label": "Kotitarveviljelijät ja -karjankasvattajat"
        },
        "6340": {
          "label": "Kotitarvekalastajat ja -metsästäjät"
        },
        "7111": {
          "label": "Talonrakentajat"
        },
        "7112": {
          "label": "Muurarit ym."
        },
        "7113": {
          "label": "Kivenhakkaajat ja -leikkaajat ym."
        },
        "7114": {
          "label": "Betonirakentajat ja raudoittajat"
        },
        "7115": {
          "label": "Kirvesmiehet ja rakennuspuusepät"
        },
        "7119": {
          "label": "Muut rakennustyöntekijät"
        },
        "7121": {
          "label": "Kattoasentajat ja -korjaajat"
        },
        "7122": {
          "label": "Lattianpäällystystyöntekijät"
        },
        "7123": {
          "label": "Rappaajat"
        },
        "7124": {
          "label": "Eristäjät"
        },
        "7125": {
          "label": "Lasinasentajat"
        },
        "7126": {
          "label": "Putkiasentajat"
        },
        "7127": {
          "label": "Ilmastointi- ja jäähdytyslaiteasentajat"
        },
        "7131": {
          "label": "Rakennusmaalarit ym."
        },
        "7132": {
          "label": "Ruiskumaalaajat ja -lakkaajat"
        },
        "7133": {
          "label": "Rakennuspuhdistajat ja nuohoojat"
        },
        "7211": {
          "label": "Muotin- ja keernantekijät"
        },
        "7212": {
          "label": "Hitsaajat ja kaasuleikkaajat"
        },
        "7213": {
          "label": "Ohutlevysepät"
        },
        "7214": {
          "label": "Paksulevysepät ja rautarakennetyöntekijät"
        },
        "7215": {
          "label": "Kaapelin- ja köysienasentajat"
        },
        "7221": {
          "label": "Sepät"
        },
        "7222": {
          "label": "Työkaluntekijät ja lukkosepät"
        },
        "7223": {
          "label": "Koneenasettajat ja koneistajat"
        },
        "7224": {
          "label": "Konehiojat, kiillottajat ja teroittajat"
        },
        "7231": {
          "label": "Moottoriajoneuvojen asentajat ja korjaajat"
        },
        "7232": {
          "label": "Lentokoneasentajat ja -korjaajat"
        },
        "7233": {
          "label": "Maatalous- ja teollisuuskoneasentajat ja -korjaajat"
        },
        "7234": {
          "label": "Polkupyöränkorjaajat ym."
        },
        "7311": {
          "label": "Kellosepät ja muut hienomekaanisten instrumenttien tekijät ja korjaajat"
        },
        "7312": {
          "label": "Soittimien tekijät ja virittäjät"
        },
        "7313": {
          "label": "Koru-, kulta- ja hopeasepät"
        },
        "7314": {
          "label": "Saven- ja tiilenvalajat ja dreijaajat"
        },
        "7315": {
          "label": "Lasinpuhaltajat, -leikkaajat, -hiojat ja -viimeistelijät"
        },
        "7316": {
          "label": "Kaivertajat, etsaajat ja koristemaalarit"
        },
        "7317": {
          "label": "Puu-, kori- yms. käsityötuotteiden tekijät"
        },
        "7318": {
          "label": "Tekstiili-, nahka- yms. käsityötuotteiden tekijät"
        },
        "7319": {
          "label": "Muut käsityöntekijät"
        },
        "7321": {
          "label": "Painopinnanvalmistajat"
        },
        "7322": {
          "label": "Painajat"
        },
        "7323": {
          "label": "Jälkikäsittelijät ja sitomotyöntekijät"
        },
        "7411": {
          "label": "Rakennussähköasentajat"
        },
        "7412": {
          "label": "Muut sähköasentajat"
        },
        "7413": {
          "label": "Linja-asentajat ja -korjaajat"
        },
        "7421": {
          "label": "Elektroniikka- ja automaatiolaitteiden asentajat ja korjaajat"
        },
        "7422": {
          "label": "Tieto- ja viestintäteknologian asentajat ja korjaajat"
        },
        "7511": {
          "label": "Lihanleikkaajat, kalankäsittelijät ym."
        },
        "7512": {
          "label": "Leipurit ja kondiittorit"
        },
        "7513": {
          "label": "Meijeristit, juustomestarit ym."
        },
        "7514": {
          "label": "Hedelmä- ja vihannestuotteiden valmistajat"
        },
        "7515": {
          "label": "Ruokien ja juomien laaduntarkkailijat"
        },
        "7516": {
          "label": "Tupakkatuotteiden valmistajat"
        },
        "7521": {
          "label": "Raakapuun käsittelijät"
        },
        "7522": {
          "label": "Huonekalupuusepät ym."
        },
        "7523": {
          "label": "Konepuusepät"
        },
        "7531": {
          "label": "Vaatturit, pukuompelijat, turkkurit ja hatuntekijät"
        },
        "7532": {
          "label": "Leikkaajat ja mallimestarit"
        },
        "7533": {
          "label": "Koru- ja muut tekstiiliompelijat"
        },
        "7534": {
          "label": "Verhoilijat"
        },
        "7535": {
          "label": "Turkisten muokkaajat ja nahkurit"
        },
        "7536": {
          "label": "Suutarit ym."
        },
        "7541": {
          "label": "Vedenalaistyöntekijät"
        },
        "7542": {
          "label": "Panostajat ja räjäyttäjät"
        },
        "7543": {
          "label": "Luokittelijat ja laaduntarkkailijat (pl. ruoat ja juomat)"
        },
        "7544": {
          "label": "Savupuhdistajat, tuholais- ja rikkakasvintorjujat"
        },
        "7549": {
          "label": "Muut pääluokkaan 7 muualla luokittelemattomat työntekijät"
        },
        "8111": {
          "label": "Kaivos- ja louhostyöntekijät"
        },
        "8112": {
          "label": "Rikastustyöntekijät"
        },
        "8113": {
          "label": "Iskuporaajat ja syväkairaajat"
        },
        "8114": {
          "label": "Betonituote- ym. teollisuuden prosessityöntekijät"
        },
        "8121": {
          "label": "Metalliteollisuuden prosessityöntekijät"
        },
        "8122": {
          "label": "Metallien teolliset päällystäjät ja viimeistelijät"
        },
        "8131": {
          "label": "Kemianteollisuuden prosessityöntekijät ym."
        },
        "8132": {
          "label": "Valokuvatuotteiden valmistuksen prosessityöntekijät"
        },
        "8141": {
          "label": "Kumituoteteollisuuden prosessityöntekijät"
        },
        "8142": {
          "label": "Muovituoteteollisuuden prosessityöntekijät"
        },
        "8143": {
          "label": "Paperituoteteollisuuden prosessityöntekijät"
        },
        "8151": {
          "label": "Kuitujenkäsittely-, kehruu- ja puolauskoneiden hoitajat"
        },
        "8152": {
          "label": "Kutoma- ja neulekoneiden hoitajat"
        },
        "8153": {
          "label": "Teollisuusompelijat"
        },
        "8154": {
          "label": "Valkaisu-, värjäys- ja puhdistuskoneiden hoitajat"
        },
        "8155": {
          "label": "Turkisten ja nahkojen teolliset käsittelijät, värjääjät ym."
        },
        "8156": {
          "label": "Jalkine- ja laukkuteollisuuden prosessityöntekijät"
        },
        "8157": {
          "label": "Pesulatyöntekijät"
        },
        "8159": {
          "label": "Muut tekstiili-, turkis- ja nahkatuoteteollisuuden prosessityöntekijät"
        },
        "8160": {
          "label": "Elintarviketeollisuuden prosessityöntekijät"
        },
        "8171": {
          "label": "Paperimassan sekä paperin ja kartongin valmistuksen prosessityöntekijät"
        },
        "8172": {
          "label": "Puu- ja sahatavaran prosessityöntekijät"
        },
        "8181": {
          "label": "Lasi- ja keramiikkateollisuuden uunienhoitajat"
        },
        "8182": {
          "label": "Höyrykoneiden ja lämmityskattiloiden hoitajat, lämmittäjät ym."
        },
        "8183": {
          "label": "Pakkaus-, pullotus- ja etiköintikoneiden hoitajat"
        },
        "8189": {
          "label": "Muut muualla luokittelemattomat prosessityöntekijät"
        },
        "8211": {
          "label": "Konepaja- ja metallituotteiden kokoonpanijat"
        },
        "8212": {
          "label": "Sähkö- ja elektroniikkalaitteiden kokoonpanijat"
        },
        "8219": {
          "label": "Muut teollisuustuotteiden kokoonpanijat"
        },
        "8311": {
          "label": "Veturinkuljettajat"
        },
        "8312": {
          "label": "Jarru-, turvalaite- ja vaihdetyöntekijät"
        },
        "8321": {
          "label": "Moottoripyörälähetit yms."
        },
        "8322": {
          "label": "Henkilö-, taksi- ja pakettiautonkuljettajat"
        },
        "8331": {
          "label": "Linja-auton- ja raitiovaununkuljettajat"
        },
        "8332": {
          "label": "Kuorma-auton ja erikoisajoneuvojen kuljettajat"
        },
        "8341": {
          "label": "Maa- ja metsätaloustyökoneiden kuljettajat"
        },
        "8342": {
          "label": "Maansiirtokoneiden ym. kuljettajat"
        },
        "8343": {
          "label": "Nosturinkuljettajat"
        },
        "8344": {
          "label": "Ahtaajat ja trukinkuljettajat ym."
        },
        "8350": {
          "label": "Kansimiehistö ym. vesiliikenteen työntekijät"
        },
        "9111": {
          "label": "Kotiapulaiset ja -siivoojat"
        },
        "9112": {
          "label": "Toimisto- ja laitossiivoojat ym."
        },
        "9121": {
          "label": "Puhdistajat ja prässääjät"
        },
        "9122": {
          "label": "Ajoneuvojen pesijät"
        },
        "9123": {
          "label": "Ikkunanpesijät"
        },
        "9129": {
          "label": "Muut puhdistustyöntekijät"
        },
        "9211": {
          "label": "Maanviljelyn avustavat työntekijät"
        },
        "9212": {
          "label": "Karjankasvatuksen avustavat työntekijät"
        },
        "9213": {
          "label": "Yhdistetyn maanviljelyn ja karjankasvatuksen avustavat työntekijät"
        },
        "9214": {
          "label": "Avustavat puutarhatyöntekijät"
        },
        "9215": {
          "label": "Metsätalouden avustavat työntekijät"
        },
        "9216": {
          "label": "Kalatalouden ja vesiviljelyn avustavat työntekijät"
        },
        "9311": {
          "label": "Kaivosten avustavat työntekijät"
        },
        "9312": {
          "label": "Maa- ja vesirakentamisen avustavat työntekijät"
        },
        "9313": {
          "label": "Rakennusalan avustavat työntekijät"
        },
        "9321": {
          "label": "Käsinpakkaajat"
        },
        "9329": {
          "label": "Muut valmistusalan avustavat työntekijät"
        },
        "9331": {
          "label": "Käsivetoisten- ja poljinkulkuneuvojen kuljettajat"
        },
        "9332": {
          "label": "Eläinvetoisten kulkuneuvojen kuljettajat"
        },
        "9333": {
          "label": "Rahdinkäsittelijät, varastotyöntekijät ym."
        },
        "9334": {
          "label": "Hyllyjen täyttäjät"
        },
        "9411": {
          "label": "Pikaruokatyöntekijät"
        },
        "9412": {
          "label": "Avustavat keittiötyöntekijät"
        },
        "9510": {
          "label": "Mainosten jakajat, kengänkiillottajat ym."
        },
        "9520": {
          "label": "Katumyyjät (pl. elintarvikkeet)"
        },
        "9611": {
          "label": "Jätteiden kerääjät"
        },
        "9612": {
          "label": "Jätteiden lajittelijat"
        },
        "9613": {
          "label": "Kadunlakaisijat ym."
        },
        "9621": {
          "label": "Sanomalehtien jakajat, lähetit ja kantajat"
        },
        "9622": {
          "label": "Satunnaistöiden tekijät"
        },
        "9623": {
          "label": "Mittareiden lukijat ym."
        },
        "9624": {
          "label": "Vedenhakijat ja polttopuun kerääjät"
        },
        "9629": {
          "label": "Muut muualla luokittelemattomat työntekijät"
        },
        "11121": {
          "label": "Valtion keskushallinnon johtajat"
        },
        "11122": {
          "label": "Alue- ja paikallishallinnon johtajat ja ylimmät virkamiehet"
        },
        "11141": {
          "label": "Työmarkkina- ja elinkeinoelämän järjestöjen johtajat"
        },
        "11142": {
          "label": "Muiden järjestöjen johtajat"
        },
        "21321": {
          "label": "Maa- ja kalatalouden erityisasiantuntijat"
        },
        "21322": {
          "label": "Metsätalouden erityisasiantuntijat"
        },
        "21531": {
          "label": "Tietoliikenneteknologian tutkijat"
        },
        "21532": {
          "label": "Tietotekniikka-alan tutkijat"
        },
        "22121": {
          "label": "Ylilääkärit"
        },
        "22122": {
          "label": "Erikoislääkärit"
        },
        "22211": {
          "label": "Ylihoitajat"
        },
        "22212": {
          "label": "Osastonhoitajat"
        },
        "23101": {
          "label": "Professorit"
        },
        "23102": {
          "label": "Lehtorit ja yliassistentit (yliopisto)"
        },
        "23103": {
          "label": "Assistentit ja tuntiopettajat (yliopisto)"
        },
        "23104": {
          "label": "Yliopettajat (AMK)"
        },
        "23105": {
          "label": "Lehtorit (AMK)"
        },
        "23106": {
          "label": "Tuntiopettajat ym. (AMK)"
        },
        "23301": {
          "label": "Matemaattisten aineiden opettajat"
        },
        "23302": {
          "label": "Äidinkielen ja kirjallisuuden opettajat"
        },
        "23303": {
          "label": "Taito- ja taideaineiden opettajat"
        },
        "23304": {
          "label": "Muut peruskoulun yläluokkien ja lukion opettajat"
        },
        "23411": {
          "label": "Luokanopettajat"
        },
        "23412": {
          "label": "Aineenopettajat (peruskoulun alaluokat)"
        },
        "23591": {
          "label": "Opinto-ohjaajat"
        },
        "23592": {
          "label": "Muut opetuksen erityisasiantuntijat"
        },
        "26211": {
          "label": "Arkistonhoitajat"
        },
        "26212": {
          "label": "Museoalan erityisasiantuntijat"
        },
        "26351": {
          "label": "Sosiaalityöntekijät ym."
        },
        "26352": {
          "label": "Sosiaalialan suunnittelijat ym."
        },
        "26421": {
          "label": "Toimituspäälliköt ja -sihteerit, ohjelmapäälliköt"
        },
        "26422": {
          "label": "Lehtien yms. toimittajat"
        },
        "26423": {
          "label": "Radio- ja tv-toimittajat"
        },
        "31121": {
          "label": "Talonrakentamisen asiantuntijat"
        },
        "31122": {
          "label": "Maa- ja vesirakentamisen asiantuntijat"
        },
        "31123": {
          "label": "Kartoituksen ja maanmittauksen asiantuntijat"
        },
        "31521": {
          "label": "Isojen alusten päälliköt ja perämiehet"
        },
        "31522": {
          "label": "Pienten alusten päälliköt"
        },
        "31523": {
          "label": "Satamaliikenteen ohjaajat ja satamakapteenit"
        },
        "32141": {
          "label": "Hammasteknikot"
        },
        "32142": {
          "label": "Apuvälineteknikot"
        },
        "32211": {
          "label": "Sairaanhoitajat"
        },
        "32212": {
          "label": "Terveydenhoitajat"
        },
        "32591": {
          "label": "Toimintaterapeutit"
        },
        "32592": {
          "label": "Muut terapeutit"
        },
        "33341": {
          "label": "Kiinteistönvälittäjät"
        },
        "33342": {
          "label": "Isännöitsijät"
        },
        "34111": {
          "label": "Oikeudenkäyntiasiamiehet ja ulosottomiehet"
        },
        "34112": {
          "label": "Asiamiehet, toimitsijat ym. järjestöalan asiantuntijat"
        },
        "34113": {
          "label": "Kuluttajaneuvojat ym."
        },
        "34121": {
          "label": "Sosiaalialan ohjaajat"
        },
        "34122": {
          "label": "Nuorisotyön ohjaajat (ei srk.)"
        },
        "34123": {
          "label": "Työn- ja askarteluohjaajat"
        },
        "34131": {
          "label": "Diakonit ja diakonissat"
        },
        "34139": {
          "label": "Muut seurakuntatyöntekijät"
        },
        "34351": {
          "label": "Kuvaussihteerit ja muut näyttämötekniset asiantuntijat"
        },
        "34359": {
          "label": "Muut taide- ja kulttuurialan tekniset asiantuntijat"
        },
        "42291": {
          "label": "Hälytyspäivystäjät"
        },
        "42299": {
          "label": "Muut muualla luokittelemattomat asiakaspalvelutyöntekijät"
        },
        "43231": {
          "label": "Raideliikenteen ohjaajat, ajojärjestelijät ym."
        },
        "43239": {
          "label": "Muut kuljetuksen ja huolinnan toimistotyöntekijät"
        },
        "44121": {
          "label": "Postinkantajat"
        },
        "44122": {
          "label": "Toimistovahtimestarit"
        },
        "51201": {
          "label": "Kokit, keittäjät ja kylmäköt"
        },
        "51202": {
          "label": "Ravintolaesimiehet ja vuoropäälliköt"
        },
        "51631": {
          "label": "Hautaustoimistonhoitajat ym."
        },
        "51632": {
          "label": "Muut hautaustyöntekijät"
        },
        "53111": {
          "label": "Päiväkotien ja muiden laitosten lastenhoitajat ym."
        },
        "53112": {
          "label": "Perhepäivähoitajat"
        },
        "53113": {
          "label": "Lastenkerhojen ohjaajat ym."
        },
        "53211": {
          "label": "Mielenterveyshoitajat"
        },
        "53212": {
          "label": "Kehitysvammaisten hoitajat"
        },
        "53213": {
          "label": "Sosiaalialan hoitajat"
        },
        "53219": {
          "label": "Muut lähihoitajat"
        },
        "53221": {
          "label": "Kotityöpalvelutyöntekijät"
        },
        "53222": {
          "label": "Henkilökohtaiset avustajat, omaishoitajat ym."
        },
        "53291": {
          "label": "Hammashoitajat"
        },
        "53292": {
          "label": "Välinehuoltajat"
        },
        "53293": {
          "label": "Apteekkien lääketyöntekijät"
        },
        "53294": {
          "label": "Hierojat ja kuntohoitajat"
        },
        "61111": {
          "label": "Peltoviljelijät"
        },
        "61112": {
          "label": "Peltoviljelytyönjohtajat ja -työntekijät"
        },
        "61131": {
          "label": "Puutarhurit ja kasvihuoneviljelijät"
        },
        "61132": {
          "label": "Puutarha- ja kasvihuonetyönjohtajat ja - työntekijät"
        },
        "61211": {
          "label": "Karjankasvattajat ym."
        },
        "61212": {
          "label": "Karjanhoitajat ym."
        },
        "61213": {
          "label": "Lemmikkieläinten kasvattajat"
        },
        "61214": {
          "label": "Maatalouslomittajat"
        },
        "61291": {
          "label": "Turkiseläinten ja porojen kasvattajat"
        },
        "61299": {
          "label": "Muualla luokittelemattomat eläintenhoitajat"
        },
        "62211": {
          "label": "Kalanviljely-yrittäjät"
        },
        "62212": {
          "label": "Kalanviljelytyönjohtajat ja -työntekijät"
        },
        "74211": {
          "label": "Elektroniikka-asentajat ja -korjaajat"
        },
        "74212": {
          "label": "Automaatioasentajat ja -korjaajat"
        },
        "81821": {
          "label": "Voima- ja jätteenkäsittelylaitosten laitosmiehet"
        },
        "81822": {
          "label": "Muut laitosmiehet ym."
        },
        "83441": {
          "label": "Ahtaajat"
        },
        "83442": {
          "label": "Trukinkuljettajat ym."
        },
        "91121": {
          "label": "Toimistosiivoojat ym."
        },
        "91122": {
          "label": "Hotellisiivoojat"
        },
        "91123": {
          "label": "Sairaala- ja laitosapulaiset"
        },
        "91124": {
          "label": "Päiväkotiapulaiset"
        },
        "91129": {
          "label": "Muut muualla luokittelemattomat siivoojat"
        },
        "01": {
          "label": "Upseerit"
        },
        "011": {
          "label": "Upseerit"
        },
        "0110": {
          "label": "Upseerit"
        },
        "02": {
          "label": "Aliupseerit"
        },
        "021": {
          "label": "Aliupseerit"
        },
        "0210": {
          "label": "Aliupseerit"
        },
        "03": {
          "label": "Sotilasammattihenkilöstö"
        },
        "031": {
          "label": "Sotilasammattihenkilöstö"
        },
        "0310": {
          "label": "Sotilasammattihenkilöstö"
        },
        "#X": {
          "label": "Tuntematon"
        },
        "#XX": {
          "label": "Tuntematon"
        },
        "#XXX": {
          "label": "Tuntematon"
        },
        "#XXXX": {
          "label": "Tuntematon"
        },
        "#XXXXX": {
          "label": "Tuntematon"
        }
      },
      "RemunerationKind": {
        "bonusPay": {
          "description": "Kertaluonteinen palkkiopalkka, jonka suuruus määräytyy kokonaan tai osittain sen mukaan, kuinka hyvin tavoitteeksi asetettu työtulos saavutetaan. Työtuloksen mittana voi olla määrä, laatu tai jokin muu tulostekijä.",
          "label": "Bonuspalkka"
        },
        "commission": {
          "description": "Pysyvän palkan osan muodostava jatkuvaluonteinen palkkiopalkka, jonka määrästä on sovittu työsopimuksessa.",
          "label": "Provisiopalkka"
        },
        "initiativeFee": {
          "description": "Kertapalkkio, joka maksetaan aloitteen tekijälle esimerkiksi toiminnan tai tuotteen kehittämiseen liittyvästä aloitteesta.",
          "label": "Aloitepalkkio"
        },
        "performanceBonus": {
          "description": "Palkka, joka maksetaan organisaation ennalta sovitun tulostavoitteen täyttymisen tai ylittymisen perusteella.",
          "label": "Tulospalkkio"
        },
        "profitSharingBonus": {
          "description": "Työntekijöille ilman ennalta sovittua suunnitelmaa yhtiökokouksen päätöksellä jaettava palkkio, joka muodostuu yrityksen voiton perusteella.",
          "label": "Voittopalkkio"
        },
        "shareIssueForEmployees": {
          "description": "Yhtiön omalle henkilökunnalle suunnattu osakeanti. Etu työsuhteeseen perustuvasta oikeudesta merkitä työnantajayrityksen osakkeita on veronalaista ansiotuloa.",
          "label": "Työsuhteeseen perustuva osakeanti"
        },
        "undefined": {
          "description": "Ei valintaa",
          "label": "Ei valintaa"
        }
      },
      "ReportCategory": {
        "all": {
          "description": "",
          "label": "All"
        },
        "monthly": {
          "description": "",
          "label": "Monthly"
        },
        "yearly": {
          "description": "",
          "label": "Yearly"
        }
      },
      "ReportType": {
        "employerReport": {
          "description": "",
          "label": "Työntekijäraportti"
        },
        "employmentContract": {
          "description": "",
          "label": "Työsopimus"
        },
        "example": {
          "description": "",
          "label": "Esimerkki"
        },
        "householdDeduction": {
          "description": "",
          "label": "Kotitalousvähennys"
        },
        "insurance": {
          "description": "",
          "label": "Vakuutus"
        },
        "monthlyDetails": {
          "description": "",
          "label": "Kuukausittaisia tietoja"
        },
        "monthlyLiikekirjuri": {
          "description": "",
          "label": ""
        },
        "monthlyPension": {
          "description": "",
          "label": "Kuukausittainen eläke"
        },
        "monthlyRapko": {
          "description": "",
          "label": ""
        },
        "paymentReport": {
          "description": "",
          "label": "Maksuraportti"
        },
        "salarySlip": {
          "description": "",
          "label": "Palkkakuitti"
        },
        "salarySlipPaid": {
          "description": "",
          "label": "Palkkakuitti maksettu"
        },
        "taxHouseholdDeduction14B": {
          "description": "",
          "label": ""
        },
        "taxHouseholdDeduction14BSpouseA": {
          "description": "",
          "label": ""
        },
        "taxHouseholdDeduction14BSpouseB": {
          "description": "",
          "label": ""
        },
        "taxMonthly4001": {
          "description": "",
          "label": "Kuukausittainen vero"
        },
        "taxYearly7801": {
          "description": "",
          "label": "Vuosittainen vero"
        },
        "undefined": {
          "description": "",
          "label": "Ei määritelty"
        },
        "unemployment": {
          "description": "",
          "label": "Työttömyys"
        },
        "yearEndReport": {
          "description": "",
          "label": "Loppuvuoden raportti"
        },
        "yearlyDetails": {
          "description": "",
          "label": "Vuosittaisia tietoja"
        },
        "yearlyWorkerSummary": {
          "description": "",
          "label": ""
        }
      },
      "SalaryKind": {
        "compensation": {
          "description": "",
          "label": "Korvaus",
          "labels": {
            "household": "Työkorvaus"
          }
        },
        "fixedSalary": {
          "description": "",
          "label": "Kiinteä palkka"
        },
        "hourlySalary": {
          "description": "",
          "label": "Tuntipalkka"
        },
        "monthlySalary": {
          "description": "",
          "label": "Kuukausipalkka"
        },
        "salary": {
          "description": "",
          "label": "Kertakorvaus"
        },
        "totalEmployerPayment": {
          "description": "",
          "label": ""
        },
        "totalWorkerPayment": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": "Ei määritelty"
        }
      },
      "SalaryPeriod": {
        "fixed": {
          "description": "",
          "label": "Fixed"
        },
        "hourly": {
          "description": "",
          "label": "Hourly"
        },
        "monthly": {
          "description": "",
          "label": "Monthly"
        },
        "notDefined": {
          "description": "",
          "label": "Not Defined"
        }
      },
      "SalaryPerMonthRange": {
        "noPensionRequired": {
          "description": "",
          "label": "Alle 57,10 €"
        },
        "normal": {
          "description": "",
          "label": "57,10 € tai yli"
        },
        "unknown": {
          "description": "",
          "label": "Ei tiedossa"
        }
      },
      "SalaryPerYearRange": {
        "noMonthlyReporting": {
          "description": "",
          "label": "201 - 1500 €"
        },
        "noReportingNecessary": {
          "description": "",
          "label": "1 - 200 €"
        },
        "normal": {
          "description": "",
          "label": "Yli 1500 €"
        },
        "zeroSalary": {
          "description": "",
          "label": 0
        }
      },
      "ServiceModel": {
        "partnerOnly": {
          "label": "Tilitoimistokäyttö"
        },
        "shared": {
          "label": "Yhteiskäyttö"
        }
      },
      "SubsidisedCommuteKind": {
        "noDeduction": {
          "description": "Ei vähennystä työntekijän palkasta",
          "label": "Ei vähennystä"
        },
        "periodicalDeduction": {
          "description": "Vähennys työntekijän palkasta jaetaan palkkakausiin (oletus 12 kuukautta).",
          "label": "Vähennys jaetaan palkkakausille"
        },
        "singleDeduction": {
          "description": "Vähennys työntekijän palkasta otetaan kerralla (tästä palkkalaskelmasta)",
          "label": "Vähennys palkasta kerralla"
        },
        "undefined": {
          "description": "Ei valintaa, oletus \"Ei vähennystä\"",
          "label": "Ei valintaa"
        }
      },
      "TaxCardIncomeType": {
        "externalSalaries": {
          "description": "",
          "label": "Ulkopuoliset palkat"
        },
        "previousEmployerSalaries": {
          "description": "Merkitse tähän työntekijän edellisten työnantajien kuluvana verovuonna maksamat palkat yhteensä euroina.",
          "label": "Edellisten työnantajien maksamat palkat"
        },
        "salaxyCalculation": {
          "description": "",
          "label": "Salaxy laskelma"
        },
        "unknown": {
          "description": "",
          "label": "Tuntematon"
        }
      },
      "TaxcardKind": {
        "defaultYearly": {
          "label": "Alkuperäinen verokortti"
        },
        "historical": {
          "label": "Vanha verokortti"
        },
        "noTaxCard": {
          "label": "Ei verokorttia (60%)"
        },
        "noWithholdingHousehold": {
          "label": "Ei ennakonpidätystä (alle 1500€)"
        },
        "others": {
          "label": "Muu verokortti"
        },
        "replacement": {
          "label": "Muutosverokortti"
        },
        "undefined": {
          "label": "Verokorttia ei ole määritetty"
        }
      },
      "TaxcardState": {
        "employerAdded": {
          "label": "Työnantajan lisäämä"
        },
        "expired": {
          "label": "Vanhentunut"
        },
        "new": {
          "label": "Uusi verokortti"
        },
        "replaced": {
          "label": "Korvattu (ei käytössä)"
        },
        "shared": {
          "label": "Työntekijän jakama"
        },
        "sharedApproved": {
          "label": "Työnantajan hyväksymä"
        },
        "sharedRejected": {
          "label": "Työnantaja hylännyt"
        },
        "sharedRejectedWithoutOpen": {
          "label": "Työnantaja hylännyt avaamatta"
        },
        "sharedWaiting": {
          "label": "Odottaa hyväksyntää"
        },
        "verifiedVero": {
          "label": "Haettu Verolta"
        }
      },
      "TaxcardValidity": {
        "expired": {
          "label": "Vanhentunut"
        },
        "future": {
          "label": "Tuleva"
        },
        "undefined": {
          "label": "Virhe"
        },
        "valid": {
          "label": "Voimassa"
        },
        "validJanuary": {
          "label": "Tammikuun voimassa"
        }
      },
      "TaxDeductionWorkCategories": {
        "carework": {
          "description": "",
          "label": "Hoiva- tai hoitotyö"
        },
        "homeImprovement": {
          "description": "",
          "label": "Asunnon kunnossapito- tai perusparannustyö"
        },
        "householdwork": {
          "description": "",
          "label": "Kotitaloustyö"
        },
        "none": {
          "description": "",
          "label": "Ei valintaa"
        },
        "ownPropety": {
          "description": "",
          "label": "Omassa käytössä oleva asunto tai vapaa-ajan asunto"
        },
        "relativesProperty": {
          "description": "",
          "label": "Vanhempien, isovanhempien yms. käytössä oleva asunto tai vapaa-ajan asunto."
        }
      },
      "TesSubtype": {
        "constructionCarpenter": {
          "description": "",
          "label": "Construction Carpenter"
        },
        "constructionFloor": {
          "description": "",
          "label": "Construction Floor"
        },
        "constructionFreeContract": {
          "description": "",
          "label": "Construction Free Contract"
        },
        "constructionOther": {
          "description": "",
          "label": "Construction Other"
        },
        "notSelected": {
          "description": "",
          "label": "Not Selected"
        }
      },
      "TestEnum": {
        "valueEight": {
          "description": "",
          "label": "Value Eight"
        },
        "valueFive": {
          "description": "",
          "label": "Value Five"
        },
        "valueFour": {
          "description": "",
          "label": "Value Four"
        },
        "valueNine": {
          "description": "",
          "label": "Value Nine"
        },
        "valueOne": {
          "description": "",
          "label": "Value One"
        },
        "valueSevdescription": "",
        "valueSevlabel": "Value Seven",
        "valueSix": {
          "description": "",
          "label": "Value Six"
        },
        "valueTdescription": "",
        "valueThree": {
          "description": "",
          "label": "Value Three"
        },
        "valueTlabel": "Value Ten",
        "valueTwo": {
          "description": "",
          "label": "Value Two"
        },
        "valueZero": {
          "description": "",
          "label": "Value Zero"
        }
      },
      "UnionPaymentKind": {
        "other": {
          "description": "Muu liitto: Työnantaja hoitaa maksun ja ilmoituksen itse.",
          "label": "Rakennusliitto, OA- tai TA-jäsen"
        },
        "raksaNormal": {
          "description": "Rakennusliiton normaalijäsenyys. Palkkaus.fi hoitaa maksun suoraan.",
          "label": "Muu liitto..."
        },
        "raksaUnemploymentOnly": {
          "description": "Rakennusliiton jäsenyys vain työttömyyskassassa. Palkkaus.fi hoitaa maksun suoraan.",
          "label": "Rakennusliitto, A-jäsen"
        },
        "undefined": {
          "description": "AY-jäsenmaksun tyyppiä ei valittu, rivi poistetaan",
          "label": "Ei jäsenyyttä"
        }
      },
      "UnionPaymentType": {
        "notSelected": {
          "description": "",
          "label": "Ei jäsenyyttä"
        },
        "other": {
          "description": "",
          "label": "Muu liitto..."
        },
        "raksaNormal": {
          "description": "",
          "label": "Rakennusliitto, A-jäsen"
        },
        "raksaUnemploymentOnly": {
          "description": "",
          "label": "Rakennusliitto, OA- tai TA-jäsen"
        }
      },
      "WageBasis": {
        "hourly": {
          "label": "Tuntipalkka"
        },
        "monthly": {
          "label": "Kuukausipalkka"
        },
        "other": {
          "label": "Palkanmaksu ilman lomalaskentaa"
        },
        "performanceBased": {
          "label": "Suorituspalkka"
        },
        "undefined": {
          "label": "Valitse..."
        }
      },
      "WebSiteUserRole": {
        "company": {
          "description": "",
          "label": "Yrittäjä"
        },
        "household": {
          "description": "",
          "label": "Työnantaja (kotitalous)"
        },
        "none": {
          "description": "",
          "label": "Valitse rooli..."
        },
        "worker": {
          "description": "",
          "label": "Työntekijä"
        }
      },
      "WorkingTimeCompensationKind": {
        "emergencyWorkCompensation": {
          "description": "Työaikakorvaus, joka maksetaan hätätyöstä. Hätätyöllä tarkoitetaan hätätilanteessa säännöllisen työajan ylittävänä aikana teetettävää työtä.",
          "label": "Hätätyökorvaus"
        },
        "eveningShiftAllowance": {
          "description": "Vuorotyökorvaus, joka maksetaan iltavuorosta.",
          "label": "Iltavuorolisä"
        },
        "eveningWorkCompensation": {
          "description": "Työaikakorvaus, joka maksetaan iltatyöstä. Iltatyökorvausta voidaan maksaa säännölliseltä työajalta tai säännöllisen työajan ylittävältä ajalta.",
          "label": "Iltatyökorvaus"
        },
        "extraWorkPremium": {
          "description": "Työaikakorvaus, joka maksetaan lisätyöstä.",
          "label": "Lisätyökorvaus"
        },
        "nightShiftCompensation": {
          "description": "Vuorotyökorvaus, joka maksetaan yövuorosta.",
          "label": "Yövuorolisä"
        },
        "nightWorkAllowance": {
          "description": "Työaikakorvaus, jota maksetaan yötyöstä.",
          "label": "Yötyökorvaus"
        },
        "otherCompensation": {
          "description": "Muiden maksettujen työaika- ja poikkeustilakorvausten yhteissumma. Näitä ovat mm. aattotyökorvaukset, arkipyhäkorvaukset ja muut mahdolliset työehtosopimusten tai työsopimusten perusteella\n maksettavat korvaukset ja lisät.",
          "label": "Muu maksettu lisä"
        },
        "overtimeCompensation": {
          "description": "Ylityöstä maksettava työaikakorvaus. Ylityönä pidetään työtä, joka tehdään työnantajan aloitteesta sovitun ylityöaikarajan ylittävänä aikana.",
          "label": "Ylityökorvaus"
        },
        "saturdayPay": {
          "description": "Työaikakorvaus, joka maksetaan lauantaityöstä.\n Lauantaityökorvausta voidaan maksaa säännölliseltä työajalta tai säännöllisen työajan ylittävältä ajalta.",
          "label": "Lauantaityökorvaus"
        },
        "standByCompensation": {
          "description": "Palkka, joka maksetaan siitä, että työntekijä on tavoitettavissa ja työnantaja voi tarvittaessa kutsua hänet hoitamaan palvelussuhteeseen kuuluvia tehtäviä.",
          "label": "Varallaolokorvaus"
        },
        "sundayWorkCompensation": {
          "description": "Sunnuntaityöstä maksettava työaikakorvaus. Sunnuntaityökorvausta voidaan maksaa säännölliseltä työajalta tai säännöllisen työajan ylittävältä ajalta.",
          "label": "Sunnuntaityökorvaus"
        },
        "undefined": {
          "description": "Ei valintaa",
          "label": "Ei valintaa"
        },
        "waitingTimeCompensation": {
          "description": "Korvaus, johon työntekijällä on oikeus, jos palkanmaksu viivästyy työsuhteen päätyttyä. Ellei toisin sovita, palkka tulee maksaa palkanmaksukauden\n viimeisenä päivänä. Odotusajan korvausta voidaan\n maksaa enintään kuudelta päivältä.",
          "label": "Odotusajan korvaus"
        },
        "weeklyRestCompensation": {
          "description": "Työaikakorvaus, jota maksetaan viikkolevon aikana tehdystä työstä.",
          "label": "Viikkolepokorvaus"
        }
      },
      "YtjCompanyType": {
        "alandFederation": {
          "description": "",
          "label": "Ahvenanmaan liikelaitos"
        },
        "asoAssociation": {
          "description": "",
          "label": "Asumisoikeusyhdistys"
        },
        "association": {
          "description": "",
          "label": "Aatteellinen yhdistys"
        },
        "condominium": {
          "description": "",
          "label": "Asunto-osakeyhtiö"
        },
        "cooperativeBank": {
          "description": "",
          "label": "Osuuspankki"
        },
        "europeanCooperative": {
          "description": "",
          "label": "Eurooppaosuuskunta"
        },
        "europeanCooperativeBank": {
          "description": "",
          "label": "Eurooppaosuuspankki"
        },
        "federationOfMunicipalitiesEstablishment": {
          "description": "",
          "label": "Kuntainliiton liikelaitos"
        },
        "financialAssociation": {
          "description": "",
          "label": "Taloudellinen yhdistys"
        },
        "forestCareAssociation": {
          "description": "",
          "label": "Metsänhoitoyhdistys"
        },
        "foundation": {
          "description": "",
          "label": "Säätiö"
        },
        "housingCooperative": {
          "description": "",
          "label": "Asunto-osuuskunta"
        },
        "hypoAssociation": {
          "description": "",
          "label": "Hypoteekkiyhdistys"
        },
        "insuranceAssociation": {
          "description": "",
          "label": "Vakuutusyhdistys"
        },
        "ky": {
          "description": "",
          "label": "Kommandiittiyhtiö"
        },
        "municipalEstablishment": {
          "description": "",
          "label": "Kunnallinen liikelaitos"
        },
        "mutualInsuranceAssociation": {
          "description": "",
          "label": "Keskinäinen vahinkovak.yhdistys"
        },
        "noCompanyType": {
          "description": "",
          "label": "Ei yritysmuotoa"
        },
        "openCompany": {
          "description": "",
          "label": "Avoin yhtiö"
        },
        "osuuskunta": {
          "description": "",
          "label": "Osuuskunta"
        },
        "otherAssociation": {
          "description": "",
          "label": "Muu yhdistys"
        },
        "otherFinancialAssociation": {
          "description": "",
          "label": "Muu taloudellinen yhdistys"
        },
        "oy": {
          "description": "",
          "label": "Osakeyhtiö"
        },
        "privateEntrepreneur": {
          "description": "",
          "label": "Yksityinen elinkeinonharjoittaja l. toiminimi"
        },
        "reindeerHerdingCooperative": {
          "description": "",
          "label": "Paliskunta"
        },
        "savingsBank": {
          "description": "",
          "label": "Säästöpankki"
        },
        "specialPurposeAssociation": {
          "description": "",
          "label": "Erityislainsäädäntöön perustuva yhdistys"
        },
        "stateEstablishment": {
          "description": "",
          "label": "Valtion liikelaitos"
        },
        "unknown": {
          "description": "",
          "label": "Tuntematon yritysmuoto"
        }
      }
    },
    "UI_TERMS": {
      "add": "Lisää",
      "addNew": "Lisää uusi",
      "addRow": "Lisää rivi",
      "areYouSure": "Oletko varma?",
      "back": "Takaisin",
      "calculate": "Laske",
      "calculations": "Laskelmat",
      "cancel": "Peruuta",
      "cancelEditing": "Peru muokkaaminen",
      "choose": "Valitse...",
      "chooseWorkDateRange": "Määritä työn alku- ja loppupäivät",
      "chooseWorkDuration": "Määritä työn kesto...",
      "clear": "Tyhjennä",
      "close": "Sulje",
      "confirm": "Vahvista",
      "copy": "Kopio",
      "copyAsNew": "Kopioi uudeksi",
      "copyAsRequest": "Kopio pyynnöksi",
      "copyToClipboard": "Kopioi",
      "dateFormats": {
        "dm": "pv.kk."
      },
      "dateNotChosen": "päivämäärä ei valittuna",
      "delete": "Poista",
      "demo": "KOKEILUVERSIO",
      "dismiss": "Hylkää",
      "done": "Valmis",
      "edit": "Muokkaa",
      "gotIt": "Ymmärrän",
      "isDeleting": "Poistetaan...",
      "isSaving": "Tallennetaan...",
      "learnMore": "Lue lisää",
      "loading": "Ladataan",
      "login": "Kirjaudu",
      "logout": "Kirjaudu ulos",
      "logoutAlt": "Päätä nykyinen sessio",
      "makeOrder": "Tee tilaus",
      "minCharacters": "Kirjoita vähintään 3 merkkiä...",
      "monthHalf": "½ kuukautta",
      "monthShort0": "joulu",
      "monthShort1": "tammi",
      "monthShort10": "loka",
      "monthShort11": "marras",
      "monthShort12": "joulu",
      "monthShort2": "helmi",
      "monthShort3": "maalis",
      "monthShort4": "huhti",
      "monthShort5": "touko",
      "monthShort6": "kesä",
      "monthShort7": "heinä",
      "monthShort8": "elo",
      "monthShort9": "syys",
      "moreInfo": "Lisätietoja",
      "navigation": "Navigaatio",
      "newCalculation": "Uusi laskelma",
      "next": "Seuraava",
      "no": "Ei",
      "noteRequiredField": "* Pakollinen kenttä",
      "noThanks": "Ei kiitos",
      "ok": "OK",
      "oldVersion": "vanha versio",
      "orChoose": "...tai valitse...",
      "otherPeriod": "Muu periodi",
      "pay": "Maksa",
      "pleaseWait": "Ole hyvä ja odota...",
      "postpone": "Ei nyt",
      "previous": "Edellinen",
      "printable": "Tuloste",
      "recalculate": "Päivitä laskelma",
      "register": "Rekisteröidy",
      "save": "Tallenna",
      "saveEdited": "Tallenna muutokset",
      "search": "Hae...",
      "searchByName": "Hae nimeltä...",
      "select": "Valitse",
      "selectAlt": "Valitse...",
      "sendEmail": "Lähetä sähköposti",
      "show": "Näytä",
      "skip": "Ohita",
      "sureToDeleteRecord": "Haluatko varmasti poistaa tämän tietueen?",
      "today": "Tämä päivä",
      "updateCalculation": "Päivitä laskelma",
      "updatingCalculation": "Päivitetään laskelmaa...",
      "use": "Ota käyttöön",
      "week2": "2 viikkoa",
      "yes": "Kyllä"
    },
    "VALIDATION": {
      "Files": {
        "ContentTypeError": "Tiedostomuotoa ei hyväksytä",
        "Empty": "Ladattava tiedosto on tyhjä",
        "MaxFileSize": "Ladattava tiedosto on liian suuri",
        "NoFiles": "Kutsun mukana ei tullut yhtään tiedostoa.",
        "SaveError": "Tuntematon virhe tiedoston tallennuksessa."
      },
      "IllegalWorkerForCompanyType": {
        "description": "Yksityinen elinkeinonharjoittaja eli toiminimi ei voi maksaa itselleen palkkaa. Palkanmaksu hoidetaan yksityisnostoina. Kirjanpitäjäsi voi neuvoa sinua miten toimia.",
        "label": "Ei-sallittu palkka"
      },
      "IncompleteAdvancePayment": {
        "description": "Palkkaennakko laskelmalla on liian suuri. Korjaa palkkaennakko pienemmäksi tai bruttopalkka suuremmaksi. Mahdolliset ennakkoon maksetut kulukorvaukset tulee ilmoittaa omalla palkkalajilla.",
        "label": "Palkka ei riitä ilmoitettuun palkkaennakkoon"
      },
      "IncompleteForeclosure": {
        "description": "Rahapalkka ei riitä ulosmittauksen tekemiseen. Tarkista ulosmittauksen määrä maksukiellosta ja muista huomioida maksukiellossa ilmoitettu suojaosuus.",
        "label": "Palkka ei riitä ulosmittauksen tekemiseen"
      },
      "IncompleteOtherDeductions": {
        "description": "Palkka ei riitä ilmoitettuihin muihin vähennyksiin",
        "label": "Palkka ei riitä"
      },
      "IncompletePensionPayment": {
        "description": "Palkka ei riitä työntekijän osuuteen TyEL-maksusta",
        "label": "Palkka ei riitä"
      },
      "IncompletePrepaidExpenses": {
        "description": "Ennakkoon maksettu kulukorvaus laskelmalla on liian suuri. Ennakkoon maksettu kulukorvaus ylittää laskelmalle lisätyt kulukorvaukset.",
        "label": "Ennakkoon maksettu kulukorvaus laskelmalla on liian suuri"
      },
      "IncompleteUnemploymentInsurancePayment": {
        "description": "Palkka ei riitä työntekijän osuuteen työttömyysvakuutusmaksusta",
        "label": "Palkka ei riitä"
      },
      "IncompleteUnionPayment": {
        "description": "Palkka ei riitä AY-jäsenmaksuun",
        "label": "Palkka ei riitä"
      },
      "MissingOccupationCode": {
        "description": "Työn tyyppi puuttuu. Lisää se kohdassa 1 hakemalla Tilastokeskuksen luokituksesta. Ammattiluokitus on pakollinen tieto Tulorekisterin palkkatietoilmoituksessa.",
        "label": "Ammattiluokitus puuttuu"
      },
      "NegativeRowTotalNotSupported": {
        "description": "Rivin yhteenlaskettu summa on negatiivinen. Rivillä ei sallita negatiivista summaa.",
        "label": "Rivillä ei sallita negatiivista summaa"
      },
      "Payroll": {
        "Calc": {
          "EmailAddress": {
            "label": "Sähköposti on virheellinen"
          },
          "FirstNameNotEmpty": {
            "label": "Työntekijän etunimi puuttuu"
          },
          "IbanInvalid": {
            "label": "Tilinumero on virheellinen (IBAN-muoto)"
          },
          "IbanNotEmpty": {
            "label": "Tilinumero puuttuu"
          },
          "InvalidStatusForPayroll": {
            "label": "Palkkalistan laskelmalla väärä status."
          },
          "LastNameNotEmpty": {
            "label": "Työntekijän sukunimi puuttuu"
          },
          "PayrollIdConflict": {
            "label": "Laskelman palkkalista-tunnus on virheellinen."
          },
          "SocialSecurityNumber_InTest": {
            "label": "Henkilötunnus on virheellinen. Testissä voit käyttää tarkistusnumerona tähteä ('*')"
          },
          "SocialSecurityNumber": {
            "label": "Henkilötunnus on virheellinen."
          },
          "TaxCardType": {
            "label": "Verokortti puuttuu tai tyyppiä ei ole määritetty."
          },
          "TaxPercent": {
            "label": "Veroprosenttia ei ole määritetty"
          },
          "TelephoneOrEmailNotEmpty": {
            "label": "Anna jompikumpi tai molemmat yhteystiedot (puhelinnumero ja sähköposti)."
          },
          "TotalGreaterThan0": {
            "label": "Palkkalaskelman summa on nolla"
          }
        },
        "NoCalculations": {
          "label": "Ei yhtään laskelmaa."
        }
      },
      "SharedTaxcard": {
        "description": "Työntekijäsi on jakanut uuden verokortin ja toivoo, että sitä käytetään palkanmaksuun. Tarkista ja hyväksy verokortti palkkalaskurin ruudun 1. Työntekijä kautta ennen palkan maksamista, jotta ennakonpidätys päivittyy laskelmalle oikein.",
        "label": "Uusi jaettu verokortti!"
      },
      "ValidationErrors": {
        "maxlength": "Pituus enintään {{maxlength}} merkkiä",
        "minlength": "Pituus vähintään {{minlength}} merkkiä",
        "required": "Kenttä on pakollinen",
        "sxyCompanyIdFi": "Y-tunnuksen muoto on väärä",
        "sxyCurrency": "Rahamäärän muoto on väärä",
        "sxyEmail": "Sähköpostin muoto on väärä",
        "sxyIban": "TIlinumeron (IBAN) muoto on väärä",
        "sxyInteger": "Kentän pitää olla kokonaisluku",
        "sxyMobilePhone": "Puhelinnumeron muoto on väärä",
        "sxyNumber": "Kentän pitää olla numero",
        "sxyPensionContractNumber": "Tarkista sopimusnumero",
        "sxyPersonalIdFi": "Henkilötunnuksen muoto on väärä",
        "sxyPostalCodeFi": "Postinumeron muoto on väärä",
        "sxySmsVerificationCode": "Tarkista varmennuskoodi",
        "sxyTaxPercent": "Veroprosentin muoto on väärä",
        "sxyTemporaryPensionContractNumber": "Syötit tilapäisen sopimuksen numeron."
      }
    }
  }
};
_dictionary["sv"] = {
  "SALAXY": {
    "ENUM": {
      "AbcSection": {
        "association": {
          "description": "",
          "label": "Föreningar"
        },
        "blog": {
          "description": "",
          "label": "Blogg"
        },
        "businessOwner": {
          "description": "",
          "label": "Företagare"
        },
        "documentTemplates": {
          "description": "",
          "label": "Dokument och formulär"
        },
        "doNotShow": {
          "description": "",
          "label": "Ej visad till användare (endast Admin)"
        },
        "employee": {
          "description": "",
          "label": "Anställd"
        },
        "employment": {
          "description": "",
          "label": "Anställningsförhållande och arbetsavtal"
        },
        "entrepreneur": {
          "description": "",
          "label": "Företag eller samfund"
        },
        "examples": {
          "description": "",
          "label": "Exempel av anställningssituationer"
        },
        "householdEmployer": {
          "description": "",
          "label": "Hushåll"
        },
        "instructionsAndExamples": {
          "description": "",
          "label": "Bruksanvisning"
        },
        "insurance": {
          "description": "",
          "label": "Försäkringar och pension"
        },
        "palkkausGeneral": {
          "description": "",
          "label": "Information om Palkkaus.fi-tjänsten"
        },
        "palkkausInstructions": {
          "description": "",
          "label": "Bruksavisningar"
        },
        "personEmployer": {
          "description": "",
          "label": "Hushållsarbetsgivare"
        },
        "press": {
          "description": "",
          "label": "Meddelanden och material"
        },
        "productLongDescription": {
          "description": "",
          "label": "Produktbeskrivning"
        },
        "salary": {
          "description": "",
          "label": "Löneräkning och lönebetalning"
        },
        "undefined": {
          "description": "",
          "label": "Nya artiklar / odefinierat"
        },
        "worker": {
          "description": "",
          "label": "Arbetsgivare"
        }
      },
      "AbsenceCauseCode": {
        "accruedHoliday": {
          "label": "Pekkasledighet"
        },
        "annualLeave": {
          "label": "Semester"
        },
        "childCareLeave": {
          "label": "Vårdledighet"
        },
        "childIllness": {
          "label": "Sjukt barn eller tvingande familjeskäl"
        },
        "illness": {
          "label": "Sjukdom"
        },
        "industrialAction": {
          "label": "Strejk eller lockout"
        },
        "interruptionInWorkProvision": {
          "label": "Avbrott i utbud av arbete"
        },
        "jobAlternationLeave": {
          "label": "Alterneringsledighet"
        },
        "layOff": {
          "label": "Permittering"
        },
        "leaveOfAbsence": {
          "label": "Tjänstledighet"
        },
        "midWeekHoliday": {
          "label": "Söckenhelg"
        },
        "militaryRefresherTraining": {
          "label": "Repetitionsövning"
        },
        "militaryService": {
          "label": "Militär- eller civiltjänst"
        },
        "occupationalAccident": {
          "label": "Arbetsolycka"
        },
        "other": {
          "label": "Annan orsak"
        },
        "parentalLeave": {
          "label": "Moderskaps-, faderskaps- och föräldraledighet"
        },
        "partTimeAbsenceDueToRehabilitation": {
          "label": "Partiell frånvaro på grund av rehabilitering"
        },
        "partTimeChildCareLeave": {
          "label": "Partiell vårdledighet"
        },
        "partTimeSickLeave": {
          "label": "Partiell sjukfrånvaro"
        },
        "personalReason": {
          "label": ""
        },
        "rehabilitation": {
          "label": "Rehabilitering"
        },
        "specialMaternityLeave": {
          "label": "Särskild moderskapsledighet"
        },
        "studyLeave": {
          "label": "Studieledighet"
        },
        "training": {
          "label": "Utbildning"
        },
        "unpaidLeave": {
          "label": ""
        }
      },
      "AgeGroupCode": {
        "a": {
          "label": ""
        },
        "b": {
          "label": ""
        },
        "c": {
          "label": ""
        },
        "u": {
          "label": ""
        }
      },
      "AgeRange": {
        "age_15": {
          "description": "",
          "label": "15 år"
        },
        "age16": {
          "description": "",
          "label": "16 år"
        },
        "age17": {
          "description": "",
          "label": "17 år"
        },
        "age18_52": {
          "description": "",
          "label": "18-52 år"
        },
        "age53_62": {
          "description": "",
          "label": "53-62 år"
        },
        "age63_64": {
          "description": "",
          "label": "63-64 år"
        },
        "age65_67": {
          "description": "",
          "label": "65-67 år"
        },
        "age68AndOVer": {
          "description": "",
          "label": "68 år och över"
        },
        "unknown": {
          "description": "",
          "label": "Okänd"
        }
      },
      "AllowanceCode": {
        "expensesWorkingAbroad": {
          "label": ""
        },
        "fullDailyAllowance": {
          "label": ""
        },
        "internationalDailyAllowance": {
          "label": ""
        },
        "mealAllowance": {
          "label": ""
        },
        "partialDailyAllowance": {
          "label": ""
        }
      },
      "AnnualLeavePaymentKind": {
        "draftCalc": {
          "description": "",
          "label": ""
        },
        "endSaldo": {
          "description": "",
          "label": ""
        },
        "info": {
          "description": "",
          "label": ""
        },
        "manualBonus": {
          "description": "",
          "label": ""
        },
        "manualCompensation": {
          "description": "",
          "label": ""
        },
        "manualSalary": {
          "description": "",
          "label": ""
        },
        "paidCalc": {
          "description": "",
          "label": ""
        },
        "startSaldo": {
          "description": "",
          "label": ""
        },
        "total": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "ApiTestErrorType": {
        "default": {
          "description": "",
          "label": "Default"
        },
        "userFriendly": {
          "description": "",
          "label": "Användarvänlig"
        }
      },
      "ApiValidationErrorType": {
        "general": {
          "label": "Allmän"
        },
        "invalid": {
          "label": "Ogiltig"
        },
        "required": {
          "label": "Krävs"
        },
        "warning": {
          "label": ""
        }
      },
      "AuthenticationMethod": {
        "auth0Database": {
          "description": "",
          "label": "E-post-konto"
        },
        "emailPwdLocal": {
          "description": "",
          "label": "Databas"
        },
        "facebook": {
          "description": "",
          "label": "Facebook-konto"
        },
        "google": {
          "description": "",
          "label": "Google eller Gmail-konto"
        },
        "internalLocalhost": {
          "description": "",
          "label": ""
        },
        "linkedIn": {
          "description": "",
          "label": "LinkedIn-konto"
        },
        "microsoft": {
          "description": "",
          "label": "Microsoft-konto"
        },
        "salaxy": {
          "description": "",
          "label": ""
        },
        "test": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": "Okänd"
        },
        "x509": {
          "description": "",
          "label": ""
        }
      },
      "AuthorizationStatus": {
        "accessDenied": {
          "description": "",
          "label": "Tillträde förbjudet"
        },
        "expiredOauthTokdescription": "",
        "expiredOauthToklabel": "Session föråldrad",
        "invalidOAuthTokdescription": "",
        "invalidOAuthToklabel": "Fel i autentisering",
        "noOauthTokdescription": "",
        "noOauthToklabel": "Fel i autentisering (ingen token)",
        "oK": {
          "description": "",
          "label": "OK"
        },
        "undefined": {
          "description": "",
          "label": "Odefinierad"
        }
      },
      "AuthorizationType": {
        "companyContract": {
          "description": "",
          "label": "Arbetsgivarkontrakt"
        },
        "employerAuthorization": {
          "description": "",
          "label": "Fullmakt"
        },
        "manual": {
          "description": "",
          "label": "Fullmakt inlämnat skilt"
        },
        "none": {
          "description": "",
          "label": "Ej identifierad"
        },
        "temporary": {
          "description": "",
          "label": "Tillfällig identifiering"
        },
        "workerContract": {
          "description": "",
          "label": "Arbetstagarkontrakt"
        }
      },
      "AvatarPictureType": {
        "gravatar": {
          "description": "",
          "label": "Gravatar"
        },
        "icon": {
          "description": "",
          "label": "Ikon"
        },
        "uploaded": {
          "description": "",
          "label": "Bild från egen dator"
        }
      },
      "BlobFileType": {
        "authorizationPdf": {
          "description": "",
          "label": "Fullmakt"
        },
        "avatar": {
          "description": "",
          "label": "Ikon"
        },
        "calcReport": {
          "description": "",
          "label": ""
        },
        "expensesReceipt": {
          "description": "",
          "label": "Kvitto för utlägg"
        },
        "monthlyReport": {
          "description": "",
          "label": "Månatlig rapport"
        },
        "raksaloki": {
          "description": "",
          "label": "Kvitto för utlägg"
        },
        "taxCard": {
          "description": "",
          "label": "Skattekort"
        },
        "template": {
          "description": "",
          "label": "Mall"
        },
        "unknown": {
          "description": "",
          "label": "Okänd"
        },
        "yearlyReport": {
          "description": "",
          "label": "Årlig rapport"
        }
      },
      "BlobRepository": {
        "cdnImages": {
          "description": "",
          "label": ""
        },
        "fileSystemContent": {
          "description": "",
          "label": ""
        },
        "gitContent": {
          "description": "",
          "label": ""
        },
        "payload": {
          "description": "",
          "label": ""
        },
        "reports": {
          "description": "",
          "label": ""
        },
        "systemFiles": {
          "description": "",
          "label": ""
        },
        "userFiles": {
          "description": "",
          "label": ""
        },
        "versionHistory": {
          "description": "",
          "label": ""
        }
      },
      "BoardKind": {
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "CalculationFlag": {
        "accidentInsurance": {
          "label": ""
        },
        "cfDeduction": {
          "label": ""
        },
        "cfDeductionAtSalaxy": {
          "label": ""
        },
        "cfNoPayment": {
          "label": ""
        },
        "cfPayment": {
          "label": ""
        },
        "exclude": {
          "label": ""
        },
        "healthInsurance": {
          "label": ""
        },
        "noTax": {
          "label": ""
        },
        "pensionInsurance": {
          "label": ""
        },
        "tax": {
          "label": ""
        },
        "taxDeduction": {
          "label": ""
        },
        "unemploymentInsurance": {
          "label": ""
        }
      },
      "CalculationRowSource": {
        "manualRow": {
          "description": "",
          "label": "Tilläggsrad"
        },
        "salaryPage": {
          "description": "",
          "label": "Huvudlön"
        },
        "tesSelection": {
          "description": "",
          "label": "Användarfall"
        }
      },
      "CalculationRowType": {
        "accomodationBenefit": {
          "amountLabel": "",
          "description": "Beskattningsbar boendeförmån.",
          "iconText": "",
          "label": "Boendetillägg",
          "moreInfo": "",
          "priceLabel": ""
        },
        "advance": {
          "amountLabel": "",
          "description": "Avdrag pga. löneförskott som betalats ut tidigare.",
          "iconText": "",
          "label": "Löneförskott",
          "moreInfo": "",
          "priceLabel": "Förskottsbetalning"
        },
        "board": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "carBenefit": {
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "chainsawReduction": {
          "amountLabel": "",
          "description": "Avdrag för bruk av motorsåg, i specialfall (förskottsbetalningslag § 15, förskottsbetalnings 15§).",
          "iconText": "",
          "label": "Välj…",
          "moreInfo": "",
          "priceLabel": ""
        },
        "childCareSubsidy": {
          "amountLabel": "",
          "description": "FPA stöd för kommunala arbetare för barnvård. Påverkar sidokostnader.",
          "iconText": "FPA",
          "label": "Barnvårdsunderstöd",
          "moreInfo": "",
          "priceLabel": "FPA betalar"
        },
        "compensation": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "Arbetskompensation",
          "moreInfo": "",
          "priceLabel": "Arbetsersättning"
        },
        "dailyAllowance": {
          "amountLabel": "Dagar",
          "description": "Dagsersättning, 40 euro, om arbetsresan är över 10 timmar eller 15 km.",
          "iconText": 40,
          "label": "Dagsersättning",
          "moreInfo": "",
          "priceLabel": "Euro / dag"
        },
        "dailyAllowanceHalf": {
          "amountLabel": "Dagar",
          "description": "Halvdagsersättning, 19 euro, om arbetsresan är över 6 timmar eller 15 km.",
          "iconText": 19,
          "label": "Halvdagsersättning",
          "moreInfo": "",
          "priceLabel": "Euro / dag"
        },
        "employmentTermination": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "eveningAddition": {
          "amountLabel": "Timmar",
          "description": "Ersättning för kvällsarbete enligt kollektivavtal.",
          "iconText": "KVÄLL",
          "label": "Kvällstillägg",
          "moreInfo": "",
          "priceLabel": "Tillägg (per timme)"
        },
        "expenses": {
          "amountLabel": "",
          "description": "Ersättning för utlägg som arbetstagaren gjort för arbetsgivarens del (resekostnader, kontorsmaterial etc.).",
          "iconText": "kostnad",
          "label": "Ersättning för utlägg",
          "moreInfo": "",
          "priceLabel": "Erssättning för kostnader"
        },
        "foreclosure": {
          "amountLabel": "",
          "description": "Löneerhållning, betalt rakt till befallningsmannen.",
          "iconText": "",
          "label": "Löneerhållning",
          "moreInfo": "",
          "priceLabel": "Summa"
        },
        "foreclosureByPalkkaus": {
          "amountLabel": "",
          "description": "Löneerhållning gjort av Palkkaus.fi, betalt rakt till befallningsmannen.",
          "iconText": "",
          "label": "Löneerhållning av Palkkaus.fi",
          "moreInfo": "",
          "priceLabel": ""
        },
        "holidayBonus": {
          "amountLabel": "Procent",
          "description": "Semesterersättning enligt rådande kollektivavtal.",
          "iconText": "Kollektivavtal",
          "label": "Semesterersättning",
          "moreInfo": "",
          "priceLabel": "Lön för semestertid"
        },
        "holidayCompensation": {
          "amountLabel": "Procent",
          "description": "Ersättning som betalas ut ifall man inte håller sin årliga semester.",
          "iconText": "11,5%",
          "label": "Semestertillägg",
          "moreInfo": "",
          "priceLabel": "Lönesumma"
        },
        "holidaySalary": {
          "amountLabel": "",
          "description": "Lön för årlig semester.",
          "iconText": "semester",
          "label": "Semesterlön",
          "moreInfo": "",
          "priceLabel": "Lön för semestertid"
        },
        "hourlySalary": {
          "amountLabel": "",
          "description": "Lönebetalning baserat på insatta timmar.",
          "iconText": "timme",
          "label": "Timlön",
          "moreInfo": "",
          "priceLabel": "Timlön"
        },
        "irIncomeType": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "mealBenefit": {
          "amountLabel": "",
          "description": "Beskattningsbar måltidsförmån.",
          "iconText": "mat",
          "label": "Måltidsförmån",
          "moreInfo": "",
          "priceLabel": ""
        },
        "mealCompensation": {
          "amountLabel": "Dagar",
          "description": "Måltidsersättning när dagsersättning inte kan betalas.",
          "iconText": "måltid",
          "label": "Måltidsersättning",
          "moreInfo": "",
          "priceLabel": "Ersättning / dag"
        },
        "milageDaily": {
          "amountLabel": "Dagar",
          "description": "Kilometerersättning enligt rådande kollektivavtal.",
          "iconText": "km",
          "label": "Daglig kilometerersättning",
          "moreInfo": "",
          "priceLabel": "Ersättning / dag"
        },
        "milageOther": {
          "amountLabel": "Dagar",
          "description": "Övrig/annan kilometerersättning.",
          "iconText": "km",
          "label": "Annan kilometerersättning",
          "moreInfo": "",
          "priceLabel": "Ersättning / dag"
        },
        "milageOwnCar": {
          "amountLabel": "",
          "description": "Kilometerersättning för bruk av egen bil.",
          "iconText": "0,43€",
          "label": "Kilometerersättning",
          "moreInfo": "",
          "priceLabel": "Ersättning / km"
        },
        "monthlySalary": {
          "amountLabel": "",
          "description": "Fast månadslön.",
          "iconText": "månad",
          "label": "Månadslön",
          "moreInfo": "",
          "priceLabel": "Månatlig lön"
        },
        "nightimeAddition": {
          "amountLabel": "Timmar",
          "description": "Ersättning för nattararbete enligt kollektivavtal.",
          "iconText": "NAT",
          "label": "Nattarbetstillägg",
          "moreInfo": "",
          "priceLabel": "Tillägg (per timme)"
        },
        "nonProfitOrg": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "otherAdditions": {
          "amountLabel": "",
          "description": "Övriga tillägg enligt kollektivavtal.",
          "iconText": "tillägg",
          "label": "Övriga tillägg",
          "moreInfo": "",
          "priceLabel": ""
        },
        "otherBenefit": {
          "amountLabel": "",
          "description": "Övriga beskattningsbara förmåner.",
          "iconText": "Förmån",
          "label": "Övriga förmåner",
          "moreInfo": "",
          "priceLabel": ""
        },
        "otherCompensation": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "otherDeductions": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "overtime": {
          "amountLabel": "Procent",
          "description": "Övertidsersättningar.",
          "iconText": "över",
          "label": "Övertidskompensation",
          "moreInfo": "",
          "priceLabel": "Lönesumma"
        },
        "phoneBenefit": {
          "amountLabel": "Månader",
          "description": "Beskattningsbar mobiltelefonförmån.",
          "iconText": 20,
          "label": "Mobiltelefonförmån",
          "moreInfo": "",
          "priceLabel": "Telefonförmån / mån"
        },
        "prepaidExpenses": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "remuneration": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "salary": {
          "amountLabel": "",
          "description": "Klumpsumma för insatt arbete.",
          "iconText": "D",
          "label": "Lön eller provision",
          "moreInfo": "",
          "priceLabel": ""
        },
        "saturdayAddition": {
          "amountLabel": "Procent",
          "description": "Tillägg för lördagsarbete enligt kollektivavtal.",
          "iconText": "LÖR",
          "label": "Lördagstillägg",
          "moreInfo": "",
          "priceLabel": "Lönesumma"
        },
        "subsidisedCommute": {
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        },
        "sundayWork": {
          "amountLabel": "",
          "description": "Tillägg för söndagsarbete enligt kollektivavtal.",
          "iconText": "SÖN",
          "label": "Söndags- och helgdagstillägg",
          "moreInfo": "",
          "priceLabel": "Lönesumma"
        },
        "tesWorktimeShortening": {
          "amountLabel": "Procent",
          "description": "Skild lönedel som är definierad enligt kollektivavtal. Även känt som \"pekkasdagar\" på finska.",
          "iconText": "Kollektivavtal",
          "label": "Ersättning för arbetstidsutjämning",
          "moreInfo": "",
          "priceLabel": "Lönesumma"
        },
        "toolCompensation": {
          "amountLabel": "Dagar",
          "description": "Ersättning för använding av egna arbetsrverktyg. Ersättningen enligt rådande kollektivavtal.",
          "iconText": "",
          "label": "Arbetsredskapsersättning",
          "moreInfo": "",
          "priceLabel": "Ersättning / dag"
        },
        "totalEmployerPayment": {
          "amountLabel": "",
          "description": "Uträkning av totalkostnader: vad lönebetalningen kostar för arbetsgivaren.",
          "iconText": "sum",
          "label": "Total kostnad",
          "moreInfo": "",
          "priceLabel": ""
        },
        "totals": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "Totalt",
          "moreInfo": "",
          "priceLabel": ""
        },
        "totalWorkerPayment": {
          "amountLabel": "",
          "description": "Uträkning av nettolönen: vad som blir kvar i handen efter skatter och andra avdrag.",
          "iconText": "net",
          "label": "Nettolön",
          "moreInfo": "",
          "priceLabel": ""
        },
        "unionPayment": {
          "amountLabel": "",
          "description": "Fackföreningsavgift betalt rakt till fackföreningen, om arbetstagaren så önskar.",
          "iconText": "Fackförening",
          "label": "Fackföreningsmedlemskap",
          "moreInfo": "",
          "priceLabel": ""
        },
        "unknown": {
          "amountLabel": "",
          "description": "Radtyp odefinierad.",
          "iconText": "",
          "label": "Välj…",
          "moreInfo": "",
          "priceLabel": ""
        },
        "workingTimeCompensation": {
          "amountLabel": "",
          "description": "",
          "iconText": "",
          "label": "",
          "moreInfo": "",
          "priceLabel": ""
        }
      },
      "CalculationRowUnit": {
        "count": {
          "description": "",
          "label": "st"
        },
        "days": {
          "description": "",
          "label": "dagar"
        },
        "kilometers": {
          "description": "",
          "label": "km"
        },
        "percent": {
          "description": "",
          "label": "%"
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "CalculationStatus": {
        "draft": {
          "description": "",
          "label": "Utkast"
        },
        "externalPaymentCanceled": {
          "description": "",
          "label": "Utbetalning avbruten utanför Salaxy-tjänsten"
        },
        "externalPaymentError": {
          "description": "",
          "label": "Fel (utanför Salaxy-tjänsten)"
        },
        "externalPaymentStarted": {
          "description": "",
          "label": "Utbetalning påbörjad utnaför Salaxy-tjänsten"
        },
        "externalPaymentSucceeded": {
          "description": "",
          "label": "Utbetalning genomförd utanför Salaxy-tjänsten"
        },
        "paymentCanceled": {
          "description": "",
          "label": "Utbetalning avbruten"
        },
        "paymentError": {
          "description": "",
          "label": "Fel i utbetalning"
        },
        "paymentRefunded": {
          "description": "",
          "label": "Utbetalning avbruten och returnerad"
        },
        "paymentStarted": {
          "description": "",
          "label": "Utbetalning påbörjad"
        },
        "paymentSucceeded": {
          "description": "",
          "label": "Utbetalning genomförd"
        },
        "paymentWorkerCopy": {
          "description": "",
          "label": "Arbetstagarens kvitto"
        },
        "payrollDraft": {
          "description": "",
          "label": ""
        },
        "proDraft": {
          "label": ""
        },
        "sharedApproved": {
          "description": "",
          "label": ""
        },
        "sharedRejected": {
          "description": "",
          "label": ""
        },
        "sharedWaiting": {
          "description": "",
          "label": ""
        },
        "template": {
          "description": "",
          "label": ""
        },
        "workerRequestAccepted": {
          "description": "",
          "label": "Lönespecifikation gjord av arbetstagaren godkänd"
        },
        "workerRequestDeclined": {
          "description": "",
          "label": "Lönespecifikation gjord av arbetstagaren avslagen"
        },
        "workerRequested": {
          "description": "",
          "label": "Lönespecifikation gjord av arbetstagaren"
        }
      },
      "CarBenefitCode": {
        "fullCarBenefit": {
          "label": ""
        },
        "limitedCarBenefit": {
          "label": ""
        }
      },
      "CarBenefitKind": {
        "fullCarBenefit": {
          "description": "",
          "label": ""
        },
        "limitedCarBenefit": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "CompanyType": {
        "fiOy": {
          "description": "",
          "label": "Aktiebolag, även bostadsaktiebolag och publika aktiebolag"
        },
        "fiRy": {
          "description": "",
          "label": "Föreningar"
        },
        "fiTm": {
          "description": "",
          "label": "Privat näringsidkare eller jordbrukare"
        },
        "fiYy": {
          "description": "",
          "label": "Annan, t.ex. Kommanditbolag, öppna bolag etc."
        },
        "unknown": {
          "description": "",
          "label": ""
        }
      },
      "ContractPartyType": {
        "customContact": {
          "description": "",
          "label": ""
        },
        "person": {
          "description": "",
          "label": ""
        }
      },
      "DailyAllowanceKind": {
        "fullDailyAllowance": {
          "description": "",
          "label": ""
        },
        "internationalDailyAllowance": {
          "description": "",
          "label": ""
        },
        "mealAllowance": {
          "description": "",
          "label": ""
        },
        "partialDailyAllowance": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "DateOfBirthAccuracy": {
        "ageBased": {
          "description": "",
          "label": "Åldersbaserad"
        },
        "ageGroupBased": {
          "description": "",
          "label": "Åldersgruppbaserad"
        },
        "assumption": {
          "description": "",
          "label": "Antagande"
        },
        "exact": {
          "description": "",
          "label": "Exakt datum"
        },
        "monthCorrect": {
          "description": "",
          "label": "Rätt månad"
        },
        "verified": {
          "description": "",
          "label": "Bekräftat"
        }
      },
      "EmployerType": {
        "fullTimeEmployer": {
          "description": "",
          "label": "Fortlöpande kontrakt eller över 8178 € / 6 månader"
        },
        "miniEmployer": {
          "description": "",
          "label": "Under 1300 € / år"
        },
        "smallEmployer": {
          "description": "",
          "label": "Minst 1300 € / år"
        },
        "tyelOwnContractEmployer": {
          "description": "",
          "label": "ArPL-årskontrakt eller omskött av kunden"
        },
        "unknown": {
          "description": "",
          "label": "Okänd"
        }
      },
      "EmploymentContract": {
        "annualLeave": {
          "label": "Annual leave"
        },
        "benefitsException": {
          "label": "Luontaisedut"
        },
        "noticeException": {
          "label": "Term of notice"
        },
        "otherTerms": {
          "label": "Other terms"
        }
      },
      "EmploymentRelationType": {
        "athlete": {
          "description": "",
          "label": ""
        },
        "boardMember": {
          "description": "",
          "label": ""
        },
        "compensation": {
          "description": "",
          "label": ""
        },
        "employedByStateEmploymentFund": {
          "description": "",
          "label": ""
        },
        "entrepreneur": {
          "description": "",
          "label": ""
        },
        "entrepreneurNew": {
          "description": "",
          "label": ""
        },
        "farmer": {
          "description": "",
          "label": ""
        },
        "hourlySalary": {
          "description": "",
          "label": ""
        },
        "keyEmployee": {
          "description": "",
          "label": ""
        },
        "monthlySalary": {
          "description": "",
          "label": ""
        },
        "performingArtist": {
          "description": "",
          "label": ""
        },
        "salary": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "EmploymentSalaryType": {
        "hourlyPay": {
          "description": "",
          "label": "Timlön",
          "metricAbbrev": ""
        },
        "montlyPay": {
          "description": "",
          "label": "Månadslön",
          "metricAbbrev": ""
        },
        "otherPay": {
          "description": "",
          "label": "Annan"
        },
        "unknown": {
          "description": "",
          "label": "Välj…"
        }
      },
      "EmploymentTerminationKind": {
        "monetaryWorkingTimeBankCompensation": {
          "description": "",
          "label": ""
        },
        "noticePeriodCompensation": {
          "description": "",
          "label": ""
        },
        "pensionPaidByEmployer": {
          "description": "",
          "label": ""
        },
        "terminationAndLayOffDamages": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        },
        "voluntaryTerminationCompensation": {
          "description": "",
          "label": ""
        }
      },
      "EmploymentType": {
        "fullTime": {
          "description": "",
          "label": "Tills vidare anställning"
        },
        "job": {
          "description": "",
          "label": "Engångsarbete"
        },
        "partTime": {
          "description": "",
          "label": "Visstidsanställd"
        },
        "unknown": {
          "description": "",
          "label": "Välj…"
        }
      },
      "EmploymentWorkHoursType": {
        "hourPerThreeWeeks": {
          "description": "",
          "label": "Timmar/tre veckor"
        },
        "hourPerTwoWeeks": {
          "description": "",
          "label": "Timmar/två veckor"
        },
        "hoursPerDay": {
          "description": "",
          "label": "Timmar/dygn"
        },
        "hoursPerWeek": {
          "description": "",
          "label": "Timmar/vecka"
        }
      },
      "FrameworkAgreement": {
        "childCare": {
          "description": "Tills vidare barnskötare och där tillhörande stöd.",
          "label": "Övrig barnomsorg"
        },
        "cleaning": {
          "description": "Städ- och fastighetsarbete.",
          "label": "Städ- och fastighetsbranschen"
        },
        "construction": {
          "description": "Arbete enligt kollektivavtalet för byggnadsbranschen.",
          "label": "Byggnads- och renoveringsarbete"
        },
        "entrepreneur": {
          "description": "Företagarens egen lön.",
          "label": "Företagare"
        },
        "mll": {
          "description": "Lön för MLL:s barnskötare.",
          "label": "MLL rekommendationer"
        },
        "notDefined": {
          "description": "Kollektivavtal eller rekommendation odefinierat.",
          "label": "Odefinierad"
        },
        "other": {
          "description": "Annat arbete eller inget kollektivavtal.",
          "label": "Övrigt arbete"
        },
        "santaClaus": {
          "description": "Hembesök av jultomten.",
          "label": "Jultomte"
        }
      },
      "Gender": {
        "female": {
          "description": "",
          "label": "Kvinna"
        },
        "male": {
          "description": "",
          "label": "Man"
        },
        "unknown": {
          "description": "",
          "label": ""
        }
      },
      "HelttiIndustry": {
        "i": {
          "description": "",
          "label": "Boende- och näringsverksamhet"
        },
        "j": {
          "description": "",
          "label": "Informationssystem och kommunikation"
        },
        "k": {
          "description": "",
          "label": "Finans- och försäkringsverksamhet"
        },
        "m": {
          "description": "",
          "label": "Professional, scientific and technical activities"
        },
        "n": {
          "description": "",
          "label": "Administrations- och stödverksamhet"
        },
        "notDefined": {
          "description": "",
          "label": "Välj…"
        },
        "o": {
          "description": "",
          "label": "Offentlig förvaltning och nationellt försvar"
        },
        "other": {
          "description": "",
          "label": "Övriga industrier"
        },
        "q": {
          "description": "",
          "label": "Hälso- och socialverksamhet"
        },
        "r": {
          "description": "",
          "label": "Konst, nöje och rekration"
        },
        "s": {
          "description": "",
          "label": "Övrig serviceverksamhet"
        }
      },
      "HelttiProductPackage": {
        "large": {
          "description": "",
          "label": "Hifi"
        },
        "medium": {
          "description": "",
          "label": "Tillväxt"
        },
        "notDefined": {
          "description": "",
          "label": "Inget kontrakt"
        },
        "small": {
          "description": "",
          "label": "Start"
        }
      },
      "HolidayAccrualKind": {
        "calculation": {
          "label": ""
        },
        "holiday": {
          "label": ""
        },
        "holidayCompensated": {
          "label": ""
        },
        "manual": {
          "label": ""
        },
        "saldo": {
          "label": ""
        }
      },
      "HolidayBonusPaymentMethod": {
        "none": {
          "label": ""
        },
        "pay24Days": {
          "label": ""
        },
        "payAllBonus": {
          "label": ""
        },
        "payForHolidaySalary": {
          "label": ""
        },
        "paySummerBonus": {
          "label": ""
        }
      },
      "HolidayCode": {
        "holidayCompensation": {
          "description": "",
          "label": ""
        },
        "holidayCompensationIncluded": {
          "description": "",
          "label": ""
        },
        "noHolidays": {
          "description": "",
          "label": ""
        },
        "permanent14Days": {
          "description": "",
          "label": ""
        },
        "permanent35Hours": {
          "description": "",
          "label": ""
        },
        "temporaryTimeOff": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "HolidayYearInitType": {
        "createEmpty": {
          "label": ""
        },
        "createFromCalcs": {
          "label": ""
        },
        "createMonths": {
          "label": ""
        },
        "ignore": {
          "label": ""
        }
      },
      "HouseHoldUseCaseTree": {
        "childCare": {
          "description": "",
          "kela": {
            "description": "",
            "label": ""
          },
          "label": "",
          "mll": {
            "description": "",
            "label": ""
          },
          "other": {
            "description": "",
            "label": ""
          }
        },
        "cleaning": {
          "51530": {
            "description": "",
            "label": ""
          },
          "53221": {
            "description": "",
            "label": ""
          },
          "61132": {
            "description": "",
            "label": ""
          },
          "91110": {
            "description": "",
            "label": ""
          },
          "description": "",
          "label": "",
          "other": {
            "description": "",
            "label": ""
          }
        },
        "construction": {
          "carpenter": {
            "description": "",
            "label": ""
          },
          "description": "",
          "floor": {
            "description": "",
            "label": ""
          },
          "label": "",
          "new": {
            "description": "",
            "label": ""
          },
          "painter": {
            "description": "",
            "label": ""
          },
          "renovation": {
            "description": "",
            "label": ""
          },
          "roof": {
            "description": "",
            "label": ""
          }
        },
        "other": {
          "51530": {
            "description": "",
            "label": "Fastighetsskötare"
          },
          "53112": {
            "description": "",
            "label": "Familjedagvårdare"
          },
          "53212": {
            "description": "",
            "label": "Vårdare av utvecklingshämmade"
          },
          "53213": {
            "description": "",
            "label": "Vårdare inom socialsektorn"
          },
          "53221": {
            "description": "",
            "label": "Hemvårdspersonal"
          },
          "53222": {
            "description": "",
            "label": "Personliga assistenter, närståendevårdare m.fl."
          },
          "61112": {
            "description": "",
            "label": "Arbetsledare och arbetare inom åkerodling"
          },
          "61132": {
            "description": "",
            "label": "Arbetsledare och anställda inom trädgårds- och växthusodling"
          },
          "62100": {
            "description": "",
            "label": "Skogsbrukare och skogsarbetare"
          },
          "71110": {
            "description": "",
            "label": "Byggnadsarbetare"
          },
          "71150": {
            "description": "",
            "label": "Timmermän och byggnadssnickare"
          },
          "71210": {
            "description": "",
            "label": "Takmontörer och takreparatörer"
          },
          "71310": {
            "description": "",
            "label": "Byggnadsmålare m.fl."
          },
          "91110": {
            "description": "",
            "label": "Hembiträden och städare"
          },
          "description": "",
          "label": "Annan",
          "undefined": {
            "description": "",
            "label": "Annan"
          }
        }
      },
      "InsuranceCompany": {
        "aktia": {
          "description": "",
          "label": "Aktia"
        },
        "alandia": {
          "description": "",
          "label": "Alandia"
        },
        "aVakuutus": {
          "description": "",
          "label": "A-Vakuutus"
        },
        "fennia": {
          "description": "",
          "label": "Fennia"
        },
        "folksam": {
          "description": "",
          "label": "Folksam"
        },
        "if": {
          "description": "",
          "label": "If"
        },
        "lähiTapiola": {
          "description": "",
          "label": "LähiTapiola"
        },
        "none": {
          "description": "",
          "label": "(Välj försäkringsbolag)"
        },
        "other": {
          "description": "",
          "label": "Annat bolag"
        },
        "pohjantähti": {
          "description": "",
          "label": "Pohjantähti"
        },
        "pohjola": {
          "description": "",
          "label": "OP Vakuutus"
        },
        "redarnas": {
          "description": "",
          "label": "Redarnas"
        },
        "tryg": {
          "description": "",
          "label": "Tryg"
        },
        "turva": {
          "description": "",
          "label": "Turva"
        },
        "ålands": {
          "description": "",
          "label": "Ålands"
        }
      },
      "InvoiceStatus": {
        "canceled": {
          "description": "",
          "label": ""
        },
        "error": {
          "description": "",
          "label": ""
        },
        "forecast": {
          "description": "",
          "label": ""
        },
        "paid": {
          "description": "",
          "label": ""
        },
        "paymentStarted": {
          "description": "",
          "label": ""
        },
        "preview": {
          "description": "",
          "label": ""
        },
        "read": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        },
        "unread": {
          "description": "",
          "label": ""
        },
        "waitingConfirmation": {
          "description": "",
          "label": ""
        },
        "waitingPalkkaus": {
          "description": "",
          "label": ""
        }
      },
      "IrFlags": {
        "noMoney": {
          "label": ""
        },
        "oneOff": {
          "label": ""
        },
        "unjustEnrichment": {
          "label": ""
        }
      },
      "IrIncomeTypeKind": {
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "IrInsuranceExceptions": {
        "excludeAccidentInsurance": {
          "label": ""
        },
        "excludeAll": {
          "label": ""
        },
        "excludeHealthInsurance": {
          "label": ""
        },
        "excludePension": {
          "label": ""
        },
        "excludeUnemployment": {
          "label": ""
        },
        "includeAccidentInsurance": {
          "label": ""
        },
        "includeAll": {
          "label": ""
        },
        "includeHealthInsurance": {
          "label": ""
        },
        "includePension": {
          "label": ""
        },
        "includeUnemployment": {
          "label": ""
        }
      },
      "LegalEntityType": {
        "company": {
          "description": "",
          "label": "FO-nummer"
        },
        "partner": {
          "description": "",
          "label": "Partner"
        },
        "person": {
          "description": "",
          "label": "Person"
        },
        "personCreatedByEmployer": {
          "description": "",
          "label": "Arbetstagare"
        },
        "undefined": {
          "description": "",
          "label": "Okänd"
        }
      },
      "MealBenefitKind": {
        "cateringContract": {
          "description": "",
          "label": ""
        },
        "institute": {
          "description": "",
          "label": ""
        },
        "mealAllowance": {
          "description": "",
          "label": ""
        },
        "mealTicket": {
          "description": "",
          "label": ""
        },
        "restaurantWorker": {
          "description": "",
          "label": ""
        },
        "taxableAmount": {
          "description": "",
          "label": ""
        },
        "teacher": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "MessageType": {
        "email": {
          "description": "",
          "label": "E-post"
        },
        "sms": {
          "description": "",
          "label": "Textmeddelande"
        }
      },
      "NonProfitOrgKind": {
        "accomodationAllowance": {
          "description": "",
          "label": ""
        },
        "dailyAllowance": {
          "description": "",
          "label": ""
        },
        "kilometreAllowance": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "OnboardingStatus": {
        "created": {
          "description": "",
          "label": "Skapad"
        },
        "done": {
          "description": "",
          "label": "Slutförd"
        },
        "opdescription": "",
        "oplabel": "Pågående"
      },
      "OtherCompensationKind": {
        "accruedTimeOffCompensation": {
          "description": "",
          "label": ""
        },
        "capitalIncomePayment": {
          "description": "",
          "label": ""
        },
        "employeeInventionCompensation": {
          "description": "",
          "label": ""
        },
        "employeeStockOption": {
          "description": "",
          "label": ""
        },
        "employeeStockOptionWithLowerPrice": {
          "description": "",
          "label": ""
        },
        "lectureFee": {
          "description": "",
          "label": ""
        },
        "meetingFee": {
          "description": "",
          "label": ""
        },
        "membershipOfGoverningBodyCompensation": {
          "description": "",
          "label": ""
        },
        "monetaryGiftForEmployees": {
          "description": "",
          "label": ""
        },
        "otherTaxableIncomeAsEarnedIncome": {
          "description": "",
          "label": ""
        },
        "positionOfTrustCompensation": {
          "description": "",
          "label": ""
        },
        "stockOptionsAndGrants": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        },
        "useCompensationAsCapitalIncome": {
          "description": "",
          "label": ""
        },
        "useCompensationAsEarnedIncome": {
          "description": "",
          "label": ""
        },
        "workEffortBasedDividendsAsNonWage": {
          "description": "",
          "label": ""
        },
        "workEffortBasedDividendsAsWage": {
          "description": "",
          "label": ""
        }
      },
      "PalkkausCampaignPricing": {
        "campaignAfterFirstUse": {
          "description": "",
          "label": ""
        },
        "campaignFirstUse": {
          "description": "",
          "label": ""
        },
        "companyFreeTrial": {
          "description": "",
          "label": ""
        },
        "none": {
          "description": "",
          "label": ""
        }
      },
      "PartnerSite": {
        "careDotCom": {
          "description": "",
          "label": ""
        },
        "dev": {
          "description": "",
          "label": ""
        },
        "duunitori": {
          "description": "",
          "label": ""
        },
        "example": {
          "description": "",
          "label": ""
        },
        "mol": {
          "description": "",
          "label": ""
        },
        "palkkaapakolaindescription": "",
        "palkkaapakolainlabel": "",
        "palkkaus": {
          "description": "",
          "label": ""
        },
        "palkkausEmployerFillIn": {
          "description": "",
          "label": ""
        },
        "palkkausIdentified": {
          "description": "",
          "label": ""
        },
        "raksa": {
          "description": "",
          "label": ""
        },
        "refugeeJobs": {
          "description": "",
          "label": ""
        },
        "talkkarit": {
          "description": "",
          "label": ""
        },
        "unknown": {
          "description": "",
          "label": ""
        }
      },
      "PaymentChannel": {
        "accountorGo": {
          "description": "",
          "label": ""
        },
        "finagoSolo": {
          "description": "",
          "label": ""
        },
        "holviCfa": {
          "description": "",
          "label": ""
        },
        "palkkausCfaFinvoice": {
          "description": "",
          "label": ""
        },
        "palkkausCfaPaytrail": {
          "description": "",
          "label": ""
        },
        "palkkausCfaReference": {
          "description": "",
          "label": ""
        },
        "palkkausManual": {
          "description": "",
          "label": ""
        },
        "palkkausWS": {
          "description": "",
          "label": ""
        },
        "talenomOnline": {
          "description": "",
          "label": ""
        },
        "test": {
          "description": "",
          "label": ""
        },
        "zeroPayment": {
          "description": "",
          "label": ""
        }
      },
      "PaymentStatus": {
        "bankDelivered": {
          "description": "",
          "label": "Godkänt av banken"
        },
        "bankError": {
          "description": "",
          "label": "Fel hos banken"
        },
        "bankPartialError": {
          "description": "",
          "label": "Fel hos banken"
        },
        "bankTechApproval": {
          "description": "",
          "label": "Mottaget av banken"
        },
        "cancelled": {
          "description": "",
          "label": "Betalning avrbuten"
        },
        "new": {
          "description": "",
          "label": ""
        },
        "paid": {
          "description": "",
          "label": "Betalning genomförd"
        },
        "sentToBank": {
          "description": "",
          "label": "Skickat till banken"
        },
        "unknown": {
          "description": "",
          "label": "Okänd"
        }
      },
      "PayrollStatus": {
        "draft": {
          "label": "Utkast"
        },
        "paymentCanceled": {
          "label": "Utbetalning avbruten"
        },
        "paymentError": {
          "label": "Fel i utbetalning"
        },
        "paymentStarted": {
          "label": "Utbetalning påbörjad"
        },
        "paymentSucceeded": {
          "label": "Utbetalning genomförd"
        },
        "template": {
          "label": ""
        }
      },
      "PensionCalculation": {
        "athlete": {
          "description": "",
          "label": ""
        },
        "boardRemuneration": {
          "description": "",
          "label": ""
        },
        "compensation": {
          "description": "",
          "label": ""
        },
        "employee": {
          "description": "",
          "label": ""
        },
        "entrepreneur": {
          "description": "",
          "label": ""
        },
        "farmer": {
          "description": "",
          "label": ""
        },
        "partialOwner": {
          "description": "",
          "label": ""
        },
        "smallEntrepreneur": {
          "description": "",
          "label": ""
        },
        "smallFarmer": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "PensionCompany": {
        "apteekkien": {
          "description": "",
          "label": "Apteekkien Eläkekassa"
        },
        "elo": {
          "description": "",
          "label": "Elo"
        },
        "etera": {
          "description": "",
          "label": "Etera"
        },
        "ilmarindescription": "",
        "ilmarinen": {
          "label": "Ilmarinen"
        },
        "none": {
          "description": "",
          "label": "Välj…"
        },
        "other": {
          "description": "",
          "label": "Annat bolag"
        },
        "pensionsAlandia": {
          "description": "",
          "label": "Alandia"
        },
        "varma": {
          "description": "",
          "label": "Varma"
        },
        "veritas": {
          "description": "",
          "label": "Veritas"
        },
        "verso": {
          "description": "",
          "label": "Verso"
        }
      },
      "PeriodType": {
        "custom": {
          "label": ""
        },
        "month": {
          "label": ""
        },
        "quarter": {
          "label": ""
        },
        "year": {
          "label": ""
        }
      },
      "PricingModel": {
        "fixedFee": {
          "label": ""
        },
        "noFee": {
          "label": ""
        },
        "palkkausFee": {
          "label": ""
        }
      },
      "Product": {
        "accounting": {
          "description": "",
          "label": "",
          "status": ""
        },
        "baseService": {
          "description": "",
          "label": "",
          "status": ""
        },
        "healthCareHeltti": {
          "description": "",
          "label": "",
          "status": ""
        },
        "insurance": {
          "description": "",
          "label": "",
          "status": ""
        },
        "pension": {
          "description": "",
          "label": "",
          "status": ""
        },
        "pensionEntrepreneur": {
          "description": "",
          "label": "",
          "status": ""
        },
        "tax": {
          "description": "",
          "label": "",
          "status": ""
        },
        "taxCard": {
          "description": "",
          "label": "",
          "status": ""
        },
        "unemployment": {
          "description": "",
          "label": "",
          "status": "",
          "statusTVR": ""
        }
      },
      "ProductListFilter": {
        "all": {
          "description": "",
          "label": ""
        },
        "available": {
          "description": "",
          "label": ""
        }
      },
      "Profession": {
        "0": {
          "label": "Militärer"
        },
        "1": {
          "label": "Chefer"
        },
        "2": {
          "label": "Specialister"
        },
        "3": {
          "label": "Experter"
        },
        "4": {
          "label": "Kontors- och kundtjänstpersonal"
        },
        "5": {
          "label": "Service- och försäljningspersonal"
        },
        "6": {
          "label": "Jordbrukare, skogsarbetare m.fl."
        },
        "7": {
          "label": "Byggnads-, reparations- och tillverkningsarbetare"
        },
        "8": {
          "label": "Process- och transportarbetare"
        },
        "9": {
          "label": "Övriga arbetstagare"
        },
        "11": {
          "label": "Chefer, högre tjänstemän och organisationschefer"
        },
        "12": {
          "label": "Administrativa och kommersiella chefer"
        },
        "13": {
          "label": "Chefer inom tillverkning, utbildning, vård m.m."
        },
        "14": {
          "label": "Chefer inom hotell och restaurangbranschen, detaljhandel och andra servicenäringar"
        },
        "21": {
          "label": "Specialister inom naturvetenskap och teknik"
        },
        "22": {
          "label": "Hälso- och sjukvårdsspecialister"
        },
        "23": {
          "label": "Lärare och andra specialister inom undervisningsbranschen"
        },
        "24": {
          "label": "Specialister inom företagsekonomi och administration"
        },
        "25": {
          "label": "Specialister inom informations- och kommunikationsteknologi"
        },
        "26": {
          "label": "Specialister inom juridik samt inom social- och kultursektorn"
        },
        "31": {
          "label": "Experter inom naturvetenskap och teknik"
        },
        "32": {
          "label": "Hälso- och sjukvårdsexperter"
        },
        "33": {
          "label": "Experter inom företagsekonomi och administration"
        },
        "34": {
          "label": "Juridiska assistenter samt experter inom social- och kultursektorn"
        },
        "35": {
          "label": "Experter inom informations- och kommunikationsteknologi"
        },
        "41": {
          "label": "Kontorspersonal"
        },
        "42": {
          "label": "Kundtjänstpersonal"
        },
        "43": {
          "label": "Kontorspersonal inom ekonomi- och lagerförvaltning"
        },
        "44": {
          "label": "Annan kontors- och kundtjänstpersonal"
        },
        "51": {
          "label": "Servicepersonal"
        },
        "52": {
          "label": "Försäljare, butiksinnehavare m.fl."
        },
        "53": {
          "label": "Vård- och omsorgspersonal"
        },
        "54": {
          "label": "Säkerhets- och bevakningspersonal"
        },
        "61": {
          "label": "Jordbrukare och djuruppfödare m.fl."
        },
        "62": {
          "label": "Arbetare inom skogsbruk och fiske"
        },
        "63": {
          "label": "Växtodlare, fiskare och jägare (självhushållning)"
        },
        "71": {
          "label": "Byggnadsarbetare m.fl. (utom elmontörer)"
        },
        "72": {
          "label": "Verkstads- och gjuteriarbetare samt montörer och reparatörer"
        },
        "73": {
          "label": "Konsthantverkare, finmekaniker och tryckeripersonal"
        },
        "74": {
          "label": "Personal inom el- och elektronikbranschen"
        },
        "75": {
          "label": "Personal inom livsmedel, trä, textil m.m."
        },
        "81": {
          "label": "Processarbetare"
        },
        "82": {
          "label": "Hopsättare, industriprodukter"
        },
        "83": {
          "label": "Transportarbetare"
        },
        "91": {
          "label": "Städare, hembiträden och andra rengöringsarbetare"
        },
        "92": {
          "label": "Medhjälpare inom jordbruk, skogsbruk och fiske"
        },
        "93": {
          "label": "Medhjälpare inom industrin och byggverksamhet m.fl."
        },
        "94": {
          "label": "Köks- och restaurangbiträden"
        },
        "95": {
          "label": "Gatuförsäljare, skoputsare m.fl."
        },
        "96": {
          "label": "Gaturenhållningsarbetare och avfallshanterare m.fl."
        },
        "111": {
          "label": "Högre tjänstemän och organisationschefer"
        },
        "112": {
          "label": "Verkställande direktörer och verkschefer"
        },
        "121": {
          "label": "Chefer inom företagstjänster och administration"
        },
        "122": {
          "label": "Försäljnings-, marknadsförings- och utvecklingschefer"
        },
        "131": {
          "label": "Chefer inom jordbruk, skogsbruk och fiske"
        },
        "132": {
          "label": "Produktionschefer inom industri, gruvdrift, byggverksamhet och distribution"
        },
        "133": {
          "label": "Chefer inom informations- och kommunikationsteknologi (IKT)"
        },
        "134": {
          "label": "Chefer inom utbildning, vård, omsorg och finansiell verksamhet"
        },
        "141": {
          "label": "Hotell- och restaurangchefer"
        },
        "142": {
          "label": "Chefer inom detalj- och partihandel"
        },
        "143": {
          "label": "Chefer inom andra servicenäringar"
        },
        "211": {
          "label": "Specialister inom naturvetenskap och geovetenskap"
        },
        "212": {
          "label": "Specialister inom matematik och statistik"
        },
        "213": {
          "label": "Specialister inom biovetenskap"
        },
        "214": {
          "label": "Specialister inom teknik (utom elektroteknik)"
        },
        "215": {
          "label": "Specialister inom elektroteknik"
        },
        "216": {
          "label": "Arkitekter, planerare och lantmätare"
        },
        "221": {
          "label": "Läkare"
        },
        "222": {
          "label": "Specialister inom vårdarbete"
        },
        "223": {
          "label": "Specialister inom traditionella och alternativa vårdformer"
        },
        "224": {
          "label": "Producenter av andra hälsovårdstjänster"
        },
        "225": {
          "label": "Veterinärer"
        },
        "226": {
          "label": "Andra hälso- och sjukvårdsspecialister"
        },
        "231": {
          "label": "Universitets- och högskollärare"
        },
        "232": {
          "label": "Lärare i yrkesämnen"
        },
        "233": {
          "label": "Gymnasie- och högstadielärare"
        },
        "234": {
          "label": "Lågstadie-och barnträdgårdslärare"
        },
        "235": {
          "label": "Andra specialister inom undervisning"
        },
        "241": {
          "label": "Specialister inom finanssektorn"
        },
        "242": {
          "label": "Specialister inom administration"
        },
        "243": {
          "label": "Specialister inom försäljning, marknadsföring och information"
        },
        "251": {
          "label": "Specialister inom systemarbete"
        },
        "252": {
          "label": "Specialister, databaser, -nätverk och systemprogram"
        },
        "261": {
          "label": "Juridiska specialister"
        },
        "262": {
          "label": "Bibliotekarier, arkivarier och specialister inom museisektorn"
        },
        "263": {
          "label": "Specialister inom samhälls- och socialsektorn samt inom religion"
        },
        "264": {
          "label": "Journalister, författare och språkvetare"
        },
        "265": {
          "label": "Konstnärer"
        },
        "311": {
          "label": "Experter inom fysik, kemi och teknik"
        },
        "312": {
          "label": "Arbetsledare inom gruvdrift, industri och byggverksamhet"
        },
        "313": {
          "label": "Experter inom processövervakning"
        },
        "314": {
          "label": "Experter inom biovetenskap"
        },
        "315": {
          "label": "Fartygs-, flyg- och hamntrafikbefäl och -ledare"
        },
        "321": {
          "label": "Tekniska experter inom hälso- och sjukvården"
        },
        "322": {
          "label": "Sjukskötare, barnmorskor m.fl."
        },
        "323": {
          "label": "Utövare av naturmedicin och alternativ medicin"
        },
        "324": {
          "label": "Seminörer m.fl."
        },
        "325": {
          "label": "Andra hälso- och sjukvårdsexperter"
        },
        "331": {
          "label": "Experter inom finansiering, försäkring och redovisning"
        },
        "332": {
          "label": "Inköps- och försäljningsagenter"
        },
        "333": {
          "label": "Förmedlare av företagstjänster"
        },
        "334": {
          "label": "Administrativa och specialiserade sekreterare"
        },
        "335": {
          "label": "Tjänstemän inom beredning och bevakning i den offentliga förvaltningen"
        },
        "341": {
          "label": "Juridiska assistenter samt personal inom socialsektorn och församlingar"
        },
        "342": {
          "label": "Idrottare, idrottstränare och -instruktörer m.fl."
        },
        "343": {
          "label": "Experter inom konst och kultur samt kökschefer"
        },
        "351": {
          "label": "IKT-tekniker samt användarstöd"
        },
        "352": {
          "label": "Teletekniker samt radio- och tv-tekniker"
        },
        "411": {
          "label": "Kontorsassistenter"
        },
        "412": {
          "label": "Kontorssekreterare"
        },
        "413": {
          "label": "Textbehandlare och dataregistrerare"
        },
        "421": {
          "label": "Kassapersonal m.fl."
        },
        "422": {
          "label": "Annan kundtjänstpersonal"
        },
        "431": {
          "label": "Löneräknare, försäkringshandläggare m.fl."
        },
        "432": {
          "label": "Kontorspersonal inom transport och lager"
        },
        "441": {
          "label": "Annan kontors- och kundtjänstpersonal"
        },
        "511": {
          "label": "Personal inom resetjänster, konduktörer och guider"
        },
        "512": {
          "label": "Restaurang- och storhushållspersonal"
        },
        "513": {
          "label": "Serveringspersonal"
        },
        "514": {
          "label": "Frisörer, kosmetologer m.fl."
        },
        "515": {
          "label": "Arbetsledare inom städning och fastighetsskötsel"
        },
        "516": {
          "label": "Annan servicepersonal, personliga tjänster"
        },
        "521": {
          "label": "Gatu- och torgförsäljare"
        },
        "522": {
          "label": "Försäljare och butiksinnehavare"
        },
        "523": {
          "label": "Butikskassörer och biljettförsäljare"
        },
        "524": {
          "label": "Annan försäljningspersonal"
        },
        "531": {
          "label": "Barnskötare och skolgångsbiträden"
        },
        "532": {
          "label": "Närvårdare, annan vårdpersonal och hemvårdare"
        },
        "541": {
          "label": "Säkerhets- och bevakningspersonal"
        },
        "611": {
          "label": "Åker- och trädgårdsodlare"
        },
        "612": {
          "label": "Djuruppfödare"
        },
        "613": {
          "label": "Växtodlare och djuruppfödare, blandad drift"
        },
        "621": {
          "label": "Skogsbrukare och skogsarbetare"
        },
        "622": {
          "label": "Fiskodlare, fiskare och jägare"
        },
        "631": {
          "label": "Växtodlare (självhushållning)"
        },
        "632": {
          "label": "Boskapsuppfödare (självhushållning)"
        },
        "633": {
          "label": "Växtodlare och boskapsuppfödare (självhushållning)"
        },
        "634": {
          "label": "Fiskare och jägare (självhushållning)"
        },
        "711": {
          "label": "Byggnadsarbetare m.fl."
        },
        "712": {
          "label": "Byggnadshantverkare"
        },
        "713": {
          "label": "Målare och saneringsarbetare"
        },
        "721": {
          "label": "Gjuteriarbetare, svetsare, plåtslagare m.fl."
        },
        "722": {
          "label": "Smeder, verktygsmakare och maskinställare"
        },
        "723": {
          "label": "Maskinmontörer och reparatörer"
        },
        "731": {
          "label": "Konsthantverkare och finmekaniker"
        },
        "732": {
          "label": "Tryckeripersonal"
        },
        "741": {
          "label": "Elmontörer och -reparatörer"
        },
        "742": {
          "label": "Montörer och reparatörer inom elektronik och teleteknik"
        },
        "751": {
          "label": "Styckare, bagare, mejerister m.fl."
        },
        "752": {
          "label": "Trävaruhanterare, snickare m.fl."
        },
        "753": {
          "label": "Personal inom konfektionsbranschen m.fl."
        },
        "754": {
          "label": "Annan personal, oklassificerad under andra moment i huvudgrupp 7"
        },
        "811": {
          "label": "Maskinoperatörer i gruv- och stenbrottsarbete"
        },
        "812": {
          "label": "Processoperatörer och efterbehandlare, metallindustri"
        },
        "813": {
          "label": "Processoperatörer, kemisk industri och fotografiska produkter"
        },
        "814": {
          "label": "Processoperatörer, gummi-, plast- och pappersvaruindustri"
        },
        "815": {
          "label": "Processoperatörer, textil-, skinn- och läderindustri"
        },
        "816": {
          "label": "Processoperatörer, livsmedelsindustri"
        },
        "817": {
          "label": "Processoperatörer, sågvaror samt pappers- och kartongindustri"
        },
        "818": {
          "label": "Andra processoperatörer"
        },
        "821": {
          "label": "Hopsättare, industriprodukter"
        },
        "831": {
          "label": "Spårtrafikförare m.fl."
        },
        "832": {
          "label": "Person- och paketbilsförare samt motorcykelförare"
        },
        "833": {
          "label": "Förare av tunga motorfordon"
        },
        "834": {
          "label": "Arbetsmaskinförare"
        },
        "835": {
          "label": "Däckmanskap m.fl. sjötrafikarbetare"
        },
        "911": {
          "label": "Hem-, hotell- och kontorsstädare m.fl."
        },
        "912": {
          "label": "Fordonstvättare och fönsterputsare m.fl."
        },
        "921": {
          "label": "Medhjälpare inom jordbruk, skogsbruk och fiske"
        },
        "931": {
          "label": "Medhjälpare inom gruvdrift och byggverksamhet"
        },
        "932": {
          "label": "Medhjälpare inom tillverkning"
        },
        "933": {
          "label": "Godshanterare, lagerarbetare m.fl."
        },
        "941": {
          "label": "Köks- och restaurangbiträden"
        },
        "951": {
          "label": "Reklamutdelare, skoputsare m.fl."
        },
        "952": {
          "label": "Gatuförsäljare (utom livsmedel)"
        },
        "961": {
          "label": "Avfallshanterare"
        },
        "962": {
          "label": "Tidningsutdelare, bud m.fl."
        },
        "1111": {
          "label": "Lagstiftare"
        },
        "1112": {
          "label": "Högre tjänstemän inom offentlig förvaltning"
        },
        "1113": {
          "label": "Stam- och byhövdingar"
        },
        "1114": {
          "label": "Ledare i organisationssektorn"
        },
        "1120": {
          "label": "Verkställande direktörer och verkschefer"
        },
        "1211": {
          "label": "Ekonomichefer"
        },
        "1212": {
          "label": "Personalchefer"
        },
        "1213": {
          "label": "Politik- och planeringschefer"
        },
        "1219": {
          "label": "Övriga chefer inom företagstjänster och administration"
        },
        "1221": {
          "label": "Försäljnings- och marknadsföringschefer"
        },
        "1222": {
          "label": "Reklam- och kommunikationschefer, PR-chefer"
        },
        "1223": {
          "label": "Forsknings- och utvecklingschefer"
        },
        "1311": {
          "label": "Chefer inom jord- och skogsbruk"
        },
        "1312": {
          "label": "Chefer inom vattenbruk och fiske"
        },
        "1321": {
          "label": "Produktionschefer inom industri"
        },
        "1322": {
          "label": "Produktionschefer inom gruvdrift"
        },
        "1323": {
          "label": "Produktionschefer inom byggverksamhet"
        },
        "1324": {
          "label": "Inköps- och distributionschefer m.fl."
        },
        "1330": {
          "label": "Chefer inom informations- och kommunikationsteknik (IKT)"
        },
        "1341": {
          "label": "Chefer inom barnomsorg"
        },
        "1342": {
          "label": "Chefer inom hälso- och sjukvård"
        },
        "1343": {
          "label": "Chefer inom äldrevård"
        },
        "1344": {
          "label": "Chefer inom socialvård"
        },
        "1345": {
          "label": "Chefer inom utbildning"
        },
        "1346": {
          "label": "Chefer inom bank- och försäkringstjänster"
        },
        "1349": {
          "label": "Övriga chefer inom samhälleliga tjänster"
        },
        "1411": {
          "label": "Hotellchefer"
        },
        "1412": {
          "label": "Restaurangchefer"
        },
        "1420": {
          "label": "Chefer inom detalj- och partihandel"
        },
        "1431": {
          "label": "Chefer för sport-, fritids- och kulturcenter"
        },
        "1439": {
          "label": "Övriga chefer inom servicenäringar"
        },
        "2111": {
          "label": "Fysiker och astronomer"
        },
        "2112": {
          "label": "Meteorologer"
        },
        "2113": {
          "label": "Kemister"
        },
        "2114": {
          "label": "Geologer och geofysiker"
        },
        "2120": {
          "label": "Matematiker, aktuarier och statistiker"
        },
        "2131": {
          "label": "Biologer, botanister, zoologer m.fl."
        },
        "2132": {
          "label": "Specialister inom jordbruk, skogsbruk och fiske"
        },
        "2133": {
          "label": "Specialister inom miljö- och naturskydd"
        },
        "2141": {
          "label": "Specialister inom industriell produktion och produktionsteknik"
        },
        "2142": {
          "label": "Specialister inom anläggningsarbeten"
        },
        "2143": {
          "label": "Specialister inom miljöteknik"
        },
        "2144": {
          "label": "Specialister inom maskinteknik"
        },
        "2145": {
          "label": "Specialister inom träförädling och kemisk processteknik"
        },
        "2146": {
          "label": "Specialister inom gruvindustri, metallurgi m.fl."
        },
        "2149": {
          "label": "Övriga specialister inom teknik"
        },
        "2151": {
          "label": "Specialister inom elteknik"
        },
        "2152": {
          "label": "Specialister inom elektronik"
        },
        "2153": {
          "label": "Specialister inom IKT-branschen"
        },
        "2161": {
          "label": "Arkitekter inom husbyggande"
        },
        "2162": {
          "label": "Landskapsarkitekter"
        },
        "2163": {
          "label": "Produkt- och modedesigner"
        },
        "2164": {
          "label": "Samhälls- och trafikplanerare"
        },
        "2165": {
          "label": "Specialister inom kartläggning och lantmäteri"
        },
        "2166": {
          "label": "Grafiska designer och multimediadesigner"
        },
        "2211": {
          "label": "Allmänläkare"
        },
        "2212": {
          "label": "Överläkare och specialistläkare"
        },
        "2221": {
          "label": "Överskötare och avdelningsskötare"
        },
        "2222": {
          "label": "Ledande barnmorskor"
        },
        "2230": {
          "label": "Specialister inom traditionella och alternativa vårdformer"
        },
        "2240": {
          "label": "Producenter av andra hälsotjänster"
        },
        "2250": {
          "label": "Veterinärer"
        },
        "2261": {
          "label": "Tandläkare"
        },
        "2262": {
          "label": "Provisorer"
        },
        "2263": {
          "label": "Specialister inom miljöhälsa och arbetarskydd"
        },
        "2264": {
          "label": "Specialister inom fysioterapi"
        },
        "2265": {
          "label": "Specialister inom näringslära"
        },
        "2266": {
          "label": "Audiologer och logopeder"
        },
        "2267": {
          "label": "Specialister inom optometri"
        },
        "2269": {
          "label": "Övriga specialister inom hälso- och sjukvården utan annan klassificering"
        },
        "2310": {
          "label": "Universitets- och yrkeshögskolelärare"
        },
        "2320": {
          "label": "Lärare i yrkesämnen"
        },
        "2330": {
          "label": "Gymnasielärare och högstadielärare"
        },
        "2341": {
          "label": "Lågstadielärare"
        },
        "2342": {
          "label": "Barnträdgårdslärare"
        },
        "2351": {
          "label": "Specialister i pedagogiska metoder"
        },
        "2352": {
          "label": "Speciallärare"
        },
        "2353": {
          "label": "Övriga språklärare"
        },
        "2354": {
          "label": "Övriga musiklärare"
        },
        "2355": {
          "label": "Övriga lärare i konstnärliga ämnen"
        },
        "2356": {
          "label": "Övriga lärare och utbildare inom informationsteknik"
        },
        "2359": {
          "label": "Studiehandledare och övriga specialister inom undervisning"
        },
        "2411": {
          "label": "Specialister inom redovisning och revision"
        },
        "2412": {
          "label": "Finans- och investeringsrådgivare"
        },
        "2413": {
          "label": "Finansanalytiker"
        },
        "2421": {
          "label": "Specialister inom ledarskap och organisationer"
        },
        "2422": {
          "label": "Specialister inom utveckling av administration och näringar"
        },
        "2423": {
          "label": "Specialister inom personaladministration och karriärplanering"
        },
        "2424": {
          "label": "Specialister inom personalutveckling och utbildning"
        },
        "2431": {
          "label": "Specialister inom reklam och marknadsföring"
        },
        "2432": {
          "label": "Informatörer"
        },
        "2433": {
          "label": "Försäljningsingenjörer och läkemedelsrepresentanter (utom IKT)"
        },
        "2434": {
          "label": "Specialister inom försäljning av informations- och kommunikationsteknik (IKT)"
        },
        "2511": {
          "label": "Specialister inom applikationsarkitektur"
        },
        "2512": {
          "label": "Applikationsplanerare"
        },
        "2513": {
          "label": "Webb- och multimediautvecklare"
        },
        "2514": {
          "label": "Applikationsprogrammerare"
        },
        "2519": {
          "label": "Övriga program- och applikationsutvecklare"
        },
        "2521": {
          "label": "Databasutvecklare och databasansvariga"
        },
        "2522": {
          "label": "Systemadministratörer"
        },
        "2523": {
          "label": "Nätverksspecialister"
        },
        "2529": {
          "label": "Övriga databas- och nätverksspecialister"
        },
        "2611": {
          "label": "Advokater"
        },
        "2612": {
          "label": "Domare"
        },
        "2619": {
          "label": "Övriga specialister inom juridik"
        },
        "2621": {
          "label": "Arkivarier och specialister inom museisektorn"
        },
        "2622": {
          "label": "Bibliotekarier, informatiker m.fl."
        },
        "2631": {
          "label": "Nationalekonomer"
        },
        "2632": {
          "label": "Samhälls- och kulturforskare"
        },
        "2633": {
          "label": "Historiker, statsvetare och filosofer"
        },
        "2634": {
          "label": "Psykologer"
        },
        "2635": {
          "label": "Specialister inom socialt arbete"
        },
        "2636": {
          "label": "Präster och övriga specialister inom religion"
        },
        "2641": {
          "label": "Författare m.fl."
        },
        "2642": {
          "label": "Journalister"
        },
        "2643": {
          "label": "Översättare, tolkar och andra språkvetare"
        },
        "2651": {
          "label": "Bildkonstnärer"
        },
        "2652": {
          "label": "Musiker, sångare och kompositörer"
        },
        "2653": {
          "label": "Danskonstnärer och koreografer"
        },
        "2654": {
          "label": "Regissörer och producenter"
        },
        "2655": {
          "label": "Skådespelare"
        },
        "2656": {
          "label": "Presentatörer, konferencierer m.fl."
        },
        "2659": {
          "label": "Övriga konstnärer"
        },
        "3111": {
          "label": "Tekniska experter inom naturvetenskap"
        },
        "3112": {
          "label": "Experter inom byggande"
        },
        "3113": {
          "label": "Experter inom elteknik"
        },
        "3114": {
          "label": "Experter inom elektronik"
        },
        "3115": {
          "label": "Experter inom maskinteknik"
        },
        "3116": {
          "label": "Experter inom kemisk processteknik"
        },
        "3117": {
          "label": "Experter inom gruvindustri och metallurgi"
        },
        "3118": {
          "label": "Tekniska ritare"
        },
        "3119": {
          "label": "Övriga experter inom fysik, kemi och teknik"
        },
        "3121": {
          "label": "Arbetsledare inom gruvdrift"
        },
        "3122": {
          "label": "Arbetsledare inom industri"
        },
        "3123": {
          "label": "Arbetsledare inom byggverksamhet"
        },
        "3131": {
          "label": "Processövervakare, kraftverk"
        },
        "3132": {
          "label": "Processövervakare, förbränningsstationer och vattenreningsverk"
        },
        "3133": {
          "label": "Processövervakare, kemisk industri"
        },
        "3134": {
          "label": "Processövervakare, raffinering av petroleum och naturgas"
        },
        "3135": {
          "label": "Processövervakare, metallframställning"
        },
        "3139": {
          "label": "Övriga experter inom processövervakning"
        },
        "3141": {
          "label": "Laboranter m.fl."
        },
        "3142": {
          "label": "Jordbruks- och fiskeritekniker"
        },
        "3143": {
          "label": "Skogsbrukstekniker"
        },
        "3151": {
          "label": "Maskinchefer och -mästare på fartyg"
        },
        "3152": {
          "label": "Befäl och styrmän inom sjötrafik"
        },
        "3153": {
          "label": "Flygkaptener och -styrmän"
        },
        "3154": {
          "label": "Flygledare"
        },
        "3155": {
          "label": "Tekniska experter inom flygövervakning"
        },
        "3211": {
          "label": "Experter, bilddiagnostik och medicinteknisk utrustning"
        },
        "3212": {
          "label": "Bioanalytiker (hälsovården)"
        },
        "3213": {
          "label": "Farmaceuter"
        },
        "3214": {
          "label": "Tand- och hjälpmedelstekniker"
        },
        "3221": {
          "label": "Sjukskötare m.fl."
        },
        "3222": {
          "label": "Barnmorskor"
        },
        "3230": {
          "label": "Utövare av naturmedicin och alternativ medicin"
        },
        "3240": {
          "label": "Seminörer m.fl."
        },
        "3251": {
          "label": "Munhygienister"
        },
        "3252": {
          "label": "Patientadministratörer"
        },
        "3253": {
          "label": "Hälsorådgivare"
        },
        "3254": {
          "label": "Optiker"
        },
        "3255": {
          "label": "Fysioterapeuter m.fl."
        },
        "3256": {
          "label": "Sjukvårdsassistenter"
        },
        "3257": {
          "label": "Hälso- och arbetarskyddsinspektörer"
        },
        "3258": {
          "label": "Akutvårdare inom sjuktransporten"
        },
        "3259": {
          "label": "Övriga experter inom hälso- och sjukvården utan annan klassificering"
        },
        "3311": {
          "label": "Värdepappers- och valutamäklare"
        },
        "3312": {
          "label": "Kredit- och låneexperter"
        },
        "3313": {
          "label": "Experter inom bokföring och redovisning"
        },
        "3314": {
          "label": "Experter inom statistik och matematik"
        },
        "3315": {
          "label": "Värderare och skadeinspektörer"
        },
        "3321": {
          "label": "Försäljare av försäkringstjänster"
        },
        "3322": {
          "label": "Försäljningsrepresentanter"
        },
        "3323": {
          "label": "Inköpare"
        },
        "3324": {
          "label": "Affärsförmedlare"
        },
        "3331": {
          "label": "Speditörer, tull- och fartygsklarerare"
        },
        "3332": {
          "label": "Konferens- och evenemangsarrangörer"
        },
        "3333": {
          "label": "Arbetsförmedlare"
        },
        "3334": {
          "label": "Fastighetsmäklare och disponenter"
        },
        "3339": {
          "label": "Övriga experter inom affärslivet"
        },
        "3341": {
          "label": "Arbetsledare för kontorspersonal"
        },
        "3342": {
          "label": "Juristsekreterare"
        },
        "3343": {
          "label": "Ledningsassistenter och avdelningssekreterare"
        },
        "3344": {
          "label": "Byråsekreterare, hälso- och sjukvård"
        },
        "3351": {
          "label": "Tull- och gränstjänstemän"
        },
        "3352": {
          "label": "Skatteberedare och -inspektörer"
        },
        "3353": {
          "label": "Handläggare av socialskyddsförmåner"
        },
        "3354": {
          "label": "Tillståndstjänstemän"
        },
        "3355": {
          "label": "Kommissarier och överkonstaplar"
        },
        "3359": {
          "label": "Övriga tjänstemän inom beredning och tillsyn inom offentlig förvaltning"
        },
        "3411": {
          "label": "Juridiska assistenter och experter inom intresseorganisationer"
        },
        "3412": {
          "label": "Handledare och rådgivare inom socialsektorn"
        },
        "3413": {
          "label": "Församlingspersonal"
        },
        "3421": {
          "label": "Idrottare"
        },
        "3422": {
          "label": "Idrottstränare och funktionärer"
        },
        "3423": {
          "label": "Idrottsinstruktörer och fritidsledare"
        },
        "3431": {
          "label": "Fotografer"
        },
        "3432": {
          "label": "Inredningsplanerare m.fl."
        },
        "3433": {
          "label": "Galleri-, musei- och bibliotekstekniker"
        },
        "3434": {
          "label": "Kökschefer"
        },
        "3435": {
          "label": "Övriga experter inom konst och kultur"
        },
        "3511": {
          "label": "Driftoperatörer"
        },
        "3512": {
          "label": "Datasupportpersonal"
        },
        "3513": {
          "label": "Nätverkstekniker"
        },
        "3514": {
          "label": "Webbmaster och webbtekniker"
        },
        "3521": {
          "label": "Sändnings- och AV-tekniker"
        },
        "3522": {
          "label": "Tekniska experter inom telekommunikation"
        },
        "4110": {
          "label": "Kontorsassistenter"
        },
        "4120": {
          "label": "Kontorssekreterare"
        },
        "4131": {
          "label": "Textbehandlare"
        },
        "4132": {
          "label": "Dataregistrerare"
        },
        "4211": {
          "label": "Banktjänstemän m.fl."
        },
        "4212": {
          "label": "Vadförmedlare, croupierer m.fl."
        },
        "4213": {
          "label": "Pantlånare"
        },
        "4214": {
          "label": "Inkasserare"
        },
        "4221": {
          "label": "Resebyråtjänstemän"
        },
        "4222": {
          "label": "Telefonservicerådgivare"
        },
        "4223": {
          "label": "Växeltelefonister"
        },
        "4224": {
          "label": "Hotellreceptionister"
        },
        "4225": {
          "label": "Kundrådgivare vid informationsdisk"
        },
        "4226": {
          "label": "Receptions- och rådgivningspersonal"
        },
        "4227": {
          "label": "Undersöknings- och marknadsundersökningsintervjuare"
        },
        "4229": {
          "label": "Övrig kundtjänstpersonal utan annan klassificering"
        },
        "4311": {
          "label": "Kontorspersonal inom ekonomiförvaltning"
        },
        "4312": {
          "label": "Kontorspersonal inom statistik-, finansierings- och försäkringssektorn"
        },
        "4313": {
          "label": "Löneräknare"
        },
        "4321": {
          "label": "Lagerförvaltare m.fl."
        },
        "4322": {
          "label": "Produktionsassistenter"
        },
        "4323": {
          "label": "Kontorspersonal inom transport"
        },
        "4411": {
          "label": "Bibliotekspersonal"
        },
        "4412": {
          "label": "Brevbärare och postsorterare"
        },
        "4413": {
          "label": "Kodare, korrekturläsare m.fl."
        },
        "4414": {
          "label": "Skrivare m.fl."
        },
        "4415": {
          "label": "Arkivanställda"
        },
        "4416": {
          "label": "Assisterande kontorspersonal inom personalförvaltning"
        },
        "4419": {
          "label": "Övrig kontors- och kundtjänstpersonal utan annan klassificering"
        },
        "5111": {
          "label": "Flygvärdinnor, purser m.fl."
        },
        "5112": {
          "label": "Konduktörer, biljettkontrollanter m.fl."
        },
        "5113": {
          "label": "Guider"
        },
        "5120": {
          "label": "Restaurang- och storhushållspersonal"
        },
        "5131": {
          "label": "Servitörer"
        },
        "5132": {
          "label": "Bartendrar"
        },
        "5141": {
          "label": "Frisörer"
        },
        "5142": {
          "label": "Kosmetologer m.fl."
        },
        "5151": {
          "label": "Arbetsledare inom städning på kontor, hotell m.fl."
        },
        "5152": {
          "label": "Hushållerskor i privata hem"
        },
        "5153": {
          "label": "Fastighetsskötare"
        },
        "5161": {
          "label": "Astrologer, siare m.fl."
        },
        "5162": {
          "label": "Personliga betjänter"
        },
        "5163": {
          "label": "Personal inom begravningstjänster"
        },
        "5164": {
          "label": "Djurskötare och djurfrisörer"
        },
        "5165": {
          "label": "Trafiklärare"
        },
        "5169": {
          "label": "Övrig servicepersonal inom personliga tjänster utan annan klassificering"
        },
        "5211": {
          "label": "Kiosk- och torgförsäljare"
        },
        "5212": {
          "label": "Gatuförsäljare (livsmedel)"
        },
        "5221": {
          "label": "Butiksinnehavare (småföretagare)"
        },
        "5222": {
          "label": "Butiksföreståndare"
        },
        "5223": {
          "label": "Försäljare"
        },
        "5230": {
          "label": "Butikskassörer, biljettförsäljare m.fl."
        },
        "5241": {
          "label": "Modeller"
        },
        "5242": {
          "label": "Varudemonstratörer"
        },
        "5243": {
          "label": "Direktförsäljare"
        },
        "5244": {
          "label": "Försäljare, telefonförsäljning och kontaktcenter"
        },
        "5245": {
          "label": "Bensinstationspersonal"
        },
        "5246": {
          "label": "Försäljare, kaféer och barer"
        },
        "5249": {
          "label": "Övrig försäljningspersonal utan annan klassificering"
        },
        "5311": {
          "label": "Personal inom barnomsorgen"
        },
        "5312": {
          "label": "Skolgångsbiträden"
        },
        "5321": {
          "label": "Närvårdare"
        },
        "5322": {
          "label": "Hemvårdare (hemvårdstjänster)"
        },
        "5329": {
          "label": "Övrig personal inom hälso- och sjukvården"
        },
        "5411": {
          "label": "Brandmän"
        },
        "5412": {
          "label": "Poliser"
        },
        "5413": {
          "label": "Fångvaktare"
        },
        "5414": {
          "label": "Väktare"
        },
        "5419": {
          "label": "Övrig säkerhetspersonal"
        },
        "6111": {
          "label": "Odlare, åkerbruk och frilandsodling"
        },
        "6112": {
          "label": "Odlare av fruktträd och buskar m.fl."
        },
        "6113": {
          "label": "Odlare och arbetare inom trädgårds- och växthusodling"
        },
        "6114": {
          "label": "Odlare av blandade växtslag"
        },
        "6121": {
          "label": "Uppfödare av kött- och mjölkboskap och andra husdjur"
        },
        "6122": {
          "label": "Fjäderfäuppfödare"
        },
        "6123": {
          "label": "Biodlare m.fl."
        },
        "6129": {
          "label": "Övriga djuruppfödare och djurskötare"
        },
        "6130": {
          "label": "Växtodlare och djuruppfödare, blandad drift"
        },
        "6210": {
          "label": "Skogsbrukare och skogsarbetare"
        },
        "6221": {
          "label": "Fiskodlare och fiskodlingspersonal"
        },
        "6222": {
          "label": "Fiskare"
        },
        "6223": {
          "label": "Djuphavsfiskare"
        },
        "6224": {
          "label": "Viltvårdare och jägare"
        },
        "6310": {
          "label": "Växtodlare (självhushållning)"
        },
        "6320": {
          "label": "Djuruppfödare (självhushållning)"
        },
        "6330": {
          "label": "Växtodlare och djuruppfödare (självhushållning)"
        },
        "6340": {
          "label": "Fiskare och jägare (självhushållning)"
        },
        "7111": {
          "label": "Byggnadsarbetare"
        },
        "7112": {
          "label": "Murare m.fl."
        },
        "7113": {
          "label": "Stenhuggare m.fl."
        },
        "7114": {
          "label": "Betongarbetare och armerare"
        },
        "7115": {
          "label": "Timmermän och byggnadssnickare"
        },
        "7119": {
          "label": "Övriga byggnadsarbetare"
        },
        "7121": {
          "label": "Takmontörer och takreparatörer"
        },
        "7122": {
          "label": "Golvläggare"
        },
        "7123": {
          "label": "Rappare"
        },
        "7124": {
          "label": "Isoleringsmontörer"
        },
        "7125": {
          "label": "Glasmästare"
        },
        "7126": {
          "label": "Rörläggare"
        },
        "7127": {
          "label": "Ventilations- och kylmontörer"
        },
        "7131": {
          "label": "Byggnadsmålare m.fl."
        },
        "7132": {
          "label": "Sprutlackerare"
        },
        "7133": {
          "label": "Saneringsarbetare och sotare"
        },
        "7211": {
          "label": "Formare och kärnmakare"
        },
        "7212": {
          "label": "Svetsare och gasskärare"
        },
        "7213": {
          "label": "Tunnplåtslagare"
        },
        "7214": {
          "label": "Grovplåtslagare och stålkonstruktionsmontörer"
        },
        "7215": {
          "label": "Kabelläggare och riggare"
        },
        "7221": {
          "label": "Smeder"
        },
        "7222": {
          "label": "Verktygsmakare och låssmeder"
        },
        "7223": {
          "label": "Maskinställare och verkstadsmekaniker"
        },
        "7224": {
          "label": "Maskinslipare, -polerare och -vässare"
        },
        "7231": {
          "label": "Motorfordonsmontörer och -reparatörer"
        },
        "7232": {
          "label": "Flygmontörer och flygreparatörer"
        },
        "7233": {
          "label": "Maskinmontörer och -reparatörer (jordbruks- och industrimaskiner)"
        },
        "7234": {
          "label": "Cykelreparatörer m.fl."
        },
        "7311": {
          "label": "Urmakare och övriga tillverkare av finmekaniska instrument"
        },
        "7312": {
          "label": "Musikinstrumentmakare och -stämmare"
        },
        "7313": {
          "label": "Juvel-, guld- och silversmeder"
        },
        "7314": {
          "label": "Ler- och tegelgjutare, drejare"
        },
        "7315": {
          "label": "Glasblåsare, -skärare, -slipare och -efterbehandlare"
        },
        "7316": {
          "label": "Glasgravörer, glasetsare och skyltmålare"
        },
        "7317": {
          "label": "Konsthantverkare i trä m.m."
        },
        "7318": {
          "label": "Konsthantverkare i textil, läder m.m."
        },
        "7319": {
          "label": "Övriga konsthantverkare"
        },
        "7321": {
          "label": "Prepressoperatörer"
        },
        "7322": {
          "label": "Tryckare"
        },
        "7323": {
          "label": "Efterbehandlare och bokbinderiarbetare"
        },
        "7411": {
          "label": "Byggnadselmontörer"
        },
        "7412": {
          "label": "Övriga elmontörer"
        },
        "7413": {
          "label": "Linjemontörer och -reparatörer"
        },
        "7421": {
          "label": "Montörer och reparatörer, elektronik och automation"
        },
        "7422": {
          "label": "Montörer och reparatörer, IKT"
        },
        "7511": {
          "label": "Slaktare, fiskberedare m.fl."
        },
        "7512": {
          "label": "Bagare och konditorer"
        },
        "7513": {
          "label": "Mejerister, ostmästare m.fl."
        },
        "7514": {
          "label": "Tillverkare av frukt- och grönsaksprodukter"
        },
        "7515": {
          "label": "Kvalitetsbedömare (mat och dryck)"
        },
        "7516": {
          "label": "Tillverkare av tobaksprodukter"
        },
        "7521": {
          "label": "Trävaruhanterare"
        },
        "7522": {
          "label": "Möbelsnickare m.fl."
        },
        "7523": {
          "label": "Maskinsnickare"
        },
        "7531": {
          "label": "Skräddare, ateljésömmerskor, körsnärer och modister"
        },
        "7532": {
          "label": "Tillskärare och modellmästare"
        },
        "7533": {
          "label": "Broderare och andra textilsömmare"
        },
        "7534": {
          "label": "Tapetserare"
        },
        "7535": {
          "label": "Garvare och skinnberedare"
        },
        "7536": {
          "label": "Skomakare m.fl."
        },
        "7541": {
          "label": "Dykare"
        },
        "7542": {
          "label": "Laddare och sprängare"
        },
        "7543": {
          "label": "Klassificerare och kvalitetskontrollanter (utom drycker och livsmedel)"
        },
        "7544": {
          "label": "Röksanerare, skadedjurs- och ogrässanerare"
        },
        "7549": {
          "label": "Övrig personal, oklassificerad under andra moment i huvudgrupp 7"
        },
        "8111": {
          "label": "Gruv- och stenbrottsarbetare"
        },
        "8112": {
          "label": "Anrikningsarbetare"
        },
        "8113": {
          "label": "Slagborrare och djupborrare"
        },
        "8114": {
          "label": "Processoperatörer inom betongindustri m.fl."
        },
        "8121": {
          "label": "Processoperatörer inom metallindustri"
        },
        "8122": {
          "label": "Ytbeläggare och efterbehandlare inom metallindustri"
        },
        "8131": {
          "label": "Processoperatörer, kemisk industri m.fl."
        },
        "8132": {
          "label": "Processoperatörer, fotografiska produkter"
        },
        "8141": {
          "label": "Processoperatörer, gummiindustri"
        },
        "8142": {
          "label": "Processoperatörer, plastindustri"
        },
        "8143": {
          "label": "Processoperatörer, pappersvaruindustri"
        },
        "8151": {
          "label": "Maskinoperatörer, fibertillverkning, spinning och spolning"
        },
        "8152": {
          "label": "Maskinoperatörer, vävning och stickning"
        },
        "8153": {
          "label": "Symaskinsoperatörer"
        },
        "8154": {
          "label": "Maskinoperatörer, tvättning, blekning och färgning"
        },
        "8155": {
          "label": "Maskinoperatörer, pälsskinnsberedning, färgning"
        },
        "8156": {
          "label": "Processoperatörer, sko- och väskindustri"
        },
        "8157": {
          "label": "Tvätteripersonal"
        },
        "8159": {
          "label": "Övriga processoperatörer inom textil-, skinn- och läderindustri"
        },
        "8160": {
          "label": "Processoperatörer, livsmedelsindustri"
        },
        "8171": {
          "label": "Processoperatörer, pappers-, kartong- och massaindustri"
        },
        "8172": {
          "label": "Processoperatörer, trä- och sågvaror"
        },
        "8181": {
          "label": "Ugnsoperatörer, glas- och keramikindustri"
        },
        "8182": {
          "label": "Operatörer och eldare m.fl., ångmaskiner och ångpannor"
        },
        "8183": {
          "label": "Maskinoperatörer, packning, påfyllning och märkning"
        },
        "8189": {
          "label": "Övriga processoperatörer utan annan klassificering"
        },
        "8211": {
          "label": "Hopsättare, verkstads- och metallprodukter"
        },
        "8212": {
          "label": "Hopsättare, elektrisk och elektronisk utrustning"
        },
        "8219": {
          "label": "Övriga hopsättare av industriprodukter"
        },
        "8311": {
          "label": "Lokförare"
        },
        "8312": {
          "label": "Bangårdspersonal m.fl."
        },
        "8321": {
          "label": "Motorcykelbud m.fl."
        },
        "8322": {
          "label": "Person-, taxi- och paketbilsförare"
        },
        "8331": {
          "label": "Buss- och spårvagnsförare"
        },
        "8332": {
          "label": "Lastbils- och specialfordonsförare"
        },
        "8341": {
          "label": "Förare av jordbruks- och skogsmaskiner"
        },
        "8342": {
          "label": "Anläggningsmaskinförare m.fl."
        },
        "8343": {
          "label": "Kranförare"
        },
        "8344": {
          "label": "Stuveriarbetare och truckförare m.fl."
        },
        "8350": {
          "label": "Däckmanskap, sjötrafikarbetare m.fl."
        },
        "9111": {
          "label": "Hembiträden och städare"
        },
        "9112": {
          "label": "Kontors- och hotellstädare m.fl."
        },
        "9121": {
          "label": "Rengörare och pressare"
        },
        "9122": {
          "label": "Fordonstvättare"
        },
        "9123": {
          "label": "Fönsterputsare"
        },
        "9129": {
          "label": "Övriga rengöringsarbetare"
        },
        "9211": {
          "label": "Medhjälpare, jordbruk"
        },
        "9212": {
          "label": "Medhjälpare, djuruppfödning"
        },
        "9213": {
          "label": "Medhjälpare, växtodling och djuruppfödning, blandad drift"
        },
        "9214": {
          "label": "Medhjälpare, trädgårdsodling"
        },
        "9215": {
          "label": "Medhjälpare, skogsbruk"
        },
        "9216": {
          "label": "Medhjälpare, fiske och vattenbruk"
        },
        "9311": {
          "label": "Medhjälpare, gruvor"
        },
        "9312": {
          "label": "Medhjälpare, anläggningsverksamhet"
        },
        "9313": {
          "label": "Medhjälpare, byggnadsverksamhet"
        },
        "9321": {
          "label": "Handpaketerare"
        },
        "9329": {
          "label": "Övriga medhjälpare inom industrin"
        },
        "9331": {
          "label": "Förare av hand- och pedaldrivna bilar"
        },
        "9332": {
          "label": "Förare av djurdragna fordon"
        },
        "9333": {
          "label": "Godshanterare, lagerarbetare m.fl."
        },
        "9334": {
          "label": "Varupåfyllare"
        },
        "9411": {
          "label": "Personal inom snabbmatssektorn"
        },
        "9412": {
          "label": "Köksbiträden"
        },
        "9510": {
          "label": "Reklamutdelare, skoputsare m.fl."
        },
        "9520": {
          "label": "Gatuförsäljare (utom livsmedel)"
        },
        "9611": {
          "label": "Avfallsinsamlare"
        },
        "9612": {
          "label": "Avfallssorterare"
        },
        "9613": {
          "label": "Gatsopare m.fl."
        },
        "9621": {
          "label": "Tidnings- och reklamutdelare, bud"
        },
        "9622": {
          "label": "Diversearbetare"
        },
        "9623": {
          "label": "Mätaravläsare m.fl."
        },
        "9624": {
          "label": "Vattenhämtare och vedinsamlare"
        },
        "9629": {
          "label": "Övrig personal utan annan klassificering"
        },
        "11121": {
          "label": "Direktörer inom statens centralförvaltning"
        },
        "11122": {
          "label": "Direktörer och högre tjänstemän inom statlig distrikts- och lokalförvaltning"
        },
        "11141": {
          "label": "Chefer i arbetsmarknads- och näringslivsorganisationer"
        },
        "11142": {
          "label": "Chefer i andra organisationer"
        },
        "21321": {
          "label": "Specialister inom jordbruk och fiske"
        },
        "21322": {
          "label": "Specialister inom skogsbruk"
        },
        "21531": {
          "label": "Forskare inom teleteknik"
        },
        "21532": {
          "label": "Forskare inom informationsteknik"
        },
        "22121": {
          "label": "Överläkare"
        },
        "22122": {
          "label": "Specialistläkare"
        },
        "22211": {
          "label": "Överskötare"
        },
        "22212": {
          "label": "Avdelningsskötare"
        },
        "23101": {
          "label": "Professorer"
        },
        "23102": {
          "label": "Lektorer och överassistenter (universitet)"
        },
        "23103": {
          "label": "Assistenter och timlärare (universitet)"
        },
        "23104": {
          "label": "Överlärare (yrkeshögskolor)"
        },
        "23105": {
          "label": "Lektorer (yrkeshögskolor)"
        },
        "23106": {
          "label": "Timlärare m.fl. (yrkeshögskolor)"
        },
        "23301": {
          "label": "Lärare i matematiska ämnen"
        },
        "23302": {
          "label": "Lärare i modersmål och litteratur"
        },
        "23303": {
          "label": "Lärare i konst- och färdighetsämnen"
        },
        "23304": {
          "label": "Övriga högstadie- och gymnasielärare"
        },
        "23411": {
          "label": "Klasslärare"
        },
        "23412": {
          "label": "Ämneslärare (lågstadiet)"
        },
        "23591": {
          "label": "Studiehandledare"
        },
        "23592": {
          "label": "Övriga specialister inom undervisning"
        },
        "26211": {
          "label": "Arkivarier"
        },
        "26212": {
          "label": "Specialister inom museisektorn"
        },
        "26351": {
          "label": "Socialarbetare m.fl."
        },
        "26352": {
          "label": "Planerare inom socialsektorn m.fl."
        },
        "26421": {
          "label": "Redaktionschefer och redaktionssekreterare, programchefer"
        },
        "26422": {
          "label": "Redaktörer vid tidningar m.fl."
        },
        "26423": {
          "label": "Radio- och TV-redaktörer"
        },
        "31121": {
          "label": "Experter inom husbyggnad"
        },
        "31122": {
          "label": "Experter inom anläggningsarbeten"
        },
        "31123": {
          "label": "Experter inom kartläggning och lantmäteri"
        },
        "31521": {
          "label": "Fartygsbefäl och styrmän (stora fartyg)"
        },
        "31522": {
          "label": "Befäl (mindre fartyg)"
        },
        "31523": {
          "label": "Hamntrafikledare och hamnkaptener"
        },
        "32141": {
          "label": "Tandtekniker"
        },
        "32142": {
          "label": "Hjälpmedelstekniker"
        },
        "32211": {
          "label": "Sjukskötare"
        },
        "32212": {
          "label": "Hälsovårdare"
        },
        "32591": {
          "label": "Ergoterapeuter"
        },
        "32592": {
          "label": "Övriga terapeuter"
        },
        "33341": {
          "label": "Fastighetsmäklare"
        },
        "33342": {
          "label": "Disponenter"
        },
        "34111": {
          "label": "Rättegångsombud och utsökningsmän"
        },
        "34112": {
          "label": "Ombud, funktionärer m.fl. experter inom organisationssektorn"
        },
        "34113": {
          "label": "Konsumentrådgivare m.fl."
        },
        "34121": {
          "label": "Handledare inom socialsektorn"
        },
        "34122": {
          "label": "Ungdomsledare (ej församl.)"
        },
        "34123": {
          "label": "Handledare och sysselsättningsledare"
        },
        "34131": {
          "label": "Diakoner och diakonissor"
        },
        "34139": {
          "label": "Övriga församlingsarbetare"
        },
        "34351": {
          "label": "Scriptor och övriga experter inom scenteknik"
        },
        "34359": {
          "label": "Övriga experter inom konst- och konstindustrisektorn"
        },
        "42291": {
          "label": "Nödcentraloperatörer"
        },
        "42299": {
          "label": "Övrig kundtjänstpersonal utan annan klassificering"
        },
        "43231": {
          "label": "Spårtrafikledare, trafiksamordnare m.fl."
        },
        "43239": {
          "label": "Övriga kontorstjänstemän inom transport och spedition"
        },
        "44121": {
          "label": "Brevbärare"
        },
        "44122": {
          "label": "Kontorsvaktmästare"
        },
        "51201": {
          "label": "Kockar, kokerskor och kallskänkor"
        },
        "51202": {
          "label": "Restaurangföreståndare och skiftledare"
        },
        "51631": {
          "label": "Begravningsbyråföreståndare m.fl."
        },
        "51632": {
          "label": "Övrig begravningspersonal"
        },
        "53111": {
          "label": "Barnskötare, daghem och andra institutioner m.fl."
        },
        "53112": {
          "label": "Familjedagvårdare"
        },
        "53113": {
          "label": "Barnklubbsledare m.fl."
        },
        "53211": {
          "label": "Mentalhälsovårdare"
        },
        "53212": {
          "label": "Vårdare av utvecklingshämmade"
        },
        "53213": {
          "label": "Vårdare inom socialsektorn"
        },
        "53219": {
          "label": "Övriga närvårdare"
        },
        "53221": {
          "label": "Hemvårdspersonal"
        },
        "53222": {
          "label": "Personliga assistenter, närståendevårdare m.fl."
        },
        "53291": {
          "label": "Tandskötare"
        },
        "53292": {
          "label": "Instrumentvårdare"
        },
        "53293": {
          "label": "Läkemedelsarbetare på apotek"
        },
        "53294": {
          "label": "Massörer och konditionsskötare"
        },
        "61111": {
          "label": "Åkerodlare"
        },
        "61112": {
          "label": "Arbetsledare och arbetare inom åkerodling"
        },
        "61131": {
          "label": "Trädgårds- och växthusodlare"
        },
        "61132": {
          "label": "Arbetsledare och anställda inom trädgårds- och växthusodling"
        },
        "61211": {
          "label": "Boskapsuppfödare m.fl."
        },
        "61212": {
          "label": "Boskapsskötare m.fl."
        },
        "61213": {
          "label": "Sällskapsdjursuppfödare"
        },
        "61214": {
          "label": "Lantbruksavbytare"
        },
        "61291": {
          "label": "Pälsdjurs- och renskötare"
        },
        "61299": {
          "label": "Övriga djurskötare utan annan klassificering"
        },
        "62211": {
          "label": "Fiskodlingsföretagare"
        },
        "62212": {
          "label": "Arbetsledare och arbetare inom fiskodling"
        },
        "74211": {
          "label": "Elektronikmontörer och -reparatörer"
        },
        "74212": {
          "label": "Automationsmontörer och -reparatörer"
        },
        "81821": {
          "label": "Maskinreparatörer, kraftverk och avfallsbehandlingsanläggningar"
        },
        "81822": {
          "label": "Övriga maskinreparatörer m.fl."
        },
        "83441": {
          "label": "Stuveriarbetare"
        },
        "83442": {
          "label": "Truckförare m.fl."
        },
        "91121": {
          "label": "Kontorsstädare m.fl."
        },
        "91122": {
          "label": "Hotellstädare"
        },
        "91123": {
          "label": "Sjukhus- och vårdbiträden"
        },
        "91124": {
          "label": "Daghemsbiträden"
        },
        "91129": {
          "label": "Övriga städare utan annan klassificering"
        },
        "01": {
          "label": "Officerare"
        },
        "011": {
          "label": "Officerare"
        },
        "0110": {
          "label": "Officerare"
        },
        "02": {
          "label": "Underofficerare"
        },
        "021": {
          "label": "Underofficerare"
        },
        "0210": {
          "label": "Underofficerare"
        },
        "03": {
          "label": "Yrkesutbildad personal inom militären"
        },
        "031": {
          "label": "Yrkesutbildad personal inom militären"
        },
        "0310": {
          "label": "Yrkesutbildad personal inom militären"
        },
        "#X": {
          "label": "Okänd"
        },
        "#XX": {
          "label": "Okänd"
        },
        "#XXX": {
          "label": "Okänd"
        },
        "#XXXX": {
          "label": "Okänd"
        },
        "#XXXXX": {
          "label": "Okänd"
        }
      },
      "RemunerationKind": {
        "bonusPay": {
          "description": "",
          "label": ""
        },
        "commission": {
          "description": "",
          "label": ""
        },
        "initiativeFee": {
          "description": "",
          "label": ""
        },
        "performanceBonus": {
          "description": "",
          "label": ""
        },
        "profitSharingBonus": {
          "description": "",
          "label": ""
        },
        "shareIssueForEmployees": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "ReportCategory": {
        "all": {
          "description": "",
          "label": ""
        },
        "monthly": {
          "description": "",
          "label": ""
        },
        "yearly": {
          "description": "",
          "label": ""
        }
      },
      "ReportType": {
        "employerReport": {
          "description": "",
          "label": "Arbetstagarrapport"
        },
        "employmentContract": {
          "description": "",
          "label": "Arbetskontrakt"
        },
        "example": {
          "description": "",
          "label": "Exempel"
        },
        "householdDeduction": {
          "description": "",
          "label": "Hushållsavdrag"
        },
        "insurance": {
          "description": "",
          "label": "Försäkring"
        },
        "monthlyDetails": {
          "description": "",
          "label": "Månatliga detlajer"
        },
        "monthlyLiikekirjuri": {
          "description": "",
          "label": ""
        },
        "monthlyPension": {
          "description": "",
          "label": "Månatlig pension"
        },
        "monthlyRapko": {
          "description": "",
          "label": ""
        },
        "paymentReport": {
          "description": "",
          "label": "Betalningsrapport"
        },
        "salarySlip": {
          "description": "",
          "label": "Lönekvitto"
        },
        "salarySlipPaid": {
          "description": "",
          "label": "Lönekvitto betalt"
        },
        "taxHouseholdDeduction14B": {
          "description": "",
          "label": ""
        },
        "taxHouseholdDeduction14BSpouseA": {
          "description": "",
          "label": ""
        },
        "taxHouseholdDeduction14BSpouseB": {
          "description": "",
          "label": ""
        },
        "taxMonthly4001": {
          "description": "",
          "label": "Månatlig skatt"
        },
        "taxYearly7801": {
          "description": "",
          "label": "Årlig skatt"
        },
        "undefined": {
          "description": "",
          "label": "Odefinierad"
        },
        "unemployment": {
          "description": "",
          "label": "Arbetslöshet"
        },
        "yearEndReport": {
          "description": "",
          "label": "Årsslutsrapport"
        },
        "yearlyDetails": {
          "description": "",
          "label": "Årliga uppgifter"
        },
        "yearlyWorkerSummary": {
          "description": "",
          "label": ""
        }
      },
      "SalaryKind": {
        "compensation": {
          "description": "",
          "label": "Kompensation",
          "labels": {
            "household": ""
          }
        },
        "fixedSalary": {
          "description": "",
          "label": "Fast lön"
        },
        "hourlySalary": {
          "description": "",
          "label": "Timlön"
        },
        "monthlySalary": {
          "description": "",
          "label": "Månadslön"
        },
        "salary": {
          "description": "",
          "label": ""
        },
        "totalEmployerPayment": {
          "description": "",
          "label": ""
        },
        "totalWorkerPayment": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": "Odefinierad"
        }
      },
      "SalaryPeriod": {
        "fixed": {
          "description": "",
          "label": ""
        },
        "hourly": {
          "description": "",
          "label": ""
        },
        "monthly": {
          "description": "",
          "label": ""
        },
        "notDefined": {
          "description": "",
          "label": ""
        }
      },
      "SalaryPerMonthRange": {
        "noPensionRequired": {
          "description": "",
          "label": "Under 57,10 €"
        },
        "normal": {
          "description": "",
          "label": "57,10 € eller över"
        },
        "unknown": {
          "description": "",
          "label": "Okänd"
        }
      },
      "SalaryPerYearRange": {
        "noMonthlyReporting": {
          "description": "",
          "label": "203 - 1500 €"
        },
        "noReportingNecessary": {
          "description": "",
          "label": "3 - 200 €"
        },
        "normal": {
          "description": "",
          "label": "Över 1500 €"
        },
        "zeroSalary": {
          "description": "",
          "label": 2
        }
      },
      "ServiceModel": {
        "partnerOnly": {
          "label": ""
        },
        "shared": {
          "label": ""
        }
      },
      "SubsidisedCommuteKind": {
        "noDeduction": {
          "description": "",
          "label": ""
        },
        "periodicalDeduction": {
          "description": "",
          "label": ""
        },
        "singleDeduction": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "TaxCardIncomeType": {
        "externalSalaries": {
          "description": "",
          "label": "Utomstående löner"
        },
        "previousEmployerSalaries": {
          "description": "Lön som personen förtjänat tidigare under samma år. Markera dem i euro i en klumpsumma.",
          "label": "Lön betalt av tidigare arbetsgivare"
        },
        "salaxyCalculation": {
          "description": "",
          "label": "Salaxy uträkning"
        },
        "unknown": {
          "description": "",
          "label": "Okänd"
        }
      },
      "TaxcardKind": {
        "defaultYearly": {
          "label": ""
        },
        "historical": {
          "label": ""
        },
        "noTaxCard": {
          "label": ""
        },
        "noWithholdingHousehold": {
          "label": ""
        },
        "others": {
          "label": ""
        },
        "replacement": {
          "label": ""
        },
        "undefined": {
          "label": ""
        }
      },
      "TaxcardState": {
        "employerAdded": {
          "label": ""
        },
        "expired": {
          "label": ""
        },
        "new": {
          "label": "Ny"
        },
        "replaced": {
          "label": ""
        },
        "shared": {
          "label": "Ny"
        },
        "sharedApproved": {
          "label": ""
        },
        "sharedRejected": {
          "label": "Avslagen"
        },
        "sharedRejectedWithoutOpen": {
          "label": ""
        },
        "sharedWaiting": {
          "label": "Väntar"
        },
        "verifiedVero": {
          "label": ""
        }
      },
      "TaxcardValidity": {
        "expired": {
          "label": ""
        },
        "future": {
          "label": ""
        },
        "undefined": {
          "label": ""
        },
        "valid": {
          "label": ""
        },
        "validJanuary": {
          "label": ""
        }
      },
      "TaxDeductionWorkCategories": {
        "carework": {
          "description": "",
          "label": "Omvårdnadsarbete"
        },
        "homeImprovement": {
          "description": "",
          "label": "Hemförbättringsarbete"
        },
        "householdwork": {
          "description": "",
          "label": "Hushållsarbete"
        },
        "none": {
          "description": "",
          "label": "Välj…"
        },
        "ownPropety": {
          "description": "",
          "label": "Fritidshus i eget bruk"
        },
        "relativesProperty": {
          "description": "",
          "label": "Föräldrars eller släktingars fritidshus"
        }
      },
      "TesSubtype": {
        "constructionCarpenter": {
          "description": "",
          "label": ""
        },
        "constructionFloor": {
          "description": "",
          "label": ""
        },
        "constructionFreeContract": {
          "description": "",
          "label": ""
        },
        "constructionOther": {
          "description": "",
          "label": ""
        },
        "notSelected": {
          "description": "",
          "label": ""
        }
      },
      "TestEnum": {
        "valueEight": {
          "description": "",
          "label": ""
        },
        "valueFive": {
          "description": "",
          "label": ""
        },
        "valueFour": {
          "description": "",
          "label": ""
        },
        "valueNine": {
          "description": "",
          "label": ""
        },
        "valueOne": {
          "description": "",
          "label": ""
        },
        "valueSevdescription": "",
        "valueSevlabel": "",
        "valueSix": {
          "description": "",
          "label": ""
        },
        "valueTdescription": "",
        "valueThree": {
          "description": "",
          "label": ""
        },
        "valueTlabel": "",
        "valueTwo": {
          "description": "",
          "label": ""
        },
        "valueZero": {
          "description": "",
          "label": ""
        }
      },
      "UnionPaymentKind": {
        "other": {
          "description": "",
          "label": ""
        },
        "raksaNormal": {
          "description": "",
          "label": ""
        },
        "raksaUnemploymentOnly": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        }
      },
      "UnionPaymentType": {
        "notSelected": {
          "description": "",
          "label": "Inget medlemskap"
        },
        "other": {
          "description": "",
          "label": "Annan fackförening"
        },
        "raksaNormal": {
          "description": "",
          "label": ""
        },
        "raksaUnemploymentOnly": {
          "description": "",
          "label": ""
        }
      },
      "WageBasis": {
        "hourly": {
          "label": ""
        },
        "monthly": {
          "label": ""
        },
        "other": {
          "label": ""
        },
        "performanceBased": {
          "label": ""
        },
        "undefined": {
          "label": ""
        }
      },
      "WebSiteUserRole": {
        "company": {
          "description": "",
          "label": "Företagare"
        },
        "household": {
          "description": "",
          "label": "Arbetsgivare"
        },
        "none": {
          "description": "",
          "label": "Välj…"
        },
        "worker": {
          "description": "",
          "label": "Arbetstagare"
        }
      },
      "WorkingTimeCompensationKind": {
        "emergencyWorkCompensation": {
          "description": "",
          "label": ""
        },
        "eveningShiftAllowance": {
          "description": "",
          "label": ""
        },
        "eveningWorkCompensation": {
          "description": "",
          "label": ""
        },
        "extraWorkPremium": {
          "description": "",
          "label": ""
        },
        "nightShiftCompensation": {
          "description": "",
          "label": ""
        },
        "nightWorkAllowance": {
          "description": "",
          "label": ""
        },
        "otherCompensation": {
          "description": "",
          "label": ""
        },
        "overtimeCompensation": {
          "description": "",
          "label": ""
        },
        "saturdayPay": {
          "description": "",
          "label": ""
        },
        "standByCompensation": {
          "description": "",
          "label": ""
        },
        "sundayWorkCompensation": {
          "description": "",
          "label": ""
        },
        "undefined": {
          "description": "",
          "label": ""
        },
        "waitingTimeCompensation": {
          "description": "",
          "label": ""
        },
        "weeklyRestCompensation": {
          "description": "",
          "label": ""
        }
      },
      "YtjCompanyType": {
        "alandFederation": {
          "description": "",
          "label": "Åländskt företag"
        },
        "asoAssociation": {
          "description": "",
          "label": "Bostadsrättsförening"
        },
        "association": {
          "description": "",
          "label": "Idelogisk förening"
        },
        "condominium": {
          "description": "",
          "label": "Fastighets aktiebolag"
        },
        "cooperativeBank": {
          "description": "",
          "label": "Andelsbank"
        },
        "europeanCooperative": {
          "description": "",
          "label": "Europeiskt andelslag"
        },
        "europeanCooperativeBank": {
          "description": "",
          "label": "Europeisk andelsbank"
        },
        "federationOfMunicipalitiesEstablishment": {
          "description": "",
          "label": "Federal inrättning"
        },
        "financialAssociation": {
          "description": "",
          "label": "Ekonomisk förening"
        },
        "forestCareAssociation": {
          "description": "",
          "label": "Skogsbruksförening"
        },
        "foundation": {
          "description": "",
          "label": "Fond"
        },
        "housingCooperative": {
          "description": "",
          "label": "Bostadsandelslag"
        },
        "hypoAssociation": {
          "description": "",
          "label": "Hypoteksförening"
        },
        "insuranceAssociation": {
          "description": "",
          "label": "Försäkringsförening"
        },
        "ky": {
          "description": "",
          "label": "Kommanditbolag"
        },
        "municipalEstablishment": {
          "description": "",
          "label": "Kommunal inrättning"
        },
        "mutualInsuranceAssociation": {
          "description": "",
          "label": "Ömsesidig skadeförsäkringsförening"
        },
        "noCompanyType": {
          "description": "",
          "label": "Ingen företagsform"
        },
        "openCompany": {
          "description": "",
          "label": "Öppet bolag"
        },
        "osuuskunta": {
          "description": "",
          "label": "Andelslag"
        },
        "otherAssociation": {
          "description": "",
          "label": "Annan förening"
        },
        "otherFinancialAssociation": {
          "description": "",
          "label": "Annan finansiell förening"
        },
        "oy": {
          "description": "",
          "label": "Aktiebolag"
        },
        "privateEntrepreneur": {
          "description": "",
          "label": "Privat näringsidkare"
        },
        "reindeerHerdingCooperative": {
          "description": "",
          "label": "Renbetes association"
        },
        "savingsBank": {
          "description": "",
          "label": "Sparbank"
        },
        "specialPurposeAssociation": {
          "description": "",
          "label": "Förening grundad på särskild lagstiftning"
        },
        "stateEstablishment": {
          "description": "",
          "label": "Statligt företag"
        },
        "unknown": {
          "description": "",
          "label": "Okänd företagsform"
        }
      }
    },
    "UI_TERMS": {
      "add": "Lägg till",
      "addNew": "",
      "addRow": "Lägg till rad",
      "areYouSure": "",
      "back": "Tillbaka",
      "calculate": "Beräkna",
      "calculations": "Beräkningar",
      "cancel": "Avbryt",
      "cancelEditing": "",
      "choose": "Välj…",
      "chooseWorkDateRange": "",
      "chooseWorkDuration": "",
      "clear": "Avmarkera",
      "close": "Stäng",
      "confirm": "",
      "copy": "",
      "copyAsNew": "Kopiera som ny",
      "copyAsRequest": "",
      "copyToClipboard": "",
      "dateFormats": {
        "dm": ""
      },
      "dateNotChosen": "",
      "delete": "Ta bort",
      "demo": "",
      "dismiss": "Avslå",
      "done": "Färdig",
      "edit": "Ändra",
      "gotIt": "Jag förstår",
      "isDeleting": "",
      "isSaving": "",
      "learnMore": "Läs mer",
      "loading": "",
      "login": "Logga in",
      "logout": "Logga ut",
      "logoutAlt": "",
      "makeOrder": "",
      "minCharacters": "Skriv minst 3 tecken…",
      "monthHalf": "",
      "monthShort0": "",
      "monthShort1": "",
      "monthShort10": "",
      "monthShort11": "",
      "monthShort12": "",
      "monthShort2": "",
      "monthShort3": "",
      "monthShort4": "",
      "monthShort5": "",
      "monthShort6": "",
      "monthShort7": "",
      "monthShort8": "",
      "monthShort9": "",
      "moreInfo": "Tilläggsuppgifter",
      "navigation": "Navigering",
      "newCalculation": "Ny beräkning",
      "next": "Nästa",
      "no": "",
      "noteRequiredField": "",
      "noThanks": "Nej tack",
      "ok": "OK",
      "oldVersion": "",
      "orChoose": "",
      "otherPeriod": "",
      "pay": "Betala",
      "pleaseWait": "",
      "postpone": "Inte nu",
      "previous": "",
      "printable": "Utskrift",
      "recalculate": "Uppdatera uträkning",
      "register": "Registrera dig",
      "save": "Spara",
      "saveEdited": "Spara ändringarna",
      "search": "",
      "searchByName": "",
      "select": "Välj",
      "selectAlt": "Välj...",
      "sendEmail": "Skicka e-postmeddelande",
      "show": "Visa",
      "skip": "Hoppa över",
      "sureToDeleteRecord": "",
      "today": "I dag",
      "updateCalculation": "",
      "updatingCalculation": "",
      "use": "Ta i bruk",
      "week2": "",
      "yes": "Ja"
    },
    "VALIDATION": {
      "Files": {
        "ContentTypeError": "",
        "Empty": "",
        "MaxFileSize": "",
        "NoFiles": "",
        "SaveError": ""
      },
      "IllegalWorkerForCompanyType": {
        "description": "",
        "label": ""
      },
      "IncompleteAdvancePayment": {
        "description": "",
        "label": ""
      },
      "IncompleteForeclosure": {
        "description": "",
        "label": ""
      },
      "IncompleteOtherDeductions": {
        "description": "",
        "label": ""
      },
      "IncompletePensionPayment": {
        "description": "",
        "label": ""
      },
      "IncompletePrepaidExpenses": {
        "description": "",
        "label": ""
      },
      "IncompleteUnemploymentInsurancePayment": {
        "description": "",
        "label": ""
      },
      "IncompleteUnionPayment": {
        "description": "",
        "label": ""
      },
      "MissingOccupationCode": {
        "description": "",
        "label": ""
      },
      "NegativeRowTotalNotSupported": {
        "description": "",
        "label": ""
      },
      "Payroll": {
        "Calc": {
          "EmailAddress": {
            "label": ""
          },
          "FirstNameNotEmpty": {
            "label": ""
          },
          "IbanInvalid": {
            "label": ""
          },
          "IbanNotEmpty": {
            "label": ""
          },
          "InvalidStatusForPayroll": {
            "label": ""
          },
          "LastNameNotEmpty": {
            "label": ""
          },
          "PayrollIdConflict": {
            "label": ""
          },
          "SocialSecurityNumber_InTest": {
            "label": ""
          },
          "SocialSecurityNumber": {
            "label": ""
          },
          "TaxCardType": {
            "label": ""
          },
          "TaxPercent": {
            "label": ""
          },
          "TelephoneOrEmailNotEmpty": {
            "label": ""
          },
          "TotalGreaterThan0": {
            "label": ""
          }
        },
        "NoCalculations": {
          "label": ""
        }
      },
      "SharedTaxcard": {
        "description": "",
        "label": ""
      },
      "ValidationErrors": {
        "maxlength": "",
        "minlength": "",
        "required": "",
        "sxyCompanyIdFi": "",
        "sxyCurrency": "",
        "sxyEmail": "",
        "sxyIban": "",
        "sxyInteger": "",
        "sxyMobilePhone": "",
        "sxyNumber": "",
        "sxyPensionContractNumber": "",
        "sxyPersonalIdFi": "",
        "sxyPostalCodeFi": "",
        "sxySmsVerificationCode": "",
        "sxyTaxPercent": "",
        "sxyTemporaryPensionContractNumber": ""
      }
    }
  }
};
/** Internationalization dictionary
@hidden */
export const dictionary = _dictionary;
