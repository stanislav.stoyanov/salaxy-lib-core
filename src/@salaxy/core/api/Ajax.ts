
/**
 * Abstracts the interface for Http / Ajax functionality that is required by Salaxy core classes.
 * This interface is implemented in this project for jQuery and Node.
 * Additional implementations exists for Angular.js (salaxy-lib-ng1) and Angular.io / ngx (salaxy-lib-ng).
 * You may also create your own implementation for the framework or http component that you want to support.
 */
export interface Ajax {

  /**
   * The server address - root of the server. This is settable field.
   * Will probably be changed to a configuration object in the final version.
   */
  serverAddress: string;

  /**
   * By default credentials are not used in http-calls.
   * Enable credentials with this flag (force disabled when the token is set).
   */
  useCredentials: boolean;

  /**
   * Sets Salaxy Bearer token for this http context.
   *
   * @param token - Salaxy JWT token.
   */
  setCurrentToken(token: string);

  /** Gets the current Salaxy Bearer token */
  getCurrentToken(): string;

  /** Gets the Server address that is used as bases to the HTML queries. E.g. 'https://test-api.salaxy.com' */
  getServerAddress(): string;

  /** Gets the API address with version information. E.g. 'https://test-api.salaxy.com/v02/api' */
  getApiAddress(): string;

  /**
   * Gets a JSON-message from server using the API
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
   *
   * @return A Promise with result data. Standard Promise rejection to be used for error handling.
   */
  getJSON(method: string): Promise<any>;

  /**
   * Gets a HTML-message from server using the API
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
   *
   * @return A Promise with result html. Standard Promise rejection to be used for error handling.
   */
  getHTML(method: string): Promise<string>;

  /**
   * POSTS data to server and receives back a JSON-message.
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
   * @param data - The data that is posted to the server.
   *
   * @return A Promise with result data. Standard Promise rejection to be used for error handling.
   */
  postJSON(method: string, data: any): Promise<any>;

  /**
   * POSTS data to server and receives back HTML.
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
   * @param data - The data that is posted to the server.
   *
   * @return A Promise with result data. Standard Promise rejection to be used for error handling.
   */
  postHTML(method: string, data: any): Promise<string>;

  /**
   * Sends a DELETE-message to server using the API
   *
   * @param method - The API method is the url path after the api version segments (e.g. '/v02/api')
   *        and starts with a forward slash, e.g. '/calculator/new', or a full URL address.
   *
   * @return A Promise with result data. Standard Promise rejection to be used for error handling.
   */
  remove(method: string): Promise<any>;
}
