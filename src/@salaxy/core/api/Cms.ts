import { AbcSection, Article } from "../model";
import { Ajax } from "./Ajax";

/**
 * Read-only access to CMS Articles and other documentation
 */
export class Cms {

  /**
   * For NG1-dependency injection
   * @hidden
   */
  public static $inject = ["AjaxNg1"];

  constructor(private ajax: Ajax) {}

  /**
   * Gets articles from the server
   *
   * @param filter is one of the ABC-section categories. Use null to list all the articles.
   *
   * @return A Promise with result data (Article[]).
   */
  public getArticles(filter: AbcSection): Promise<Article[]> {
    let method = "/cms/articles";
    if (filter) {
      method += "?filter=" + filter;
    }
    return this.ajax.getJSON(method);
  }

  /**
   * Gets an article from the server
   *
   * @param id Identifier for the article
   * @param articleLinkPattern Optional pattern for CMS links from one article to another.
   *        The default is "/Cms/Article/{0}" where parameter '{0}' is replaced with article id.
   *        The caller can change this to direct internal links to appropriate page template.
   *
   * @return A Promise with result data (Article).
   */
  public getArticle(id: string, articleLinkPattern: string = null): Promise<Article> {
    let method = "/cms/articles/" + id;
    if (articleLinkPattern) {
      method += "?articleLinkPattern=" + encodeURIComponent(articleLinkPattern);
    }
    return this.ajax.getJSON(method);
  }
}
