import { Accounts } from "./Accounts";
import { Ajax } from "./Ajax";
import { Calculations } from "./Calculations";
import { Calculator } from "./Calculator";
import { Cms } from "./Cms";
import { Files } from "./Files";
import { OAuth2 } from "./OAuth2";
import { Onboardings } from "./Onboardings";
import { Payments } from "./Payments";
import { Profiles } from "./Profiles";
import { Reports } from "./Reports";
import { Session } from "./Session";
import { Taxcards } from "./Taxcards";
import { Test } from "./Test";
import { Workers } from "./Workers";

/**
 * Client to Salaxy API.
 * Abstracts the raw REST api to async methods that are easier to call from TypeScript / JavaScript.
 * Differnet platforms (NG1, NG, jQuery etc.) create derived Clients typically at least by injecting their own Ajax implementation,
 * potentially also by overriding other methods.
 */
export abstract class Client {

  private _ajax: Ajax;
  private _accountsApi: Accounts;
  private _calculationsApi: Calculations;
  private _calculatorApi: Calculator;
  private _reportsApi: Reports;
  private _filesApi: Files;
  private _sessionApi: Session;
  private _cmsApi: Cms;
  private _profilesApi: Profiles;
  private _paymentsApi: Payments;
  private _oAuth2Api: OAuth2;
  private _testApi: Test;
  private _taxcardsApi: Taxcards;
  private _onboardingsApi: Onboardings;
  private _workersApi: Workers;

  constructor(ajaxImpl: Ajax) {
    this._ajax = ajaxImpl;
  }

  /** The platform (NG1, NG, jQuery etc.) dependent class that performs the actual Ajax calls */
  public get ajax(): Ajax {
    return this._ajax;
  }

  /** Gets the Accounts API: Access to current account, its products and workers */
  public get accountsApi() {
    if (!this._accountsApi) {
      this._accountsApi = new Accounts(this._ajax);
    }
    return this._accountsApi;
  }

  /** Gets the Calculations API: CRUD operations on calculations */
  public get calculationsApi() {
    if (!this._calculationsApi) {
      this._calculationsApi = new Calculations(this._ajax);
    }
    return this._calculationsApi;
  }

  /** Gets the Calculator API: Anonymous salary calculation methods */
  public get calculatorApi() {
    if (!this._calculatorApi) {
      this._calculatorApi = new Calculator(this._ajax);
    }
    return this._calculatorApi;
  }

  /** Gets the Reports API: Viewing and generating reports in different formats */
  public get reportsApi() {
    if (!this._reportsApi) {
      this._reportsApi = new Reports(this._ajax);
    }
    return this._reportsApi;
  }

  /** Gets the Files API: Files stored by the user, thumbnails of pictures etc. */
  public get filesApi() {
    if (!this._filesApi) {
      this._filesApi = new Files(this._ajax);
    }
    return this._filesApi;
  }

  /** Gets the Session API: Access to current session, user and roles */
  public get sessionApi() {
    if (!this._sessionApi) {
      this._sessionApi = new Session(this._ajax);
    }
    return this._sessionApi;
  }

  /** Gets the Content Management API: Articles about salary payment and employment in general. */
  public get cmsApi() {
    if (!this._cmsApi) {
      this._cmsApi = new Cms(this._ajax);
    }
    return this._cmsApi;
  }

  /** Gets the Profiles API: Worker profile (Osaajaprofiili) */
  public get profilesApi() {
    if (!this._profilesApi) {
      this._profilesApi = new Profiles(this._ajax);
    }
    return this._profilesApi;
  }

  /** Gets the Payments API for paying salaries using different methods */
  public get paymentsApi() {
    if (!this._paymentsApi) {
      this._paymentsApi = new Payments(this._ajax);
    }
    return this._paymentsApi;
  }

  /** Gets the OAuth2 API: Authentication methods */
  public get oAuth2Api() {
    if (!this._oAuth2Api) {
      this._oAuth2Api = new OAuth2(this._ajax);
    }
    return this._oAuth2Api;
  }

  /** Gets the Test API: Testing different functionalities of the API and system in general. */
  public get testApi() {
    if (!this._testApi) {
      this._testApi = new Test(this._ajax);
    }
    return this._testApi;
  }

  /** Gets the Tax cards API */
  public get taxcardsApi() {
    if (!this._taxcardsApi) {
      this._taxcardsApi = new Taxcards(this._ajax);
    }
    return this._taxcardsApi;
  }

  /** Gets the Workers API */
  public get workersApi() {
    if (!this._workersApi) {
      this._workersApi = new Workers(this._ajax);
    }
    return this._workersApi;
  }

  /** Gets the Onboarding API for creating a new user */
  public get onboardingsApi() {
    if (!this._onboardingsApi) {
      this._onboardingsApi = new Onboardings(this._ajax);
    }
    return this._onboardingsApi;
  }
}
