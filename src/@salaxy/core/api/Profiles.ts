import { ApiProfileSearchResult, City, Contact, Profile, ProfileJob, ProfileJobCategory } from "../model";
import { Ajax } from "./Ajax";

/**
 * Anonymous methods for accessing the Public Profiles (Osaajaprofiilit).
 * This is read-only access to third-party sites.
 * For modifying and non-public profiles, use MyProfiles autenticated service instead.
 */
export class Profiles {

  /**
   * For NG1-dependency injection
   * @hidden
   */
  public static $inject = ["AjaxNg1"];

  constructor(private ajax: Ajax) {}

  /**
   * Gets the Cities in Finland. Use the key-property when searching for Profiles
   *
   * @return A Promise with result data (list of cities).
   */
  public getCities(): Promise<City[]> {
    return this.ajax.getJSON("/pro/cities");
  }

  /**
   * Gets the Jobs that are defined for Profiles. Use the id-property when searching for Profiles
   * This method may later provide different Job categorizations for different purposes (e.g. different partners).
   *
   * @return A Promise with result data (list of jobs).
   */
  public getJobs(): Promise<ProfileJob[]> {
    return this.ajax.getJSON("/pro/jobs");
  }

  /**
   * Gets the Job Categories that are defined for Profiles. Use the id-property when searching for Profiles
   * This method may later provide different Job categorizations for different purposes (e.g. different partners).
   *
   * @return A Promise with result data (list of categories).
   */
  public getCategories(): Promise<ProfileJobCategory[]> {
    return this.ajax.getJSON("/pro/categories");
  }

  /**
   * Gets a single profile - including the details.
   * Note that this unauthenticated method does not show contact details.
   *
   * @param id - The identifier of the profile.
   *
   * @return A Promise with result data (Profile).
   */
  public getProfile(id: string): Promise<Profile> {
    return this.ajax.getJSON("/pro/profiles/" + id);
  }

  /**
   * Search method for the public profiles.
   *
   * @param city - Filter by city key
   * @param job - Filter by Job key
   * @param category - Filter by job category key
   * @param searchText - Filter by search text
   *
   * @return A Promise with result data (search results object).
   */
  public search(city: string, job: string, category: string, searchText: string): Promise<ApiProfileSearchResult> {
    const method = "/pro/profiles?city=" + (city || "") + "&job=" + (job || "") + "&category=" + (category || "") + "&searchText=" + (searchText || "");
    return this.ajax.getJSON(method);
  }

  /**
   * Gets the profile for the current authenticated user
   *
   * @return A Promise with result data (Profile).
   */
  public getMyProfile(): Promise<Profile> {
    return this.ajax.getJSON("/myprofiles/Default");
  }

  /**
   * Updates user's profile or creates a new one if none exists.
   *
   * @param profile - The profile information that should be updated
   *
   * @return A Promise with result data (Profile).
   */
  public saveMyProfile(profile: Profile): Promise<Profile> {
    return this.ajax.postJSON("/myprofiles", profile);
  }

  /**
   * Returns the url where to post the avatar image file
   */
  public getAvatarImageUploadUrl() {
    return this.ajax.getApiAddress() + "/myprofiles/avatarImage";
  }

  /**
   * Gets a contact for a single profile.
   *
   * @param id - The identifier of the profile.
   *
   * @return A Promise with result data (Contact).
   */
  public getContact(id: string): Promise<Contact> {
    const method = "/pro/profiles/{id}/contact".replace("{id}", id);
    return this.ajax.getJSON(method);
  }
}
