import { ApiTestErrorType, Onboarding, TestAccountInfo, TestValues } from "../model";
import { Ajax } from "./Ajax";

/**
 * Methods for testing the API.
 */
export class Test {

  /**
   * For NG1-dependency injection
   * @hidden
   */
  public static $inject = ["AjaxNg1"];

  constructor(private ajax: Ajax) {}

  /**
   * Basic hello world message. Echoes the given message back.
   *
   * @param message - Message.
   *
   * @return A Promise with result data (message).
   */
  public hello(message: string): Promise<string> {
    return this.ajax.getJSON("/test/hello?message=" + encodeURIComponent("" + message));
  }

  /**
   * Test a long lasting request.
   *
   * @param seconds - Delay in seconds.
   *
   * @return A Promise with result data (Delay in seconds).
   */
  public delay(seconds: number): Promise<number> {
    return this.ajax.getJSON("/test/delay?seconds=" + encodeURIComponent("" + seconds));
  }

  /**
   * Test JSON serialization. Echoes a given object either in success callback or in the promise.
   *
   * @param testValues - TestValues object.
   *
   * @return A Promise with result.
   */
  public values(testValues: TestValues): Promise<TestValues> {
    return this.ajax.postJSON("/test/values", testValues);
  }

  /**
   * Creates a test account and returns test account details.
   *
   * @param testValues - Onboarding object.
   *
   * @return A Promise with result data (test accounts).
   */
  public createTestAccount(onboarding: Onboarding): Promise<TestAccountInfo> {
    return this.ajax.postJSON("/test/accounts", onboarding);
  }

  /**
   * Deletes a test account and related objects
   *
   * @param accountId - Account id to delete.
   *
   * @return A Promise with result data "Object deleted".
   */
  public deleteTestAccount(accountId: string): Promise<string> {
    return this.ajax.remove("/test/accounts/" + encodeURIComponent("" + accountId));
  }

  /**
   * Test methods that throws an exception on server side.
   *
   * @param errorType Type of error
   * @param success Function that would theoretically be called if the call was not an error. Will never be called.
   *
   * @return A Promise if success parameter is null. For throwException,
   *        the promise is always rejected after the error is thrown in the server.
   */
  public throwException(errorType: ApiTestErrorType): Promise<string> {
    return this.ajax.getJSON("/test/exceptions/" + errorType);
  }

  /**
   * Delete the current account data and credentials (including Auth0 user).
   * Can be called only in a test environment.
   */
  public deleteCurrentAccount(): Promise<string> {
    return this.ajax.remove("/accounts/current");
  }

  /**
   * Remove all calculations, workers etc. user objects except products and signature from the account.
   * Can be called only in a test environment.
   */
  public deleteCurrentAccountData(): Promise<string> {
    return this.ajax.remove("/accounts/data/all");
  }

  /**
   * Remove all calculations, payrolls and payments from the account.
   * Can be called only in a test environment.
   */
  public deleteCurrentAccountCalculations(): Promise<string> {
    return this.ajax.remove("/accounts/calculation/all");
  }

  /**
   * Remove workers including calculations, employment contracts and tax cards from the account.
   * Can be called only in a test environment.
   */
  public deleteCurrentAccountWorkers(): Promise<string> {
    return this.ajax.remove("/accounts/worker/all");
  }

  /**
   * Remove all holiday year from all workers. Does not touch the default values of holidays in Worker Employment relation.
   * Can be called only in a test environment.
   */
  public deleteCurrentAccountHolidays(): Promise<string> {
    return this.ajax.remove("/accounts/holidays/all");
  }

  /**
   * Remove pension and insurance from the account.
   * Can be called only in a test environment.
   */
  public deleteCurrentAccountPensionAndInsurance(): Promise<string> {
    return this.ajax.remove("/accounts/product/pensionAndInsurance");
  }

  /**
   * Remove all products from the account.
   * Can be called only in a test environment.
   */
  public deleteCurrentAccountProducts(): Promise<string> {
    return this.ajax.remove("/accounts/product/all");
  }

  /**
   * Remove the signature from the account.
   * Can be called only in a test environment.
   */
  public deleteCurrentAccountSignature(): Promise<string> {
    return this.ajax.remove("/accounts/signature");
  }

  /**
   * Delete all empty accounts (company or worker) created by this account.
   * Can be called only in a test environment.
   */
  public deleteCurrentAccountAuthorizingAccounts(): Promise<string> {
    return this.ajax.remove("/accounts/authorizingAccount/all");
  }
}
