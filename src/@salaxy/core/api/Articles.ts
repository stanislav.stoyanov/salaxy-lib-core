import { Article } from "../model";
import { Ajax } from "./Ajax";
import { CrudApiBase } from "./CrudApiBase";

/**
 * Provides CRUD access for CMS Articles and other documentation
 */
export class Articles extends CrudApiBase<Article> {

  /**
   * For NG1-dependency injection
   * @hidden
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/cms/articles";

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
  public getBlank(): Article {
    return {
      //
    };
  }

  /**
   * Gets the article based on its ID.
   * TODO: getSingle or getArticle ?
   *
   * @param articleId Identifier for the article
   * @param articleLinkPattern Optional pattern for CMS links from one article to another.
   *        The default is "/Cms/Article/{0}" where parameter '{0}' is replaced with article id.
   *        The caller can change this to direct internal links to appropriate page template.
   *
   * @return A Promise with article data.
   */
  public getArticle(articleId: string, articleLinkPattern: string = null): Promise<Article> {
    let method = this.baseUrl + "/" + articleId;
    if (articleLinkPattern) {
      method += "?articleLinkPattern=" + encodeURIComponent(articleLinkPattern);
    }
    return this.ajax.getJSON(method);
  }

}
