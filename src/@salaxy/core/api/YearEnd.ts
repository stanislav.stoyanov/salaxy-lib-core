import { YearlyFeedback } from "../model";
import { Ajax } from "./Ajax";
import { CrudApiBase } from "./CrudApiBase";

/**
 * Methods for getting and saving yearly feedback data.
 */
export class YearEnd extends CrudApiBase<YearlyFeedback> {

    /**
     * For NG1-dependency injection
     * @hidden
     */
    public static $inject = ["AjaxNg1"];

    /** Base URL for details etc. */
    protected baseUrl = "/yearEnd/feedback";

    constructor(ajax: Ajax) {
        super(ajax);
     }

  /**
   * Client-side (synchronous) method for getting a new blank CRUD item as bases for UI binding.
   * The basic idea is that all the child object properties should be non-null to avoid null reference exceptions.
   * In special cases, when null is significant it may be used: e.g. Calculation.result is null untill the recalculation has been done.
   * Strings, dates and booleans may be null (false if bool), but typically just undefined.
   */
    public getBlank(): YearlyFeedback {
        throw new Error("Not implemented");
    }

    /**
     * Gets a single item based on identier
     * @param id - Unique identifier for the object
     * @return A Promise with result data.
     */
    public getSingle(id: string): Promise<YearlyFeedback> {
        return  super.getAll().then( (all: YearlyFeedback[]) => {
            if (all && all.length !== 0) {
                return all[0];
            }
            return null;
        });
    }

  /**
   * Deletes an single item from the sotrage
   * @param id - Unique identifier for the object
   * @return A Promise with result data "Object deleted".
   */
  public delete(id: string): Promise<string> {
    throw new Error("Not implemented");
  }
}
