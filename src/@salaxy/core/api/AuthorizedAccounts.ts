import { Avatar, CompanyAccount, PersonAccount } from "../model";
import { Ajax } from "./Ajax";
import { CrudApiBase } from "./CrudApiBase";

/**
 * Provides CRUD access for authenticated user to access a his/her own authorized accounts
 */
export class AuthorizedAccounts extends CrudApiBase<Avatar> {

  /**
   * For NG1-dependency injection
   * @hidden
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/accounts/authorizedAccount";

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /**
   * Returns an empty avatar without any values.
   */
  public getBlank(): Avatar {
    return {
    };
  }

  /**
   * Lists all accounts on behalf of which this account has been authorized to act.
   *
   * @return A Promise with result data (list of account objects).
   */
   // tslint:disable-next-line
   public getAuthorizingAccounts(): Promise<(PersonAccount|CompanyAccount)[]> {
    const method = "/accounts/authorizingAccount";
    return this.ajax.getJSON(method);
  }
}
