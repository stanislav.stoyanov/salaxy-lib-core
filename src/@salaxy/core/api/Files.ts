import { BlobFile, BlobFileType } from "../model";
import { ODataQueryOptions, ODataResult } from "../util";
import { Ajax } from "./Ajax";
import { CrudApiBase } from "./CrudApiBase";

/**
 * Provides access to user files (of the authenticated user).
 */
export class Files extends CrudApiBase<BlobFile> {

  /**
   * For NG1-dependency injection
   * @hidden
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v03-rc/api/files";

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /**
   * Gets a list of files - filtered by a type
   *
   * @param type: Logical file type.
   *
   * @return A Promise with result data (list of files).
   */
  public filesOfType(type: BlobFileType): Promise<BlobFile[]> {
    return this.ajax.getJSON(`${this.baseUrl}/type/${type}`);
  }

  /**
   * Returns the content url for the file id.
   * The returned does not contain access token.
   * @param fileUri - Identifier of the file.
   */
  public getContentUrl(id: string): string {
    return `${this.ajax.getApiAddress()}/${this.baseUrl}/${id}/stream`;
  }

  /**
   * Returns the preview image url for the file id.
   * The returned url does not contain access token.
   * @param id - Identifier of the file.
   */
  public getPreviewUrl(id: string): string {
    return `${this.ajax.getApiAddress()}/${this.baseUrl}/${id}/preview`;
  }

  /**
   * Downloads the content as bytes.
   * @param id - The identifier of the file.
   */
  public download(id: string): Promise<any> {
    return this.ajax.getJSON(`${this.baseUrl}/${id}/content`);
  }

  /**
   * Client-side (synchronous) method for getting a new blank CRUD item as bases for UI binding.
   * The basic idea is that all the child object properties should be non-null to avoid null reference exceptions.
   * In special cases, when null is significant it may be used: e.g. Calculation.result is null untill the recalculation has been done.
   * Strings, dates and booleans may be null (false if bool), but typically just undefined.
   */
  public getBlank(): BlobFile {
    throw new Error("Not supported.");
  }

  /**
   * Gets all the items of a given type.
   * @return A Promise with result data array.
   */
  public getAll(): Promise<BlobFile[]> {
    throw new Error("Not supported.");
  }

  /**
   * Deletes an single item from the sotrage
   * @param id - Unique identifier for the object
   * @return A Promise with result data "Object deleted".
   */
  public delete(id: string): Promise<string> {
    throw new Error("Not supported.");
  }

  /**
   * Saves an item to the storage.
   * If id is null, this is add/insert. If id exists, this is update.
   * @param itemToSave - The item that is updated/inserted.
   * @return A Promise with result data as saved to the storage (contains id, createAt, owner etc.).
   */
  public save(itemToSave): Promise<BlobFile> {
    throw new Error("Not supported.");
  }
}
