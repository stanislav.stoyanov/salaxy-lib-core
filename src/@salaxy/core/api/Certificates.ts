import { Certificate } from "../model";
import { Ajax } from "./Ajax";
import { CrudApiBase } from "./CrudApiBase";

/**
 * Provides CRUD access for authenticated user to access a his/her own Certificates
 */
export class Certificates extends CrudApiBase<Certificate> {

  /**
   * For NG1-dependency injection
   * @hidden
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/accounts/certificate";

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /**
   * Returns an empty certificate without any values.
   */
  public getBlank(): Certificate {
    return {};
  }

}
