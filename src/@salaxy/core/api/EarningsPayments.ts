import { nir } from "../model";
import { Ajax } from "./Ajax";
import { CrudApiBase } from "./CrudApiBase";

/**
 * Provides CRUD access for authenticated user to access a his/her own Earnings Payments objects
 */
export class EarningsPayments extends CrudApiBase<nir.EarningsPayment> {

  /**
   * For NG1-dependency injection
   * @hidden
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = null;

  constructor(ajax: Ajax) {
    super(ajax);
    this.baseUrl = `${ajax.getServerAddress()}/nir/earningsPayment`;
  }

  /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
  public getBlank(): nir.EarningsPayment {
    return {
      deliveryData : {},
      info: {},
      validation: {},
    };
  }

  /**
   * Validates an Earnings Payment object for delivery.
   * @param earningsPaymentToValidate Earnings payment that should be validated.
   * @returns The Earnings Payment object with validation.
   */
  public validate(earningsPaymentToValidate: nir.EarningsPayment): Promise<nir.EarningsPayment> {
    return this.ajax.postJSON(`${this.baseUrl}/validate`, earningsPaymentToValidate);
  }
}
