import { ESalaryPayment } from "../model";
import { Ajax } from "./Ajax";
import { CrudApiBase } from "./CrudApiBase";

/**
 * Provides CRUD access for authenticated user to access a his/her own E-salary payments
 */
export class ESalaryPayments extends CrudApiBase<ESalaryPayment> {

  /**
   * For NG1-dependency injection
   * @hidden
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/esalary";

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
  public getBlank(): ESalaryPayment {
    return {
      calc: {},
      household: {},
      request: {},
    };
  }

}
