import { CalculationSettings } from "../../model";
import { Ajax } from "../Ajax";
import { ConfigurationApiBase } from "./ConfigurationApiBase";

/**
 * Provides CRUD access for calculations settings
 */
export class CalculationsConfiguration extends ConfigurationApiBase<CalculationSettings> {

  /**
   * For NG1-dependency injection
   * @hidden
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v03-rc/api/settings/calculations";

  constructor(ajax: Ajax) {
    super(ajax);
  }
}
