import { AccountingSettings } from "../../model";
import { Ajax } from "../Ajax";
import { ConfigurationApiBase } from "./ConfigurationApiBase";

/**
 * Provides CRUD access for accounting settings
 */
export class AccountingConfiguration extends ConfigurationApiBase<AccountingSettings> {

  /**
   * For NG1-dependency injection
   * @hidden
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v03-rc/api/settings/accounting";

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /**
   * Gets a single accountings settings object with targets only.
   * @return A Promise with settings object.
   */
  public getTargets(): Promise<AccountingSettings> {
    return this.ajax.getJSON(`${this.baseUrl}/targets`);
  }

}
