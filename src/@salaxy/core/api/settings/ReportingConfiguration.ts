import { ReportingSettings } from "../../model";
import { Ajax } from "../Ajax";
import { ConfigurationApiBase } from "./ConfigurationApiBase";

/**
 * Provides CRUD access for reporting settings
 */
export class ReportingConfiguration extends ConfigurationApiBase<ReportingSettings> {

  /**
   * For NG1-dependency injection
   * @hidden
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v03-rc/api/settings/reporting";

  constructor(ajax: Ajax) {
    super(ajax);
  }
}
