For adding new settings objects in the API, follow the guideline:

- name the API as ```{object name}Configuration```
  - for example: ``` AccountingConfiguration```
- implement the required backend REST interface using path: 
   ```/v03-rc/api/settings/{object name}```
   - for example ```/v03-rc/api/settings/accounting```
- and use ```ConfigurationApiBase``` as a base for the api service class.
