import { PaymentSettings } from "../../model";
import { Ajax } from "../Ajax";
import { ConfigurationApiBase } from "./ConfigurationApiBase";

/**
 * Provides CRUD access for payments settings
 */
export class PaymentsConfiguration extends ConfigurationApiBase<PaymentSettings> {

  /**
   * For NG1-dependency injection
   * @hidden
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v03-rc/api/settings/payments";

  constructor(ajax: Ajax) {
    super(ajax);
  }
}
