import { AccountInIndex, PrimaryPartner } from "../../model";
import { Ajax } from "../Ajax";

/**
 * Provides CRUD access for accounting settings
 */
export class PartnersConfiguration {

  /**
   * For NG1-dependency injection
   * @hidden
   */
  public static $inject = ["AjaxNg1"];

  constructor(private ajax: Ajax) {
  }

  /**
   * Returns a single partner.
   * @param partnerId Id for the partner.
   */
  public get(partnerId: string = null ): Promise<PrimaryPartner | PrimaryPartner[]> {
    if (partnerId == null) {
      // This is obsolete, do not use, use odata instead (/v03-rc/api/settings/primary-partners)
      return this.ajax.getJSON("/v03-rc/api/settings/partners");
    }
    return this.ajax.getJSON(`/v03-rc/api/settings/primary-partners/${partnerId}`);
  }

 /**
  * Makes a search to YTJ.
  * @param search Search text: company name or business id.
  */
  public searchYtj(search: string): Promise<AccountInIndex[]> {
    return this.ajax.getJSON(`/v03-rc/api/settings/ytj-companies?search=${encodeURIComponent(search)}`);
  }

}
