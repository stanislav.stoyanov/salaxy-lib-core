import { WorkerAbsences } from "../model";
import { Ajax } from "./Ajax";
import { CrudApiBase } from "./CrudApiBase";

/**
 * Provides CRUD access for Absences of a Worker
 */
export class Absences extends CrudApiBase<WorkerAbsences> {

  /**
   * For NG1-dependency injection
   * @hidden
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v03-rc/api/absences";

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /** Client-side (synchronous) method for getting a new blank item as bases for UI binding. */
  public getBlank(workerId?: string): WorkerAbsences {
    return {
      employmentId: null,
      workerId,
      periods: [],
    };
  }

  /**
   * Gets the Absences for a specific employment relation.
   * @param employmentId Identifier for the Employment relation.
   * @return A Promise with Absences.
   */
  public getForEmployment(employmentId: string): Promise<WorkerAbsences> {
    return this.ajax.getJSON(`${this.baseUrl}/employment/${employmentId}`);
  }

  /**
   * Gets the Absences for the given employment relations.
   * @param employmentIds Identifiers for the Employment relations.
   * @return A Promise with result data array.
   */
  public getForEmployments(employmentIds: string[]): Promise<WorkerAbsences[]> {
    if (employmentIds == null || employmentIds.length === 0) {
      return Promise.resolve([]);
    }
    let method = `${this.baseUrl}/employment/set?`;
    employmentIds.forEach((x) => { method += "employmentIds=" + encodeURIComponent("" + x) + "&"; });
    method = method.replace(/[?&]$/, "");
    return this.ajax.getJSON(method);
  }
}
