import {
  AccountProducts,
  Avatar,
  CompanyAccount,
  MessageType,
  PersonAccount,
} from "../model";
import { Ajax } from "./Ajax";

/**
 * Methods for updating account information
 */
export class Accounts {

  /**
   * For NG1-dependency injection
   * @hidden
   */
  public static $inject = ["AjaxNg1"];

  constructor(private ajax: Ajax) {}

  /**
   * Gets the current company account if the current account is a company.
   * NOTE: Typically the account data is fetched in Session check => You do not need to fetch it separately.
   *
   * @return A Promise with result data (Company account or null).
   */
  public getCompany(): Promise<CompanyAccount> {
    return this.ajax.getJSON("/accounts/company");
  }

  /**
   * Gets the current Person account:
   * Either the current account or the Primary contact person account if the current account is a company.
   * NOTE: Typically the current account data is fetched in Session check => You do not need to fetch it separately.
   * => Only use this to fetch the Primary contact person account if the current account is a company.
   *
   * @return A Promise with result data (Personal account).
   */
  public getPerson(): Promise<PersonAccount> {
    return this.ajax.getJSON("/accounts/person");
  }

  /**
   * Saves the avatar and contact for the current Person account:
   * Either the current account or the Primary contact person account if the current account is a company.
   *
   * @return A Promise with result data (Personal account).
   */
  public savePerson(personAccount: PersonAccount): Promise<PersonAccount> {
    return this.ajax.postJSON("/accounts/person", personAccount);
  }

  /**
   * Returns the url where to post the avatar file
   */
  public getAvatarUploadUrl() {
    return `${this.ajax.getApiAddress()}/accounts/avatar`;
  }

  /**
   * Gets the current set of products for this account
   *
   * @return A Promise with result data (AccountProducts).
   */
  public getProducts(): Promise<AccountProducts> {
    return this.ajax.getJSON("/accounts/products/available");
  }

  /**
   * Updates the current set of products for this account
   *
   * @param products - The products object with modified products. You may leave unmodified products as null - they will not be set as null.
   *
   * @return A Promise with result data (updated products).
   */
  public updateProducts(products: AccountProducts): Promise<AccountProducts> {
    return this.ajax.postJSON("/accounts/products", products);
  }

  /**
   * Switches the current web site usage role.
   *
   * @param role - household or worker. You cannot change to company at the moment using this method.
   *
   * @return A Promise with result data (new role).
   */
  public switchRole(role: "worker" | "household"): Promise<"worker"|"household"> {
    return this.ajax.postJSON("/accounts/switchRole?role=" + role, {});
  }

  /**
   * Sends a verification.
   *
   * @param type - Verification type.
   * @param address - Verification address.
   *
   * @return A Promise with result data (pin code in test environments).
   */
  public sendVerification(type: MessageType, address: string): Promise<string> {
    return this.ajax.postJSON("/accounts/verification/" + type, JSON.stringify(address));
  }

  /**
   * Confirms a verification.
   *
   * @param type - Verification type.
   * @param pin - Verification pin code.
   *
   * @return A Promise with result data (true/false).
   */
  public confirmVerification(type: MessageType, pin: string): Promise<boolean> {
    return this.ajax.postJSON("/accounts/verification/" + type + "/confirm", JSON.stringify(pin));
  }
}
