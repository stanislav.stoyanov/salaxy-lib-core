import { Calculation, Invoice, InvoiceStatus } from "../model";
import { DatelyObject, Dates } from "../util";
import { Ajax } from "./Ajax";
import { CrudApiBase } from "./CrudApiBase";

/**
 * Provides read access for authenticated user to access his/her own invoices.
 */
export class Invoices extends CrudApiBase<Invoice> {

  /**
   * For NG1-dependency injection
   * @hidden
   */
  public static $inject = ["AjaxNg1"];

  /** Base URL for details etc. */
  protected baseUrl = "/v03-rc/api/invoices";

  constructor(ajax: Ajax) {
    super(ajax);
  }

  /**
   * Client-side (synchronous) method for getting a new blank item as bases for UI binding.
   * NOTE: At the moment, this method is pretty useless as you cannot save invoice items:
   * You need to create them using createInvoice() method based on a calculation.
   */
  public getBlank(): Invoice {
    return {
      header: {},
      rows: [],
      payer: {},
      recipient: {},
    };
  }

  /**
   * Creates invoices to pay a salary calculation.
   * @param calc Calculation for which the invoices are created.
   * @param channel Payment channel to use.
   * @returns Invoices which have been created in the Invoices queue.
   */
  public createInvoices(calc: Calculation, channel: string): Promise<Invoice[]> {
    if (!channel) {
      throw new Error("Payment channel must be specified.");
    }
    const url = `${this.baseUrl}/create/${channel}`;
    return this.ajax.postJSON(url, calc);
  }

  /**
   * Creates invoices to pay a salary calculation(s).
   * @param channel Payment channel to use.
   * @param calcIds One or several identifiers of the Calculation for which the invoices are created.
   * @returns Invoices which have been created in the Invoices queue.
   */
  public createInvoicesById(channel: string, ... calcIds: string[]): Promise<Invoice[]> {
    if (!channel || !calcIds || calcIds.length < 1) {
      throw new Error("One or more calculations Ids and Payment channel must be specified.");
    }
    const url = `${this.baseUrl}/create-by-id/${channel}`;
    return this.ajax.postJSON(url, calcIds);
  }

  /**
   *  Creates invoices to pay a set of salary calculation.
   * @param channel Payment channel to use.
   * @param payrollId Identifier for the Payroll for which the invoices are created.
   * @returns Invoices which have been created in the Invoices queue.
   */
  public createInvoicesForPayroll(channel: string, payrollId: string): Promise<Invoice[]> {
    if (!channel || !payrollId) {
      throw new Error("The payroll ID and Payment channel must be specified.");
    }
    const url = `${this.baseUrl}/create-for-payroll/${channel}/${payrollId}`;
    return this.ajax.postJSON(url, null);
  }

  /**
   * Preview invoices to pay a salary calculation(s).
   * @param channel Payment channel to use.
   * @param calcIds One or several identifiers of the Calculation for which to create preview invoices.
   * @returns Invoices for preview purposes: Not saved and not added to invoices queue.
   */
  public previewInvoicesById(channel: string, ... calcIds: string[]): Promise<Invoice[]> {
    if (!channel || !calcIds || calcIds.length < 1) {
      throw new Error("One or more calculations Ids and Payment channel must be specified.");
    }
    const url = `${this.baseUrl}/preview-by-id/${channel}`;
    return this.ajax.postJSON(url, calcIds);
  }

  /**
   * Preview invoices to pay a set of salary calculation.
   * @param channel Payment channel to use.
   * @param payrollId Identifier for the Payroll for which to create preview invoices.
   * @returns Invoices for preview purposes: Not saved and not added to invoices queue.
   */
  public previewInvoicesForPayroll(channel: string, payrollId: string): Promise<Invoice[]> {
    if (!channel || !payrollId) {
      throw new Error("The payroll ID and Payment channel must be specified.");
    }
    const url = `${this.baseUrl}/preview-for-payroll/${channel}/${payrollId}`;
    return this.ajax.postJSON(url, null);
  }

  /**
   * Makes a request for updating the status of the invoice.
   * @param id Identifier for the invoice.
   * @param status The requested new status of the invoice.
   */
  public requestStatusUpdate(id: string, status: InvoiceStatus): Promise<Invoice> {
    const url = `${this.baseUrl}/${id}/status/${status}`;
    return this.ajax.postJSON(url, null);
  }

  /**
   * Returns content (PDF) url for an invoice.
   * @param id The id of the invoice.
   * @param inline If true, returns the content as inline (disposition) in browser.
   * If false, the content is returned as an attachment (download).
   */
  public getContentUrl(id: string, inline: boolean = false): string {
    return this.ajax.getServerAddress()
      + `${this.baseUrl}/${id}/content?inline=${inline}`
      + `&access_token=${encodeURIComponent(this.ajax.getCurrentToken())}`;
  }

}
