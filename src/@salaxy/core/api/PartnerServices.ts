import { VarmaPensionOrder } from "../model";
import { Ajax } from "./Ajax";

/**
 * API for partner services, like pension, insurance and health care.
 */
export class PartnerServices {

  /**
   * For NG1-dependency injection
   * @hidden
   */
  public static $inject = ["AjaxNg1"];

  constructor(private ajax: Ajax) {}

  /**
   * Returns a new Varma pension order with default values for the current account.
   */
  public getNewVarmaPensionOrder(): Promise<VarmaPensionOrder> {
    return this.ajax.getJSON("/partner-services/varma/pension/new");
  }

  /**
   * Validate the given Varma pension order in the server.
   */
  public validateVarmaPensionOrder(order: VarmaPensionOrder): Promise<VarmaPensionOrder> {
    return this.ajax.postJSON("/partner-services/varma/pension/validate", order);
  }

  /**
   * Send a new Varma pension order for later processing.
   */
  public sendVarmaPensionOrder(order: VarmaPensionOrder): Promise<VarmaPensionOrder> {
    return this.ajax.postJSON("/partner-services/varma/pension/order", order);
  }
}
