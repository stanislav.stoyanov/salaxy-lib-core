import {
  CalcGroup,
  Calculation,
  CalculationRowType,
  Dates,
  FrameworkAgreement,
  LegacyTaxcardType,
  nir,
  Numeric,
  Objects,
  PaymentChannel,
  PensionCalculation,
  ReportType,
  TaxcardKind,
  UnionPaymentKind,
  UnionPaymentType,
  Unit,
  Years,
} from "@salaxy/core";

import { CalculationReport } from "./CalculationReport";
import { CalculationReportImpl } from "./CalculationReportImpl";
import { CalculationReportRow } from "./CalculationReportRow";
import { CalculationReportRowGroup} from "./CalculationReportRowGroup";
import { CalculationTableRowData } from "./CalculationTableRowData";
import { PaymentTableRowData} from "./PaymentTableRowData";

/**
 * Maps Calculation-object to report template ready objects.
 */
export class CalculationMapper {

  /**
   * Creates a new CalculationReport from the fiven Calculation.
   * @param calc Calcuation for the report.
   * @param reportType Report type.
   */
  public static getCalculationReport(calc: Calculation, reportType: ReportType): CalculationReport {
    if (calc == null) {
      return;
    }
    // do not mutate...
    const calculation = Objects.copy(calc);
    const report = new CalculationReportImpl(reportType);

    this.createHeader(report, calculation);

    switch (reportType) {
      case ReportType.SalarySlip:
        this.createValidationMessages(report, calculation);
        this.createSalarySlipRowGroups(report, calculation);
        this.createSalarySlipNotes(report, calculation);
        break;
      case ReportType.EmployerReport:
        this.createValidationMessages(report, calculation);
        this.createEmployerReportRowGroups(report, calculation);
        this.createEmployerReportNotes(report, calculation);
        break;
      case ReportType.PaymentReport:
        this.createPaymentReportRowGroups(report, calculation);
        break;
    }

    return report;
  }

  private static createSalarySlipRowGroups(report: CalculationReport, calc: Calculation) {

    // Ordered set of groups
    const groups: CalcGroup[] = [
      CalcGroup.BaseSalary,
      CalcGroup.SalaryAdditions,
      CalcGroup.OtherNoPayment,
      CalcGroup.Benefits,
      CalcGroup.Deductions,
      CalcGroup.Expenses,
      CalcGroup.Totals,
      CalcGroup.Disabled,
    ];

    const rowGroups = groups.map((group) => {
      const irRows = calc.result.irRows.filter((r) => r.calcData.grouping === group);
      return new CalculationReportRowGroup<CalculationReportRow<CalculationTableRowData>>(
        {
          grouping: group,
          rows: irRows
            .map((i) => new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData({ irRow: i }))),
        });
    });
    report.rowGroups.push(...rowGroups);

    // Deductions
    const deductionGroup = rowGroups.find((g) => g.grouping === CalcGroup.Deductions);

    // HACK: Move prepaid expenses to expenses group
    const expensesGroup = rowGroups.find((g) => g.grouping === CalcGroup.Expenses);
    expensesGroup.rows.push(...deductionGroup.rows.filter((x) =>
      this.getEnum<CalculationRowType>(x.data.irRow.data.calculationRowType) ===
      CalculationRowType.PrepaidExpenses));

    deductionGroup.rows = deductionGroup.rows.filter((x) =>
    this.getEnum<CalculationRowType>(x.data.irRow.data.calculationRowType) !==
      CalculationRowType.PrepaidExpenses);

    for (const row of deductionGroup.rows) {

      const calculationRowType = this.getEnum<CalculationRowType>(row.data.irRow.data.calculationRowType);
      switch (calculationRowType) {
        case CalculationRowType.Advance:
          if (calc.result.workerCalc.fullSalaryAdvance > calc.result.workerCalc.salaryAdvance) {
            row.data.fullTotal = row.data.irRow.total;
            row.data.irRow.total = 0;
            row.data.isTotalReduced = true;
          }

          break;
        case CalculationRowType.OtherDeductions:
          if (calc.result.workerCalc.fullOtherDeductions > calc.result.workerCalc.otherDeductions) {
            row.data.fullTotal = row.data.irRow.total;
            row.data.irRow.total = 0;
            row.data.isTotalReduced = true;
          }
          break;
        case CalculationRowType.UnionPayment:

          row.data.irRow.count = row.data.irRow.total / row.data.irRow.price;

          if (calc.result.workerCalc.fullUnionPayment > calc.result.workerCalc.unionPayment) {
            row.data.fullTotal = row.data.irRow.total;
            row.data.irRow.total = 0;
            row.data.isTotalReduced = true;
          }
          break;
      }
    }

    // Set prepaid expenses

    for (const row of expensesGroup.rows) {
      const calculationRowType = this.getEnum<CalculationRowType>(row.data.irRow.data.calculationRowType);
      switch (calculationRowType) {
        case CalculationRowType.PrepaidExpenses:
          row.data.irRow.price = -1 * row.data.irRow.price;
          row.data.irRow.total = -1 * row.data.irRow.total;
          if (calc.result.workerCalc.fullPrepaidExpenses > calc.result.workerCalc.prepaidExpenses) {
            row.data.fullTotal = row.data.irRow.total;
            row.data.irRow.total = 0;
            row.data.isTotalReduced = true;
          }
          break;
      }
    }

    // First deduct taxes
    deductionGroup.rows.unshift(
      // Tax
      new CalculationReportRow<CalculationTableRowData>(
        new CalculationTableRowData(
          {
            irRow:
            {
              message: "SALAXY.REPORTS.salary.heading.withholdingTax",
              count: calc.result.totals.totalTaxable !== 0
                ? calc.result.workerCalc.tax / calc.result.totals.totalTaxable
                : 0,
              price: calc.result.totals.totalTaxable,
              total: calc.result.workerCalc.tax,
              unit: Unit.Percent,
            },
            fullTotal: calc.result.workerCalc.fullTax,
            isTotalReduced: calc.result.workerCalc.fullTax > calc.result.workerCalc.tax,
          },
        )));

    deductionGroup.rows.push(
      // Pension
      new CalculationReportRow<CalculationTableRowData>(
        new CalculationTableRowData(
          {
            irRow:
            {
              message: "SALAXY.REPORTS.salary.heading.pensionPayment", //
              count: calc.result.totals.totalSocialSecurityBase !== 0
                ? calc.result.workerCalc.pension /
                calc.result.totals.totalSocialSecurityBase
                : 0,
              price: calc.result.totals.totalSocialSecurityBase,
              total: calc.result.workerCalc.pension,
              unit: Unit.Percent,
            },
            comment: (calc.result.workerCalc.fullPension > 0
              ? this.getPensionCompanyName(calc)
              : ""),
            fullTotal: calc.result.workerCalc.fullPension,
            isTotalReduced: calc.result.workerCalc.fullPension >
              calc.result.workerCalc.pension,
          },
        )));
    deductionGroup.rows.push(
      // Unemployment
      new CalculationReportRow<CalculationTableRowData>(
        new CalculationTableRowData(
          {
            irRow:
            {
              message: "SALAXY.REPORTS.salary.heading.unemploymentInsurancePayment",
              count: calc.result.totals.totalSocialSecurityBase !== 0
                ? calc.result.workerCalc.unemploymentInsurance /
                calc.result.totals.totalSocialSecurityBase
                : 0,
              price: calc.result.totals.totalSocialSecurityBase,
              total: calc.result.workerCalc.unemploymentInsurance,
              unit: Unit.Percent,
            },
            fullTotal: calc.result.workerCalc.fullUnemploymentInsurance,
            isTotalReduced: calc.result.workerCalc.fullUnemploymentInsurance >
              calc.result.workerCalc.unemploymentInsurance,
          },
        )));

    // Totals
    const totalsGroup = rowGroups.find((g) => g.grouping === CalcGroup.Totals);
    totalsGroup.rows.push(
      // Gross salary
      new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
        {
          irRow:
          {
            message: "SALAXY.REPORTS.ENUM.CalcGroup.baseSalary.label",
            total: calc.result.totals.totalGrossSalary,
          },
        })));
    totalsGroup.rows.push(
      // Deductions
      new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
        {
          irRow:
          {
            message: "SALAXY.REPORTS.ENUM.CalcGroup.deductions.label",
            total: -1 * calc.result.workerCalc.deductions,
          },
        })));
    totalsGroup.rows.push(
      // Expenses
      new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
        {
          irRow:
          {
            message: "SALAXY.REPORTS.ENUM.CalcGroup.expenses.label",
            total: calc.result.totals.totalExpenses - calc.result.workerCalc.prepaidExpenses,
          },
        })));
  }

  private static createEmployerReportRowGroups(report: CalculationReport, calc: Calculation) {

    const paymentChannel: PaymentChannel = ((calc.info as any).paymentChannel || PaymentChannel.Undefined);
    const hasPaymentChannel = !(paymentChannel === PaymentChannel.Undefined || paymentChannel === PaymentChannel.PalkkausCfaReference || paymentChannel === PaymentChannel.PalkkausCfaFinvoice || paymentChannel === PaymentChannel.PalkkausCfaPaytrail );

    // Ordered set of groups
    const groups: CalcGroup[] = [

      CalcGroup.BaseSalary,
      CalcGroup.SalaryAdditions,
      CalcGroup.OtherNoPayment,
      CalcGroup.Expenses,
    ];

    const rowGroups = groups.map((group) => {
      const irRows = calc.result.irRows.filter((r) => r.calcData.grouping === group);
      return new CalculationReportRowGroup<CalculationReportRow<CalculationTableRowData>>(
        {
          grouping: group,
          rows: irRows.map((i) => new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData({ irRow: i }))),
        });
    });

    // HACK: Move prepaid expenses to expenses group
    if (calc.result.workerCalc.prepaidExpenses !== 0) {
      const expensesGroup = rowGroups.find((g) => g.grouping === CalcGroup.Expenses);
      expensesGroup.rows.push(...calc.result.irRows.filter((r) =>
      this.getEnum<CalculationRowType>(r.data.calculationRowType) ===
        CalculationRowType.PrepaidExpenses).map((x) => {
          const row = new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData({ irRow: x }));
          row.data.irRow.price = -1 * row.data.irRow.price;
          row.data.irRow.total = -1 * row.data.irRow.total;
          return row;
        }));
    }

    report.rowGroups.push(...rowGroups);

    const mandatorySideCostsGroup = new CalculationReportRowGroup<CalculationReportRow<CalculationTableRowData>>(
      {
        grouping: CalcGroup.Undefined,
        groupingTerm: "SALAXY.REPORTS.salary.heading.mandatorySideCosts",
      });

    const totalSalaryCostGroup = new CalculationReportRowGroup<CalculationReportRow<CalculationTableRowData>>(
      {
        grouping: CalcGroup.Undefined,
        groupingTerm: "SALAXY.REPORTS.salary.heading.salaryCost",
      });

    const finalCostGroup = new CalculationReportRowGroup<CalculationReportRow<CalculationTableRowData>>(
      {
        grouping: CalcGroup.Undefined,
        groupingTerm: "SALAXY.REPORTS.salary.heading.totalCost",
      });

    const finalCostGroupAfterTax = new CalculationReportRowGroup<CalculationReportRow<CalculationTableRowData>>(
      {
        grouping: CalcGroup.Undefined,
        groupingTerm: "SALAXY.REPORTS.salary.heading.totalCostAfterTax",
      });

    const totalsGroup = new CalculationReportRowGroup<CalculationReportRow<CalculationTableRowData>>(
      {
        grouping: CalcGroup.Totals,
        isHidden: hasPaymentChannel,
      });

    mandatorySideCostsGroup.rows.push(
      new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
        {
          irRow:
          {
            message: "SALAXY.REPORTS.salary.heading.pensionPayment",
            total: calc.result.employerCalc.pension,
            count: calc.result.totals.totalSocialSecurityBase !== 0
              ? calc.result.employerCalc.pension /
              calc.result.totals.totalSocialSecurityBase
              : 0,
            price: calc.result.totals.totalSocialSecurityBase,
            unit: Unit.Percent,
          },
          comment: this.getPensionCompanyName(calc),
        })));

    mandatorySideCostsGroup.rows.push(
      new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
        {
          irRow:
          {
            message: "SALAXY.REPORTS.salary.heading.socialInsurancePayment",
            total: calc.result.employerCalc.socialSecurity,
            count: calc.result.totals.totalSocialSecurityBase !== 0
              ? calc.result.employerCalc.socialSecurity /
              calc.result.totals.totalSocialSecurityBase
              : 0,
            price: calc.result.totals.totalSocialSecurityBase,
            unit: Unit.Percent,
          },
        })));

    mandatorySideCostsGroup.rows.push(
      new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
        {
          irRow:
          {
            message: "SALAXY.REPORTS.salary.heading.unemploymentInsurancePayment",
            total: calc.result.employerCalc.unemployment,
            count: calc.result.totals.totalSocialSecurityBase !== 0
              ? calc.result.employerCalc.unemployment /
              calc.result.totals.totalSocialSecurityBase
              : 0,
            price: calc.result.totals.totalSocialSecurityBase,
            unit: Unit.Percent,
          },
        })));

    totalSalaryCostGroup.rows.push(
      new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
        {
          irRow:
          {
            message: "SALAXY.REPORTS.salary.heading.grossSalaryLikeBaseSalaryAbove",
            total: calc.result.totals.totalGrossSalary,
            count: 0,
            unit: Unit.Percent,
          },
        })));

    const mandatorySideCosts = new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
      {
        irRow:
        {
          message: "SALAXY.REPORTS.salary.heading.mandatorySideCosts",
          total: mandatorySideCostsGroup.total,
          count: 0,
          unit: Unit.Percent,
        },
      }));

    totalSalaryCostGroup.rows.push(mandatorySideCosts);

    totalSalaryCostGroup.rows.push(new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
      {
        irRow:
        {
          message: "SALAXY.REPORTS.salary.heading.palkkausFee",
          total: calc.result.employerCalc.palkkaus,
          price: calc.result.totals.totalGrossSalary,
          count: calc.result.totals.totalGrossSalary !== 0
            ? (calc.result.employerCalc.palkkaus / 1.24) /
            calc.result.totals.totalGrossSalary
            : 0,
          unit: Unit.Percent,
        },
        comment:
          `(ALV 24%, ${ Numeric.formatPrice(calc.result.employerCalc.palkkaus * (0.24 / 1.24))})`,
      })));

    const totalSalaryCost = new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
      {
        irRow:
        {
          message: "SALAXY.REPORTS.salary.heading.salaryCost",
          total: totalSalaryCostGroup.total,
          count: 0,
          unit: Unit.Percent,
        },
      }));
    finalCostGroup.rows.push(totalSalaryCost);

    if (calc.result.totals.totalExpenses > 0) {
      finalCostGroup.rows.push(new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
        {
          irRow:
          {
            message: "SALAXY.REPORTS.ENUM.CalcGroup.expenses.label",
            total: calc.result.totals.totalExpenses - calc.result.workerCalc.prepaidExpenses,
            count: 0,
            unit: Unit.Percent,
          },
        })));
    }

    const finalCost = new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
      {
        irRow:
        {
          message: finalCostGroup.rows.length > 1 ? "SALAXY.REPORTS.salary.heading.totalCost" : "SALAXY.REPORTS.salary.heading.salaryCost",
          total: finalCostGroup.total,
          count: 0,
          unit: Unit.Percent,
        },
      }));
    totalsGroup.rows.push(finalCost);

    if (calc.salary.isHouseholdDeductible === true && calc.result.employerCalc.householdDeduction > 0) {

      finalCostGroupAfterTax.rows.push(finalCost);

      finalCostGroupAfterTax.rows.push(
        new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
          {
            irRow:
            {
              message: "SALAXY.REPORTS.salary.heading.householdDeduction",
              total: -1 * calc.result.employerCalc.householdDeduction,
              count: 0,
              unit: Unit.Percent,
            },
          },
        )));
    }

    if (calc.result.employerCalc.deductionSalaryAdvance > 0) {
      totalsGroup.rows.push(
        new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
          {
            irRow:
            {
              message: "SALAXY.REPORTS.salary.heading.employerPaidSalaryAdvance",
              total: -1 * calc.result.employerCalc.deductionSalaryAdvance,
              count: 0,
              unit: Unit.Percent,
            },
          })));
    }

    if (calc.result.employerCalc.deductionOtherDeductions > 0) {
      totalsGroup.rows.push(
        new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
          {
            irRow:
            {
              message: "SALAXY.REPORTS.salary.heading.employerPaidOtherDeduction",
              total: -1 * calc.result.employerCalc.deductionOtherDeductions,
              count: 0,
              unit: Unit.Percent,
            },
          })));
    }

    if (calc.result.employerCalc.deductionForeclosure > 0) {
      totalsGroup.rows.push(
        new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
          {
            irRow:
            {
              message: "SALAXY.REPORTS.salary.heading.employerPaidForeclosure",
              total: -1 * calc.result.employerCalc.deductionForeclosure,
              count: 0,
              unit: Unit.Percent,
            },
          })));
    }

    if (calc.result.employerCalc.deductionPensionSelfPayment > 0) {
      totalsGroup.rows.push(
        new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
          {
            irRow:
            {
              message: "SALAXY.REPORTS.salary.heading.employerPaidPension",
              total: -1 * calc.result.employerCalc.deductionPensionSelfPayment,
              count: calc.result.totals.totalSocialSecurityBase !== 0
                ? calc.result.employerCalc.deductionPensionSelfPayment /
                calc.result.totals.totalSocialSecurityBase
                : 0,
              price: calc.result.totals.totalSocialSecurityBase,
              unit: Unit.Percent,
            },
          })));
    }
    if (calc.result.employerCalc.deductionUnemploymentSelfPayment > 0) {
      totalsGroup.rows.push(
        new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
          {
            irRow:
            {
              message: "SALAXY.REPORTS.salary.heading.employerPaidUnemploymentInsurance",
              total: -1 * calc.result.employerCalc.deductionUnemploymentSelfPayment,
              count: calc.result.totals.totalSocialSecurityBase !== 0
                ? calc.result.employerCalc.deductionUnemploymentSelfPayment /
                calc.result.totals.totalSocialSecurityBase
                : 0,
              price: calc.result.totals.totalSocialSecurityBase,
              unit: Unit.Percent,
            },
          })));
    }

    if (calc.result.employerCalc.deductionUnionPayment > 0) {
      totalsGroup.rows.push(
        new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
          {
            irRow:
            {
              message: "SALAXY.REPORTS.salary.heading.employerPaidUnion",
              total: -1 * calc.result.employerCalc.deductionUnionPayment,
              count: calc.result.totals.totalTaxable !== 0
                ? calc.result.employerCalc.deductionUnionPayment /
                calc.result.totals.totalTaxable
                : 0,
              price: calc.result.totals.totalTaxable,
              unit: Unit.Percent,
            },
          })));
    }

    if ((calc.result.employerCalc as any).deductionTaxAndSocialSecuritySelfPayment > 0) {
      totalsGroup.rows.push(
        new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
          {
            irRow:
            {
              message: "SALAXY.REPORTS.salary.heading.employerPaidTaxAndSocialSecurity",
              total: -1 * (calc.result.employerCalc as any).deductionTaxAndSocialSecuritySelfPayment,
              count: 0,
              unit: Unit.Percent,
            },
          })));
    }

    if ((calc.result.employerCalc as any).deductionWorkerSelfPayment > 0) {
      totalsGroup.rows.push(
        new CalculationReportRow<CalculationTableRowData>(new CalculationTableRowData(
          {
            irRow:
            {
              message: "SALAXY.REPORTS.salary.heading.employerPaidWorker",
              total: -1 * (calc.result.employerCalc as any).deductionWorkerSelfPayment,
              count: 0,
              unit: Unit.Percent,
            },
          })));
    }

    report.rowGroups.push(mandatorySideCostsGroup);
    report.rowGroups.push(totalSalaryCostGroup);

    if (finalCostGroup.rows.length > 1) {
      report.rowGroups.push(finalCostGroup);
    }
    if (finalCostGroupAfterTax.rows.length > 1) {
      report.rowGroups.push(finalCostGroupAfterTax);
    }
    report.rowGroups.push(totalsGroup);
  }

  private static createPaymentReportRowGroups(report: CalculationReport, calc: Calculation) {

    const paymentChannel: PaymentChannel = ((calc.info as any).paymentChannel || PaymentChannel.Undefined);
    const hasPaymentChannel = !(paymentChannel === PaymentChannel.Undefined || paymentChannel === PaymentChannel.PalkkausCfaReference || paymentChannel === PaymentChannel.PalkkausCfaFinvoice || paymentChannel === PaymentChannel.PalkkausCfaPaytrail );

    // Groups

    // Payments
    const paymentsGroup = new CalculationReportRowGroup<CalculationReportRow<PaymentTableRowData>>(
      {
        grouping: CalcGroup.Undefined,
        groupingTerm: "SALAXY.REPORTS.salary.heading.paymentPart",
        params: { hideColumnHeaders: "" },
      });

    const finalCostGroupAfterTax = new CalculationReportRowGroup<CalculationReportRow<PaymentTableRowData>>(
      {
        grouping: CalcGroup.Undefined,
        groupingTerm: "SALAXY.REPORTS.salary.heading.totalCostAfterTax",
        params: { hideColumnHeaders: true },
      });

    // Totals
    const totalsGroup = new CalculationReportRowGroup<CalculationReportRow<PaymentTableRowData>>(
      {
        grouping: CalcGroup.Totals,
        params: { hideColumnHeaders: true },
        isHidden: hasPaymentChannel,
      });

    if (calc.result.totals.totalExpenses !== 0) {
      paymentsGroup.rows.push(
        new CalculationReportRow<PaymentTableRowData>(
          new PaymentTableRowData(
            {
              label: "SALAXY.REPORTS.ENUM.CalcGroup.expenses.label",
              total: calc.result.totals.totalExpenses,
              employerPercent: 0,

              receiver: "SALAXY.REPORTS.salary.heading.worker",
              workerPercent: null,
            })));
    }

    if (calc.result.workerCalc.prepaidExpenses !== 0) {
      paymentsGroup.rows.push(
        new CalculationReportRow<PaymentTableRowData>(
          new PaymentTableRowData(
            {
              label: "SALAXY.REPORTS.salary.heading.prepaidExpenses",
              total: -1 * calc.result.workerCalc.prepaidExpenses,
              employerPercent: 0,

              receiver: "SALAXY.REPORTS.salary.heading.worker",
              workerPercent: null,
            })));
    }

    if (calc.result.workerCalc.salaryAdvance !== 0) {
      paymentsGroup.rows.push(
        new CalculationReportRow<PaymentTableRowData>(
          new PaymentTableRowData(
            {
              label: "SALAXY.REPORTS.salary.heading.salaryAdvance",
              total: calc.result.workerCalc.salaryAdvance,
              baseAmount: 0,
              employerPercent: 0,

              receiver: "SALAXY.REPORTS.salary.heading.worker",
              workerPercent: null,
            })));
    }

    if (calc.result.workerCalc.otherDeductions > 0) {
      paymentsGroup.rows.push(
        new CalculationReportRow<PaymentTableRowData>(
          new PaymentTableRowData(
            {
              label: "SALAXY.REPORTS.salary.heading.otherDeductions",
              total: calc.result.employerCalc.deductionOtherDeductions,
              baseAmount: 0,
              employerPercent: 0,
              receiver: "SALAXY.REPORTS.salary.heading.worker",
            })));
    }

    if (calc.result.workerCalc.foreclosure !== 0) {
      paymentsGroup.rows.push(
        new CalculationReportRow<PaymentTableRowData>(
          new PaymentTableRowData(
            {
              label: "SALAXY.REPORTS.salary.heading.foreclosure",
              total: calc.result.workerCalc.foreclosure,
              employerPercent: 0
              ,
              receiver: "SALAXY.REPORTS.salary.heading.foreclosureExecutor",
              workerPercent: null,
            })));
    }

    paymentsGroup.rows.push(
      new CalculationReportRow<PaymentTableRowData>(
        new PaymentTableRowData(
          {
            label: "SALAXY.REPORTS.salary.heading.withholdingTax",
            total: calc.result.workerCalc.tax,
            baseAmount: calc.result.totals.totalTaxable,
            employerPercent: 0
            ,
            receiver: "SALAXY.REPORTS.salary.heading.taxman",
            workerPercent: calc.result.totals.totalTaxable !== 0 ? calc.result.workerCalc.tax / calc.result.totals.totalTaxable : 0,
          })));

    paymentsGroup.rows.push(
      new CalculationReportRow<PaymentTableRowData>(
        new PaymentTableRowData(
          {
            label: "SALAXY.REPORTS.salary.heading.socialInsurancePayment",
            total: calc.result.employerCalc.socialSecurity,
            baseAmount: calc.result.totals.totalSocialSecurityBase,
            employerPercent: calc.result.totals.totalSocialSecurityBase !== 0 ? calc.result.employerCalc.socialSecurity / calc.result.totals.totalSocialSecurityBase : 0
            ,
            receiver: "SALAXY.REPORTS.salary.heading.taxman",
            workerPercent: null,
          })));

    paymentsGroup.rows.push(
      new CalculationReportRow<PaymentTableRowData>(
        new PaymentTableRowData(
          {
            label: "SALAXY.REPORTS.salary.heading.pensionPayment",
            total: calc.result.totals.pension,
            baseAmount: calc.result.totals.totalSocialSecurityBase,
            employerPercent: calc.result.totals.totalSocialSecurityBase !== 0 ? calc.result.employerCalc.pension / calc.result.totals.totalSocialSecurityBase : 0
            ,
            receiver: this.getPensionCompanyName(calc),
            workerPercent: calc.result.totals.totalSocialSecurityBase !== 0 ? calc.result.workerCalc.pension / calc.result.totals.totalSocialSecurityBase : 0,
          })));

    paymentsGroup.rows.push(
      new CalculationReportRow<PaymentTableRowData>(
        new PaymentTableRowData(
          {
            label: "SALAXY.REPORTS.salary.heading.unemploymentInsurancePayment",
            total: calc.result.totals.unemployment,
            baseAmount: calc.result.totals.totalSocialSecurityBase,
            employerPercent: calc.result.totals.totalSocialSecurityBase !== 0 ? calc.result.employerCalc.unemployment / calc.result.totals.totalSocialSecurityBase : 0
            ,
            receiver: "SALAXY.REPORTS.salary.heading.tvr",
            workerPercent: calc.result.totals.totalSocialSecurityBase !== 0 ? calc.result.workerCalc.unemploymentInsurance / calc.result.totals.totalSocialSecurityBase : 0,
          })));

    if (calc.result.workerCalc.unionPayment > 0) {

      const isRaksa = this.isRaksa(calc);

      paymentsGroup.rows.push(
        new CalculationReportRow<PaymentTableRowData>(
          new PaymentTableRowData(
            {
              label: "SALAXY.REPORTS.salary.heading.unionPayment",
              total: calc.result.workerCalc.unionPayment,
              baseAmount: calc.result.totals.totalTaxable,
              employerPercent: 0
              ,
              receiver: isRaksa
                ? "SALAXY.REPORTS.salary.heading.raksa"
                : "SALAXY.REPORTS.salary.heading.union",
              workerPercent: calc.result.totals.totalTaxable !== 0
                ? calc.result.workerCalc.unionPayment / calc.result.totals.totalTaxable
                : 0,
              comment: calc.result.employerCalc.deductionUnionPayment > 0 ? "SALAXY.REPORTS.salary.heading.workersEmployerHandledUnionPayment" : "",
            })));
    }

    paymentsGroup.rows.push(new CalculationReportRow<PaymentTableRowData>(
      new PaymentTableRowData(
        {
          label: "SALAXY.REPORTS.salary.heading.palkkausFee",
          total: calc.result.employerCalc.palkkaus,
          baseAmount: calc.result.totals.totalGrossSalary,
          employerPercent: calc.result.totals.totalGrossSalary !== 0
            ? (calc.result.employerCalc.palkkaus / 1.24) /
            calc.result.totals.totalGrossSalary
            : 0,

          receiver: "SALAXY.REPORTS.salary.heading.palkkausfi",
          workerPercent: null,
          comment: `- ALV 24%, veron osuus ${Numeric.formatPrice(calc.result.employerCalc.palkkaus * (0.24 / 1.24))}`,
        })));

    paymentsGroup.rows.push(
      new CalculationReportRow<PaymentTableRowData>(
        new PaymentTableRowData(
          {
            label: "SALAXY.REPORTS.salary.heading.netSalary",
            total: calc.result.workerCalc.salaryPayment,
            employerPercent: 0,

            receiver: "SALAXY.REPORTS.salary.heading.worker",
            workerPercent: null,
          })));

    if (calc.salary.isHouseholdDeductible === true && calc.result.employerCalc.householdDeduction > 0) {

      finalCostGroupAfterTax.rows.push(new CalculationReportRow<PaymentTableRowData>(
        new PaymentTableRowData(
          {
            label: "SALAXY.REPORTS.salary.heading.totalCost",
            total: paymentsGroup.total,
            employerPercent: 0,
          })));

      finalCostGroupAfterTax.rows.push(
        new CalculationReportRow<PaymentTableRowData>(
          new PaymentTableRowData(
            {
              label: "SALAXY.REPORTS.salary.heading.householdDeduction",
              total: -1 * calc.result.employerCalc.householdDeduction,
              baseAmount: 0,
              employerPercent: 0,
            })));
    }

    totalsGroup.rows.push(
      new CalculationReportRow<PaymentTableRowData>(
        new PaymentTableRowData(
          {
            label: "SALAXY.REPORTS.salary.heading.totalCost",
            total: paymentsGroup.total,
            baseAmount: 0,
            employerPercent: 0,
          })));

    if (calc.result.employerCalc.deductionSalaryAdvance > 0) {
      totalsGroup.rows.push(
        new CalculationReportRow<PaymentTableRowData>(
          new PaymentTableRowData(
            {
              label: "SALAXY.REPORTS.salary.heading.employerPaidSalaryAdvance",
              total: -1 * calc.result.employerCalc.deductionSalaryAdvance,
              baseAmount: 0,
              employerPercent: 0,
            })));
    }

    if (calc.result.employerCalc.deductionOtherDeductions > 0) {
      totalsGroup.rows.push(
        new CalculationReportRow<PaymentTableRowData>(
          new PaymentTableRowData(
            {
              label: "SALAXY.REPORTS.salary.heading.employerPaidOtherDeduction",
              total: -1 * calc.result.employerCalc.deductionOtherDeductions,
              baseAmount: 0,
              employerPercent: 0,
            })));
    }

    if (calc.result.employerCalc.deductionForeclosure > 0) {
      totalsGroup.rows.push(
        new CalculationReportRow<PaymentTableRowData>(
          new PaymentTableRowData(
            {
              label: "SALAXY.REPORTS.salary.heading.employerPaidForeclosure",
              total: -1 * calc.result.employerCalc.deductionForeclosure,
              baseAmount: 0,
              employerPercent: 0,
            })));
    }

    if (calc.result.employerCalc.deductionPensionSelfPayment > 0) {
      totalsGroup.rows.push(
        new CalculationReportRow<PaymentTableRowData>(
          new PaymentTableRowData(
            {
              label: "SALAXY.REPORTS.salary.heading.employerPaidPension",
              total: -1 * calc.result.employerCalc.deductionPensionSelfPayment,
              baseAmount: 0,
              employerPercent: 0,
            })));
    }

    if (calc.result.employerCalc.deductionUnemploymentSelfPayment > 0) {
      totalsGroup.rows.push(
        new CalculationReportRow<PaymentTableRowData>(
          new PaymentTableRowData(
            {
              label: "SALAXY.REPORTS.salary.heading.employerPaidUnemploymentInsurance",
              total: -1 * calc.result.employerCalc.deductionUnemploymentSelfPayment,
              baseAmount: 0,
              employerPercent: 0,
            })));
    }

    if (calc.result.employerCalc.deductionUnionPayment > 0) {
      totalsGroup.rows.push(
        new CalculationReportRow<PaymentTableRowData>(
          new PaymentTableRowData(
            {
              label: "SALAXY.REPORTS.salary.heading.employerPaidUnion",
              total: -1 * calc.result.employerCalc.deductionUnionPayment,
              baseAmount: 0,
              employerPercent: 0,
            })));
    }

    if ((calc.result.employerCalc as any).deductionTaxAndSocialSecuritySelfPayment > 0) {
      totalsGroup.rows.push(
        new CalculationReportRow<PaymentTableRowData>(
          new PaymentTableRowData(
            {
              label: "SALAXY.REPORTS.salary.heading.employerPaidTaxAndSocialSecurity",
              total: -1 * (calc.result.employerCalc as any).deductionTaxAndSocialSecuritySelfPayment,
              baseAmount: 0,
              employerPercent: 0,
            })));
    }

    if ((calc.result.employerCalc as any).deductionWorkerSelfPayment > 0) {
      totalsGroup.rows.push(
        new CalculationReportRow<PaymentTableRowData>(
          new PaymentTableRowData(
            {
              label: "SALAXY.REPORTS.salary.heading.employerPaidWorker",
              total: -1 * (calc.result.employerCalc as any).deductionWorkerSelfPayment,
              baseAmount: 0,
              employerPercent: 0,
            })));
    }

    report.rowGroups.push(paymentsGroup);
    if (finalCostGroupAfterTax.rows.length > 1) {
      report.rowGroups.push(finalCostGroupAfterTax);
    }
    report.rowGroups.push(totalsGroup);

  }

  private static createHeader(report: CalculationReport, calc: Calculation) {
    // Employer
    report.header = {};
    report.header.employer = {};
    report.header.employer.avatar = calc.employer.avatar;

    // Address and official id
    report.header.employer.businessId = null; // N/A
    report.header.employer.contact = {}; // N/A

    // Worker
    report.header.worker = {};
    report.header.worker.avatar = calc.worker.avatar;
    report.header.worker.socialSecurityNumberValid = calc.worker.paymentData.socialSecurityNumberValid;
    report.header.worker.ibanNumber = calc.worker.paymentData.ibanNumber;
    report.header.worker.contact = {
      email: calc.worker.paymentData.email,
      telephone: calc.worker.paymentData.telephone,
    }; // N/A

    // Work/salary details
    report.header.numberOfDays = calc.framework.numberOfDays;
    report.header.paidAt = calc.workflow.paidAt;
    report.header.salaryDate = calc.workflow.salaryDate;
    report.header.requestedSalaryDate = calc.workflow.requestedSalaryDate;
    report.header.workDescription = calc.info.workDescription;
    report.header.salarySlipMessage = calc.info.salarySlipMessage;
    report.header.workStartDate = calc.info.workStartDate;
    report.header.workEndDate = calc.info.workEndDate;
    report.header.employmentPeriod = (calc.worktime || {}).employmentPeriod;
    report.header.taxPercent = calc.result.totals.totalTaxable !== 0
      ? calc.result.workerCalc.tax / calc.result.totals.totalTaxable
      : 0;
    report.header.workerHasNoTaxcard = calc.worker.tax.snapshot
      && calc.worker.tax.snapshot.kind === TaxcardKind.NoTaxCard;
    report.header.noWithholdingHousehold = calc.worker.tax.snapshot
      && calc.worker.tax.snapshot.kind === TaxcardKind.NoWithholdingHousehold;
    if (!report.header.workerHasNoTaxcard && calc.worker.tax.snapshot != null && calc.worker.tax.snapshot.kind !== TaxcardKind.Undefined) {
      const taxcard = calc.worker.tax.snapshot;
      report.header.taxcardTaxPercent = taxcard.taxPercent / 100.0;
      report.header.taxcardTaxPercent2 = taxcard.taxPercent2 / 100.0;
      report.header.taxcardIncomeLimit = taxcard.incomeLimit;
      report.header.taxcardCumulativeIncome = taxcard.cumulativeIncome;
    }
  }
  private static createValidationMessages(report: CalculationReport, calc: Calculation) {
    report.validationMessages = [];
    if (calc.result.workerCalc.fullSalaryAdvance > calc.result.workerCalc.salaryAdvance) {
      report.validationMessages.push(
        {
          text: "SALAXY.REPORTS.salary.text.salaryAdvanceError.html",
          parameters: {
            fullAmount:
              Numeric.formatNumber(calc.result.workerCalc.fullSalaryAdvance),
            differenceAmount:
              Numeric.formatNumber(calc.result.workerCalc.fullSalaryAdvance -
              calc.result.workerCalc.salaryAdvance),
          },
        });
    }
    if (calc.result.workerCalc.fullPension > calc.result.workerCalc.pension) {
      report.validationMessages.push({
        text: "SALAXY.REPORTS.salary.text.pensionError.html",
        parameters: {
          fullAmount:
            Numeric.formatNumber(calc.result.workerCalc.fullPension),
          differenceAmount:
            Numeric.formatNumber(calc.result.workerCalc.fullPension -
            calc.result.workerCalc.pension),
        },
      });
    }
    if (calc.result.workerCalc.fullUnemploymentInsurance > calc.result.workerCalc.unemploymentInsurance) {
      report.validationMessages.push({
        text: "SALAXY.REPORTS.salary.text.unemploymentInsuranceError.html",
        parameters: {
          fullAmount:
            Numeric.formatNumber(calc.result.workerCalc.fullUnemploymentInsurance),
          differenceAmount:
            Numeric.formatNumber(calc.result.workerCalc.fullUnemploymentInsurance -
            calc.result.workerCalc.unemploymentInsurance),
        },
      });
    }
    if (calc.result.workerCalc.fullUnionPayment > calc.result.workerCalc.unionPayment) {
      report.validationMessages.push({
        text: "SALAXY.REPORTS.salary.text.unionPaymentError.html",
        parameters: {
          fullAmount:
            Numeric.formatNumber(calc.result.workerCalc.fullUnionPayment),
          differenceAmount:
            Numeric.formatNumber(calc.result.workerCalc.fullUnionPayment -
            calc.result.workerCalc.unionPayment),
        },
      });
    }
    if (calc.result.workerCalc.fullOtherDeductions > calc.result.workerCalc.otherDeductions) {
      report.validationMessages.push({
        text: "SALAXY.REPORTS.salary.text.otherDeductionsError.html",
        parameters: {
          fullAmount:
          Numeric.formatNumber(calc.result.workerCalc.fullOtherDeductions),
          differenceAmount:
          Numeric.formatNumber(calc.result.workerCalc.fullOtherDeductions -
            calc.result.workerCalc.otherDeductions),
        },
      });
    }
    if (calc.result.workerCalc.fullPrepaidExpenses > calc.result.workerCalc.prepaidExpenses) {
      report.validationMessages.push({
        text: "SALAXY.REPORTS.salary.text.prepaidExpensesError.html",
        parameters: {
          fullAmount:
          Numeric.formatNumber(calc.result.workerCalc.fullPrepaidExpenses),
          differenceAmount:
          Numeric.formatNumber(calc.result.workerCalc.fullPrepaidExpenses -
            calc.result.workerCalc.prepaidExpenses),
        },
      });
    }
  }

  private static createEmployerReportNotes(report: CalculationReport, calc: Calculation) {
    if (calc.framework.type === FrameworkAgreement.Construction && !calc.framework.isTesIncludedInSalary) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.framework",
          text: "SALAXY.REPORTS.salary.text.followsRaksaFramework",
          parameters:
          {
          },
        });
    }

    if (this.isCompensation(calc)) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.noPensionPayment",
          text: "SALAXY.REPORTS.salary.text.compensationPension",
          parameters:
          {
          },
        });
    } else if ( calc.result.responsibilities.pensionCalculation === PensionCalculation.Entrepreneur ||
                calc.result.responsibilities.pensionCalculation === PensionCalculation.SmallEntrepreneur ) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.noPensionPayment",
          text: "SALAXY.REPORTS.salary.text.entrepreneurPension",
          parameters:
          {
          },
        });
    } else if (calc.result.responsibilities.pensionCalculation === PensionCalculation.Farmer ||
               calc.result.responsibilities.pensionCalculation === PensionCalculation.SmallFarmer) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.noPensionPayment",
          text: "SALAXY.REPORTS.salary.text.farmerPension",
          parameters:
          {
          },
        });
    } else if (calc.result.responsibilities.pensionCalculation === PensionCalculation.Athlete) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.noPensionPayment",
          text: "SALAXY.REPORTS.salary.text.athletePension",
          parameters:
          {
          },
        });
    } else if (calc.result.totals.pension === 0) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.nullPensionPayment",
          text: "SALAXY.REPORTS.salary.text.nullPensionPayment",
          parameters:
          {
            amount:
            Numeric.formatNumber(Years.getYearlyChangingNumbers(calc.result.responsibilities.calculationPaymentDate || Dates.getToday()).sideCosts.tyelIncomeLimit),
          },
        });
    } /* else if (this.isPensionSelfHandling(calc) === true) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.employerHandledPensionPayment",
          text: "SALAXY.REPORTS.salary.text.employerHandledPensionPayment.html",
          parameters:
          {
            company:
              this.getPensionCompanyName(calc),
            contractNo:
              this.getPensionContractNumber(calc),
          },
        });
    } else if (this.isPensionContractDone(calc) === true) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.palkkausHandledPensionPayment",
          text: "SALAXY.REPORTS.salary.text.palkkausHandledPensionPayment.html",
          parameters:
          {
            company:
              this.getPensionCompanyName(calc),
            contractNo:
              this.getPensionContractNumber(calc),
          },
        });
    } else {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.palkkausHandledTempPensionPayment",
          text: "SALAXY.REPORTS.salary.text.palkkausHandledTempPensionPayment.html",
          parameters:
          {
            company:
              this.getPensionCompanyName(calc),
          },
        });
    }*/

    if (calc.result.totals.unemployment > 0) {

      if (calc.result.employerCalc.deductionUnemploymentSelfPayment > 0) {
        let text = "SALAXY.REPORTS.salary.text.employerHandledUnemploymentInsurancePayment";
        if (calc.result.responsibilities.pensionCalculation === PensionCalculation.PartialOwner) {
          text = "SALAXY.REPORTS.salary.text.employerHandledPartialOwnerUnemploymentInsurancePayment";

        }

        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.employerHandledUnemploymentInsurancePayment",
            text,
            parameters:
            {
              employerAmount:
              Numeric.formatNumber(calc.result.employerCalc.unemployment),
              workerAmount:
              Numeric.formatNumber(calc.result.workerCalc.unemploymentInsurance),
            },
          });
      } else {
        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.palkkausHandledUnemploymentInsurancePayment",
            text: "SALAXY.REPORTS.salary.text.palkkausHandledUnemploymentInsurancePayment",
            parameters:
            {
              employerAmount:
              Numeric.formatNumber(calc.result.employerCalc.unemployment),
              workerAmount:
              Numeric.formatNumber(calc.result.workerCalc.unemploymentInsurance),
            },
          });
      }
    } else {

      if (this.isCompensation(calc)) {
        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.noUnemploymentInsurancePayment",
            text: "SALAXY.REPORTS.salary.text.compensationUnemploymentInsurance",
            parameters:
            {
            },
          });
      } else if (calc.result.responsibilities.pensionCalculation === PensionCalculation.Entrepreneur ||
                 calc.result.responsibilities.pensionCalculation === PensionCalculation.SmallEntrepreneur) {
        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.noUnemploymentInsurancePayment",
            text: "SALAXY.REPORTS.salary.text.entrepreneurUnemploymentInsurance",
            parameters:
            {
            },
          });
      } else if (calc.result.responsibilities.pensionCalculation === PensionCalculation.Farmer ||
                calc.result.responsibilities.pensionCalculation === PensionCalculation.SmallFarmer) {
        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.noUnemploymentInsurancePayment",
            text: "SALAXY.REPORTS.salary.text.farmerUnemploymentInsurance",
            parameters:
            {
            },
          });
      } else if (calc.result.responsibilities.pensionCalculation === PensionCalculation.Athlete) {
        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.noUnemploymentInsurancePayment",
            text: "SALAXY.REPORTS.salary.text.athleteUnemploymentInsurance",
            parameters:
            {
            },
          });
      } else {

        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.nullUnemploymentInsurancePayment",
            text: "SALAXY.REPORTS.salary.text.nullUnemploymentInsurancePayment",
            parameters:
            {
            },
          });
      }
    }

    if (calc.result.workerCalc.unionPayment > 0) {
      const isRaksa = this.isRaksa(calc);

      const unionPaymentPercent = calc.result.totals.totalTaxable !== 0
        ? calc.result.workerCalc.unionPayment / calc.result.totals.totalTaxable
        : 0;

      if (calc.result.employerCalc.deductionUnionPayment > 0) {
        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.employerHandledUnionPayment",
            text: "SALAXY.REPORTS.salary.text.employerHandledUnionPayment",
            parameters:
            {
              paymentRound:
              Numeric.formatNumber(calc.result.workerCalc.unionPayment),
              percent:
              Numeric.formatPercent(unionPaymentPercent * 100.0),
            },
          });
      } else {
        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.palkkausHandledUnionPayment",
            text: "SALAXY.REPORTS.salary.text.palkkausHandledUnionPayment",
            parameters:
            {
              paymentRound:
              Numeric.formatNumber(calc.result.workerCalc.unionPayment),
              percent:
              Numeric.formatPercent(unionPaymentPercent * 100.0),
            },
          });
      }
    }

    if (calc.salary.isHouseholdDeductible) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.householdDeduction",
          text: "SALAXY.REPORTS.salary.text.householdDeductionVar",
          parameters:
          {
            ownRisk: Dates.asMoment(calc.result.responsibilities.calculationPaymentDate).year() === 2020 ? "100" : "100",
            ownRiskDouble: Dates.asMoment(calc.result.responsibilities.calculationPaymentDate).year() === 2020 ? "200" : "200",
            max: Dates.asMoment(calc.result.responsibilities.calculationPaymentDate).year() === 2020 ? "2 250" : "2 400",
            maxDouble: Dates.asMoment(calc.result.responsibilities.calculationPaymentDate).year() === 2020 ? "4 500" : "4 800",
          },
        });
    }

    if (this.isCompensation(calc) ||
      calc.result.responsibilities.pensionCalculation === PensionCalculation.Entrepreneur ||
      calc.result.responsibilities.pensionCalculation === PensionCalculation.Farmer ||
      calc.result.responsibilities.pensionCalculation === PensionCalculation.Athlete ||
      calc.result.responsibilities.pensionCalculation === PensionCalculation.SmallEntrepreneur ||
      calc.result.responsibilities.pensionCalculation === PensionCalculation.SmallFarmer) {

      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.noAccidentInsurancePayment",
          text: "SALAXY.REPORTS.salary.text.noAccidentInsurance",
          parameters:
          {
          },
        });
      } /* else {
      if (this.getInsuranceCompany(calc) != null &&
        this.getInsuranceCompany(calc) !== InsuranceCompany.None &&
        this.getInsuranceCompany(calc) !== InsuranceCompany.Pending) {

        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.accidentInsurancePayment",
            text: "SALAXY.REPORTS.salary.text.accidentInsurancePayment.html",
            parameters:
            {
              company:
                this.getInsuranceCompanyName(calc),
              contractNo:
                this.getInsuranceContractNumber(calc),
            },
          });
      } else {
        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.accidentInsurancePayment",
            text: "SALAXY.REPORTS.salary.text.missingAccidentInsurance",
            parameters:
            {
            },
          });
      }
    } */

    if (this.isCompensation(calc)) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.noSocialInsurancePayment",
          text: "SALAXY.REPORTS.salary.text.compensationSocialInsurance",
          parameters:
          {
          },
        });
    } else if (calc.result.totals.totalSocialSecurityBase > 0) {

      if ((calc.result.employerCalc as any).deductionTaxAndSocialSecuritySelfPayment > 0) {
        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.socialInsurancePayment",
            text: "SALAXY.REPORTS.salary.text.socialSecuritySelfPayment",
            parameters:
            {
            },
          });
      } else {

        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.socialInsurancePayment",
            text: "SALAXY.REPORTS.salary.text.socialInsurancePayment",
            parameters:
              {},
          });
      }
    } else {

      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.nullSocialInsurancePayment",
          text: "SALAXY.REPORTS.salary.text.nullSocialInsurancePayment",
          parameters:
          {
          },
        });

    }

    report.notes.push(
      {
        heading: "SALAXY.REPORTS.salary.heading.incomesRegister",
        text: "SALAXY.REPORTS.salary.text.incomesRegisterNote",
        parameters:
        {
        },
      });

    if ((calc.result.employerCalc as any).deductionWorkerSelfPayment > 0) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.workerSelfPayment",
          text: "SALAXY.REPORTS.salary.text.workerSelfPayment",
          parameters:
          {
          },
        });
    }

    if ((calc.result.employerCalc as any).deductionTaxAndSocialSecuritySelfPayment > 0) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.taxSelfPayment",
          text: "SALAXY.REPORTS.salary.text.taxSelfPayment",
          parameters:
          {
          },
        });
    }
  }

  private static createSalarySlipNotes(report: CalculationReport, calc: Calculation) {
    if (calc.framework.type === FrameworkAgreement.Construction && !calc.framework.isTesIncludedInSalary) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.framework",
          text: "SALAXY.REPORTS.salary.text.followsRaksaFramework",
          parameters:
          {
          },
        });
    }

    if (this.isCompensation(calc)) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.noPensionPayment",
          text: "SALAXY.REPORTS.salary.text.compensationPension",
          parameters:
          {
          },
        });
    } else if (calc.result.responsibilities.pensionCalculation === PensionCalculation.Entrepreneur ||
               calc.result.responsibilities.pensionCalculation === PensionCalculation.SmallEntrepreneur) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.noPensionPayment",
          text: "SALAXY.REPORTS.salary.text.entrepreneurPension",
          parameters:
          {
          },
        });
    } else if (calc.result.responsibilities.pensionCalculation === PensionCalculation.Farmer ||
               calc.result.responsibilities.pensionCalculation === PensionCalculation.SmallFarmer) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.noPensionPayment",
          text: "SALAXY.REPORTS.salary.text.farmerPension",
          parameters:
          {
          },
        });
    } else if (calc.result.responsibilities.pensionCalculation === PensionCalculation.Athlete) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.noPensionPayment",
          text: "SALAXY.REPORTS.salary.text.athletePension",
          parameters:
          {
          },
        });
    } else if (calc.result.totals.pension === 0) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.nullPensionPayment",
          text: "SALAXY.REPORTS.salary.text.nullPensionPayment",
          parameters:
          {
            amount:
              Numeric.formatNumber(Years.getYearlyChangingNumbers(calc.result.responsibilities.calculationPaymentDate || Dates.getToday()).sideCosts.tyelIncomeLimit),
          },
        });
    } /* else if (this.isPensionSelfHandling(calc) === true) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.workersEmployerHandledPensionPayment",
          text: "SALAXY.REPORTS.salary.text.workersEmployerHandledPensionPayment.html",
          parameters:
          {
            company:
              this.getPensionCompanyName(calc),
            contractNo:
              this.getPensionContractNumber(calc),
          },
        });
    } else if (this.isPensionContractDone(calc) === true) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.workersPalkkausHandledPensionPayment",
          text: "SALAXY.REPORTS.salary.text.workersPalkkausHandledPensionPayment.html",
          parameters:
          {
            company:
              this.getPensionCompanyName(calc),
            contractNo:
              this.getPensionContractNumber(calc),
          },
        });
    } else {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.palkkausHandledTempPensionPayment",
          text: "SALAXY.REPORTS.salary.text.palkkausHandledTempPensionPayment.html",
          parameters:
          {
            company:
              this.getPensionCompanyName(calc),
          },
        });
    } */

    if (calc.result.totals.unemployment > 0) {

      if (calc.result.employerCalc.deductionUnemploymentSelfPayment > 0) {
        let text = "SALAXY.REPORTS.salary.text.workersEmployerHandledUnemploymentInsurancePayment";
        if (calc.result.responsibilities.pensionCalculation === PensionCalculation.PartialOwner) {
          text = "SALAXY.REPORTS.salary.text.workersEmployerHandledPartialOwnerUnemploymentInsurancePayment";

        }

        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.workersEmployerHandledUnemploymentInsurancePayment",
            text,
            parameters:
            {
            },
          });
      } else {
        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.workersPalkkausHandledUnemploymentInsurancePayment",
            text: "SALAXY.REPORTS.salary.text.workersPalkkausHandledUnemploymentInsurancePayment",
            parameters:
            {
            },
          });
      }
    } else {

      if (this.isCompensation(calc)) {
        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.noUnemploymentInsurancePayment",
            text: "SALAXY.REPORTS.salary.text.compensationUnemploymentInsurance",
            parameters:
            {
            },
          });
      } else if (calc.result.responsibilities.pensionCalculation === PensionCalculation.Entrepreneur ||
                 calc.result.responsibilities.pensionCalculation === PensionCalculation.SmallEntrepreneur) {
        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.noUnemploymentInsurancePayment",
            text: "SALAXY.REPORTS.salary.text.entrepreneurUnemploymentInsurance",
            parameters:
            {
            },
          });
      } else if (calc.result.responsibilities.pensionCalculation === PensionCalculation.Farmer ||
                 calc.result.responsibilities.pensionCalculation === PensionCalculation.SmallFarmer) {
        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.noUnemploymentInsurancePayment",
            text: "SALAXY.REPORTS.salary.text.farmerUnemploymentInsurance",
            parameters:
            {
            },
          });
      } else if (calc.result.responsibilities.pensionCalculation === PensionCalculation.Athlete) {
        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.noUnemploymentInsurancePayment",
            text: "SALAXY.REPORTS.salary.text.athleteUnemploymentInsurance",
            parameters:
            {
            },
          });
      } else {

        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.nullUnemploymentInsurancePayment",
            text: "SALAXY.REPORTS.salary.text.nullUnemploymentInsurancePayment",
            parameters:
            {
            },
          });
      }
    }

    if (calc.result.workerCalc.unionPayment > 0) {
      const unionPaymentPercent = calc.result.totals.totalTaxable !== 0
        ? calc.result.workerCalc.unionPayment / calc.result.totals.totalTaxable
        : 0;

      if (calc.result.employerCalc.deductionUnionPayment > 0) {
        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.employerHandledUnionPayment",
            text: "SALAXY.REPORTS.salary.text.employerHandledUnionPayment",
            parameters:
            {
              paymentRound:
              Numeric.formatNumber(calc.result.workerCalc.unionPayment),
              percent:
              Numeric.formatPercent(unionPaymentPercent * 100.0),
            },
          });
      } else {
        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.palkkausHandledUnionPayment",
            text: "SALAXY.REPORTS.salary.text.palkkausHandledUnionPayment",
            parameters:
            {
              paymentRound:
              Numeric.formatNumber(calc.result.workerCalc.unionPayment),
              percent:
              Numeric.formatPercent(unionPaymentPercent * 100.0),
            },
          });
      }
    }
    if (calc.salary.isHouseholdDeductible) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.householdDeduction",
          text: "SALAXY.REPORTS.salary.text.householdDeductionVar",
          parameters:
          {
            ownRisk: Dates.asMoment(calc.result.responsibilities.calculationPaymentDate).year() === 2020 ? "100" : "100",
            ownRiskDouble: Dates.asMoment(calc.result.responsibilities.calculationPaymentDate).year() === 2020 ? "200" : "200",
            max: Dates.asMoment(calc.result.responsibilities.calculationPaymentDate).year() === 2020 ? "2 250" : "2 400",
            maxDouble: Dates.asMoment(calc.result.responsibilities.calculationPaymentDate).year() === 2020 ? "4 500" : "4 800",
          },
        });
    }

    if (this.isCompensation(calc) ||
    calc.result.responsibilities.pensionCalculation === PensionCalculation.Entrepreneur ||
    calc.result.responsibilities.pensionCalculation === PensionCalculation.Farmer ||
    calc.result.responsibilities.pensionCalculation === PensionCalculation.Athlete ||
    calc.result.responsibilities.pensionCalculation === PensionCalculation.SmallEntrepreneur ||
    calc.result.responsibilities.pensionCalculation === PensionCalculation.SmallFarmer) {

      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.noAccidentInsurancePayment",
          text: "SALAXY.REPORTS.salary.text.noAccidentInsurance",
          parameters:
          {
          },
        });
      } /* else {
      if (this.getInsuranceCompany(calc) != null &&
        this.getInsuranceCompany(calc) !== InsuranceCompany.None &&
        this.getInsuranceCompany(calc) !== InsuranceCompany.Pending) {

        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.workersAccidentInsurancePayment",
            text: "SALAXY.REPORTS.salary.text.workersAccidentInsurancePayment.html",
            parameters:
            {
              company:
                this.getInsuranceCompanyName(calc),
              contractNo:
                this.getInsuranceContractNumber(calc),
            },
          });
      } else {
        report.notes.push(
          {
            heading: "SALAXY.REPORTS.salary.heading.workersAccidentInsurancePayment",
            text: "SALAXY.REPORTS.salary.text.workersMissingAccidentInsurance",
            parameters:
            {
            },
          });
      }
    } */

    if (calc.result.totals.totalSocialSecurityBase > 0) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.workersSocialInsurancePayment",
          text: "SALAXY.REPORTS.salary.text.workersSocialInsurancePayment",
          parameters:
          {
            year:
              Dates.getYear(calc.result.responsibilities.calculationPaymentDate || Dates.getToday()),
            percent:
            Numeric.formatNumber(Years.getYearlyChangingNumbers(calc.result.responsibilities.calculationPaymentDate || Dates.getToday()).sideCosts.illnessInsurancePercent * 100.0),
            totalBaseRound:
            Numeric.formatNumber(calc.result.totals.totalSocialSecurityBase),
            amountRound:
            Numeric.formatNumber(calc.result.totals.totalSocialSecurityBase * Years.getYearlyChangingNumbers(calc.result.responsibilities.calculationPaymentDate || Dates.getToday()).sideCosts.illnessInsurancePercent),
          },
        });
    } else {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.workersNullSocialInsurancePayment",
          text: "SALAXY.REPORTS.salary.text.workersNullSocialInsurancePayment",
          parameters:
          {
          },
        });
    }

    report.notes.push(
      {
        heading: "SALAXY.REPORTS.salary.heading.incomesRegister",
        text: "SALAXY.REPORTS.salary.text.incomesRegisterNote",
        parameters:
        {
        },
      });

    if ((calc.result.employerCalc as any).deductionWorkerSelfPayment > 0) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.workerSelfPayment",
          text: "SALAXY.REPORTS.salary.text.workerSelfPayment",
          parameters:
          {
          },
        });
    }

    if ((calc.result.employerCalc as any).deductionTaxAndSocialSecuritySelfPayment > 0) {
      report.notes.push(
        {
          heading: "SALAXY.REPORTS.salary.heading.taxSelfPayment",
          text: "SALAXY.REPORTS.salary.text.taxSelfPayment",
          parameters:
          {
          },
        });
    }

  }

  private static getPensionCompanyName(calc: Calculation): string {
    if (calc.result.responsibilities) {
      const pensionCompany = calc.result.responsibilities.pensionCompany;
      if (pensionCompany) {
          return "SALAXY.ENUM.PensionCompany." + pensionCompany + ".label";
      }
      return "";
    }
  }
  private static isCompensation(calc: Calculation): boolean {
    // check all rows
    return calc.result.irRows.some((r) => r.irData.code === 336 ); // NonWageCompensationForWork
  }

  private static getEnum<T>(value: any): T {
    if (!value) {
      return value;
    }
    let str = "" + value;
    str = str.charAt(0).toLowerCase() + str.slice(1);
    return str as any as T;
  }

  private static isRaksa(calc: Calculation): boolean {
    let isRaksa = false;
    if ((calc.framework as any).unionPayment === UnionPaymentType.RaksaNormal ||
      (calc.framework as any).unionPayment === UnionPaymentType.RaksaUnemploymentOnly) {
      isRaksa = true;
    } else {
      for (const row of calc.rows) {
        if (row.rowType === CalculationRowType.UnionPayment) {
          const type = this.getEnum<UnionPaymentKind>(row.data.kind);
          if (type === UnionPaymentKind.RaksaNormal || type === UnionPaymentKind.RaksaUnemploymentOnly) {
            isRaksa = true;
            break;
          }
        }
      }
    }

    return isRaksa;
  }
}
