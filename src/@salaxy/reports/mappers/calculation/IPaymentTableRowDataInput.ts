/** Row data for payment tables. */
export interface IPaymentTableRowDataInput {

  /** Name for the payment. */
  label?: string;

  /** Receiver of the payment. */
  receiver?: string;
  /** Base amount for the payment calculation. */
  baseAmount?: number;

  /** Worker part of the payment. */
  workerPercent?: number;

  /** Employer part of the payment. */
  employerPercent?: number;

  /** Total amount. */
  total?: number;

  /** Comment for the row. */
  comment?: string;
}
