import {
  CalcGroup,
} from "@salaxy/core";

import { ICalculationReportRow } from "./ICalculationReportRow";
import {ICalculationReportRowGroup} from "./ICalculationReportRowGroup";
import { ICalculationReportRowGroupInput } from "./ICalculationReportRowGroupInput";

/** Row group for calculation. Groups logically calculation rows. */
export class CalculationReportRowGroup<T extends ICalculationReportRow> implements ICalculationReportRowGroup {

  /** Grouping for the row group. */
  public grouping: CalcGroup;

  /** Grouping term */
  public get groupingTerm(): string {
    return this._groupingTerm || ("SALAXY.REPORTS.ENUM.CalcGroup." +
      this.grouping + ".label");
  }

  /** Grouping term */
  public set groupingTerm(value: string) {
    this._groupingTerm = value;
  }

  /** Total term */
  public get totalTerm(): string {
    return this._totalTerm || "SALAXY.REPORTS.salary.heading.total";
  }

  /** Total term */
  public set totalTerm(value: string) {
    this._totalTerm = value;
  }

  /** Additional ui data for the renderer */
  public params: any = null;

  /** Returns true if the group has rows. */
  public get hasRows(): boolean {
    return this.rows.length > 0;
  }

  /** Returns true if this is Deductions group. */
  public get isDeductions(): boolean { return this.grouping === CalcGroup.Deductions; }

  /** Returns true if this is Totals group. */
  public get isTotals(): boolean { return this.grouping === CalcGroup.Totals; }

  /** Returns true if this is Benefits group. */
  public get isBenefits(): boolean { return this.grouping === CalcGroup.Benefits; }

  /** Returns true if this is Expenses group. */
  public get isExpenses(): boolean { return this.grouping === CalcGroup.Expenses; }

  /** Returns true if this is SalaryAdditions group. */
  public get isSalaryAdditions(): boolean { return this.grouping === CalcGroup.SalaryAdditions; }

  /** Returns true if this is BaseSalary group. */
  public get isBaseSalary(): boolean { return this.grouping === CalcGroup.BaseSalary; }

  /** Returns true if this is Disabled group. */
  public get isDisabled(): boolean { return this.grouping === CalcGroup.Disabled; }

  /** Returns true if this is OtherNoPayment group. */
  public get isOtherNoPayment(): boolean { return this.grouping === CalcGroup.OtherNoPayment; }

  /** Returns true if this is Undefined group. */
  public get isUndefined(): boolean { return this.grouping === CalcGroup.Undefined; }

  /** Rows that are compatible with Incomes Register (tulorekisteri) process. */
  public rows: T[] = new Array<T>();

  /** If true, the group is not visible in the report */
  public isHidden: boolean;

  /** Group total. */
  public get total(): number {
    return this._total || this.rows.filter((x) => x.level === 0).reduce((a, b) => a + b.subTotal, 0);
  }

  /** Group total. */
  public set total(value: number | null) {
    this._total = value;
  }

  private _groupingTerm: string = null;

  private _totalTerm: string = null;

  private _total?: number = null;

  /** Constructor */
  constructor(data: ICalculationReportRowGroupInput<T>) {
    if (!data) {
      return;
    }
    this.grouping = data.grouping;
    this.groupingTerm = data.groupingTerm;
    this.params = data.params;
    this.total = data.total;
    this.totalTerm = data.totalTerm;
    if (data.rows) {
      this.rows = data.rows;
    }
    this.isHidden = data.isHidden;
  }
}
