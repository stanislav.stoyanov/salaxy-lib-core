import { ICalculationReportRowData } from "./ICalculationReportRowData";
import { IPaymentTableRowDataInput } from "./IPaymentTableRowDataInput";

/** Row data for payment tables. */
export class PaymentTableRowData implements ICalculationReportRowData {

  /** Name for the payment. */
  public label: string;

  /** Receiver of the payment. */
  public receiver: string;
  /** Base amount for the payment calculation. */
  public baseAmount?: number;

  /** Worker part of the payment. */
  public workerPercent?: number;

  /** Employer part of the payment. */
  public employerPercent?: number;

  /** Total amount. */
  public total: number;

  /** Comment for the row. */
  public comment?: string;

  /** Constructor */
  constructor(data: IPaymentTableRowDataInput) {
    if (!data) {
      return;
    }
    this.baseAmount = data.baseAmount;
    this.comment = data.comment;
    this.employerPercent = data.employerPercent;
    this.label = data.label;
    this.receiver = data.receiver;
    this.total = data.total;
    this.workerPercent = data.workerPercent;
  }
}
