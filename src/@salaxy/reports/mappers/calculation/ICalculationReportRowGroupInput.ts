import {
  CalcGroup,
} from "@salaxy/core";

import {ICalculationReportRow} from "./ICalculationReportRow";

/** Row group for calculation. Groups logically calculation rows. */
export interface ICalculationReportRowGroupInput<T extends ICalculationReportRow> {

  /** Grouping for the row group. */
  grouping?: CalcGroup;

  /** Grouping term */
  groupingTerm?: string;

  /** Total term */
  totalTerm?: string;

  /** Additional ui data for the renderer */
  params?: any;

  /** Rows that are compatible with Incomes Register (tulorekisteri) process. */
  rows?: T[];

  /** Group total. */
  total?: number;

  /** If true, the group is not visible in the report */
  isHidden?: boolean | null;
}
