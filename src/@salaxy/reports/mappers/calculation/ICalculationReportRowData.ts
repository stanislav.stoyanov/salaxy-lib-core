/** Interface for report row data. */
export interface ICalculationReportRowData {
  /** Total amount. */
  total?: number | null;
}
