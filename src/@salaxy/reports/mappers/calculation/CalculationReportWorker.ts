import {Avatar, Contact } from "@salaxy/core";
/** Worker name and contact details for reports. */
export interface CalculationReportWorker {
  /** The Person ID of the worker if known. */
  socialSecurityNumberValid?: string | null;
  /** Bank account number. */
  ibanNumber?: string | null;
  /** Employer avatar. */
  avatar?: Avatar | null;
  /** Worker contact information. */
  contact?: Contact | null;
}
