Salary reporting library
------------------------

Reporting library for Salaxy. 

Usage:

- Run `npm install --save @salaxy/reports@latest`
- Reference or copy TypeScript files from `node_modules/@salaxy/reports`

| File                   | Description                                                               |
| ---------------------- | ------------------------------------------------------------------------- |
| index.js               | ES6 module compilation                                                    |
| salaxy-lib-reports.js  | Browser friendly (browserify) compilation with Handlebars runtime         | 


For more information and documentation about Salaxy platform, please go to https://developers.salaxy.com

Please note that the salaxy-lib-reports.js includes the following libraries:
- Handlebars.js (https://www.npmjs.com/package/handlebars)