General plain JavaScript / TypeScript libraries
-----------------------------------------------

Plain JavaScript / TypeScript stack for Salaxy. 
These libraries are independent from libraries such as Angular, Angular 2, ReactJS etc.
They may be used inside these libraries.

Usage:

- Run `npm install --save @salaxy/core@latest`
- Reference or copy TypeScript files from `node_modules/@salaxy/core`
- We will shortly add a pre-built JavaScript and typings files. If you need them now, please e-mail contact@salaxy.com 

For more information and documentation about Salaxy platform, please go to https://developers.salaxy.com
