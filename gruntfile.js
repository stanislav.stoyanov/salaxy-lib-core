// salaxy-lib-core
// Grunt dev and build configuration v2.0
// MJ 9.3.2017

module.exports = function(grunt) {
    require('jit-grunt')(grunt);
    grunt.initConfig({
        concat: {
            core: {
                options: {
                    banner: '"use strict";\n\n/***** SALAXY CORE library, See https://developers.salaxy.com for details *****/\n\nvar require;\n',
                    process: function(src, filepath) {
                        return '// Source: ' + filepath + '\n' +
                            src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
                    },
                },
                src: [
                    'node_modules/moment/moment.js',
                    'node_modules/babel-polyfill/dist/polyfill.js',
                    'dist/npm/@salaxy/core/salaxy-core-all.js',
                ],
                dest: 'dist/npm/@salaxy/core/salaxy-core-all.js',
                nonull: true
            },
            dictionary: {
                options: {
                    banner: "/* tslint:disable */\n",
                    footer: "\n/** Internationalization dictionary\n@hidden */\nexport const dictionary = _dictionary;\n"
                },
                files: {
                    'src/@salaxy/core/i18n/dictionary.ts': 'src/@salaxy/core/i18n/dictionary.ts'
                },
                nonull: true
            },
            occupations: {
                options: {
                    banner: "/* tslint:disable */\n/** @hidden */\nexport const occupationsData = \n"
                },
                files: {
                    'src/@salaxy/core/i18n/occupationcodes.ts': 'src/@salaxy/core/i18n/occupationlist.json'
                },
                nonull: true
            },
            jquery: {
              options: {
                  banner: "'use strict';\n\n/***** SALAXY FULL LIBRARY (salaxy-lib-jquery, salaxy-lib-core and external dependencies), See https://developers.salaxy.com for details *****/\n\n",
                  process: function(src, filepath) {
                      return '// Source: ' + filepath + '\n' + src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
                  }
              },
              src: [
                  'dist/npm/@salaxy/core/salaxy-core-all.js',
                  'dist/npm/@salaxy/jquery/salaxy-lib-jquery.js',

              ],
              dest: 'dist/npm/@salaxy/jquery/salaxy-lib-jquery-all.js',
              nonull: true
            },
            reportsDictionary: {
              options: {
                  banner: "/* tslint:disable */\n",
                  footer: "\n/** Internationalization dictionary\n@hidden */\nexport const dictionary = _dictionary;\n"
              },
              files: {
                  'src/@salaxy/reports/i18n/dictionary.ts': 'src/@salaxy/reports/i18n/dictionary.ts'
              },
              nonull: true
            },
            reports: {
              options: {
                  banner: '"use strict";\n\n/***** SALAXY REPORTS library, See https://developers.salaxy.com for details *****/\n',
                  process: function(src, filepath) {
                      return '// Source: ' + filepath + '\n' +
                          src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
                  },
              },
              src: [
                  'node_modules/handlebars/dist/handlebars.js',
                  'dist/npm/@salaxy/reports/salaxy-lib-reports.js',
              ],
              dest: 'dist/npm/@salaxy/reports/salaxy-lib-reports.js',
              nonull: true
          },
        },
        ts: {
            core: {
              tsconfig: {
                tsconfig: 'src/@salaxy/core/tsconfig_for_build.json',
                passThrough: true
              }
            },
            coreTypes: {
              tsconfig: {
                tsconfig: 'src/@salaxy/core/tsconfig_for_build_types.json',
                passThrough: true
              }
            },
            node: {
              tsconfig: {
                tsconfig: 'src/@salaxy/node/tsconfig_for_build.json',
                passThrough: true
              }
            },
            jquery: {
              tsconfig: {
                tsconfig: 'src/@salaxy/jquery/tsconfig_for_build.json',
                passThrough: true
              }
            },
            jqueryTypes: {
              tsconfig: {
                tsconfig: 'src/@salaxy/jquery/tsconfig_for_build_types.json',
                passThrough: true
              }
            },
            reports: {
              tsconfig: {
                tsconfig: 'src/@salaxy/reports/tsconfig_for_build.json',
                passThrough: true
              }
            },
            reportsTypes: {
              tsconfig: {
                tsconfig: 'src/@salaxy/reports/tsconfig_for_build_types.json',
                passThrough: true
              }
            },
        },
        browserify: {
            core: {
                src: ['dist/npm/@salaxy/core/index.js'],
                dest: 'dist/npm/@salaxy/core/salaxy-core-all.js',
                options: {
                    alias: { '@salaxy/core': "./dist/npm/@salaxy/core/index.js" },
                    browserifyOptions: {
                        bundleExternal: false,
                    },
                    transform: ['browserify-shim']
                }
            },
            jquery: {
              src: ['dist/npm/@salaxy/jquery/index.js'],
              dest: 'dist/npm/@salaxy/jquery/salaxy-lib-jquery.js',
              options: {
                  alias: { '@salaxy/jquery': "./dist/npm/@salaxy/jquery/index.js" },
                  external: ["@salaxy/core"],
                  browserifyOptions: {
                      bundleExternal: false,
                  },
                  transform: ['browserify-shim']
              }
           },
          reports: {
            src: ["dist/npm/@salaxy/reports/index.js"],
            dest: 'dist/npm/@salaxy/reports/salaxy-lib-reports.js',
            options: {
                alias: { '@salaxy/reports': "./dist/npm/@salaxy/reports/index.js" },
                external: ["@salaxy/core"],
                browserifyOptions: {
                    bundleExternal: false,
                },
                transform: ['browserify-shim']
            }
          }
        },
        uglify: {
            all: {
                files: {
                    'dist/npm/@salaxy/core/salaxy-core-all.min.js': ['dist/npm/@salaxy/core/salaxy-core-all.js'],
                    'dist/npm/@salaxy/jquery/salaxy-lib-jquery.min.js': ['dist/npm/@salaxy/jquery/salaxy-lib-jquery.js'],
                    'dist/npm/@salaxy/jquery/salaxy-lib-jquery-all.min.js': ['dist/npm/@salaxy/jquery/salaxy-lib-jquery-all.js'],
                    'dist/npm/@salaxy/reports/salaxy-lib-reports.min.js': ['dist/npm/@salaxy/reports/salaxy-lib-reports.js']
                }
            }
        },
        copy: {
            npm: {
                files: [
                    { expand: true, cwd: 'src/@salaxy', src: ['core/package.json', 'core/readme.md'], dest: 'dist/npm/@salaxy/' },
                    { expand: true, cwd: 'src/@salaxy', src: ['node/package.json', 'node/readme.md'], dest: 'dist/npm/@salaxy/' },
                    { expand: true, cwd: 'src/@salaxy', src: ['jquery/package.json', 'jquery/readme.md'], dest: 'dist/npm/@salaxy/' },
                    { expand: true, cwd: 'dist/npm/@salaxy/core', src: 'salaxy-core-all.*', dest: 'dist/npm/@salaxy/jquery' },
                    { expand: true, cwd: 'src/@salaxy', src: ['reports/package.json', 'reports/readme.md'], dest: 'dist/npm/@salaxy/' },
                    { expand: true, cwd: 'src/@salaxy', src: ['reports/hbs/**/*.hbs'], dest: 'dist/npm/@salaxy/' },
                    { expand: true, cwd: 'src/@salaxy', src: ['reports/i18n/*.json'], dest: 'dist/npm/@salaxy/reports', flatten:"true" }                    
                ]
            }
        },
        clean: {
            npm: {
                src: ['dist/npm/@salaxy/core','dist/npm/@salaxy/node','dist/npm/@salaxy/jquery','dist/npm/@salaxy/reports']
            },
            dictionaries: {
                src: ['src/@salaxy/core/i18n/temp/']
            }
        },
        tslint: {
            options: { configuration: "tslint.json" },
            files: {
                src: ['src/@salaxy/core/**/*.ts', 'src/@salaxy/node/**/*.ts', 'src/@salaxy/jquery/**/*.ts', 'src/@salaxy/reports/**/*.ts' ]
            }
        },
        http: {
            dictionary: {
                options: {
                    url: 'http://docs.google.com/feeds/download/spreadsheets/Export?key=1i76Rwx8uNsmx9WLCNBsQUYkGLeK8B2Eov_CSW5RMKhM&exportFormat=csv&gid=0'
                },
                dest: 'src/@salaxy/core/i18n/temp/dictionary.csv'
            },
            occupations: {
                options: {
                    url: 'http://docs.google.com/feeds/download/spreadsheets/Export?key=1EbFtsU76GJsaU3xkmMhtfC02_prU6-R60gjOcVDft44&exportFormat=csv&gid=0'
                },
                dest: 'src/@salaxy/core/i18n/temp/occupationlist.csv'
            },
            reportsDictionary: {
              options: {
                  url: 'http://docs.google.com/feeds/download/spreadsheets/Export?key=1JXwSj2vj5dzVUu8ZVtWixQYT10fTaAs-WhjQHa6Ay5w&exportFormat=csv&gid=0'
              },
              dest: 'src/@salaxy/reports/i18n/reports.csv'
          }
        },
        csvjson: {
          dictionary: {
                src: 'src/@salaxy/core/i18n/temp/dictionary.csv',
                dest: 'src/@salaxy/core/i18n/temp'
            },
            reportsDictionary: {
                src: 'src/@salaxy/reports/i18n/reports.csv',
                dest: 'src/@salaxy/reports/i18n'
          }
        },
        replace: {
            json: {
                options: {
                    patterns: [{
                        match: /"#(\d+)"\s*:/g,
                        replacement: '"$1":'
                    }]
                },
                files: [
                    { expand: true, flatten: true, src: ['src/@salaxy/core/i18n/temp/*.json'], dest: 'src/@salaxy/core/i18n/temp' }
                ]
            }
        },
        json: {
            dictionary: {
                options: {
                    namespace: '_dictionary',
                    includePath: false,
                    pretty: true
                },
                src: ['src/@salaxy/core/i18n/temp/*.json'],
                dest: 'src/@salaxy/core/i18n/dictionary.ts'
            },
            reportsDictionary: {
              options: {
                  namespace: '_dictionary',
                  includePath: false,
                  pretty: true
              },
              src: 'src/@salaxy/reports/i18n/*.json',
              dest: 'src/@salaxy/reports/i18n/dictionary.ts'
          }

        },
        handlebars: {         
          commonjs: {
              options: {
                  namespace: 'salaxy.reports.templates',
                  processName: function(filePath) {
                      var pieces = filePath.split('/');
                      return pieces[pieces.length - 1].split('.')[0];
                  },
                  node: true,
              },
              files: {
                  'src/@salaxy/reports/hbs/index.js': 'src/@salaxy/reports/hbs/**/*.hbs',
                  'dist/npm/@salaxy/reports/hbs/index.js': 'src/@salaxy/reports/hbs/**/*.hbs',
              }
          }       
        },
        less: {
          reports: {
              options: {
                  sourceMap: true,
                  sourceMapFileInline: false,
                  sourceMapRootpath: "/",
              },
              files: {
                  "dist/npm/@salaxy/reports/salaxy-lib-reports.css": "src/@salaxy/reports/less/salaxy-reports.less",
              }
          }
      },
        csvcolkeyjson: {
            occ2json: {
                src: 'src/@salaxy/core/i18n/temp/occupationlist.csv',
                dest: 'src/@salaxy/core/i18n/occupationlist.json'
            }
        },
        yearlyNumbers: {
          default: {
                url: "http://localhost:82/v02/api/calculator/yearlyNumbers", // TODO: to be changed to test-api.
                years: ["2018", "2019"],
                dest: "src/@salaxy/core/codegen/yearlyChangingNumbersYears.ts"
          }
        }
    });



    grunt.registerMultiTask('csvcolkeyjson', 'Creates json file from csv with keys as columns', function() {
        var config = this.data;
        var done = this.async();
        //read source
        grunt.log.writeln('Reading source file ' + config.src);
        var csv = require("csvtojson")();
        csv.fromFile(config.src).then(function(jsonObj) {
                grunt.log.writeln('Writing target file ' + config.dest);
                var jsonContent = JSON.stringify(jsonObj);
                grunt.file.write(config.dest, jsonContent);
                done();
            },
            function(error) {
                grunt.log.writeln('Error ' + error);
                done();
            });
    });

    /**
     * Custom task for getting yearly changing numbers and holidays from the api
     * and serializing them as a class for the client side library.
     */
    grunt.registerMultiTask('yearlyNumbers', 'Creates yearly changing numbers array into codegen', function() {
      var config = this.data;
      var done = this.async();
      var request =  require('request');
      var numbers = [];

      var finish = function() {
        console.log("Saving to codegen.");
        var tsContent = JSON.stringify(numbers, null, "  ");
        var header = "/* tslint:disable */\n\n";
        header += "import {YearlyChangingNumbers} from \"../model\";\n\n";
        header += "/**\n";
        header += " * Yearly changing numbers. Contains side cost percentages and holidays.\n";
        header += " *\n";
        header += " */\n\n";
        header += "export const yearlyChangingNumbersYears =\n";
        var footer = " as YearlyChangingNumbers[];\n"

        grunt.file.write(config.dest, header + tsContent + footer);
        console.log("Saved.");
        done();
      }
      
      var getYear = function(i, years) {
        console.log("Getting yearly numbers for year: " + years[i]);
        request(config.url + "/" + years[i] +"-01-01", function (error, response, body) {
          if (error || !response || response.statusCode != 200) {
            if (error) {
              console.log("error:", error);
            }
            if (response) {
              console.log("statusCode:", response.statusCode);
            }
            console.error("No files saved.");
            done();
          } else {
            numbers.push(JSON.parse(body));
            i++;
            if (i == years.length) {
              finish();
            } else {
              getYear(i, years);
            }
          }
        });
      }     
      var start = function() {
        getYear(0, config.years)
      }

      start();
    });

    // ===========================================================================
    // BUILD JOBS 		  ========================================================
    // ===========================================================================
    grunt.loadNpmTasks('grunt-csv-json');
    grunt.loadNpmTasks('grunt-http');
    grunt.registerTask('updateoccupations', ['http:occupations', 'csvcolkeyjson:occ2json']);
    grunt.registerTask('generateoccupations', ['concat:occupations']);
    grunt.registerTask('updatetranslation', ['http:dictionary', 'http:reportsDictionary', 'csvjson:dictionary', 'csvjson:reportsDictionary', 'replace', 'json:dictionary', 'json:reportsDictionary', 'concat:dictionary', 'concat:reportsDictionary', 'clean:dictionaries']);
    var tasks = ['clean:npm', 'updatetranslation', 'handlebars', 'ts', 'browserify', 'concat:core', 'concat:jquery', 'concat:reports', 'uglify', 'less', 'copy', 'tslint', 'noDocsLintWarning'];
    grunt.registerTask('noDocsLintWarning', function() {
        grunt.log.writeln('WARNING: Grunt-tslint is unable to check documentation. Run "npm run lint" BEFORE PUSH (scroll up if errors).');
    });
    grunt.registerTask('default', tasks);
    grunt.registerTask('build', tasks);
};
