import * as chai from "chai";
import * as dirtyChai from "dirty-chai";

import {
  Calculation,
  Calculations,
  CalculatorLogic,
  Dates,
  Mappers,
  Objects,
  TableFormat,
  Tables,
} from "@salaxy/core";

describe("Unit tests", () => {
  describe("Logic, Import, Tables", () => {
    const expect = chai.expect;
    chai.use(dirtyChai);

    it("Should convert json to csv table and back ", () => {

      const locale = "fi";

      const sToday = Dates.getToday();

      const rows = [
        {
          field1: "hello",
          field2: 10,
          field3: sToday,
          field4: true,
        },
        {
          field1: "hello",
          field2: 9.11,
          field3: sToday,
          field4: false,
        },
        {
          field1: "hello",
          field2: 11.2,
          field3: sToday,
          field4: true,
        },
      ];
      // console.log(rows);

      const csv = Tables.export(rows, TableFormat.Csv, locale) as string;
      // console.log(csv);
      expect(csv.length, "Exported csv has some content").gt(0);
      const parsedRows = Tables.import(csv, TableFormat.Csv, locale);
      // console.log(parsedRows.data);
      expect(parsedRows.data.length, "Imported data contains all exported data rows").equal(rows.length);

      expect(Objects.equal(rows, parsedRows.data), "Export/import roundtrip does not change data").equal(true);

    });

    it("Should give errors if headers.length differs from data row length ", () => {

      const locale = "fi";

      const sToday = Dates.getToday();

      const csv = "header1;header2;header3\ndata1;data2;data3\ndata1;data2";

      const parsedRows = Tables.import(csv, TableFormat.Csv, locale);

      expect(parsedRows.errors, "Errors length").to.have.length(1);

    });

    it("Should show ignored lines ", () => {

      const locale = "fi";

      const sToday = Dates.getToday();

      const csv = "header1;header2;header3\ndata1;data2;data3\n\n\ndata1;data2;data3\n;;";

      const parsedRows = Tables.import(csv, TableFormat.Csv, locale);

      expect(parsedRows.ignoredLines, "Count of ignored lines").equal(3);

    });

    it("Should convert flat calculation to csv table and back ", () => {

      const calcs: Calculation[] = [];
      const calc1 = CalculatorLogic.getBlank();
      calc1.id = "one";
      calc1.rows.push(...[
        {
          message: "onefirst",
          count: 1,
        },
        {
          message: "onesecond",
          count: 2,
        },
        {
          message: "onethird",
          count: 3,
        },
      ]);
      calcs.push(calc1);
      const calc2 = CalculatorLogic.getBlank();
      calc2.id = "two";
      calc2.rows.push(...[
        {
          message: "twofirst",
          count: 1,
        },
        {
          message: "twosecond",
          count: 2,
        },
      ]);
      calcs.push(calc2);

      const calcMapper = new Calculations(null).getObjectMappers()[0];
      const rowData = calcMapper.fromObjects(calcs);
      console.log(rowData);

      const csv = Tables.export(rowData, TableFormat.Csv, "fi") as string;
      console.log(csv);

      const parsedRows = Tables.import(csv, TableFormat.Csv, "fi");
      console.log(parsedRows.data);

      const parsedCalcs =  calcMapper.fromRows(parsedRows.data);
      console.log(parsedCalcs);

      expect(parsedCalcs.length, "Imported object contains all exported objects").equal(calcs.length);
    });

  });
});
