import { Calculation, CalculationRowType, CalculatorLogic, LegalEntityType, SalaryKind, YearlyChangingNumbers, Numeric } from "@salaxy/core";

/** Helpers specific for unemployment calculations. */
export class UnemploymentTestHelpers {

  /**
   * Reference amounts for 100 euro salary
   * Unemployment insurance percentages need to be changed yearly
   */
  public refAmounts: {
    /** Employer part of the unemployment insurance */
    employer: number,
    /** Worker part of the unemployment insurance */
    worker: number,
    /** Total unemployment insurance */
    total: number,
    /** Household deduction percent */
    householdDeductionPercent: number,
  };

  constructor(yearlyNumbers: YearlyChangingNumbers) {
    this.refAmounts = {
      employer: Numeric.round(yearlyNumbers.sideCosts.unemploymentEmployerPercent * 100),
      worker: Numeric.round(yearlyNumbers.sideCosts.unemploymentWorkerPercent * 100),
      total: null,
      householdDeductionPercent: yearlyNumbers.sideCosts.householdDeduction,
    };
    this.refAmounts.total = this.refAmounts.employer + this.refAmounts.worker;
  }

  /**
   * Gets a default calculation: 100€ salary
   * @param isCompany If true, the Employer is marked as a company.
   */
  public getCalculation(isCompany: boolean): Calculation {
    const calc = CalculatorLogic.getBlank();
    calc.worker.accountId = "example-default";
    calc.salary = {
      price: 100,
      kind: SalaryKind.FixedSalary,
    };
    calc.rows.push({
       message: "Expenses should not affect Unemployment insurance payments.",
       price: 20,
       rowType: CalculationRowType.Expenses,
    });
    // calc.workflow.paidAt = "2020-08-01";
    if (isCompany) {
      calc.employer.avatar.entityType = LegalEntityType.Company;
    } else {
      calc.employer.avatar.entityType = LegalEntityType.Person;
    }
    return calc;
  }

}
