import * as chai from "chai";
import * as chaiAlmost from "chai-almost";
import * as dirtyChai from "dirty-chai";
import { config } from "../../../../config";

import {
  CalculationRowType, CalculationRowUnit, CalculatorLogic, SalaryKind,
} from "@salaxy/core";

import {
  TestHelper,
} from "@salaxy/node";

import { IncomesRegisterTestHelper } from "../IncomesRegisterTestHelper";

describe("API tests", () => {
  describe("Incomes Register (IR): Backward compatibility, Salary additions", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    chai.use(chaiAlmost(0.02));
    const sxyHelper = new TestHelper(config, this);
    const irHelper = new IncomesRegisterTestHelper(sxyHelper, expect);

    it("CalculationRowType.Overtime", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 0.5,
        price: 200,
        rowType: CalculationRowType.Overtime,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.Overtime, 100, true, false, null, 235);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Percent);
      expect(results.resultRow.message).to.equal("Ylityöt");
      expect(results.reportRow.accountNumber).to.equal("5100");
    });

    it("CalculationRowType.TesWorktimeShortening", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 0.077,
        price: 1000,
        rowType: CalculationRowType.TesWorktimeShortening,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.TesWorktimeShortening, 77, true, false, null, 225);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Percent);
      expect(results.resultRow.message).to.equal("Erillinen palkanosa");
      expect(results.reportRow.accountNumber).to.equal("5110");
    });

    it("CalculationRowType.EveningAddition", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 3,
        price: 1.20,
        rowType: CalculationRowType.EveningAddition,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.EveningAddition, 3.60, true, false, null, 206);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Iltavuorolisä");
      expect(results.reportRow.accountNumber).to.equal("5110");
    });

    it("CalculationRowType.NightimeAddition", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 10,
        price: 2.40,
        rowType: CalculationRowType.NightimeAddition,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.NightimeAddition, 24, true, false, null, 236);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Yövuorolisä");
      expect(results.reportRow.accountNumber).to.equal("5110");
    });

    it("CalculationRowType.SaturdayAddition", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 0.5,
        price: 300,
        rowType: CalculationRowType.SaturdayAddition,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.SaturdayAddition, 150, true, false, null, 211);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Percent);
      expect(results.resultRow.message).to.equal("Lauantailisä");
      expect(results.reportRow.accountNumber).to.equal("5110");
    });

    it("CalculationRowType.SundayWork", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1, // 100%
        price: 232,
        rowType: CalculationRowType.SundayWork,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.SundayWork, 232, true, false, null, 221);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Sunnuntai- / pyhätyö");
      expect(results.reportRow.accountNumber).to.equal("5110");
    });

    it("CalculationRowType.OtherAdditions", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 600,
        rowType: CalculationRowType.OtherAdditions,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.OtherAdditions, 600, true, false, null, 216);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Muut lisät");
      expect(results.reportRow.accountNumber).to.equal("5110");
    });

    it("CalculationRowType.HolidayCompensation", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 100,
        rowType: CalculationRowType.HolidayCompensation,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.HolidayCompensation, 100, true, false, null, 234);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Lomakorvaus");
      expect(results.reportRow.accountNumber).to.equal("5310");
    });

    it("CalculationRowType.HolidayBonus", async () => {
      const inputCalc = CalculatorLogic.getBlank();
      inputCalc.rows.push({
        count: 1,
        price: 100,
        rowType: CalculationRowType.HolidayBonus,
      });
      const results = await irHelper.runCommonTestsForSalaries(inputCalc, CalculationRowType.HolidayBonus, 100, true, false, null, 213);
      expect(results.resultRow.unit).to.equal(CalculationRowUnit.Count);
      expect(results.resultRow.message).to.equal("Lomaraha");
      expect(results.reportRow.accountNumber).to.equal("5320");
    });

  });
});
