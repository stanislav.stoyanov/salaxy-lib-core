import * as chai from "chai";
import * as dirtyChai from "dirty-chai";
import { config } from "../config";

import {
  TestHelper,
} from "@salaxy/node";

describe("API tests", () => {
  describe("_Template", function() {
    const expect = chai.expect;
    chai.use(dirtyChai);

    const sxyHelper = new TestHelper(config, this);

    it("is there for copy-pasting.", async () => {
      const demoData = "OK";
      const target = await Promise.resolve(demoData); // Make the API call(s)

      expect(target).equals(demoData);
    });
  });
});
