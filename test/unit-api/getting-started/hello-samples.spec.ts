import * as chai from "chai";
import * as dirtyChai from "dirty-chai";
import { config } from "../../config";

import {
  TestHelper,
} from "@salaxy/node";

describe("API tests", () => {
  describe("Getting Started: Basic tests demonstrate the use of tester methods.", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);
    const sxyHelper = new TestHelper(config, this);

    it("Hello!", () => {
      return sxyHelper.api.test.hello("Hello World!").then((message) => {
        expect(message).to.not.be.null();
        expect(message).equal("Hello World!");
      });
    });

    it("Delay on server", () => {
      return sxyHelper.api.test.delay(0).then((data) => {
        expect(data).equal(0);
      });
    });

    it("Values", () => {
      return sxyHelper.api.test.values(null).then((data) => {
        expect(data.stringValue).equal("test");
      });
    });
  });
});
