import {
  WorkerAccount,
  WorkerLogic,
  Workers,
} from "@salaxy/core";
import * as chai from "chai";
import * as dirtyChai from "dirty-chai";
import { config } from "../../config";

import {
  TestHelper,
} from "@salaxy/node";

xdescribe("API tests", () => {
  xdescribe("Workers: Create and delete", function() {

    const expect = chai.expect;
    chai.use(dirtyChai);

    const sxyHelper = new TestHelper(config, this);

    before("authenticate, will be removed eventually", async () => {
      await sxyHelper.authenticate();
    });

    // npm run test-path "test/unit-api/workers/assure-api.spec.ts"

    it("Creates and deletes worker account", async () => {

      const workers = new Workers(sxyHelper.ajax);
      const workerAccount = await workers.save({
        avatar: {
          firstName: "TempTemp",
          lastName: "Worker",
        },
        contact: {
          email: "temptemp@palkkaus.fi",
          telephone: "555666777",
        },
        ibanNumber: "FI9517453000181869",
        officialPersonId: "080564-921D",
      });

      expect(workerAccount).to.not.be.null();
      expect(workerAccount.id).to.not.be.null();

      const result = await workers.delete(workerAccount.id);
      expect(result).to.not.be.null();
    });
  });
});
