import * as chai from "chai";
import * as dirtyChai from "dirty-chai";
import { config } from "../../config";

import {
  TestHelper,
} from "@salaxy/node";

describe("System tests", () => {
  describe("CRUD: ESalaryPayments", function () {
    const expect = chai.expect;
    chai.use(dirtyChai);

    const sxyHelper = new TestHelper(config, this);

    const crudApiToTest = sxyHelper.api.eSalaryPayments;
    let itemInStorageId = null;
    const uniqueCancelUrl = "https://www.palkkaus.fi/fail";

    before("Authenticate", sxyHelper.authenticate);

    after("Remove item if created.", async () => {
      if (itemInStorageId) {
        await crudApiToTest.delete(itemInStorageId);
        itemInStorageId = null;
      }
    });

    it("should create an new item (CRUD)", async () => {
      expect(itemInStorageId).to.be.null("Before create new starts itemInStorageId should be null. This should be the first test in the test set.");

      const newItem = crudApiToTest.getBlank();
      newItem.request.cancelUrl = uniqueCancelUrl;
      const result = await crudApiToTest.save(newItem);

      expect(result).to.not.be.null();
      expect(result.id).to.not.be.empty();
      expect(result.request.cancelUrl).to.equal(uniqueCancelUrl);

      itemInStorageId = result.id;
    });

    it("should find the created item in the list (CRUD)", async () => {
      const existingId = await getItemInStorageId();
      const list = await crudApiToTest.getAll();

      expect(list.length).to.be.greaterThan(0);
      expect(list.map((x) => x.id)).to.contain(existingId);
    });

    it("should find the created item by id (CRUD)", async () => {
      const existingId = await getItemInStorageId();
      const itemInServer = await crudApiToTest.getSingle(existingId);

      expect(itemInServer).to.not.be.null();
      expect(itemInServer.id).to.equal(existingId);
      expect(itemInServer.request.cancelUrl).to.equal(uniqueCancelUrl);
    });

    it("should delete the created item (CRUD)", async () => {
      const existingId = await getItemInStorageId();
      const initialList = await crudApiToTest.getAll();
      expect(initialList.map((x) => x.id)).to.contain(existingId);

      const result = await crudApiToTest.delete(existingId);
      expect(result).to.equal("Object deleted");
      itemInStorageId = null;

      const newList = await crudApiToTest.getAll();
      expect(newList.length).to.equal(initialList.length - 1);
      expect(newList.map((x) => x.id)).to.not.contain(existingId);
    });

    const getItemInStorageId = async () => {
      if (itemInStorageId) {
        return itemInStorageId;
      }
      const newItem = crudApiToTest.getBlank();
      newItem.request.cancelUrl = uniqueCancelUrl;
      const result = await crudApiToTest.save(newItem);
      itemInStorageId = result.id;
      return result.id;
    };

  });
});
