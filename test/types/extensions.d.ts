declare module Chai {
  interface Assertion {
    almost: Equal;
  }
}
